﻿//
// Thinkbox.Windows.Forms
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Thinkbox.Windows.Forms
{
    public class GradientPanel : Panel
    {
        #region PROPERTIES
        private Color startColor = Color.White;
        public Color StartColor
        {
            get { return startColor; }
            set { startColor = value; if (DesignMode) Invalidate(); }
        }
        private Color endColor = Color.White;
        public Color EndColor
        {
            get { return endColor; }
            set { endColor = value; if (DesignMode) Invalidate(); }
        }
        private float angle = 0;
        public float Angle
        {
            get { return angle; }
            set { angle = value; if (DesignMode) Invalidate(); }
        }
        #endregion

        #region CONSTRUCTORS
        public GradientPanel() {}
        #endregion

        #region EVENTS
        protected override void OnPaint(PaintEventArgs e)
        {
            //ColorBlend cb = new ColorBlend()
            //{
            //    Positions = new[] 
            //    {
            //        0, 1 / 6f, 2 / 6f, 3 / 6f, 4 / 6f, 5 / 6f, 1
            //    },
            //    Colors = new[] 
            //    {
            //        Color.Red, Color.Orange, Color.Yellow,
            //        Color.Green, Color.Blue, Color.Indigo, Color.Violet
            //    }
            //};
            //LinearGradientBrush br = new LinearGradientBrush(this.ClientRectangle, StartColor, EndColor, 0, false);
            //br.InterpolationColors = cb;
            //br.RotateTransform(Angle);
            //e.Graphics.FillRectangle(br, this.ClientRectangle);
            //base.OnPaint(e);
            
            LinearGradientBrush br = new LinearGradientBrush(
                this.ClientRectangle, StartColor, EndColor, Angle);
            e.Graphics.FillRectangle(br, this.ClientRectangle);
            base.OnPaint(e);
        }
        #endregion
    }
}
