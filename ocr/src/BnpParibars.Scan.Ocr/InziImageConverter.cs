﻿//
// BnpParibars.Scan.Ocr.InziImageConverter
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System;
using System.Linq;
using System.Runtime.InteropServices;
using log4net;

namespace BnpParibars.Scan.Ocr
{
    public class InziImageConverter
    {
        #region VARIABLES
        private readonly static ILog logger = LogManager.GetLogger("ImageConverter");
        #endregion

        #region EXTERNAL LIBRARY METHODS
        /// <summary>
        /// Get image file format.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns>
        /// 1: BMP, 2: JPEG, 3: JPEG200, 4: TIFF, 5: JBIG2, 6: IZT, 7: GIF, 
        /// 8: PNG, 11: PDF
        /// 0: Invalid file name or Unknown image format
        /// -6: Library(*.dll) load failed
        /// -7: No appropriate method in Library(*.dll)
        /// </returns>
        [DllImport(@"lib\sconverter\InziImage2Image.dll", CharSet = CharSet.Ansi)]
        extern private static int getFileTypeFILE(string fileName);

        /// <summary>
        /// Get number of total pages.
        /// </summary>
        /// <param name="filename">The name of tiff image file.</param>
        /// <returns>
        /// 0보다 크면 이미지의 수
        /// -1: 파일 이름이 유효하지 않음
        /// -2: 이미지 개수를 받아오는 부분에서 에러가 발생했습니다
        /// -3: 파일 이름이 유효하지 않음, DLL(so,sl) 로드 실패
        /// -4: DLL 모듈에 적절한 함수가 없음
        /// -6: DLL 로드 실패
        /// -7: DLL 모듈에 적절한 함수가 없음
        /// </returns>
        [DllImport(@"lib\sconverter\InziImage2Image.dll", CharSet = CharSet.Ansi)]
        extern private static int getTIFFTotalPageFILE(string filename);

        /// <summary>
        /// Extract multi-tiff image to single-tiff image
        /// </summary>
        /// <param name="srcFilename">multi-tiff file name</param>
        /// <param name="pageNo">page number in multi-tff file. 1 base</param>
        /// <param name="dstFilename">extracted (single tiff)file name</param>
        /// <returns></returns>
        [DllImport(@"lib\sconverter\InziImage2Image.dll", CharSet = CharSet.Ansi)]
        extern private static int extractTIFFFILE(
            string srcFilename,
            int pageNo, 
            string dstFilename);

        /// <summary>
        /// Convert pdf to image file.
        /// </summary>
        /// <param name="filename">The name of the file to be converted.</param>
        /// <param name="dstFoldername">The name of the folder where the file to be converted is saved.</param>
        /// <param name="resolution">Resolution value to be applied to the header when converting to a tiff file.
        /// DPI value to be applied to the image
        /// </param>
        /// <param name="comprate">
        /// Compression value for images with compression enabled.
        /// JPEG, JPEG in TIFF:1 ~ 100
        /// JPEG2000, JPEG2000 in TIFF: 24 - 1bpp, 12 - 2bpp, 48 - 0.5bpp
        /// </param>
        /// <param name="filetype">
        /// Format of the image file to be converted.
        /// 1 - BMP, 2 - JPEG, 3 - JPEG200, 4 - TIFF
        /// </param>
        /// <param name="comptype">
        /// Compression method to be applied when the format of the image file 
        /// to be converted is tiff.
        /// 1 - Do not compress, 2 - RLE, 3 - G3, 4 - G4, 5 - LZW, 7 - JPEG, 34712 - JPEG200-, 34663 - JBIG2
        /// </param>
        /// <param name="binarize">
        /// Set binarize flag. 
        /// Available when file type is 1 or 4.
        /// Or Available when file type is 4 and comptype is 3, 4  or 34663.
        /// 0 - Do not binarize, others - bibarize</param>
        /// <param name="threshold">
        /// If binarize is 1, the binarization reference value is used, 
        /// and if it is 0, the value is internally binarized.
        /// </param>
        /// <returns>
        /// Failure if less than 1.
        /// Success if 1 or more. Number of files to be converted.
        /// </returns>
        [DllImport(@"lib\sconverter\InziPDF2Image.dll", CharSet = CharSet.Ansi)]
        extern private static int convertPDF2Image(
            string filename,
            string dstFoldername,
            int resolution,
            int comprate,
            int filetype,
            int comptype, 
            int binarize,
            int threshold);

        /// <summary>
        /// Convert pdf to image file.
        /// </summary>
        /// <param name="srcFilename">The name of the file to be converted.</param>
        /// <param name="dstFoldername">The name of the folder where the file to be converted is saved.</param>
        /// /// <param name="dstFilername">The name of the file to be converted.</param>
        /// <param name="resolution">Resolution value to be applied to the header when converting to a tiff file.
        /// DPI value to be applied to the image
        /// </param>
        /// <param name="comprate">
        /// Compression value for images with compression enabled.
        /// JPEG, JPEG in TIFF:1 ~ 100
        /// JPEG2000, JPEG2000 in TIFF: 24 - 1bpp, 12 - 2bpp, 48 - 0.5bpp
        /// </param>
        /// <param name="filetype">
        /// Format of the image file to be converted.
        /// 1 - BMP, 2 - JPEG, 3 - JPEG200, 4 - TIFF
        /// </param>
        /// <param name="comptype">
        /// Compression method to be applied when the format of the image file 
        /// to be converted is tiff.
        /// 1 - Do not compress, 2 - RLE, 3 - G3, 4 - G4, 5 - LZW, 7 - JPEG, 34712 - JPEG200-, 34663 - JBIG2
        /// </param>
        /// <param name="binarize">
        /// Set binarize flag. 
        /// Available when file type is 1 or 4.
        /// Or Available when file type is 4 and comptype is 3, 4  or 34663.
        /// 0 - Do not binarize, others - bibarize</param>
        /// <param name="threshold">
        /// If binarize is 1, the binarization reference value is used, 
        /// and if it is 0, the value is internally binarized.
        /// </param>
        /// <returns>
        /// Failure if less than 1.
        /// Success if 1 or more. Number of files to be converted.
        /// </returns>
        [DllImport(@"lib\sconverter\InziPDF2Image.dll", CharSet = CharSet.Ansi)]
        extern private static int convertPDF2NamedImage(
            string srcFilename,
            string dstFoldername,
            string dstFilename,
            int resolution,
            int comprate,
            int filetype,
            int comptype,
            int binarize,
            int threshold);

        /// <summary>
        /// Convert specific pages to images in PDF.
        /// </summary>
        /// <param name="srcFilename">The name of the file to be converted.</param>
        /// <param name="dstFoldername">The name of the folder where the file to be converted is saved.</param>
        /// <param name="resolution">Resolution value to be applied to the header when converting to a tiff file.
        /// DPI value to be applied to the image
        /// </param>
        /// <param name="comprate">
        /// Compression value for images with compression enabled.
        /// JPEG, JPEG in TIFF:1 ~ 100
        /// JPEG2000, JPEG2000 in TIFF: 24 - 1bpp, 12 - 2bpp, 48 - 0.5bpp
        /// </param>
        /// <param name="filetype">
        /// Format of the image file to be converted.
        /// 1 - BMP, 2 - JPEG, 3 - JPEG200, 4 - TIFF
        /// </param>
        /// <param name="comptype">
        /// Compression method to be applied when the format of the image file 
        /// to be converted is tiff.
        /// 1 - Do not compress, 2 - RLE, 3 - G3, 4 - G4, 5 - LZW, 7 - JPEG, 34712 - JPEG200-, 34663 - JBIG2
        /// </param>
        /// <param name="binarize">
        /// Set binarize flag. 
        /// Available when file type is 1 or 4.
        /// Or Available when file type is 4 and comptype is 3, 4  or 34663.
        /// 0 - Do not binarize, others - bibarize</param>
        /// <param name="threshold">
        /// If binarize is 1, the binarization reference value is used, 
        /// and if it is 0, the value is internally binarized.
        /// </param>
        /// <param name="numOfPage">Number of pages to convert.</param>
        /// <returns>
        /// Failure if less than 1.
        /// Success if 1 or more. Number of files to be converted.
        /// </returns>
        [DllImport(@"lib\sconverter\InziPDF2Image.dll", CharSet = CharSet.Ansi)]
        extern private static int convertPDF2ImageOnePage(
            string srcFilename,
            string dstFoldername,
            int resolution,
            int comprate,
            int filetype,
            int comptype,
            int binarize,
            int threshold,
            int numOfPage,              // page = 0-based
            int makeizt,
            int thumbnailWidth,
            int thumbnailHeight);

        /// <summary>
        /// Convert specific pages to images in PDF.
        /// </summary>
        /// <param name="srcFilename">The name of the file to be converted.</param>
        /// <param name="dstFoldername">The name of the folder where the file to be converted is saved.</param>
        /// <param name="dstFilername">The name of the file to be converted.</param>
        /// <param name="resolution">
        /// Resolution value to be applied to the header when converting to a tiff file.
        /// DPI value to be applied to the image
        /// </param>
        /// <param name="comprate">
        /// Compression value for images with compression enabled.
        /// JPEG, JPEG in TIFF:1 ~ 100
        /// JPEG2000, JPEG2000 in TIFF: 24 - 1bpp, 12 - 2bpp, 48 - 0.5bpp
        /// </param>
        /// <param name="filetype">
        /// Format of the image file to be converted.
        /// 1 - BMP, 2 - JPEG, 3 - JPEG200, 4 - TIFF
        /// </param>
        /// <param name="comptype">
        /// Compression method to be applied when the format of the image file 
        /// to be converted is tiff.
        /// 1 - Do not compress, 2 - RLE, 3 - G3, 4 - G4, 5 - LZW, 7 - JPEG, 34712 - JPEG200-, 34663 - JBIG2
        /// </param>
        /// <param name="binarize">
        /// Set binarize flag. 
        /// Available when file type is 1 or 4.
        /// Or Available when file type is 4 and comptype is 3, 4  or 34663.
        /// 0 - Do not binarize, others - bibarize</param>
        /// <param name="threshold">
        /// If binarize is 1, the binarization reference value is used, 
        /// and if it is 0, the value is internally binarized.
        /// </param>
        /// <param name="numOfPage">Number of pages to convert.</param>
        /// <returns>
        /// Failure if less than 1.
        /// Success if 1 or more. Number of files to be converted.
        /// </returns>
        [DllImport(@"lib\sconverter\InziPDF2Image.dll", CharSet = CharSet.Ansi)]
        extern private static int convertPDF2NamedImageOnePage(
            string srcFilename,
            string dstFoldername, 
            string dstFilename, 
            int resolution, 
            int comprate,
            int filetype, 
            int comptype, 
            int binarize, 
            int threshold,
            int numOfPage,      // page = 0-based
            int makeizt,
            int thumbnailWidth,
            int thumbnailHeight);

        /// <summary>
        /// Get number of pages.
        /// </summary>
        /// <param name="filename">The name of pdf file.</param>
        /// <returns>
        /// 0: Failed, Others - Number of pages
        /// </returns>
        [DllImport(@"lib\sconverter\InziPDF2Image.dll", CharSet = CharSet.Ansi)]
        extern private static int getPDFPageCount(string filename);
        #endregion

        #region PUBLIC METHODS
        /// <summary>
        /// Get number of pages.
        /// </summary>
        /// <param name="filename">The name of file.</param>
        /// <param name="format">Image format of file.</param>
        /// <returns>0 - failed, others - number of pages</returns>
        public static int GetNumOfPages(string filename, ImageFormats format)
        {
            int nums = 0;

            try
            {
                switch (format)
                {
                    case ImageFormats.Pdf:
                        nums = getPDFPageCount(filename);
                        break;
                    case ImageFormats.Tiff:
                        nums = getTIFFTotalPageFILE(filename);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }

            return nums;
        }

        public static ImageFormats GetImageFormat(string filename)
        {
            try
            {
                if (System.IO.Path.GetExtension(filename).ToUpper().Equals(".PDF"))
                    return ImageFormats.Pdf;

                int rc = getFileTypeFILE(filename);
                if (rc >= 1 && rc <= 8)
                    return (ImageFormats)rc;
                else
                    logger.Error($"Can't get file type. {rc} returned.");
            }
            catch (Exception ex)
            {
                logger.Error($"Conversion exception occurred. {ex.Message}");
                
            }
            return ImageFormats.Unknown;
        }

        // <summary>
        /// Extract multi tiff image to single tiff image.
        /// </summary>
        /// <param name="srcFileName">source (multi tiff)file name.</param>
        /// <param name="dstFileName">extracted single tiff file name.</param>
        /// <param name="srcPageNo">source page number. 1 base</param>
        /// <returns>true - extract succeed, false - extract failed.</returns>
        public static bool ExtractTiff(string srcFilename, 
            string dstFilename, int pageNumber)
        {
            string msg = "Multiple tiff image extract failed. {0}";
            try
            {
                int rc = extractTIFFFILE(srcFilename, pageNumber, dstFilename);
                if (rc != 0)
                {
                    logger.Error(string.Format(msg, rc) + " returned.");
                    return false;
                }
            }
            catch (Exception e)
            {
                logger.Error(string.Format(msg, e.Message));
                return false;
            }
            return true;
        }

        /// <summary>
        /// Convert specific pages to images in PDF.
        /// </summary>
        /// <param name="srcFilename"></param>
        /// <param name="dstFoldername"></param>
        /// <param name="dstFileName"></param>
        /// <param name="dstFormat"></param>
        /// <param name="numOfPage">Number(0 base) of pages to convert.</param>
        /// <returns></returns>
        public static bool ConvertPDF(string srcFilename,
            string dstFoldername, string dstFileName, ImageFormats dstFormat,
            int numOfPage)
        {
            string msg = "Convert pdf to image failed. {0}";
            try
            {
                int rc = convertPDF2NamedImageOnePage(
                            srcFilename,
                            dstFoldername,
                            dstFileName,
                            200,
                            0,
                            (int)dstFormat,
                            3,                  // G3
                            1,                  // Binarize
                            0,
                            numOfPage,
                            0,
                            0,
                            0);

                if (rc < 0)
                {
                    logger.Error(string.Format(msg, rc) + " returned.");
                    return false;
                }
            }
            catch (Exception e)
            {
                logger.Error(string.Format(msg, e.Message));
                return false;
            }
            return true;
        }
        #endregion
    }
}
