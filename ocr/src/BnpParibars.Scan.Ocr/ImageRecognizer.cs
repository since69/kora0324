﻿//
// BnpParibars.Scan.Ocr.ImageRecognizer
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System;
using System.Collections.Generic;
using System.IO;
using log4net;
using AxINZIFORMPROCSE10Lib;
using INZIFORMPROCSE10Lib;

namespace BnpParibars.Scan.Ocr
{
    public class ImageRecognizer
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("ImageRecognizer");
        private readonly AxInziFormProcSE10 recognizer = new AxInziFormProcSE10();
        private readonly List<string> templates = new List<string>();
        #endregion

        #region PROPERTIES
        #endregion

        #region CONSTRUCTORS
        public ImageRecognizer()
        {
            recognizer.CreateControl();
        }
        #endregion

        #region PUBLIC METHODS
        /// <summary>
        /// Load recognition template.
        /// </summary>
        /// <param name="templateDir">Template folder name.</param>
        /// <param name="templateName">
        /// Recognition template file name without extension. 
        /// file naming rule is followed format.
        /// Document category code(2) + subcategory code(4) + revision date(8)
        /// ex) D1003320201031
        /// </param>
        /// <param name="imageFileName"></param>
        /// <returns></returns>
        public bool LoadTemplate(string templateDir,
            string templateName,
            string imageFileName)
        {
            int retVal;

            try
            {
                // Initialize OCR related controls.
                if (recognizer.SetFormPath(templateDir) < 0)
                {
                    logger.Error(string.Format(
                        "Can't initialize recognizer template path. '{0}'.",
                        recognizer.GetErrorMessage(recognizer.GetLastError())));
                    return false;
                }
                else
                {
                    // Changed the path changed by axRecognizer.SetFromPath
                    // to the original path
                    Directory.SetCurrentDirectory(@"..\");
                }

                if (recognizer.FormLoad(templateName) < 0)
                {
                    logger.Error($"Can't load recognition template {templateName}.");
                    return false;
                }

                recognizer.RecognizerLoad(templateName);
                recognizer.PreprocessDeskew();
                if (recognizer.ReadProcessImage(imageFileName) < 0)
                {
                    retVal = recognizer.GetLastError();
                    recognizer.FormUnload(templateName);
                    recognizer.FreeProcessImage();
                    logger.Error($"ReadProcessImage failed. {retVal}");
                    return false;
                }

                //InziImageInfo imgInfo = GetProcessImageInfo(imageFileName);
                //if (!Binarize(imageFileName, imgInfo))
                //{
                //    logger.Error("Binalize failed.");
                //    return false;
                //}
                if (!ChangeImageMode(imageFileName))
                {
                    recognizer.FormUnload(templateName);
                    recognizer.FreeProcessImage();
                    logger.Error("Image mode change failed.");
                    return false;
                }

                recognizer.FormSelectNone();
                recognizer.FormSelectUsingCode(templateName);
                if (recognizer.FormMapping() <= 0)
                {
                    recognizer.FormUnload(templateName);
                    recognizer.FreeProcessImage();
                    logger.Error("FormMapping failed.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                recognizer.FormUnload(templateName);
                recognizer.FreeProcessImage();
                logger.Error($"LoadTemplate failed. {ex.Message}");
                return false;
            }

            recognizer.FormUnload(templateName);
            recognizer.FreeProcessImage();
            logger.Debug("FormMapping succeed.");
            return true;
        }

        public bool ChangeImageMode(string filename, bool binarize = true)
        {
            InziImageInfo info = GetProcessImageInfo(filename);
            switch (info.bitperpixel)
            {
                case 1: return false;
                case 8:
                    if (binarize) return Binarize(filename);
                    return false;
                case 24:
                case 32:
                    int rv = recognizer.PreprocessMakeGray(1);
                    if (rv != 0)
                    {
                        logger.Error($"PreprocessMakeGray failed. {rv}");
                        return false;
                    }
                    if (!binarize)
                    {
                        if ((rv = recognizer.SaveProcessImageToFile(filename)) != 0)
                            logger.Error($"SaveProcessImageToFile failed. {rv}");
                        return rv == 0;
                    }
                    return Binarize(filename);
                default:
                    logger.Error($"{info.bitperpixel} is invalid bps.");
                    return false;
            }
        }

        public bool Recognize(List<RecognitionData> recogDatas)
        {
            int rv = recognizer.FormGetResultFieldCount();
            if (rv <= 0)
            {
                recognizer.FreeProcessImage();
                recognizer.FreeBackupImage(string.Empty);
                logger.Error($"Recognized field is not exists.");
                return false;
            }

            for (int i = 0; i < rv; i++)
            {
                recogDatas.Add(new RecognitionData()
                {
                    ID = recognizer.FormGetResultFieldID(i),
                    Name = recognizer.FormGetResultFieldName(i),
                    Result = recognizer.FormGetResultFieldString(i),
                    Top = recognizer.FormGetResultFieldPosTop(i),
                    Right = recognizer.FormGetResultFieldPosRight(i),
                    Bottom = recognizer.FormGetResultFieldPosBottom(i),
                    Left = recognizer.FormGetResultFieldPosLeft(i)
                });
            }

            recognizer.FreeProcessImage();
            recognizer.FreeBackupImage(string.Empty);

            return true;
        }
        #endregion

        #region PRIVATE METHODS
        private bool Binarize(string filename)
        {
            int rv;
            if ((rv = recognizer.PreprocessBinarize(1, "160")) != 0)
            {
                logger.Error($"PreprocessBinarize failed. {rv}");
            }
            else
            {
                if ((rv = recognizer.SaveProcessImageToFile(filename)) != 0)
                    logger.Error($"SaveProcessImageToFile failed. {rv}");
            }
            return rv == 0;
        }

        private InziImageInfo GetProcessImageInfo(string filename)
        {
            int retValue = 0;
            INZIFORMPROCSE10Lib.InziImageInfo imgInfo;
            // 빌드 속성 탭에서 '안전하지 않은 코드 허용'에 체크 필요.
            unsafe
            {
                fixed (INZIFORMPROCSE10Lib.InziImageInfo* imgTemp
                    = new INZIFORMPROCSE10Lib.InziImageInfo[1])
                {
                    IntPtr pInfo = new IntPtr(imgTemp);
                    retValue = recognizer.GetProcessImageInfo((object)pInfo);
                    imgInfo = imgTemp[0];

                    if (retValue != 0)
                        logger.Error($"GetProcessImageInfo failed. {retValue}" );
                }
            }
            return imgInfo;
        }
        #endregion
    }
}
