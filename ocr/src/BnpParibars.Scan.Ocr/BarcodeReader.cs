﻿//
// BnpParibars.Scan.Ocr.BarcodeReader
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System.IO;
using System.Drawing;
using System.Runtime.InteropServices;
using log4net;
using AxINZIBARCODELib;
using AxINZI_IFILELib;
using System.Collections.Generic;

namespace BnpParibars.Scan.Ocr
{
    public class Barcode
    {
        public string Code { get; set; } = string.Empty;
        public int Top { get; set; } = 0;
        public int Right { get; set; } = 0;
        public int Bottom { get; set; } = 0;
        public int Left { get; set; } = 0;
        public Barcode() { }
    }

    public class BarcodeReader
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("BarcodeReader");
        private readonly AxInziBarcode barcode = new AxInziBarcode();
        private readonly AxInzi_iFile ifile = new AxInzi_iFile();
        //private const int magnificationRate = 2;
        private const string P2I_LOC = @"c:\Program Files (x86)\Inzisoft\Inzi iForm 5.0\InziPDF2Image.dll";
        #endregion

        #region EXTERNAL_LIB_METHODS
        [DllImport(P2I_LOC, CharSet = CharSet.Ansi)]
        extern private static int getPDFPageCount(string filename);

        [DllImport(P2I_LOC, CharSet = CharSet.Ansi)]
        extern private static int convertPDF2NamedImageOnePage(string filenameSrc,
            string folderDst, string filenameDst, int resolution, int comparate,
            int filetype, int comptype, int binarize, int threshold, int page,
            int makeizt, int thumbnailWid, int thumbnailHgt);
        #endregion

        #region PROPERTIES
        public int ResizeRation { get; set; } = 2;
        #endregion

        #region CONSTRUCTRIONS
        public BarcodeReader()
        {
            barcode.CreateControl();
            ifile.CreateControl();
            ifile.SetCryptMode(true);
        }
        #endregion

        #region PRIVATE METHODS
        #endregion

        #region PUBLIC METHODS
        /// <summary>
        /// Convert specific pages to images in PDF.
        /// </summary>
        /// <param name="srcFilename"></param>
        /// <param name="dstFoldername"></param>
        /// <param name="dstFileName"></param>
        /// <param name="dstFormat"></param>
        /// <param name="numOfPage">Number(0 base) of pages to convert.</param>
        /// <returns></returns>
        public static bool ConvertPDF(
            string srcFilename,
            string dstFoldername, 
            string dstFileName, 
            ImageFormats dstFormat,
            int numOfPage)
        {
            string msg = "Convert pdf to image failed. {0}";
            try
            {
                int rc = convertPDF2NamedImageOnePage(
                            srcFilename,
                            dstFoldername,
                            dstFileName,
                            200,
                            0,
                            (int)dstFormat,
                            3,                  // G3
                            1,                  // Binarize
                            0,
                            numOfPage,
                            0,
                            0,
                            0);

                if (rc < 0)
                {
                    logger.Error(string.Format(msg, rc) + " returned.");
                    return false;
                }
            }
            catch (System.Exception e)
            {
                logger.Error(string.Format(msg, e.Message));
                return false;
            }
            return true;
        }

        public int GetPdfPageCount(string filename)
        {
            int rc = getPDFPageCount(filename);
            if (rc <= 0)
            {
                logger.Error($"GetPdfPageCount() failed. {rc} returned.");
                return 0;
            }
            return rc;
        }

        public int GetTiffPageCount(string filename)
        {
            int rc = (int)ifile.GetTIFFPageCount(filename);
            if (rc <= 0)
            {
                logger.Error($"GetTiffPageCount() failed. {rc} returned.");
                return 0;
            }
            return rc;
        }

        public bool ExtractTiff(string srcFile, string dstFile, short pageNo)
        {
            INZI_IFILELib.InziResultConstants rc =
                ifile.ExtractTIFFPage(pageNo, srcFile, dstFile);
            if (rc != INZI_IFILELib.InziResultConstants.izrSuccess)
            {
                logger.Error($"{srcFile} - {pageNo} page extraction failed.");
                return false;
            }
            return true;
        }

        public List<Barcode> Read(string filename, bool removeNoise = false)
        {
            try
            {
                barcode.SetParameter("TYPE", "QR", "");
                barcode.SetParameter("FILENAME", filename, "");
                barcode.SetParameter("NUMCHAR", "1", "");
                barcode.SetParameter("CONFMODE", "1", "");
                barcode.SetParameter("RECMODE1D", "0", "");
                barcode.SetParameter("REMOVE_FAX_NOISE", "0", "");
                if (removeNoise)
                    barcode.SetParameter("REMOVE_FAX_NOISE", "1", "");

                int numOfBarcodes = barcode.Recognize();
                if (numOfBarcodes != 0)
                {
                    List<Barcode> barcodes = new List<Barcode>();
                    for (int i = 0; i < numOfBarcodes; i++)
                    {
                        barcodes.Add(new Barcode()
                        {
                            Code = barcode.GetResultString((short)i),
                            Top = barcode.GetResultPositionTop((short)i),
                            Right = barcode.GetResultPositionRight((short)i),
                            Bottom = barcode.GetResultPositionBottom((short)i),
                            Left = barcode.GetResultPositionLeft((short)i)
                        });
                    }
                    return barcodes;
                }
                logger.Error("Can't read barcode or barcode is not exists.");
            }
            catch (System.Exception e)
            {
                logger.Error($"QR code read failed! {e.Message}");
            }
            return null;
        }

        public bool ResizeImage(string filename, int width, int height)
        {
            if (!File.Exists(filename))
            {
                logger.Error($"{filename} is not exists.");
                return false;
            }

            int retVal = ifile.ResizeImage(filename, filename, width, height, 5);
            return retVal == 0;
        }

        public bool RotateImage(string filename)
        {
            INZI_IFILELib.InziResultConstants rc
                = ifile.RotateImage180(filename, filename);
            return rc ==INZI_IFILELib.InziResultConstants.izrSuccess;
        }

        /// <summary>
        /// Crop the image and save it as a new file.
        /// </summary>
        /// <param name="srcFilename">Full path of original file</param>
        /// <param name="cropFilename">Full path of the file to be cropped.</param>
        /// <returns></returns>
        public bool Crop(string srcFilename, string cropFilename, 
            Rectangle rcCropArea)
        {
            bool retValue;
            try
            {
                if (!File.Exists(srcFilename))
                {
                    logger.Error("Invalid image file name.");
                    return false;
                }

                retValue = ifile.ClipImage(srcFilename,
                    rcCropArea.Left, rcCropArea.Top, rcCropArea.Right, 
                    rcCropArea.Bottom, cropFilename);
            }
            catch (System.Exception e)
            {
                logger.Error($"ClipImage() failed. {e.Message}");
                return false;
            }

            if (!retValue)
            {
                logger.Error("Failed to crop the image.");
                return false;
            }
            if (!File.Exists(cropFilename))
            {
                logger.Error("Can't find the cropped image.");
                return false;
            }
            return true;
        }
        #endregion
    }
}
