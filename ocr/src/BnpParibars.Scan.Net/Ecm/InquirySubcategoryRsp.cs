﻿//
// BnpParibars.Scan.Net.Ecm.InquirySubcategoryRsp
// 
// Authors:
//  Joohyoung Kim
//
// (C) 2020
//

namespace BnpParibars.Scan.Net.Ecm
{
    using System.Collections.Generic;

    /// <summary>
    /// DocumentSubcategory table inquiry response.
    /// </summary>
    public class InquirySubcategoryRsp : BaseResponse
    {
        #region PROPERTIES
        public List<Data.DocumentSubcategory> ResultObj { get; set; }
        #endregion
    }
}
