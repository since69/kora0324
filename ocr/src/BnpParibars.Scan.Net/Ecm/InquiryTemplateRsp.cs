﻿//
// BnpParibars.Scan.Net.Ecm.InquiryTemplateRsp
// 
// Authors:
//  Joohyoung Kim
//
// (C) 2020
//

namespace BnpParibars.Scan.Net.Ecm
{
    using System.Collections.Generic;

    /// <summary>
    /// RecognitionTemplate table inquiry response.
    /// </summary>
    public class InquiryTemplateRsp : BaseResponse
    {
        #region PROPERTIES
        public List<Data.RecognitionTemplate> ResultObj { get; set; }
        #endregion
    }
}
