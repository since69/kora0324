﻿//
// BnpParibars.Scan.Net.Ecm.InquiryContractRsp
// 
// Authors:
//  Joohyoung Kim
//
// (C) 2020
//

namespace BnpParibars.Scan.Net.Ecm
{
    using System.Collections.Generic;

    /// <summary>
    /// Contract information inquiry response.
    /// </summary>
    public class InquiryContractRsp : BaseResponse
    {
        #region PROPERTIES
        // documentCategoryId, code, name, modifieDate, sortOrder
        public Data.Contract ResultObj { get; set; }
        #endregion
    }
}
