﻿//
// BnpParibars.Scan.Net.Ecm.InquirySupplementRsp
// 
// Authors:
//  Joohyoung Kim
//
// (C) 2020
//

namespace BnpParibars.Scan.Net.Ecm
{
    using System.Collections.Generic;

    /// <summary>
    /// Supplement table inquiry response.
    /// </summary>
    public class InquirySupplementRsp : BaseResponse
    {
        #region PROPERTIES
        public List<Data.SupplementData> ResultObj { get; set; }
        #endregion
    }
}
