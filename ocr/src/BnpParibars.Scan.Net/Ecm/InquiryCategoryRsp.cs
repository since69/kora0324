﻿//
// BnpParibars.Scan.Net.Ecm.InquiryCategoryRsp
// 
// Authors:
//  Joohyoung Kim
//
// (C) 2020
//

namespace BnpParibars.Scan.Net.Ecm
{
    using System.Collections.Generic;

    /// <summary>
    /// DocumentCategory table inquiry response.
    /// </summary>
    public class InquiryCategoryRsp : BaseResponse
    {
        #region PROPERTIES
        // documentCategoryId, code, name, modifieDate, sortOrder
        public List<Data.DocumentCategory> ResultObj { get; set; }
        #endregion
    }
}
