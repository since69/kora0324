﻿//
// BnpParibars.Scan.Net.Ecm.BaseResponse
// 
// Authors:
//  Joohyoung Kim
//
// (C) 2020
//

namespace BnpParibars.Scan.Net.Ecm
{
    /// <summary>
    /// Hole ecm process results.
    /// </summary>
    public class BaseResponse
    {
        #region PROPERTIES
        public int ErrCode { get; set; }
        public string ErrName { get; set; }
        public string ErrDesc { get; set; }
        public int ResultCode { get; set; }
        public string ResultDesc { get; set; }
        #endregion
    }
}
