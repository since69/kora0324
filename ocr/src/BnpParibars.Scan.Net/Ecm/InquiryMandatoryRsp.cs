﻿//
// BnpParibars.Scan.Net.Ecm.InquiryMandatoryRsp
// 
// Authors:
//  Joohyoung Kim
//
// (C) 2020
//

namespace BnpParibars.Scan.Net.Ecm
{
    using System.Collections.Generic;

    /// <summary>
    /// MandatoryDocument table inquiry response.
    /// </summary>
    public class InquiryMandatoryRsp : BaseResponse
    {
        #region PROPERTIES
        public List<Data.MandatoryDocument> ResultObj { get; set; }
        #endregion
    }
}
