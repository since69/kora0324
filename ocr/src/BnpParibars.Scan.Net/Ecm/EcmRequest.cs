﻿//
// BnpParibars.Scan.Net.Ecm.EcmRequest
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibars.Scan.Net.Ecm
{
    using System;
    using System.Collections.Specialized;
    using System.IO;
    using System.Net;
    using System.Text;
    using log4net;

    public class EcmRequest
    {
        #region VARIABLES
        private static ILog logger = LogManager.GetLogger("EcmRequest");
        #endregion

        #region PROPERTIES
        public string UserName { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        #endregion

        #region CONSTRUCTORS
        public EcmRequest(string username, string password)
        {
            UserName = username;
            Password = password;
        }
        public EcmRequest() {}
        #endregion

        #region PRIVATE METHODS
        private static string PostData(string postUrl, 
            string postData, 
            string contentType)
        {
            string responseFromServer = string.Empty;

            try
            {
                // Create a request using a URL that can receive a post.
                WebRequest request = WebRequest.Create(postUrl);
                request.Method = "POST";

                // Create POST data and convert it to a byte array.
                //string postData = "test";
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                // Set the ContentType property of the WebRequest.
                request.ContentType = contentType;
                request.ContentLength = byteArray.Length;

                // Get the request stream.
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream boject.
                dataStream.Close();

                // Get the response.
                WebResponse response = request.GetResponse();
                // Display the status.
                //logger.Debug("Response status: " 
                //    + ((HttpWebResponse)response).StatusDescription);

                // Get the stream containing content returned by the server.
                // The using block ensures the stream is automatically closed.
                using (dataStream = response.GetResponseStream())
                {
                    // Open the stream using a StreamReader for easy access.
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.
                    responseFromServer = reader.ReadToEnd();
                    // Display the content.
                    //System.Console.WriteLine(responseFromServer);
                }

                // Close the response
                response.Close();
            }
            catch (Exception ex)
            {
                logger.Error($"Request failed. {ex.Message}");
                return string.Empty;
            }

            return responseFromServer;
        }

        private static bool UploadFiles(string url,
            string[] files,
            //NameValueCollection formFields = null,
            NameValueCollection formFields,
            out string rs)
        {
            string boundary = "----------------------------"
                + DateTime.Now.Ticks.ToString("x");

            rs = string.Empty;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "multipart/form-data; boundary=" + boundary;
                request.KeepAlive = true;

                Stream memStream = new MemoryStream();
                var boundaryBytes =
                    Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");
                var endBoundaryBytes =
                    Encoding.ASCII.GetBytes("\r\n--" + boundary + "--");

                string formDataTemplate = "\r\n--" + boundary
                    + "\r\nContent-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}";
                if (formFields != null)
                {
                    foreach (string key in formFields.Keys)
                    {
                        string formItem =
                            string.Format(formDataTemplate, key, formFields[key]);
                        byte[] formItemBytes = Encoding.UTF8.GetBytes(formItem);
                        memStream.Write(formItemBytes, 0, formItemBytes.Length);
                    }
                }

                string headerTemplate =
                    "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n" +
                    "Content-Type: application/octet-stream\r\n\r\n";
                for (int i = 0; i < files.Length; i++)
                {
                    memStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                    var header = string.Format(headerTemplate,
                        //"uplTheFile",
                        Path.GetFileName(files[i]),
                        Path.GetFileName(files[i]));
                    var headerBytes = Encoding.UTF8.GetBytes(header);

                    memStream.Write(headerBytes, 0, headerBytes.Length);

                    using (var fileStream = new FileStream(files[i], FileMode.Open, FileAccess.Read))
                    {
                        var buffer = new byte[51200];
                        var bytesRead = 0;
                        while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                            memStream.Write(buffer, 0, bytesRead);
                    }
                }

                memStream.Write(endBoundaryBytes, 0, endBoundaryBytes.Length);
                request.ContentLength = memStream.Length;

                using (Stream requestStream = request.GetRequestStream())
                {
                    memStream.Position = 0;
                    byte[] temp = new byte[memStream.Length];
                    memStream.Read(temp, 0, temp.Length);
                    memStream.Close();
                    requestStream.Write(temp, 0, temp.Length);
                }

                using (var response = request.GetResponse())
                {
                    Stream stream2 = response.GetResponseStream();
                    StreamReader reader2 = new StreamReader(stream2);
                    //string rs = reader2.ReadToEnd();
                    rs = reader2.ReadToEnd();

                    if (string.IsNullOrEmpty(rs))
                    {
                        logger.Error($"Upload failed! {rs}");
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Error($"Upload faild. {ex.Message}");
            }
            return false;
        }
        #endregion

        #region PUBLIC METHODS
        public bool UploadRequest(string url, string parameters, string[] files, out string rs)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc.Add("metaInfo", parameters);
            return UploadFiles(url, files, nvc, out rs);
        }

        public bool SendRequest(string url, string data, out string response)
        {
            response = PostData(url, data, "application/x-www-form-urlencoded");
            if (string.IsNullOrEmpty(response))
            {
                logger.Error("SendRequest faild!\n" + response);
                return false;
            }

            return true;
        }

        public bool SendRequestWithJson(string url, string data, out string response)
        {
            response = PostData(url, data, "application/json");
            if (string.IsNullOrEmpty(response))
            {
                logger.Error("SendRequestWithJson faild!\n" + response);
                return false;
            }

            return true;
        }

        public bool SendRequest(string url, out string response)
        {
            return SendRequest(url, string.Empty, out response);
        }

        public bool SendRequest(string url)
        {
            return SendRequest(url, out string _);
        }
        #endregion
    }
}
