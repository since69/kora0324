﻿//
// BnpParibars.Scan.Net.Ecm.UploadResponse
// 
// Authors:
//  Joohyoung Kim
//
// (C) 2020
//

namespace BnpParibars.Scan.Net.Ecm
{
    using System.Collections.Generic;

    /// <summary>
    /// Upload ecm process results.
    /// </summary>
    public class UploadResponse : BaseResponse
    {
        #region PROPERTIES
        public Dictionary<string, string> ResultObj { get; set; } =
            new Dictionary<string, string>();
        #endregion
    }
}
