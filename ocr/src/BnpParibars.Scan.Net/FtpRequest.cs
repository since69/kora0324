﻿//
// BnpParibars.Scan.Net.FtpRequest
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibars.Scan.Net
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using log4net;

    /// <summary>
    /// Includes the ability to download and delete files received 
    /// via fax to the OCR server using ftps.
    /// </summary>
    public class FtpRequest
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("FtpRequest");
        private FtpWebResponse response = null;
        private Stream responseStream = null;
        private StreamReader readStream = null;
        //private StreamWriter writeStream = null;
        private FileStream fileStream = null;
        private Uri faxUri = null;
        private const int buflen = 2048;
        #endregion

        #region PROPERTIES
        public string UserName { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        #endregion

        #region CONSTRUCTORS
        public FtpRequest() { }
        #endregion

        #region PRIVATE METHODS
        private bool CreateUri(string url)
        {
            faxUri = new Uri(url);

            // The serverUri parameter should use the ftp:// scheme.
            // It contains the name of the server file that is to be deleted.
            // Example: ftp://contoso.com/someFile.txt.
            if (faxUri.Scheme != Uri.UriSchemeFtp)
            {
                logger.Error("Uri is not start with the 'ftp://schema'");
                return false;
            }

            return true;
        }

        private FtpWebRequest CreateRequest(string method)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(faxUri);
            request.Method = method;
            //request.EnableSsl = true;
            //request.UsePassive = true;
            //request.KeepAlive = true;
            //request.UseBinary = true;
            //request.Timeout = 5000;
            request.Credentials = new NetworkCredential(UserName, Password);
            return request;
        }
        #endregion
                                                                                                                                                                                                                              
        #region PUBLIC METHODS
        public bool DeleteFileOnServer(string url, bool enableSsl = false)
        {
            if (!CreateUri(url)) 
                return false;

            try
            {
                // Get the object used to communicate with the server.
                FtpWebRequest request = CreateRequest(WebRequestMethods.Ftp.DeleteFile);
                request.EnableSsl = enableSsl;

                response = (FtpWebResponse)request.GetResponse();
                logger.Debug("Delete status: " + response.StatusDescription);
                response.Close();
            }
            catch (Exception e)
            {
                logger.Error("Delete failed: " + e.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Download file from ftp server with binary mode.
        /// </summary>
        /// <param name="url">url of the file to be downloaded.</param>
        /// <param name="filename">file name to be stored local.</param>
        /// <returns></returns>
        public bool DownloadBinaryFile(string url, string filename, bool enableSsl = false)
        {
            if (!CreateUri(url))
                return false;

            bool isErrorOccurred = false;
            try
            {
                // Get the object used to communicate with the server.
                // Note that the cast to FtpWebRequest is done only
                // for the purposes of illustration. If your application
                // does not set any properties other than those defined in the
                // System.Net.WebRequest class, you can use the following line instead:
                // WebRequest request = WebRequest.Create(serverUri);
                FtpWebRequest request = CreateRequest(WebRequestMethods.Ftp.DownloadFile);
                request.UsePassive = true;
                request.KeepAlive = true;
                request.UseBinary = true;
                request.EnableSsl = enableSsl;

                response = (FtpWebResponse)request.GetResponse();
                responseStream = response.GetResponseStream();

                byte[] buffer = new byte[buflen];

                int count = 0;
                int readBytes = 0;
                fileStream = new FileStream(filename, FileMode.Create);
                do
                {
                    readBytes = responseStream.Read(buffer, 0, buflen);
                    fileStream.Write(buffer, 0, readBytes);
                    count += readBytes;
                } while (readBytes != 0);

                logger.Debug(string.Format("Download status: {0}, {1}",  
                    response.StatusCode, response.StatusDescription));
            }
            catch (Exception e)
            {
                logger.Error("The process failed: " + e.Message);
                isErrorOccurred = true;
            }
            finally
            {
                if (response != null)
                    response.Close();

                if (fileStream != null)
                    fileStream.Close();
            }

            return !isErrorOccurred;
        }

        public bool UploadBinaryFile(string url, string filename, bool enableSsl = false)
        {
            if (!CreateUri(url))
                return false;

            bool isErrorOccurred = false;
            try
            {
                // Get the object used to communicate with the server.
                // Note that the cast to FtpWebRequest is done only
                // for the purposes of illustration. If your application
                // does not set any properties other than those defined in the
                // System.Net.WebRequest class, you can use the following line instead:
                // WebRequest request = WebRequest.Create(serverUrl);
                FtpWebRequest request = CreateRequest(WebRequestMethods.Ftp.UploadFile);
                request.UseBinary = true;
                request.EnableSsl = enableSsl;

                // Copy the contents of the file to the request stream.
                byte[] fileContents;
                using (StreamReader sourceStream = new StreamReader(filename))
                {
                    fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                }

                request.ContentLength = fileContents.Length;

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(fileContents, 0, fileContents.Length);
                }

                using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                {
                    logger.Info($"Upload file complete, status {response.StatusDescription}");
                }
            }
            catch (Exception e)
            {
                logger.Error("The process failed: " + e.Message);
                isErrorOccurred = true;
            }

            return !isErrorOccurred;
        }

        /// <summary>
        /// Get file list from ftp server.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="files"></param>
        /// <returns></returns>
        public bool ListFilesOnServer(string url, out string[] files, bool enableSsl= false)
        {
            ServicePointManager.ServerCertificateValidationCallback 
                += (sender, certificate, chain, sslPolicyErrors) => true;

            files = null;

            if (!CreateUri(url))
                return false;

            bool isErrorOccurred = false;
            try
            {
                // Get the object used to communicate with the server.
                FtpWebRequest request = CreateRequest(WebRequestMethods.Ftp.ListDirectory);
                request.EnableSsl = enableSsl;

                // Get the ServicePoint object used for this request, and limit it to one connection.
                // In a real-world application you might use the default number of connections (2),
                // or select a value that works best for your application.

                ServicePoint sp = request.ServicePoint;
                logger.Debug("ServicePoint connections = " + sp.ConnectionLimit);
                sp.ConnectionLimit = 1;

                response = (FtpWebResponse)request.GetResponse();
                logger.Debug("The content length is " + response.ContentLength);

                // The following streams are used to read the data returned from the server.
                responseStream = response.GetResponseStream();
                readStream = new StreamReader(responseStream, Encoding.UTF8);

                if (readStream != null)
                {
                    // Display the data received from the server.
                    StringBuilder readLines = new StringBuilder(readStream.ReadToEnd());
                    logger.Debug(readLines.ToString());
                    if (!string.IsNullOrEmpty(readLines.ToString()))
                    {
                        readLines.Replace("\r\n", "\n");
                        readLines.Remove(readLines.ToString().LastIndexOf("\n"), 1);
                        files = readLines.ToString().Split('\n');
                    }
                }
                logger.Debug("List status: " + response.StatusDescription);
            }
            catch (WebException e)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("A WebException has been caught.\n");
                sb.Append(e.ToString());
                WebExceptionStatus status = e.Status;
                if (status == WebExceptionStatus.ProtocolError)
                {
                    FtpWebResponse rsp = (FtpWebResponse)e.Response;
                    sb.Append("\nThe server returned protocol error ");
                    sb.AppendFormat("{0} - {1}", (int)rsp.StatusCode, rsp.StatusCode);
                }
                logger.Error(sb.ToString());
            }
            catch (InvalidOperationException e)
            {
                logger.Error(e.GetType().FullName);
                logger.Error(e.Message);
                isErrorOccurred = true;
            }
            catch (Exception e)
            {
                logger.Error("The process failed: " + e.Message);
                isErrorOccurred = true;
            }
            finally
            {
                if (readStream != null)
                    readStream.Close();

                if (response != null)
                    response.Close();
            }

            //Console.WriteLine("Banner message: {0}", response.BannerMessage);
            //Console.WriteLine("Welcome message: {0}", response.WelcomeMessage);
            //Console.WriteLine("Exit message: {0}", response.ExitMessage);

            return !isErrorOccurred;
        }
        #endregion
    }
}
