﻿//
// BnpParibars.Scan.Net.FtpClient
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibars.Scan.Net
{
    using System;
    using FluentFTP;
    using log4net;

    public class FtpClient
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("FtpClient");
        private FluentFTP.FtpClient client = null;
        #endregion

        #region PROPERTIES
        #endregion

        #region CONSTRUCTORS
        public FtpClient(string host, int port, string username, string password)
        {
            client = new FluentFTP.FtpClient(host, port, username, password);
        }
        #endregion

        #region PUBLIC METHODS
        
        public bool ListFilesOnServer(string remoteDir, 
            out string[] files,
            bool enableSsl = false)
        {
            files = null;

            try
            {
                if (!Connect(enableSsl)) return false;
                if (!client.DirectoryExists(remoteDir))
                {
                    logger.Error($"'{remoteDir}' is not exists.");
                    return false;
                }

                System.Collections.Generic.List<string> items
                    = new System.Collections.Generic.List<string>();
                foreach (var item in client.GetListing(remoteDir))
                {
                    if (item.Type == FtpFileSystemObjectType.File)
                        items.Add(item.FullName);
                }

                files = items.ToArray();
            }
            catch (Exception ex)
            {
                logger.Error($"Can't get file list on server. {ex.Message}");
                return false;
            }
            finally
            {
                if (client.IsConnected)
                    client.Disconnect();
            }

            return true;
        }

        public bool DownloadFile(string localFilename, 
            string remoteFilename, 
            bool enableSsl = false)
        {
            try
            {
                if (!Connect(enableSsl)) return false;
                if (!client.FileExists(remoteFilename))
                {
                    logger.Error($"'{remoteFilename}' is not exists.");
                    return false;
                }
                client.DownloadFile(localFilename, remoteFilename);
            }
            catch (Exception ex)
            {
                logger.Error($"Can't download file on server. {ex.Message}");
                return false;
            }
            finally
            {
                if (client.IsConnected)
                    client.Disconnect();
            }
            return true;
        }

        public bool DeleteFile(string remoteFilename, bool enableSsl = false)
        {
            try
            {
                if (!Connect(enableSsl)) return false;
                if (!client.FileExists(remoteFilename))
                {
                    logger.Error($"'{remoteFilename}' is not exists.");
                    return false;
                }
                client.DeleteFile(remoteFilename);
            }
            catch (Exception ex)
            {
                logger.Error($"Can't delete file on server. {ex.Message}");
                return false;
            }
            finally
            {
                if (client.IsConnected)
                    client.Disconnect();
            }
            return true;
        }
        #endregion

        #region PRIVATE METHODS
        public bool Connect(bool enableSsl = false)
        {
            try
            {
                if (enableSsl)
                {
                    client.EncryptionMode = FtpEncryptionMode.Auto;
                    client.ValidateAnyCertificate = true;
                }

                client.Connect();
            }
            catch (Exception ex)
            {
                logger.Error($"Connect failed. {ex.Message}");
                return false;
            }
            return true;
        }
        #endregion
    }
}
