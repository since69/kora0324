﻿//
// BnpParibars.Scan.Util.IniHelper
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibars.Scan.Util
{
    using System.Runtime.InteropServices;

    public static class IniHelper
    {
        #region ENTERNAL LIBRARY METHODS
        [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern long GetPrivateProfileString(string Section,
            string Key, string Default, System.Text.StringBuilder RetVal,
            int Size, string FilePath);
        #endregion

        #region PUBLIC METHODS
        public static bool KeyExists(string path, string key, string section = null)
        {
            return Read(path, key, section).Length > 0;
        }

        public static string Read(string path, string key,string section = null)
        {
            var retVal = new System.Text.StringBuilder(10240);
            GetPrivateProfileString(section, key, "", retVal, 10240, path);
            return retVal.ToString();
        }
        #endregion
    }
}
