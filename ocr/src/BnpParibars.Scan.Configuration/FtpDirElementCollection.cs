﻿// BnpParibars.Scan.Configuration.FtpDirElementCollection
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.Configuration;

namespace BnpParibars.Scan.Configuration
{
    public class FtpDirElementCollection : ConfigurationElementCollection
    {
        #region CONSTRUCTORS
        public FtpDirElementCollection() {}
        #endregion

        #region PROPERTIES
        public FtpDirElement this[int index]
        {
            get { return (FtpDirElement)base.BaseGet(index); }
            set
            {
                if (base.BaseGet(index) != null)
                    base.BaseRemoveAt(index);
                base.BaseAdd(index, value);
            }
        }

        new public FtpDirElement this[string name]
        {
            get { return (FtpDirElement)BaseGet(name); }
        }
        #endregion

        #region PROTECTED METHODS
        protected override void BaseAdd(ConfigurationElement element)
        {
            base.BaseAdd(element, false);

            // Your custom code goes here.
        }
        protected override ConfigurationElement CreateNewElement()
        {
            return new FtpDirElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((FtpDirElement)element).Dir;
        }
        #endregion

        #region PUBLIC METHODSd
        public void Add(FtpDirElement dir)
        {
            BaseAdd(dir);

            // HYour custom code goest here.
        }

        public void Remove(FtpDirElement dir)
        {
            if (BaseIndexOf(dir) >= 0)
            {
                BaseRemove(dir.Dir);

                // Your custom code goes here.
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemove(index);

            // Your custom code goes here.
        }

        public void Clear()
        {
            BaseClear();

            // Your custom code goes here.
        }

        public int IndexOf(FtpDirElement dir)
        {
            return BaseIndexOf(dir);
        }
        #endregion
    }
}