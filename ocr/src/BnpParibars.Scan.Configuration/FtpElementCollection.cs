﻿// BnpParibars.Scan.Configuration.FtpElementCollection
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.Configuration;

namespace BnpParibars.Scan.Configuration
{
    public class FtpElementCollection : ConfigurationElementCollection
    {
        #region CONSTRUCTORS
        public FtpElementCollection() {}
        #endregion

        #region PROPERTIES
        public FtpElement this[int index]
        {
            get { return (FtpElement)base.BaseGet(index); }
            set
            {
                if (base.BaseGet(index) != null)
                    base.BaseRemoveAt(index);
                base.BaseAdd(index, value);
            }
        }

        new public FtpElement this[string name]
        {
            get { return (FtpElement)BaseGet(name); }
        }
        #endregion

        #region PROTECTED METHODS
        protected override void BaseAdd(ConfigurationElement element)
        {
            base.BaseAdd(element, false);

            // Your custom code goes here.
        }
        protected override ConfigurationElement CreateNewElement()
        {
            return new FtpElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((FtpElement)element).Name;
        }
        #endregion

        #region PUBLIC METHODSd
        public void Add(FtpElement ftp)
        {
            BaseAdd(ftp);

            // HYour custom code goest here.
        }

        public void Remove(FtpElement ftp)
        {
            if (BaseIndexOf(ftp) >= 0)
            {
                BaseRemove(ftp.Name);

                // Your custom code goes here.
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemove(index);

            // Your custom code goes here.
        }

        public void Clear()
        {
            BaseClear();

            // Your custom code goes here.
        }

        public int IndexOf(FtpElement ftp)
        {
            return BaseIndexOf(ftp);
        }
        #endregion
    }
}