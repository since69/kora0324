﻿// BnpParibars.Scan.Configuration.FtpElement
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibars.Scan.Configuration
{
    using System.Configuration;

    public class FtpElement : ConfigurationElement
    {
        #region PROPERTIES
        [ConfigurationProperty("name", DefaultValue = "", IsKey = true)]
        public string Name
        {
            get { return (string)base["name"]; }
        }

        [ConfigurationProperty("ip", DefaultValue = "")]
        public string Ip
        {
            get { return (string)base["ip"]; }
        }

        [ConfigurationProperty("port", DefaultValue = "0")]
        public int Port
        {
            get { return (int)base["port"]; }
        }

        [ConfigurationProperty("baseDir", DefaultValue = "/")]
        public string BaseDir
        {
            get { return (string)base["baseDir"]; }
        }

        [ConfigurationProperty("numOfDownloads", DefaultValue = "5")]
        public int NumOfDownloads
        {
            get { return (int)base["numOfDownloads"]; }
        }

        [ConfigurationProperty("username", DefaultValue = "")]
        public string UserName
        {
            get { return (string)base["username"]; }
        }

        [ConfigurationProperty("password", DefaultValue = "")]
        public string Password
        {
            get { return (string)base["password"]; }
        }

        [ConfigurationProperty("ftpDirs", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(FtpDirElementCollection),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]
        public FtpDirElementCollection FtpDirs
        {
            get { return (FtpDirElementCollection)base["ftpDirs"]; }
        }
        #endregion
    }
}
