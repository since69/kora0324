﻿// BnpParibars.Scan.Configuration.ServletElementCollection
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.Configuration;

namespace BnpParibars.Scan.Configuration
{
    public class ServletElementCollection : ConfigurationElementCollection
    {
        #region CONSTRUCTORS
        public ServletElementCollection() {}
        #endregion

        #region PROPERTIES
        public ServletElement this[int index]
        {
            get { return (ServletElement)base.BaseGet(index); }
            set
            {
                if (base.BaseGet(index) != null)
                    base.BaseRemoveAt(index);
                base.BaseAdd(index, value);
            }
        }

        new public ServletElement this[string name]
        {
            get { return (ServletElement)BaseGet(name); }
        }
        #endregion

        #region PROTECTED METHODS
        protected override void BaseAdd(ConfigurationElement element)
        {
            base.BaseAdd(element, false);

            // Your custom code goes here.
        }
        protected override ConfigurationElement CreateNewElement()
        {
            return new ServletElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ServletElement)element).Name;
        }
        #endregion

        #region PUBLIC METHODSd
        public void Add(ServletElement servlet)
        {
            BaseAdd(servlet);

            // HYour custom code goest here.
        }

        public void Remove(ServletElement servlet)
        {
            if (BaseIndexOf(servlet) >= 0)
            {
                BaseRemove(servlet.Name);

                // Your custom code goes here.
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemove(index);

            // Your custom code goes here.
        }

        public void Clear()
        {
            BaseClear();

            // Your custom code goes here.
        }

        public int IndexOf(ServletElement servlet)
        {
            return BaseIndexOf(servlet);
        }
        #endregion
    }
}