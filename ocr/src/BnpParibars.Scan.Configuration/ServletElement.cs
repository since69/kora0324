﻿// BnpParibars.Scan.Configuration.ServletElement
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibars.Scan.Configuration
{
    using System.Configuration;

    public class ServletElement : ConfigurationElement
    {
        #region PROPERTIES
        [ConfigurationProperty("name", DefaultValue = "", IsRequired = true, IsKey = true)]
        public string Name
        {
            get { return (string)base["name"]; }
            set { this["name"] = value; }
        }

        [ConfigurationProperty("dir", DefaultValue = "", IsRequired = true)]
        public string Dir
        {
            get { return (string)base["dir"]; }
            set { this["dir"] = value; }
        }

        [ConfigurationProperty("type", DefaultValue = "0", IsRequired = true)]
        public int Type
        {
            get { return (int)base["type"]; }
            set { this["type"] = value; }
        }
        #endregion

        #region CONSTRUCTORS
        public ServletElement(string name, string dir, int type)
        {
            Name = name;
            Dir = dir;
            Type = type;
        }

        public ServletElement() { }
        #endregion
    }
}
