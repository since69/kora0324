﻿// BnpParibars.Scan.Configuration.FtpDirElement
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibars.Scan.Configuration
{
    using System.Configuration;

    public class FtpDirElement : ConfigurationElement
    {
        #region PROPERTIES
        [ConfigurationProperty("dir", DefaultValue = "", IsKey = true)]
        public string Dir
        {
            get { return (string)base["dir"]; }
        }

        [ConfigurationProperty("jobType", DefaultValue = "0")]
        public int JobType
        {
            get { return (int)base["jobType"]; }
        }
        #endregion
    }
}
