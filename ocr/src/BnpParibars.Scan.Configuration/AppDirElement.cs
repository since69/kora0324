﻿// BnpParibars.Scsan.Configuration.AppDirElement
//
// Authors:
//  Joohyoung Kim
//
// (C) 2020

namespace BnpParibars.Scan.Configuration
{
    using System.Configuration;

    public class AppDirElement : ConfigurationElement
    {
        #region PROPERTIES
        [ConfigurationProperty("baseDir", DefaultValue = @".\")]
        public string BaseDir => (string)base["baseDir"];

        [ConfigurationProperty("convertDir", DefaultValue = @"convert\")]
        public string ConvertDir => (string)base["convertDir"];

        [ConfigurationProperty("downloadDir", DefaultValue = @"download\")]
        public string DownloadDir => (string)base["downloadDir"];

        [ConfigurationProperty("failureDir", DefaultValue = @"failure\")]
        public string FailureDir => (string)base["failureDir"];

        [ConfigurationProperty("templateDir", DefaultValue = @"template\")]
        public string TemplateDir => (string)base["templateDir"];
        #endregion
    }
}
