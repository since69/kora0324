﻿// BnpParibars.Scan.Configuration.ConsentElement
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibars.Scan.Configuration
{
    using System.Configuration;

    public class ConsentElement : ConfigurationElement
    {
        #region PROPERTIES
        /// <summary>
        /// Database connection string
        /// </summary
        public string ConnectionString
        {
            get
            {
                string connStringName = string.Empty;

                if (string.IsNullOrEmpty(this.ConnectionStringName))
                    connStringName = Configurator.OcrSettings.DefaultConnectionStringName;
                else
                    connStringName = this.ConnectionStringName;

                return ConfigurationManager.ConnectionStrings[connStringName]
                    .ConnectionString;
            }
        }

        /// <summary>
        /// Database connection string name
        /// </summary
        [ConfigurationProperty("connectionStringName")]
        public string ConnectionStringName
        {
            get { return (string)base["connectionStringName"]; }
            set { base["connectionStringName"] = value; }
        }

        [ConfigurationProperty("providerType",
            DefaultValue = "BnpParibars.Scan.Data.DAL.OracleClient.OracleConsentProvider")]
        public string ProviderType
        {
            get { return (string)base["providerType"]; }
        }
        #endregion
    }
}
