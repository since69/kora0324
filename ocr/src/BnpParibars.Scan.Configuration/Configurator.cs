﻿//
// BnpParibars.Scan.Configuration.Configurator
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibars.Scan.Configuration
{
    using System.Configuration;
    using log4net;

    public static class Configurator
    {
        #region VARIABLES
        private static ILog logger = LogManager.GetLogger("Configurator");
        #endregion

        #region PROPERTIES
        public static OcrSection OcrSettings { get; private set; } = null;
        #endregion

        #region CONSTRUCTORS
        static Configurator()
        {
            string sectionName = string.Empty;

            try
            {
                sectionName = "ocr";
                OcrSettings = (OcrSection)ConfigurationManager.
                    GetSection(sectionName);
            }
            catch (ConfigurationException e)
            {
                logger.Fatal($"Can't load {sectionName} section. " + e.Message);
            }
        }
        #endregion
    }
}
