﻿// BnpParibars.Scan.Configuration.OcrSection
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibars.Scan.Configuration
{
    using System.Configuration;

    //public class UrlsSection : ConfigurationSection
    //{
    //    [ConfigurationProperty("name",
    //        DefaultValue = "MyFavorites",
    //        IsRequired = true,
    //        IsKey = false)]
    //    [StringValidator(InvalidCharacters =
    //        " ~!@#$%^&*()[]{}/;'\"|\\",
    //        MinLength = 1, MaxLength = 60)]
    //    public string Name
    //    {

    //        get
    //        {
    //            return (string)this["name"];
    //        }
    //        set
    //        {
    //            this["name"] = value;
    //        }
    //    }
    //}

    public class OcrSection : ConfigurationSection
    {
        #region PROPERTIES
        [ConfigurationProperty("defaultConnectionStringName", IsRequired = true,
            DefaultValue = "GmsUatServer")]
        public string DefaultConnectionStringName
        {
            get { return (string)base["defaultConnectionStringName"]; }
            set { base["defaultConnectionStringName"] = value; }
        }

        [ConfigurationProperty("appDir", IsRequired = true)]
        public AppDirElement AppDir => (AppDirElement)base["appDir"];

        [ConfigurationProperty("ecm", IsRequired = true)]
        public EcmElement Ecm => (EcmElement)base["ecm"];

        [ConfigurationProperty("ftps", IsDefaultCollection = true)]
        [ConfigurationCollection(typeof(FtpElementCollection),
            AddItemName = "ftp",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]
        public FtpElementCollection Ftps
        {
            get { return (FtpElementCollection)base["ftps"]; }
        }

        [ConfigurationProperty("consent", IsDefaultCollection = true)]
        public ConsentElement Consent
        {
            get { return (ConsentElement)base["consent"]; }
        }
        #endregion
    }
}
