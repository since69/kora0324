﻿//
// BnpParibars.Scan.Data.DocumentCategory
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibars.Scan.Data
{
    public class DocumentCategory
    {
        #region PROPERTIES
        public int documentCategoryId { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public int sortOrder { get; set; }
        public string modifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public DocumentCategory() { }
        #endregion
    }
}
