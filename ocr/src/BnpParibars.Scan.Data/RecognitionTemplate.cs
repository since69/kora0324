﻿//
// BnpParibars.Scan.Data.RecognitionTemplate
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibars.Scan.Data
{
    public class RecognitionTemplate
    {
        #region PROPERTIES
        public string ContractorTypeCode { get; set; }
        public string BusinessTypeCode { get; set; }
        public string ProductCategoryCode { get; set; }
        public string DocumentCategoryID { get; set; }
        public int DocumentSubcategoryID { get; set; }
        public string TemplateVersion { get; set; }
        public string TemplateFileID { get; set; }
        public string ZoneDefinitionFileID { get; set; }
        public string ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public RecognitionTemplate() { }
        #endregion
    }
}
