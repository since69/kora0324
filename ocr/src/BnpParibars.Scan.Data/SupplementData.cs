﻿//
// BnpParibars.Scan.Data.SupplementData
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibars.Scan.Data
{
    public class SupplementData
    {
        #region PROPERTIES
        public int SupplementID { get; set; }
        public int DocumentCategoryID { get; set; }
        public int DocumentSubcategoryID { get; set; }
        public string Code { get; set; }
        public string Supplement { get; set; }
        #endregion

        #region CONSTRUCTORS
        public SupplementData() { }
        #endregion
    }
}
