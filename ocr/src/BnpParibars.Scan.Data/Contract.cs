﻿//
// BnpParibars.Scan.Data.Contract
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BnpParibars.Scan.Data
{
    /// <summary>
    /// BSL을 이용하여 가져온 계약 정보를 보관하는데 사용.
    /// 계약정보 중 계약심사(Under writing)가 진행 중임을 의미하는 Accept_Date
    /// 컬럼이 널이 아닌 경우 POS를 제외한 GA, BA 업무는 등록이 되면 안됨.
    /// </summary>
    public class Contract
    {
        #region PROPERTIES
        public string BusinessTypeCode { get; set; }
        public string ProductCategoryCode { get; set; }
        public string ContractorTypeCode { get; set; }
        public int BusinessNumberType { get; set; }
        public string BusinessNumber { get; set; }
        /// <summary>
        /// Under Writting start date.
        /// </summary>
        public string Accept_Date { get; set; }
        public string Arrival_Date { get; set; }
        /// <summary>
        /// 증권번호, Policy number
        /// </summary>
        public string Cont_No { get; set; }
        public string Cont_Ymd { get; set; }
        public string Cont_Nm { get; set; }
        public string Suit_Gb { get; set; }
        public string ID_Truth_Check { get; set; }
        public string Trsf_Dd { get; set; }
        public string Silver_Plan { get; set; }
        public string Sub_Job_Nm { get; set; }
        public string Addr_Gb { get; set; }
        public string Pst_No1 { get; set; }
        public string Pst_No2 { get; set; }
        public string Pst_Addr { get; set; }
        public string Etc_Addr { get; set; }
        public string Tel_Addr { get; set; }
        public string Tel_Ddd { get; set; }
        public string Tel_Guk { get; set; }
        public string Tel_No { get; set; }
        public string Ins_Drv_Yn { get; set; }
        public string Mon_Income { get; set; }
        public string Fin_Assets { get; set; }
        public string Purpose_Tran { get; set; }
        public string Source_Of_Funds { get; set; }
        public string Source_Of_Income { get; set; }
        public string O_Comm_Can_Yn { get; set; }
        #endregion

        #region CONSTRUCTORS
        public Contract() {}
        #endregion
    }
}
