﻿//
// BnpParibars.Scan.Data.CaseSupplement
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibars.Scan.Data
{
    public class CaseSupplement
    {
        #region PROPERTIES
        public int caseId { get; set; } = 0;
        public int supplementId { get; set; } = 0;
        public int documentCategoryId { get; set; }
        public string documentCategoryCode { get; set; }
        public string documentCategoryName { get; set; }
        public int documentSubcategoryId { get; set; }
        public string documentSubcategoryCode { get; set; }
        public string documentSubcategoryName { get; set; }
        public string code { get; set; } = string.Empty;
        public string supplement { get; set; }
        #endregion

        #region CONSTRUCTORS
        public CaseSupplement() { }
        #endregion
    }
}
