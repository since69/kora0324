﻿//
// BnpParibars.Scan.Data.DAL.ConsentProvider
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020 Thinkbox Co. Ltd
//

using BnpParibars.Scan.Configuration;
using System;

namespace BnpParibars.Scan.Data.DAL
{
    public abstract class ConsentProvider : DataAccess
    {
        #region PROPERTIES
        private static ConsentProvider instance = null;

        public static ConsentProvider Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (ConsentProvider)Activator.CreateInstance(
                        Type.GetType(Configurator.OcrSettings.Consent.ProviderType));
                }
                return instance;
            }
        }
        #endregion

        #region CONSTRUCTORS
        public ConsentProvider()
        {
            ConnectionString = Configurator.OcrSettings.Consent.ConnectionString;
            //ConnectionString = "Data Source=digitalu;Persist Security Info=True;User ID=svckrgaman;Password=svckrgaman;";
        }
        #endregion

        #region ABSTRACT METHODS
        /// <summary>
        /// Update the result of consent to personal information.
        /// </summary>
        /// <param name="consentNo">Consent number</param>
        /// <param name="complementCode">Complement code. max length is 15</param>
        /// <returns></returns>
        public abstract bool UpdateComplementStatus(string consentNo, string complementCode);
        #endregion

        #region VIRTUAL METHODS
        #endregion
    }
}
