﻿//
// BnpParibars.Scan.Data.DAL.OracleClient.OracleConsentProvider
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020 Thinkbox Co. Ltd
//

using System.Text;
using Oracle.ManagedDataAccess.Client;
//using BnpParibars.Scan.Data.DAL.Model;
using log4net;

namespace BnpParibars.Scan.Data.DAL.OracleClient
{
    public class OracleConsentProvider : ConsentProvider
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("OracleConsentProvider");
        private readonly StringBuilder sql = new StringBuilder();
        #endregion

        #region PUBLIC METHODS
        public override bool UpdateComplementStatus(string consentNo, string complementCode)
        {
            sql.Clear();
            sql.AppendLine("UPDATE GA_CUST_AGREE");
            sql.AppendLine("SET APPROVAL_YN = (");
            sql.AppendLine("\t\tCASE");
            sql.AppendLine("\t\t\tWHEN");
            sql.AppendLine("\t\t\t(");
            sql.AppendLine("\t\t\t\tSELECT STATUS_CD");
            sql.AppendLine("\t\t\t\tFROM GA_ARS ARS");
            sql.AppendLine($"\t\t\t\tWHERE SUBSTR(ARS.CONSENT_NO, 5, 6) = '{consentNo}'");
            sql.AppendLine($"\t\t\t) = 'S000' AND '{complementCode}' = '000'");
            sql.AppendLine("\t\t\tTHEN 'Y'");
            sql.AppendLine($"\t\t\tWHEN '{complementCode}' != '000' THEN 'F'");
            sql.AppendLine("\t\t\tELSE 'N'");
            sql.AppendLine("\t\tEND)");
            sql.AppendLine($"\t\t, APPROVAL_STAT_CD = '{complementCode}'");
            sql.AppendLine("\t\t, CUST_AGREEFORM_ACCPT_YN = (");
            sql.AppendLine("\t\t\tCASE");
            sql.AppendLine($"\t\t\t\tWHEN '{complementCode}' = '000' THEN 'Y'");
            sql.AppendLine($"\t\t\t\tWHEN '{complementCode}' != '000' THEN 'F'");
            sql.AppendLine($"\t\t\t\tELSE 'N'");
            sql.AppendLine($"\t\t\tEND)");
            sql.AppendLine($"WHERE SUBSTR(CONSENT_NO, 5, 6) = '{consentNo}'");
            sql.AppendLine($"\tAND NVL(APPROVAL_YN, 'N') <> 'Y'");
            logger.Debug(sql.ToString());

            using (OracleConnection cn = new OracleConnection(ConnectionString))
            {
                OracleCommand cmd = new OracleCommand(sql.ToString(), cn);
                cn.Open();
                int retVal = ExecuteNonQuery(cmd);
                return (retVal != 0);
            }
        }
        #endregion
    }
}
