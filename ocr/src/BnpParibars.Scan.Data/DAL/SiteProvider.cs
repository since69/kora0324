﻿//
// BnpParibars.Scan.Data.DAL.SiteProvider
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020 Thinkbox Co. Ltd
//


namespace BnpParibars.Scan.Data.DAL
{
    public static class SiteProvider
    {
        public static ConsentProvider Consent
        {
            get { return ConsentProvider.Instance; }
        }
    }
}
