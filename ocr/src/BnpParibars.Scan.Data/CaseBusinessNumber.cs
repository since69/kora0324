﻿//
// BnpParibars.Scan.Data.CaseBusinessNumber
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibars.Scan.Data
{
    public class CaseBusinessNumber
    {
        #region PROPERTIES
        public int caseId { get; set; } = 0;
        public int businessNumberType { get; set; } = 0;
        public string businessNumber { get; set; } = string.Empty;
        public int archiveStatus { get; set; } = 0;
        #endregion

        #region CONSTRUCTORS
        public CaseBusinessNumber() { }
        #endregion
    }
}
