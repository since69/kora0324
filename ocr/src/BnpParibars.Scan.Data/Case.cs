﻿//
// BnpParibars.Scan.Data.Case
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System;
using System.Collections.Generic;

namespace BnpParibars.Scan.Data
{
    public class Case
    {
        #region PROPERTIES
        public int caseId { get; set; } = 0;
        public string businessTypeCode { get; set; } = string.Empty;
        public string productCategoryCode { get; set; } = string.Empty;
        public string contractorTypeCode { get; set; } = string.Empty;
        /// <summary>
        /// Document funnel. (SCAN/FAX/PDF)
        /// </summary>
        public string funnel { get; set; } = string.Empty;
        /// <summary>
        /// Document process status.
        /// 2 - Complement (use in orc)
        /// 4 - Fax receipt (use in ocr)
        /// 5 - Mail bag
        /// 6 - Check KYC Information
        /// 7 - Arrived
        /// </summary>
        public int status { get; set; } = 0;
        public string workerId { get; set; } = "AutoBatch";
        public string createUserId { get; set; } = "AutoBatch";
        public DateTime createdDate { get; set; }
        public DateTime modifiedDate { get; set; }

        public List<CaseBusinessNumber> caseBusinessNumberList { get; set; }
        public List<CaseDocument> caseDocumentList { get; set; }
        public List<CaseSupplement> caseSupplementList { get; set; }
        #endregion

        #region CONSTRUCTORS
        public Case() 
        {
            caseBusinessNumberList = new List<CaseBusinessNumber>();
            caseDocumentList = new List<CaseDocument>();
            caseSupplementList = new List<CaseSupplement>();
        }
        #endregion
    }
}
