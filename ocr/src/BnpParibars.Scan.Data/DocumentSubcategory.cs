﻿//
// BnpParibars.Scan.Data.DocumentSubcategory
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibars.Scan.Data
{
    public class DocumentSubcategory
    {
        #region PROPERTIES
        public int documentSubcategoryId { get; set; }
        public string code { get; set; }
        public string title { get; set; }
        public int sortOrder { get; set; }
        public string modifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public DocumentSubcategory() { }
        #endregion
    }
}
