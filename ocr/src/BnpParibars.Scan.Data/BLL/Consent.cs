﻿//
// BnpParibars.Scan.Data.BLL.Consent
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020 Thinkbox Co. Ltd
//

using log4net;
using BnpParibars.Scan.Data.DAL;

namespace BnpParibars.Scan.Data.BLL
{
    public class Consent
    {
        #region CONSTRUCTORS
        public Consent() { }
        #endregion

        #region PUBLIC METHODS
        public static bool UpdateComplementStatus(string consentNo, 
            string complementCode)
        {
            return SiteProvider.Consent.UpdateComplementStatus(
                consentNo, 
                complementCode);
        }
        #endregion
    }
}
