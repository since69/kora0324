﻿//
// BnpParibars.Scan.Data.SeperateArchiveHistory
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibars.Scan.Data
{
    public class SeperateArchiveHistory
    {
        #region PROPERTIES
        public int CaseID { get; set; }
        public System.DateTime ArchivedDate { get; set; }
        public System.DateTime DisposedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public SeperateArchiveHistory() { }
        #endregion
    }
}
