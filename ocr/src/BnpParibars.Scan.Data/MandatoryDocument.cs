﻿//
// BnpParibars.Scan.Data.MandatoryDocument
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibars.Scan.Data
{
    public class MandatoryDocument
    {
        #region PROPERTIES
        public string ContractorTypeCode { get; set; }
        public string BusinessTypeCode { get; set; }
        public string ProductCategoryCode { get; set; }
        public int DocumentCategoryID { get; set; }
        public int DocumentSubcategoryID { get; set; }
        //public bool IsMandatory { get; set; }
        public int IsMandatory { get; set; }
        public string ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public MandatoryDocument() { }
        #endregion
    }
}
