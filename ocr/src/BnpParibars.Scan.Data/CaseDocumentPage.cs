﻿//
// BnpParibars.Scan.Data.CaseDocumentPage
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibars.Scan.Data
{
    public class CaseDocumentPage
    {
        #region PROPERTIES
        public int caseDocumentPageId { get; set; } = 0;
        public int caseDocumentId { get; set; } = 0;
        public int pageNumber { get; set; } = 0;
        public int isEncrypted { get; set; } = 0;
        public string imageFileId { get; set; } = string.Empty;
        public string annotationFileId { get; set; } = string.Empty;
        #endregion

        #region CONSTRUCTORS
        public CaseDocumentPage() { }
        #endregion
    }
}
