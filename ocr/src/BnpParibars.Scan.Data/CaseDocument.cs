﻿//
// BnpParibars.Scan.Data.CaseDocument
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System.Collections.Generic;

namespace BnpParibars.Scan.Data
{
    public class CaseDocument
    {
        #region PROPERTIES
        public int caseDocumentId { get; set; } = 0;
        public int caseId { get; set; } = 0;
        public int documentCategoryId { get; set; } = 0;
        public string documentCategoryCode { get; set; } = string.Empty;
        public string documentCategoryName { get; set; } = string.Empty;
        public int documentCategorySortOrder { get; set; } = 0;
        public int documentSubcategoryId { get; set; } = 0;
        public string documentSubcategoryCode { get; set; } = string.Empty;
        public string documentSubcategoryName { get; set; } = string.Empty;
        public int documentSubcategorySortOrder { get; set; } = 0;
        public int transferDocType { get; set; } = 0;
        public int transferType { get; set; } = 0;
        public string transferRequestedDate { get; set; } = string.Empty;
        public int documentChangeNumber { get; set; } = 0;
        public System.DateTime modifiedDate { get; set; }

        public List<CaseDocumentPage> caseDocumentPageList { get; set; }
        #endregion

        #region CONSTRUCTORS
        public CaseDocument() 
        {
            caseDocumentPageList = new List<CaseDocumentPage>();
        }
        #endregion
    }
}
