﻿//
// BnpParibars.Scan.UI.Argos.Supplements
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System.Collections;
using System.Collections.Generic;
using BnpParibars.Scan.Data;
using log4net;

namespace BnpParibars.Scan.UI.Argos
{
    public class Supplements : IList<SupplementData>
    {
        #region VARIABLES
        private static ILog logger = LogManager.GetLogger("Supplements");
        private readonly IList<SupplementData> _list = new List<SupplementData>();
        #endregion

        #region PROPERTIES
        public int Count { get { return _list.Count; } }

        public bool IsReadOnly { get { return _list.IsReadOnly; } }

        public SupplementData this[int index]
        {
            get { return _list[index]; }
            set { _list[index] = value; }
        }
        #endregion

        #region CONSTRUCTORS
        public Supplements() { }
        #endregion

        #region VIRTUAL METHODS
        public virtual void Add(SupplementData item)
        {
            _list.Add(item);
        }

        public virtual void Clear()
        {
            _list.Clear();
        }

        public virtual bool Contains(SupplementData item)
        {
            return _list.Contains(item);
        }

        public virtual void CopyTo(SupplementData[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public virtual int IndexOf(SupplementData item)
        {
            return _list.IndexOf(item);
        }

        public virtual void Insert(int index, SupplementData item)
        {
            _list.Insert(index, item);
        }

        public virtual bool Remove(SupplementData item)
        {
            return _list.Remove(item);
        }

        public virtual void RemoveAt(int index) { _list.RemoveAt(index); }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public virtual IEnumerator<SupplementData> GetEnumerator()
        {
            return _list.GetEnumerator();
        }
        #endregion

        #region PUBLIC METHODS
        public void CopyFrom(List<SupplementData> supplements)
        {
            foreach (SupplementData item in supplements)
                _list.Add(item);
        }

        public SupplementData Find(int docCategoryId, 
            int docSubcategoryId, 
            string supplementCode)
        {
            foreach (SupplementData item in _list)
            {
                if (item.DocumentCategoryID == docCategoryId
                    && item.DocumentSubcategoryID == docSubcategoryId
                    && item.Code.Equals(supplementCode))
                    return item;
            }
            return null;
        }

        public List<SupplementData> Find(int docCategoryId, int docSubcategoryId)
        {
            List<SupplementData> supplements = new List<SupplementData>();
            foreach (SupplementData item in _list)
            {
                if (item.DocumentCategoryID == docCategoryId
                    && item.DocumentSubcategoryID == docSubcategoryId)
                {
                    supplements.Add(item);
                }
            }
            return supplements;
        }
        #endregion
    }
}
