﻿//
// BnpParibars.Scan.UI.Argos.OcrJob.OcrDatas
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System.Collections;
using System.Collections.Generic;

namespace BnpParibars.Scan.UI.Argos.OcrJob
{
	public class OcrDatas : IList<OcrData>
	{
		#region VARIABLES
		private readonly IList<OcrData> _list = new List<OcrData>();
		#endregion

		#region PROPERTIES
		public int Count { get { return _list.Count; } }

		public bool IsReadOnly { get { return _list.IsReadOnly; } }

		public OcrData this[int index]
		{
			get { return _list[index]; }
			set { _list[index] = value; }
		}
		#endregion

		#region CONSTRUCTORS
		public OcrDatas() { }
		#endregion

		#region VIRTUAL METHODS
		public virtual void Add(OcrData item)
		{
			_list.Add(item);
		}

		public virtual void Clear()
		{
			_list.Clear();
		}

		public virtual bool Contains(OcrData item)
		{
			return _list.Contains(item);
		}

		public virtual void CopyTo(OcrData[] array, int arrayIndex)
		{
			_list.CopyTo(array, arrayIndex);
		}

		public virtual int IndexOf(OcrData item)
		{
			return _list.IndexOf(item);
		}

		public virtual void Insert(int index, OcrData item)
		{
			_list.Insert(index, item);
		}

		public virtual bool Remove(OcrData item)
		{
			return _list.Remove(item);
		}

		public virtual void RemoveAt(int index) { _list.RemoveAt(index); }

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public virtual IEnumerator<OcrData> GetEnumerator()
		{
			return _list.GetEnumerator();
		}
		#endregion

		#region PUBLIC METHODS
		public OcrData Find(string imageFileName)
		{
			foreach (OcrData item in _list)
			{
				if (System.IO.Path.GetFileName(item.ImageFileName)
					.Equals(imageFileName))
					return item;
			}
			return null;
		}
		#endregion
	}
}
