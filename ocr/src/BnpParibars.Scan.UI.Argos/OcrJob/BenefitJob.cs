﻿//
// BnpParibars.Scan.UI.Argos.OcrJob.BenefitJob
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using log4net;
using BnpParibars.Scan.IO;
using BnpParibars.Scan.Data;

namespace BnpParibars.Scan.UI.Argos.OcrJob
{
    /// <summary>
    /// 웹팩스 보험금(CLAIM) 처리. QR 없음. 'C'로 시작하는 더미 접수번호(13자리) 생성 필요.
    /// FTP remote folder is '899'
    /// 유입경로: WEBFAX
    /// 문서유형: 보험금
    /// 업무유형: Claim
    /// 처리방법:
    ///     특정번호(8939)에 해당하는 FTP 원격폴더(939)에 들어오는 문서는 보험금으로
    ///     판단하며, QR 코드가 존재하지 않기 때문에 문서의 분리 및 변환 후 인식 과정을
    ///     거치지 않고 대분류는 클레임 서류(C0), 중분류는 클레임 문서(0061)로 분류하고
    ///     'C'로 시작하는 12자리 가접수번호를 생성하여 ECM 및 DB에 등록.
    /// </summary>
    public class BenefitJob : Job
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("BenefitJob");
        private string businessNumber = string.Empty;
        #endregion

        #region CONSTRUCTORS
        public BenefitJob() : base(JobTypes.Benefit) { }
        #endregion

        #region OVERRIDE METHODS
        protected override bool Import()
        {
            return Download();
        }

        protected override bool Preprocessing(DownloadFile downloadFile)
        {
            logger.Debug(string.Format("{0} image preprocessing started.",
                Util.EnumHelper.GetDescription(JobType)));

            if (!ConvertTiff(downloadFile)) return false;
            ResizeImage();

            logger.Debug(string.Format("{0} image preprocessing done.",
                Util.EnumHelper.GetDescription(JobType)));

            return true;
        }

        protected override void Recognize(JobData jobData)
        {
            logger.Debug(string.Format("{0} image recognition started.",
                Util.EnumHelper.GetDescription(JobType)));

            businessNumber = GenerateBusinessNumber();
            foreach (ConvertFile f in ConvertFiles)
            {
                OcrData result = new OcrData()
                {
                    BusinessTypeCode = BusinessTypes.GetType(JobType),
                    ProductCategoryCode = ProductCategories.Unknown,
                    ContractorTypeCode = ContractorTypes.Unknown,
                    BusinessNumber = businessNumber,
                    DocumentCategoryCode = "C0",
                    DocumentSubcategoryCode = "0061",
                    ImageFileName = f.FullName
                };
                jobData.OcrDatas.Add(result);
            }

            logger.Debug(string.Format("{0} image recognition is done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void Classfication(JobData jobData)
        {
            logger.Debug(string.Format("{0} classfication started.",
                Util.EnumHelper.GetDescription(JobType)));

            CreateCase(BusinessTypes.GetType(JobType), 
                ProductCategories.Unknown,
                ContractorTypes.Unknown, 
                businessNumber, jobData);

            logger.Debug(string.Format("{0} classfication done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void UploadFiles(JobData jobData)
        {
            logger.Debug(string.Format("{0} Upload started.",
                Util.EnumHelper.GetDescription(JobType)));

            UploadOcrFiles(jobData);

            logger.Debug(string.Format("{0} Upload done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void GenerateData(JobData jobData)
        {
            Case c = jobData.Cases[0];
            if (c == null)
            {
                logger.Error("Can't find any case.");
                return;
            }

            DocumentCategory category = jobData.DocCategories.Find("C0");
            DocumentSubcategory subcategory = jobData.DocSubcategories.Find("0061");
            CaseDocument newDoc = new CaseDocument()
            {
                documentCategoryId = category.documentCategoryId,
                documentCategoryCode = category.code,
                documentCategoryName = category.name,
                documentCategorySortOrder = category.sortOrder,
                documentSubcategoryId = subcategory.documentSubcategoryId,
                documentSubcategoryCode = subcategory.code,
                documentSubcategoryName = subcategory.title,
                documentSubcategorySortOrder = subcategory.sortOrder
            };

            int pageNo = 1;
            foreach (OcrData r in jobData.OcrDatas)
            {
                newDoc.caseDocumentPageList.Add(new CaseDocumentPage()
                {
                    pageNumber = pageNo++,
                    isEncrypted = 1,
                    imageFileId = r.ImageFileID,
                    annotationFileId = string.Empty
                });
            }
            c.caseDocumentList.Add(newDoc);
        }

        protected override void Verify(JobData jobData)
        {
            logger.Debug(string.Format("{0} is not required verification.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void Export(JobData jobData)
        {
            logger.Debug(string.Format("{0} export started.",
                Util.EnumHelper.GetDescription(JobType)));

            UploadDataToWas(jobData);

            logger.Debug(string.Format("{0} export done.",
                Util.EnumHelper.GetDescription(JobType)));
        }
        #endregion
    }
}
