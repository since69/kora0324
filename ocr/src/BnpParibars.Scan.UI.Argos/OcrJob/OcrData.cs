﻿//
// BnpParibars.Scan.UI.Argos.OcrJob.OcrData
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System.Collections.Generic;
using BnpParibars.Scan.Data;

namespace BnpParibars.Scan.UI.Argos.OcrJob
{
    /// <summary>
	/// Hold recognition relative data.
	/// </summary>
    public class OcrData
    {
        #region VARIABLES
        #endregion

        #region PROPERTIES
        public bool IsBarcodeRead { get; set; } = false;
        public bool IsRecognized { get; set; } = false;
        public bool IsUploaded { get; set; } = false;

        public string RawData { get; set; } = string.Empty;
        private string[] barcode = null;
        public string[] Barcode
        {
            get { return barcode; }
            set
            {
                barcode = value;
                IsBarcodeRead = barcode != null;
            }
        }
        public string BusinessTypeCode { get; set; } = BusinessTypes.Unknown;
        public string ProductCategoryCode { get; set; } = ProductCategories.Unknown;
        public string ContractorTypeCode { get; set; } = ContractorTypes.Unknown;
        public string BusinessNumber { get; set; } = string.Empty;
        public string DocumentCategoryCode { get; set; } = string.Empty;
        public string DocumentSubcategoryCode { get; set; } = string.Empty;
        public string RecognitionTemplateVersion { get; set; } = string.Empty;
        public string ImageFileName { get; set; } = string.Empty;
        public string ImageFileID { get; set; } = string.Empty;

        public List<RecognitionData> RecognitionDatas { get; set; } 
            = new List<RecognitionData>();
        #endregion

        #region METHODS
        public void GetRecognizedBarcode(JobTypes jobType)
        {
            if (jobType == JobTypes.Agreement)
            {
                ProductCategoryCode = Barcode[1];
                ContractorTypeCode = Barcode[2];
                BusinessNumber = Barcode[7];
                DocumentCategoryCode = Barcode[3];
                DocumentSubcategoryCode = Barcode[4];
                RecognitionTemplateVersion = Barcode[8];
                // result[5] -> agreement date.
                // result[6] -> planner
            }
            else
            {
                ProductCategoryCode = Barcode[1];
                ContractorTypeCode = Barcode[2];
                BusinessNumber = Barcode[3];
                DocumentCategoryCode = Barcode[4];
                DocumentSubcategoryCode = Barcode[5];
                RecognitionTemplateVersion = Barcode[6];
            }
        }

        public OcrData ShallowCopy()
        {
            return (OcrData)this.MemberwiseClone();
        }
        #endregion
    }
}
