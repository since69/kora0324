﻿//
// BnpParibars.Scan.UI.Argos.OcrJob.HNBankJob.
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System.Text;
using log4net;
using BnpParibars.Scan.IO;

namespace BnpParibars.Scan.UI.Argos.OcrJob
{
    /// <summary>
    /// 유입경로: BSL Server.
    /// 문서유형: 
    ///     청약서 - L78PFC /NB
    ///     상품설명서 - L78PFF/ NB 
    ///     청약철회신청서 - L78PFJ / POS
    /// 업무유형: Bancassurance and Pos
    /// 처리방법:
    ///     FTP 원격폴더에서 내려받은 파일의 파일 명을 문서유형 6자리, 
    ///     증번 10자리로 분해한 후 인식을 거치지 않고 케이스와 부가 
    ///     정보(문서/페이지)를 생성하여 전송.
    ///     파일명은 아래와 같은 형식을 가짐.
    ///     
    ///     문서유형(6)증권번호(10).pdf
    ///     L78PFC9000085677.PDF - 청약서
    ///     L78PFF9000085677.PDF - 상품설명서
    ///     L78PFJ9000085677.PDF - 중도해지환급금청구및영수증
    /// </summary>
    public class HNBankJob : Job
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("HnBankJob");
        private string documentCategoryCode = string.Empty;
        private string documentSubcategoryCode = string.Empty;
        private string businessNumber = string.Empty;
        #endregion

        #region CONSTRUCTORS
        public HNBankJob() : base(JobTypes.HNBank) { }
        #endregion

        #region OVERRIDE METHODS
        protected override bool Import()
        {
            return Download();
        }

        protected override bool Preprocessing(DownloadFile downloadFile)
        {
            logger.Debug(string.Format("{0} image preprocessing started.",
                Util.EnumHelper.GetDescription(JobType)));

            if (!VerifyFileName(downloadFile.NameWithoutExtension))
                return false;

            string code = downloadFile.NameWithoutExtension.Substring(0, 6);
            switch (code.ToUpper())
            {
                case "L78PFC":
                    documentCategoryCode = "S1";
                    documentSubcategoryCode = "0001";
                    break;
                case "L78PFE":
                    documentCategoryCode = "D1";
                    documentSubcategoryCode = "0032";
                    break;
                case "L78PFJ":
                    documentCategoryCode = "P0";
                    documentSubcategoryCode = "0060";
                    break;
                default:
                    documentCategoryCode = string.Empty;
                    documentSubcategoryCode = string.Empty;
                    logger.Error($"Unknown document code. {code}");
                    return false;
            }
            businessNumber = downloadFile.NameWithoutExtension.Substring(7);

            if (!ConvertPdf(downloadFile)) return false;
            ResizeImage();

            logger.Debug(string.Format("{0} image preprocessing done.",
                Util.EnumHelper.GetDescription(JobType)));

            return true;
        }

        protected override void Recognize(JobData jobData)
        {
            logger.Debug(string.Format("{0} image recognition started.",
                Util.EnumHelper.GetDescription(JobType)));

            foreach (ConvertFile f in ConvertFiles)
            {
                jobData.OcrDatas.Add(new OcrData()
                {
                    BusinessTypeCode = BusinessTypes.GetType(JobType),
                    ProductCategoryCode = ProductCategories.Unknown,
                    ContractorTypeCode = ContractorTypes.Unknown,
                    BusinessNumber = businessNumber,
                    DocumentCategoryCode = documentCategoryCode,
                    DocumentSubcategoryCode = documentSubcategoryCode,
                    ImageFileName = f.FullName
                });
            }

            logger.Debug(string.Format("{0} image recognition is done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void Classfication(JobData jobData)
        {
            logger.Debug(string.Format("{0} classfication started.",
                Util.EnumHelper.GetDescription(JobType)));

            CreateCase(BusinessTypes.GetType(JobType),
                ProductCategories.Unknown, ContractorTypes.Unknown,
                businessNumber, jobData);

            logger.Debug(string.Format("{0} classfication done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void UploadFiles(JobData jobData)
        {
            logger.Debug(string.Format("{0} Upload started.",
                Util.EnumHelper.GetDescription(JobType)));

            UploadOcrFiles(jobData);

            logger.Debug(string.Format("{0} Upload done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void GenerateData(JobData jobData)
        {
            GenerateDataForPDF(jobData,
                documentCategoryCode,
                documentSubcategoryCode);
        }

        protected override void Verify(JobData jobData)
        {
            logger.Debug(string.Format("{0} is not required verification.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void Export(JobData jobData)
        {
            logger.Debug(string.Format("{0} export started.",
                Util.EnumHelper.GetDescription(JobType)));

            UploadDataToWas(jobData);

            logger.Debug(string.Format("{0} export done.",
                Util.EnumHelper.GetDescription(JobType)));
        }
        #endregion

        #region PRIVATE METHODS
        private bool VerifyFileName(string filename)
        {
            StringBuilder sb = new StringBuilder();

            if (filename.Length != 16)
            {
                sb.Append("Invalid file name.");
                sb.AppendFormat("file name length is {0}", filename.Length);
                sb.Append("length must be 16");
                logger.Error(sb.ToString());
                return false;
            }

            return true;
        }
        #endregion
    }
}
