﻿//
// BnpParibars.Scan.UI.Argos.OcrJob.AgreementJob
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using log4net;
using BnpParibars.Scan.IO;
using BnpParibars.Scan.Data;
using BnpParibars.Scan.Data.BLL;
using System.Text;

namespace BnpParibars.Scan.UI.Argos.OcrJob
{
    /// <summary>
    /// 유입경로: WEBFAX
    /// 문서유형: 가입설계동의서
    /// 업무유형: 신계약(New Business)
    /// 처리방법:
    ///     특정번호(1004)에 해당하는 FTP 원격폴더(004)에 들어오는 문서는 설계동의서로
    ///     판단하며, QR 코드를 정상적으로 처리한 경우에는 인식을 통해 보완 코드를 생성
    ///     하고, 정상적으로 처리하지 못한 경우에는 미처리로 분류하고, 'M'으로 시작하는
    ///     9자리 동의번호를 생성하여 ECM 및 DB에 등록.
    ///      가입설계 동의서는 한장짜리 문서임으로 변환 및 분리 작업을 수행하지 않으며,
    ///     QR 및 인식 과정을 정상적으로 처리한 경우 보완 내용이 없으면 '000'으로, 보완
    ///     내용이 존재하는 경우 생성된 보완코드를 연결하여 GMS DB에 업데이트 해야 함.
    /// </summary>
    public class AgreementJob : Job
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("AgreementJob");
        #endregion

        #region CONSTRUCTORS
        public AgreementJob() : base(JobTypes.Agreement) { }
        #endregion

        #region OVERRIDE METHODS
        protected override bool Preprocessing(DownloadFile downloadFile)
        {
            logger.Debug(string.Format("{0} image preprocessing started.",
                Util.EnumHelper.GetDescription(JobType)));

            if (!ConvertTiff(downloadFile)) return false;
            ResizeImage();

            logger.Debug(string.Format("{0} image preprocessing done.",
                Util.EnumHelper.GetDescription(JobType)));

            return true;
        }

        protected override bool Import()
        {
            return Download();
        }

        protected override void Recognize(JobData jobData)
        {
            logger.Debug(string.Format("{0} image recognition started.",
                Util.EnumHelper.GetDescription(JobType)));

            RecognizeImages(jobData);

            logger.Debug(string.Format("{0} image recognition done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void Classfication(JobData jobData)
        {
            logger.Debug(string.Format("{0} classfication started.",
                Util.EnumHelper.GetDescription(JobType)));

            CreateCases(jobData);

            logger.Debug(string.Format("{0} classfication done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void UploadFiles(JobData jobData)
        {
            logger.Debug(string.Format("{0} Upload started.",
                Util.EnumHelper.GetDescription(JobType)));

            UploadOcrFiles(jobData);

            logger.Debug(string.Format("{0} Upload done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void GenerateData(JobData jobData)
        {
            foreach (Case c in jobData.Cases)
                CreateCaseDocumentAndPage(c, jobData);
        }

        protected override void Verify(JobData jobData)
        {
            logger.Debug(string.Format("{0} verify started.",
                Util.EnumHelper.GetDescription(JobType)));

            RecognitionVerifier.Verify(jobData);

            logger.Debug(string.Format("{0} verify done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void Export(JobData jobData)
        {
            logger.Debug(string.Format("{0} export started.",
                Util.EnumHelper.GetDescription(JobType)));

            UploadDataToWas(jobData);
            UpdateComplementStatus(jobData);

            logger.Debug(string.Format("{0} export done.",
                Util.EnumHelper.GetDescription(JobType)));
        }
        #endregion

        #region PRIVATE METHODS
        private void CreateCases(JobData jobData)
        {
            foreach (OcrData r in jobData.OcrDatas)
            {
                if (string.IsNullOrEmpty(r.BusinessNumber))
                {
                    r.BusinessNumber = GenerateBusinessNumber();
                    r.BusinessTypeCode = BusinessTypes.GetType(JobType);
                    r.ProductCategoryCode = ProductCategories.Unknown;
                    r.ContractorTypeCode = ContractorTypes.Unknown;
                }
                CreateCase(r.BusinessTypeCode, r.ProductCategoryCode,
                    r.ContractorTypeCode, r.BusinessNumber, jobData);
            }
        }

        private void CreateCaseDocumentAndPage(Case c, JobData jobData)
        {
            foreach (OcrData r in jobData.OcrDatas)
            {
                if (r.BusinessNumber.Equals(
                    c.caseBusinessNumberList[0].businessNumber))
                {
                    DocumentCategory category = DocCategories.Find("M0");
                    DocumentSubcategory subcategory = DocSubcategories.Find("0062");

                    CaseDocument doc = new CaseDocument()
                    {
                        documentCategoryId = category.documentCategoryId,
                        documentCategoryCode = category.code,
                        documentCategoryName = category.name,
                        documentCategorySortOrder = category.sortOrder,
                        documentSubcategoryId = subcategory.documentSubcategoryId,
                        documentSubcategoryCode = subcategory.code,
                        documentSubcategoryName = subcategory.title,
                        documentSubcategorySortOrder = subcategory.sortOrder
                    };
                    doc.caseDocumentPageList.Add(new CaseDocumentPage()
                    {
                        pageNumber = 1,
                        isEncrypted = 1,
                        imageFileId = r.ImageFileID,
                        annotationFileId = string.Empty
                    });

                    c.caseDocumentList.Add(doc);
                }
            }
        }

        /// <summary>
        /// Update case complement status to GSM.
        /// Status: normal - '000', others - complment code string.
        /// </summary>
        /// <param name="jobData"></param>
        private void UpdateComplementStatus(JobData jobData)
        {
            foreach (Case c in jobData.Cases)
            {
                string registrationNumber =
                    c.caseBusinessNumberList[0].businessNumber;

                if (registrationNumber.Length == 9)
                    continue;

                StringBuilder complements = new StringBuilder("000");
                if (c.caseSupplementList.Count > 0)
                {
                    complements.Clear();
                    for (int i = 0; i < c.caseSupplementList.Count; i++)
                    {
                        complements.Append(c.caseSupplementList[i].code);
                        if (i != c.caseSupplementList.Count - 1)
                            complements.Append("|");
                    }
                }

                bool retVal = Consent.UpdateComplementStatus(registrationNumber,
                    complements.ToString());
                if (!retVal)
                    logger.Error($"GSM update failed for {registrationNumber}.");
            }
        }
        #endregion
    }
}
