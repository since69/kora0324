﻿//
// BnpParibars.Scan.UI.Argos.OcrJob.ApplicationJob
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using log4net;
using BnpParibars.Scan.IO;
using BnpParibars.Scan.Data;

namespace BnpParibars.Scan.UI.Argos.OcrJob
{
    /// <summary>
    /// 유입경로: WEBFAX
    /// 문서유형: 청약서
    /// 업무유형: 신계약(New Business)
    /// 처리방법: 
    ///     특정번호(1002)에 해당하는 FTP 원격폴더(002)에 들어오는 문서는 청약서로
    ///     판단하며, QR 코드를 정상적으로 처리한 경우에는 인식을 통해 보완 코드를
    ///     생성하고 정상적으로 처리하지 못한 경우에는 다음 문서 유형이 나오기 전 
    ///     까지의 문서유형으로 분류하며 전체 파일을 모두 인식하지 못한 경우에는
    ///     미처리로 분류하고 'G'로 시작하는 10자리 가증번을 생성하여 ECM 및 DB에 등록.
    ///      QR 코드를 정상적으로 인식한 경우 BSL을 통해 해당 증번의 계약 상태를 
    ///     조회하고, 이미 등록된(승인 일시가 널이 아닌경우) 증번인 경우 파일 업로드를
    ///     포함한 모든 이후 절차를 무시하여 등록되지 않도록 해야함.
    /// </summary>
    public class ApplicationJob : Job
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("ApplicationJob");
        #endregion

        #region CONSTRUCTORS
        public ApplicationJob() : base(JobTypes.Application) { }
        #endregion

        #region OVERRIDE METHODS
        protected override bool Preprocessing(DownloadFile downloadFile)
        {
            logger.Debug(string.Format("{0} image preprocessing started.",
                Util.EnumHelper.GetDescription(JobType)));

            if (!ConvertTiff(downloadFile)) return false;
            ResizeImage();

            logger.Debug(string.Format("{0} image preprocessing done.",
                Util.EnumHelper.GetDescription(JobType)));

            return true;
        }

        protected override bool Import()
        {
            logger.Debug(string.Format("{0} image import started.",
                Util.EnumHelper.GetDescription(JobType)));

            bool isDownload = Download();

            logger.Debug(string.Format("{0} image import done.",
                Util.EnumHelper.GetDescription(JobType)));

            return isDownload;
        }
        
        protected override void Recognize(JobData jobData)
        {
            logger.Debug(string.Format("{0} image recognition started.",
                Util.EnumHelper.GetDescription(JobType)));

            RecognizeImages(jobData);

            logger.Debug(string.Format("{0} image recognition done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void Classfication(JobData jobData)
        {
            logger.Debug(string.Format("{0} classfication started.",
                Util.EnumHelper.GetDescription(JobType)));

            CreateCases(jobData);
            CreateSignatureDatas(jobData);

            logger.Debug(string.Format("{0} classfication done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void UploadFiles(JobData jobData)
        {
            logger.Debug(string.Format("{0} Upload started.",
                Util.EnumHelper.GetDescription(JobType)));

            UploadOcrFiles(jobData);
            UploadSignatureFiles(jobData);

            logger.Debug(string.Format("{0} Upload done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void GenerateData(JobData jobData)
        {
            foreach (Case c in jobData.Cases)
            {
                CaseBusinessNumber caseBsn = c.caseBusinessNumberList[0];
                if (caseBsn == null) continue;

                var caseResults = new List<OcrData>();
                foreach (OcrData record in jobData.OcrDatas)
                {
                    if (record.BusinessNumber.Equals(caseBsn.businessNumber))
                        caseResults.Add(record);
                }
                foreach (OcrData record in jobData.SignatureDatas)
                {
                    if (record.BusinessNumber.Equals(caseBsn.businessNumber))
                        caseResults.Add(record);
                }

                c.caseDocumentList = CreateCaseDocuments(caseResults, jobData);
                CreateCaseDocumentPages(c.caseDocumentList, caseResults);
            }
        }

        protected override void Verify(JobData jobData)
        {
            logger.Debug(string.Format("{0} verify started.",
                Util.EnumHelper.GetDescription(JobType)));

            RecognitionVerifier.Verify(jobData);

            logger.Debug(string.Format("{0} verify done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void Export(JobData jobData)
        {
            logger.Debug(string.Format("{0} export started.",
                Util.EnumHelper.GetDescription(JobType)));

            RecognitionVerifier.CheckMissingRequiredFiles(jobData);
            UploadDataToWas(jobData);

            logger.Debug(string.Format("{0} export done.",
                Util.EnumHelper.GetDescription(JobType)));
        }
        #endregion

        #region PRIVATE METHODS
        /// <summary>
        /// 증번을 검증하여 이미 등록된 증번이면 해당 케이스를 삭제하여
        /// ECM과 DB에 등록되지 않도록 한다.
        /// </summary>
        /// <param name="jobData"></param>
        private void RemoveAlreadyRegistered(JobData jobData)
        {
            var removeCases = new List<Case>();
            foreach (Case c in jobData.Cases)
            {
                CaseBusinessNumber bsn = c.caseBusinessNumberList[0];
                if (bsn == null)
                {
                    logger.Debug(string.Format(
                        "Can't find businessn number for {0} ",
                        Util.EnumHelper.GetDescription(JobType)));
                    continue;
                }
                if (string.IsNullOrEmpty(bsn.businessNumber))
                    continue;
                if (VerifyBusinessNumber(bsn.businessNumber)
                    == BsnVerifyResults.AlreadyRegistered)
                {
                    logger.Info($"{bsn.businessNumber} is already registered.");
                    removeCases.Add(c);
                }
            }
            jobData.Cases.RemoveAll(removeCases.Contains);
        }

        private void CreateSignatureDatas(JobData jobData)
        {
            foreach (OcrData ocr in jobData.OcrDatas)
            {
                foreach (RecognitionData rcg in ocr.RecognitionDatas)
                {
                    if (!rcg.Name.StartsWith("CUT_"))
                        continue;

                    string cropFilename = string.Format(
                        "{0}{1}-SIGNATURE.tif",
                        ConvertDir.Name,
                        Path.GetFileNameWithoutExtension(ocr.ImageFileName));
                    Rectangle rcArea = new Rectangle()
                    {
                        X = rcg.Left,
                        Y = rcg.Top,
                        Width = rcg.Right - rcg.Left,
                        Height = rcg.Bottom - rcg.Top
                    };

                    if (!BarcodeReader.Crop(ocr.ImageFileName, cropFilename, rcArea))
                    {
                        logger.Error("Handwrite signature crop failed.");
                    }
                    else
                    {
                        string[] parts = rcg.Name.Split('_');
                        OcrData signatureData = ocr.ShallowCopy();
                        signatureData.DocumentCategoryCode = parts[1];      // PT
                        signatureData.DocumentSubcategoryCode = parts[2];   // 0042
                        signatureData.ImageFileName = cropFilename;
                        jobData.SignatureDatas.Add(signatureData);
                    }
                    break;
                }
            }
        }

        private void CreateCases(JobData jobData)
        {
            string prevBsn = string.Empty;
            foreach (OcrData r in jobData.OcrDatas)
            {
                if (string.IsNullOrEmpty(r.BusinessNumber))
                    continue;

                if (!r.BusinessNumber.Equals(prevBsn))
                {
                    prevBsn = r.BusinessNumber;
                    CreateCase(r.BusinessTypeCode, r.ProductCategoryCode,
                        r.ContractorTypeCode, r.BusinessNumber, jobData);
                }
            }

            //if (jobData.OcrDatas.Count == 0)
            if (jobData.OcrDatas.Count == 0
                || (jobData.OcrDatas.Count == 1 
                    && jobData.OcrDatas[0].Barcode == null))
            {
                CreateCase(BusinessTypes.GetType(JobType),
                    ProductCategories.Unknown,
                    ContractorTypes.Unknown,
                    GenerateBusinessNumber(),
                    jobData);
            }
            else
                RemoveAlreadyRegistered(jobData);

            int startIndex = 0;
            foreach (Case c in jobData.Cases)
            {
                string caseBsn = c.caseBusinessNumberList[0].businessNumber;
                for (int i = startIndex; i < jobData.OcrDatas.Count; i++)
                {
                    OcrData r = jobData.OcrDatas[i];
                    if (!string.IsNullOrEmpty(r.BusinessNumber)
                        && !r.BusinessNumber.Equals(caseBsn))
                    {
                        startIndex = i;
                        break;
                    }
                    r.BusinessNumber = caseBsn;
                }
            }
        }

        private List<CaseDocument> CreateCaseDocuments(
            List<OcrData> ocrResults,
            JobData jobData)
        {
            List<CaseDocument> documents = new List<CaseDocument>();

            foreach (OcrData r in ocrResults)
            {
                DocumentCategory category = null;
                DocumentSubcategory subcategory = null;

                if (!string.IsNullOrEmpty(r.DocumentCategoryCode)
                    && !string.IsNullOrEmpty(r.DocumentSubcategoryCode))
                {
                    category = jobData.DocCategories.Find(
                        r.DocumentCategoryCode);
                    subcategory = jobData.DocSubcategories.Find(
                        r.DocumentSubcategoryCode);
                }

                if (category == null || subcategory == null)
                {
                    category = jobData.DocCategories.Find("OT");
                    subcategory = jobData.DocSubcategories.Find("0043");
                    r.DocumentCategoryCode = "OT";
                    r.DocumentSubcategoryCode = "0043";

                    StringBuilder sb = new StringBuilder("Can't find ");
                    sb.AppendFormat("category({0})", r.DocumentCategoryCode);
                    sb.AppendFormat("or subcategory({0}).", r.DocumentSubcategoryCode);
                    logger.Error(sb.ToString());
                }

                CaseDocument newCaseDoc = new CaseDocument()
                {
                    documentCategoryId = category.documentCategoryId,
                    documentCategoryCode = category.code,
                    documentCategoryName = category.name,
                    documentCategorySortOrder = category.sortOrder,
                    documentSubcategoryId = subcategory.documentSubcategoryId,
                    documentSubcategoryCode = subcategory.code,
                    documentSubcategoryName = subcategory.title,
                    documentSubcategorySortOrder = subcategory.sortOrder
                };

                if (!jobData.CaseDocumentExists(newCaseDoc.documentCategoryId,
                    newCaseDoc.documentSubcategoryId, documents))
                    documents.Add(newCaseDoc);
            }
            return documents;
        }

        private void CreateCaseDocumentPages(
            List<CaseDocument> documents,
            List<OcrData> ocrResults)
        {
            foreach (CaseDocument d in documents)
            {
                int pageNo = 1;
                foreach (OcrData r in ocrResults)
                {
                    if (d.documentCategoryCode.Equals(r.DocumentCategoryCode)
                        && d.documentSubcategoryCode.Equals(r.DocumentSubcategoryCode))
                    {
                        d.caseDocumentPageList.Add(new CaseDocumentPage()
                        {
                            pageNumber = pageNo++,
                            isEncrypted = 1,
                            imageFileId = r.ImageFileID,
                            annotationFileId = string.Empty
                        });
                    }
                }
            }
        }
        #endregion
    }
}
