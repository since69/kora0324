﻿//
// BnpParibars.Scan.UI.Argos.OcrJob.JobManager
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System.Linq;
using System.Text.Json;
using log4net;
using BnpParibars.Scan.Net.Ecm;
using BnpParibars.Scan.Ocr;
using BnpParibars.Scan.Data;
using System;

namespace BnpParibars.Scan.UI.Argos.OcrJob
{
    public class JobManager
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("JobManager");
        private readonly AppContext appContext;
        private readonly JobFactory factory;
        private readonly Jobs jobs;
        private readonly DocCategories docCategories;
        private readonly DocSubcategories docSubcategories;
        private readonly MandatoryDocs mandatoryDocs;
        private readonly RecogTemplates recogTemplates;
        private readonly Supplements supplements;
        private DateTime templateLastDownloadAt = DateTime.MinValue;

        protected EcmRequest ecm;
        protected ImageRecognizer recognizer;
        protected BarcodeReader barcodeReader;
        #endregion

        #region PROPERTIES
        public bool IsWorking { get; set; } = false;
        public bool IsWorkable { get; set; } = true;
        #endregion

        #region CONSTRUCTORS
        public JobManager(AppContext appContext)
        {
            this.appContext = appContext;
            factory = new JobFactory();
            jobs = new Jobs();
            ecm = new EcmRequest();
            recognizer = new ImageRecognizer();
            barcodeReader = new BarcodeReader();
            docCategories = new DocCategories();
            docSubcategories = new DocSubcategories();
            mandatoryDocs = new MandatoryDocs();
            recogTemplates = new RecogTemplates();
            supplements = new Supplements();
        }
        #endregion

        #region PRIVATE METHODS
        private Job CreateJob(JobTypes jobType)
        {
            Job job = factory.CreateJob(jobType);
            if (job != null)
            {
                job.AppContext = appContext;
                job.DocCategories = docCategories;
                job.DocSubcategories = docSubcategories;
                job.MandatoryDocs = mandatoryDocs;
                job.RecogTemplates = recogTemplates;
                job.Supplements = supplements;
                job.Ecm = ecm;
                job.EcmProperty = appContext.EcmProperty;
                job.Recognizer = recognizer;
                job.BarcodeReader = barcodeReader;
                job.ConvertDir = appContext.ConvertDir;
                job.DownloadDir = appContext.DownloadDir;
                job.TemplateDir = appContext.TemplateDir;
                job.FailureDir = appContext.FailureDir;
                job.InitializeJob();
            }
            return job;
        }

        private bool CreateJobs()
        {
            // DEBUG MODE
            jobs.Add(CreateJob(JobTypes.Application));
            //jobs.Add(CreateJob(JobTypes.Agreement));
            //jobs.Add(CreateJob(JobTypes.Payment));
            //jobs.Add(CreateJob(JobTypes.Benefit));
            //jobs.Add(CreateJob(JobTypes.Digital));
            //jobs.Add(CreateJob(JobTypes.KBank));
            //jobs.Add(CreateJob(JobTypes.HNBank));
            //jobs.Add(CreateJob(JobTypes.NHBank));
            //jobs.Add(CreateJob(JobTypes.SCBank));
            //jobs.Add(CreateJob(JobTypes.WRBank));
            //jobs.Add(CreateJob(JobTypes.CHBank));

            // RELEASE MODE
            //for (int i = 0; i < Enum.GetNames(typeof(JobTypes)).Length; i++)
            //{
            //    // Ignore the unknown job type
            //    if (i == 0) continue;

            //    Job job = CreateJob((JobTypes)i);
            //    if (job != null) jobs.Add(job);
            //}

            return jobs.Count > 0;
        }

        private bool GetDataFromDB(ServletTypes servletType)
        {
            string url = appContext.EcmProperty.GetServletUrl(servletType);
            if (string.IsNullOrEmpty(url))
            {
                logger.Error("Can't find url.");
                return false;
            }
            if (!ecm.SendRequest(url, out string rs)) 
                return false;

            JsonSerializerOptions serializerOptions = new JsonSerializerOptions()
            {
                WriteIndented = true,
                PropertyNameCaseInsensitive = true
            };

            switch (servletType)
            {
                case ServletTypes.InquireDocCategory:
                    InquiryCategoryRsp rspDc = JsonSerializer
                        .Deserialize<InquiryCategoryRsp>(rs, serializerOptions);
                    docCategories.CopyFrom(rspDc.ResultObj.ToList());
                    break;
                case ServletTypes.InquireDscCategory:
                    InquirySubcategoryRsp rspDs = JsonSerializer
                        .Deserialize<InquirySubcategoryRsp>(rs, serializerOptions);
                    docSubcategories.CopyFrom(rspDs.ResultObj.ToList());
                    break;
                case ServletTypes.InquireMdtDocument:
                    InquiryMandatoryRsp rspMd = JsonSerializer
                        .Deserialize<InquiryMandatoryRsp>(rs, serializerOptions);
                    mandatoryDocs.CopyFrom(rspMd.ResultObj.ToList());
                    break;
                case ServletTypes.InqueryRcgTemplate:
                    InquiryTemplateRsp rspTe = JsonSerializer
                        .Deserialize<InquiryTemplateRsp>(rs, serializerOptions);
                    recogTemplates.CopyFrom(rspTe.ResultObj.ToList());
                    break;
                case ServletTypes.InquerySupplements:
                    InquirySupplementRsp rspSp = JsonSerializer
                        .Deserialize<InquirySupplementRsp>(rs, serializerOptions);
                    supplements.CopyFrom(rspSp.ResultObj.ToList());
                    break;
                default:
                    logger.Error("Can't find servlet type.");
                    return false;
            }

            return true;
        }

        private bool InitailizeData()
        {
            if (!GetDataFromDB(ServletTypes.InquireDocCategory))
                return false;
            if (!GetDataFromDB(ServletTypes.InquireDscCategory))
                return false;
            if (!GetDataFromDB(ServletTypes.InquireMdtDocument))
                return false;
            if (!GetDataFromDB(ServletTypes.InqueryRcgTemplate))
                return false;
            if (!GetDataFromDB(ServletTypes.InquerySupplements))
                return false;
            return true;
        }
        #endregion

        #region PUBLIC METHODS
        public bool Initialize()
        {
            //IsWorkable = CreateJobs();

            /// OPERATION
            IsWorkable = InitailizeData();
            if (IsWorkable)
            {
                logger.Info("Image recognizer initialized.");
                IsWorkable = CreateJobs();
            }
            if (IsWorkable)
                logger.Info("Image recognition job initialized.");

            return IsWorkable;
        }

        public void DoRun()
        {
            if (!IsWorkable) return;
            if (templateLastDownloadAt == DateTime.MinValue
                || DateTime.Now.Day - templateLastDownloadAt.Day >= 1)
            {
                DownloadTemplates();
                templateLastDownloadAt = DateTime.Now;
            }
            jobs.GetCurrentJob().Run();
            //jobs.ChangeCurrentJob();
        }
        #endregion

        #region PRIVATE METHODS
        public void DownloadTemplates()
        {
            string remoteUrl = appContext.EcmProperty.GetServletUrl(
                ServletTypes.FileDownload);

            string[] templateFiles = appContext.TemplateDir.GetFiles();
            foreach (RecognitionTemplate template in recogTemplates)
            {
                DocumentCategory category = docCategories
                    .Find(template.DocumentCategoryID);
                DocumentSubcategory subcategory = docSubcategories
                    .Find(template.DocumentSubcategoryID);

                if (category == null)
                {
                    logger.Error(string.Format(
                        "Can't find document category with id({0}).",
                        template.DocumentCategoryID));
                    continue;
                }
                if (subcategory == null)
                {
                    logger.Error(string.Format(
                        "Can't find document subcategory with id({0}).",
                        template.DocumentCategoryID));
                    continue;
                }

                string templateFile = category.code 
                    + subcategory.code
                    + template.TemplateVersion;
                if (!Array.Exists(templateFiles, 
                    element => element.Contains(templateFile)))
                {
                    DownloadTemplate(remoteUrl, 
                        template.TemplateFileID, 
                        templateFile + ".tif");
                    DownloadTemplate(remoteUrl, 
                        template.ZoneDefinitionFileID, 
                        templateFile + ".ini");
                }
            }
        }

        private bool DownloadTemplate(string remoteUrl, string fileId, string filename)
        {
            try
            {
                System.Net.WebClient client = new System.Net.WebClient();
                client.Headers.Add("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0;");
                remoteUrl = string.Format("{0}?fileId={1}", remoteUrl, fileId);
                client.DownloadFile(remoteUrl,
                    System.IO.Path.Combine(appContext.TemplateDir.Name, filename));
            }
            catch (System.Net.WebException ex)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.AppendFormat("template {0} download failed. {1} / {2}",
                    filename,
                    ((System.Net.HttpWebResponse)ex.Response).StatusCode,
                    ((System.Net.HttpWebResponse)ex.Response).StatusDescription);
                logger.Error(sb.ToString());
                return false;
            }
            catch (Exception ex)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.AppendFormat("template {0} download failed. {1}",
                    filename, ex.Message);
                return false;
            }
            return true;
        }
        #endregion
    }
}
