﻿//
// BnpParibars.Scan.UI.Argos.OcrJob.Job
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020 Thinkbox Co. Ltd
//

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.Json;
using log4net;
using BnpParibars.Scan.Data;
using BnpParibars.Scan.IO;
using BnpParibars.Scan.Net;
using BnpParibars.Scan.Net.Ecm;
using BnpParibars.Scan.Ocr;
using BnpParibars.Scan.Util;

namespace BnpParibars.Scan.UI.Argos.OcrJob
{
    public abstract class Job
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("BaseJob");
        private FtpClient ftpClient = null;
        private FtpProperty ftpProperty = null;
        #endregion

        #region PROPERTIES
        protected AppContext appContext = null;
        public AppContext AppContext { set { appContext = value; } }
        public JobTypes JobType { get; set; } = JobTypes.Application;

        public DocCategories DocCategories { get; set; }
        public DocSubcategories DocSubcategories { get; set; }
        public MandatoryDocs MandatoryDocs { get; set; }
        public RecogTemplates RecogTemplates { get; set; }
        public Supplements Supplements { get; set; }

        public EcmRequest Ecm { get; set; } = null;
        public EcmProperty EcmProperty { get; set; } = null;
        public ImageRecognizer Recognizer { get; set; } = null;
        public BarcodeReader BarcodeReader { get; set; } = null;

        public DownloadDir DownloadDir { get; set; } = null;
        public ConvertDir ConvertDir { get; set; } = null;
        public FailureDir FailureDir { get; set; } = null;
        public TemplateDir TemplateDir { get; set; } = null;
        public List<DownloadFile> DownloadFiles { get; set; } = null;
        public List<ConvertFile> ConvertFiles { get; set; } = null;
        #endregion

        #region CONSTRUCTORS
        public Job(JobTypes jobType) 
        {
            JobType = jobType;
        }
        #endregion

        #region ABSTRACT METHODS
        protected abstract bool Import();
        protected abstract bool Preprocessing(DownloadFile file);
        protected abstract void Recognize(JobData jobData);
        protected abstract void Classfication(JobData jobData);
        protected abstract void UploadFiles(JobData jobData);
        protected abstract void GenerateData(JobData jobData);
        protected abstract void Verify(JobData jobData);
        protected abstract void Export(JobData jobData);
        #endregion

        #region PUBLIC METHODS
        public void InitializeJob()
        {
            DownloadFiles = new List<DownloadFile>();
            ConvertFiles = new List<ConvertFile>();
            ftpProperty = appContext.FtpProperties.Find(JobType);
            ftpClient = CreateFtpClient();
        }

        /// <summary>
        /// Use the following procedures to perform image recognition.
        /// --------------------------------------------------------------------
        /// IMPORT          | Download 
        /// (Images)        |   - from ftp server
        ///                 |   - download the file(s) to a specfic folder.
        ///                 |   - add download files to list.
        /// --------------------------------------------------------------------
        /// PREPROCESSING   | Convert (include seperation)
        /// --------------------------------------------------------------------
        /// RECOGNIZE       | ReadQrCode
        /// (Data)          | ImageRecognition
        /// --------------------------------------------------------------------
        /// CLASSFICATION   | CreateCases
        ///                 |   CreateCase
        ///                 |       RemoveAlreadyRegistered (Optional by job type)
        ///                 |   CreateSignatureDatas (Optional by job type)
        ///                 | UploadFiles
        ///                 |   UploadOcrFiles
        ///                 |       UploadFilesToECM
        ///                 |   UploadSignatureFiles (Optional by job type)
        ///                 |       UploadFilesToECM
        ///                 | ExtractData
        ///                 |   GenerateBusinessNumber
        ///                 |   CreateCaseDocuments
        ///                 |   CreateCaseDocumentPages
        /// --------------------------------------------------------------------
        /// VERIFY          | RecognitionVerifier.Verify
        /// (Verified Data) | RecognitionVerifire.CheckMissingRequiredDoc
        /// --------------------------------------------------------------------
        /// EXPORT          | UploadDataToWAS
        /// --------------------------------------------------------------------
        /// CLEANUP         | ConvertFiles.Clear
        ///                 | ConvertDir.DeleteAll
        /// --------------------------------------------------------------------
        /// </summary>
        /// <param name="jobData"></param>
        /// <returns></returns>
        public virtual void Run()
        {
            DownloadFiles.Clear();
            DownloadDir.DeleteAll();

            if (!Import()) return;

            foreach (DownloadFile downloadFile in DownloadFiles)
            {
                logger.Debug($"{downloadFile.Name} OCR started.");

                ConvertFiles.Clear();
                ConvertDir.DeleteAll();

                JobData jobData = new JobData(DocCategories, DocSubcategories,
                    MandatoryDocs, RecogTemplates, Supplements);

                if (!Preprocessing(downloadFile))
                {
                    downloadFile.Copy(System.IO.Path.Combine(
                        FailureDir.Name, downloadFile.Name));
                    appContext.NumOfFailed += 1;
                    continue;
                }

                Recognize(jobData);
                Classfication(jobData);
                if (jobData.Cases.Count > 0)
                {
                    UploadFiles(jobData);
                    GenerateData(jobData);
                    Verify(jobData);
                    Export(jobData);
                    logger.Debug(jobData);
                }
                else
                {
                    downloadFile.Copy(System.IO.Path.Combine(
                        FailureDir.Name, downloadFile.Name));
                    appContext.NumOfFailed += 1;
                    logger.Info($"{downloadFile.Name} upload data is not exists.");
                }

                appContext.NumOfProcessed += 1;
                logger.Debug($"{downloadFile.Name} OCR finished.");
            }

            DownloadFiles.Clear();
            DownloadDir.DeleteAll();

            logger.Debug($"{EnumHelper.GetDescription(JobType)} Completed.");
        }
        #endregion

        #region PROTECTED METHODS
        protected virtual bool ConvertPdf(DownloadFile downloadFile)
        {
            logger.Debug(string.Format("{0} - {1} conversion started.",
                EnumHelper.GetDescription(JobType),
                downloadFile.Name));

            ImageFormats format = InziImageConverter
                .GetImageFormat(downloadFile.FullName);
            if (format != ImageFormats.Pdf)
            {
                logger.Error("Invalid image format. image is not PDF.");
                return false;
            }

            //int numOfPages = InziImageConverter
            //    .GetNumOfPages(downloadFile.FullName, ImageFormats.Pdf);
            int numOfPages = BarcodeReader.GetPdfPageCount(downloadFile.FullName);
            if (numOfPages == 0)
            {
                logger.Error("Can't get page count of PDF.");
                return false;
            }

            for (int i = 0; i < numOfPages; i++)
            {
                ConvertFile convertFile = new ConvertFile(ConvertDir,
                    GenerateFileName(i + 1, "tif"));
                //if (!InziImageConverter.ConvertPDF(downloadFile.FullName,
                //    convertFile.Dir.Name, convertFile.Name,
                //    ImageFormats.Tiff, i))
                //    return false;
                if (!BarcodeReader.ConvertPDF(downloadFile.FullName,
                    convertFile.Dir.Name, convertFile.Name,
                    ImageFormats.Tiff, i))
                    return false;
                // ConvertPDF에서 변형이 일어나면 필요없음.
                Recognizer.ChangeImageMode(downloadFile.FullName, false);
                ConvertFiles.Add(convertFile);
            }

            return true;
        }

        protected virtual bool ConvertTiff(DownloadFile downloadFile)
        {
            logger.Debug(string.Format("{0} - {1} conversion started.",
                EnumHelper.GetDescription(JobType),
                downloadFile.Name));

            ImageFormats format = InziImageConverter
                .GetImageFormat(downloadFile.FullName);
            if (format != ImageFormats.Tiff)
            {
                logger.Error("Invalid image format. image is not TIFF.");
                return false;
            }

            ConvertFile cvFile;
            //int numOfPages = InziImageConverter.GetNumOfPages(
            //    downloadFile.FullName, format);
            int numOfPages = BarcodeReader.GetTiffPageCount(downloadFile.FullName);
            logger.Debug($"{downloadFile.Name} number of page is {numOfPages}");
            switch (numOfPages)
            {
                case 0:
                    logger.Error("Can't get page count of TIFF.");
                    return false;
                case 1:
                    cvFile = new ConvertFile(ConvertDir, downloadFile.Name);
                    ConvertFiles.Add(cvFile);
                    downloadFile.Copy(cvFile.FullName);
                    break;
                default:
                    for (short i = 0; i < numOfPages; i++)
                    {
                        cvFile = new ConvertFile(ConvertDir,
                            GenerateFileName(i + 1, "tif"));
                        //if (!InziImageConverter.ExtractTiff(downloadFile.FullName,
                        //    cvFile.FullName, i + 1))
                        //    return false;
                        if (!BarcodeReader.ExtractTiff(downloadFile.FullName,
                            cvFile.FullName, i))
                            return false;
                        ConvertFiles.Add(cvFile);
                    }
                    break;
            }
            return true;
        }

        protected virtual void CreateCase(string bsnTypeCode,
            string prdCategoryCode,
            string ctrTypeCode,
            string bsnNumber,
            JobData jobData)
        {
            Case newCase = new Case()
            {
                businessTypeCode = bsnTypeCode,
                productCategoryCode = prdCategoryCode,
                contractorTypeCode = ctrTypeCode,
                funnel = Funnels.GetFunnel(JobType),
                status = CaseStatus.GetStatus(JobType),
            };
            newCase.caseBusinessNumberList.Add(new CaseBusinessNumber()
            {
                businessNumberType = BusinessNumberType.GetType(JobType),
                businessNumber = bsnNumber,
                archiveStatus = ArchiveStatus.NotArchived
            });

            jobData.Cases.Add(newCase);
        }

        protected virtual string GenerateBusinessNumber()
        {
            switch (JobType)
            {
                case JobTypes.Agreement:
                    return string.Format("M{0:d8}",
                        (DateTime.Now.Ticks / 10) % 1000000000);
                case JobTypes.Application:
                    return string.Format("G{0:d9}",
                        (DateTime.Now.Ticks / 10) % 1000000000);
                case JobTypes.Benefit:
                    return string.Format("C{0:d12}",
                        (DateTime.Now.Ticks / 10) % 1000000000);
                case JobTypes.Payment:
                    return string.Format("P{0:d9}",
                        (DateTime.Now.Ticks / 10) % 1000000000);
                default:
                    logger.Error("JobType is invalid.");
                    return string.Empty;
            }
        }

        protected void GenerateDataForPDF(JobData jobData,
            string docCategoryCode,
            string docSubcategoryCode)
        {
            if (jobData.Cases[0] == null)
            {
                logger.Error("Can't find any case.");
                return;
            }

            DocumentCategory category =
                jobData.DocCategories.Find(docCategoryCode);
            DocumentSubcategory subcategory =
                jobData.DocSubcategories.Find(docSubcategoryCode);
            CaseDocument newDoc = new CaseDocument()
            {
                documentCategoryId = category.documentCategoryId,
                documentCategoryCode = category.code,
                documentCategoryName = category.name,
                documentCategorySortOrder = category.sortOrder,
                documentSubcategoryId = subcategory.documentSubcategoryId,
                documentSubcategoryCode = subcategory.code,
                documentSubcategoryName = subcategory.title,
                documentSubcategorySortOrder = subcategory.sortOrder
            };

            int pageNo = 1;
            foreach (OcrData r in jobData.OcrDatas)
            {
                newDoc.caseDocumentPageList.Add(new CaseDocumentPage()
                {
                    pageNumber = pageNo++,
                    isEncrypted = 1,
                    imageFileId = r.ImageFileID,
                    annotationFileId = string.Empty
                });
            }
            jobData.Cases[0].caseDocumentList.Add(newDoc);
        }

        /// <summary>
        /// 청약서(신계약)인 경우 XO 서버에서 계약 정보를 조회하여 이미 등록된 
        /// 계약인지를 확인하여 결과를 돌려준다.
        /// </summary>
        /// <param name="businessNumber">조회대상 업무번호</param>
        /// <returns></returns>
        protected virtual BsnVerifyResults VerifyBusinessNumber(string businessNumber)
        {
            string url = EcmProperty.GetServletUrl(
                ServletTypes.InqueryCotInformation);

            if (string.IsNullOrEmpty(url))
            {
                logger.Error("Can't find url.");
                return BsnVerifyResults.Invalid;
            }

            JsonSerializerOptions serializerOptions = new JsonSerializerOptions()
            {
                WriteIndented = true,
                PropertyNameCaseInsensitive = true
            };
            var dic = new Dictionary<string, string>
            {
                { "cont_no", businessNumber }
            };
            string postData = JsonSerializer.Serialize(dic, serializerOptions);
            if (!Ecm.SendRequestWithJson(url, postData, out string rs))
                return BsnVerifyResults.Invalid;

            InquiryContractRsp rsp = JsonSerializer
                .Deserialize<InquiryContractRsp>(rs, serializerOptions);
            if (rsp.ResultCode < 0)
            {
                logger.Error("Can't get contract information");
                return BsnVerifyResults.Invalid;
            }

            if (!string.IsNullOrEmpty(rsp.ResultObj.Accept_Date)
                && !rsp.ResultObj.Accept_Date.Equals("00000000"))
                return BsnVerifyResults.AlreadyRegistered;

            return BsnVerifyResults.Valid;
        }

        protected virtual bool Download()
        {
            logger.Debug(string.Format("{0} job image download started.",
                EnumHelper.GetDescription(JobType)));

            if (ftpClient == null)
            {
                logger.Error("FTP Client is not available.");
                return false;
            }

            string remoteDir = GetFtpDir();
            if (!ftpClient.ListFilesOnServer(remoteDir, 
                out string[] files, 
                IsEnableSsl()))
                return false;
            if (files.Length == 0)
            {
                logger.Info("Remote folder is empty.");
                return false;
            }

            int numOfDownloads = JobType == JobTypes.Agreement
                ? ftpProperty.NumOfDownloads * 2
                : ftpProperty.NumOfDownloads;
            numOfDownloads = files.Length < numOfDownloads 
                ? files.Length 
                : numOfDownloads;
            logger.Debug($"number of download files is {numOfDownloads}");
            for (int i = 0; i < numOfDownloads; i++)
            {
                if (files[i].Contains("/"))
                    files[i] = files[i].Substring(files[i].LastIndexOf('/') + 1);
                DownloadFiles.Add(new DownloadFile(DownloadDir, files[i]));
            }

            foreach (DownloadFile file in DownloadFiles)
            {
                string msg = string.Format("{0} downloading".PadRight(10, '-'), file.Name);
                string filename = string.Format("{0}/{1}", remoteDir, file.Name);
                if (!ftpClient.DownloadFile(file.FullName, filename, IsEnableSsl()))
                {
                    file.Status = false;
                    msg += "Failed.";
                }
                else
                {
                    ftpClient.DeleteFile(filename, IsEnableSsl());
                    file.Status = true;
                    msg += "Succeed.";
                }
                logger.Info(msg);
            }

            logger.Debug(string.Format("{0} job image download done.",
                EnumHelper.GetDescription(JobType)));

            return true;
        }

        protected virtual bool RecognizeImage(ConvertFile image, OcrData ocrData)
        {
            if (ocrData.Barcode == null) return false;
            ocrData.GetRecognizedBarcode(JobType);

            string templateName = string.Format("{0}{1}{2}",
                ocrData.DocumentCategoryCode, ocrData.DocumentSubcategoryCode,
                ocrData.RecognitionTemplateVersion);
            if (!Recognizer.LoadTemplate(TemplateDir.Name, 
                templateName, image.FullName))
                return false;
            return Recognizer.Recognize(ocrData.RecognitionDatas);
        }

        protected virtual void RecognizeImages(JobData jobData)
        {
            foreach (ConvertFile f in ConvertFiles)
            {
                Barcode barcode = ReadBarcode(f.FullName);
                if (barcode == null) 
                    barcode = ReadBarcode(f.FullName, true);
                else
                {
                    if (barcode.Top > 280)
                    {
                        logger.Debug("This image is flipped. rotate 180 degrees.");
                        if (!BarcodeReader.RotateImage(f.FullName))
                            logger.Error("rotate image failed.");
                    }
                }

                string rawBarcode;
                string[] parsedBarcode;
                if (barcode == null)
                {
                    rawBarcode = string.Empty;
                    parsedBarcode = null;
                    logger.Error("Valid barcode is not exists.");
                }
                else
                {
                    rawBarcode = barcode.Code;
                    parsedBarcode = ParsingBarcode(barcode.Code);
                    logger.Error($"valid barcode is {barcode.Code}.");
                }

                OcrData data = new OcrData()
                {
                    RawData = rawBarcode,
                    Barcode = parsedBarcode,
                    BusinessTypeCode = BusinessTypes.GetType(JobType),
                    BusinessNumber = string.Empty,
                    ImageFileName = f.FullName,
                };
                jobData.OcrDatas.Add(data);

                if (data.Barcode != null)
                    data.IsRecognized = RecognizeImage(f, data);
            }
        }

        /// <summary>
        /// Resize fax image to A4 size (200DPI).
        /// </summary>
        protected virtual void ResizeImage()
        {
            foreach (ConvertFile f in ConvertFiles)
                BarcodeReader.ResizeImage(f.FullName, 1652, 2338);
        }

        protected virtual void UploadOcrFiles(JobData jobData)
        {
            if (jobData.OcrDatas.Count <= 0)
            {
                logger.Error("No ocr files to transfer.");
                return;
            }

            string[] files = new string[jobData.OcrDatas.Count];
            for (int i = 0; i < jobData.OcrDatas.Count; i++)
                files[i] = jobData.OcrDatas[i].ImageFileName;
            UploadResponse rsp = UploadFilesToECM(files);
            if (rsp == null) return;

            foreach (KeyValuePair<string, string> entry in rsp.ResultObj)
            {
                OcrData ocrData = jobData.OcrDatas.Find(entry.Key.ToString());
                if (ocrData != null)
                {
                    ocrData.ImageFileID = entry.Value.ToString();
                    ocrData.IsUploaded = !string.IsNullOrEmpty(ocrData.ImageFileID);
                }
                else
                    logger.Error($"Can't find {entry.Key} in ocr result list.");
            };
        }

        protected virtual void UploadSignatureFiles(JobData jobData)
        {
            if (jobData.SignatureDatas.Count <= 0)
            {
                logger.Info("No signature files to transfer.");
                return;
            }

            string[] files = new string[jobData.SignatureDatas.Count];
            for (int i = 0; i < jobData.SignatureDatas.Count; i++)
                files[i] = jobData.SignatureDatas[i].ImageFileName;

            UploadResponse rsp = UploadFilesToECM(files);
            if (rsp == null) return;

            foreach (KeyValuePair<string, string> entry in rsp.ResultObj)
            {
                OcrData ocrData = jobData.SignatureDatas.Find(entry.Key.ToString());
                if (ocrData != null)
                {
                    ocrData.ImageFileID = entry.Value.ToString();
                    ocrData.IsUploaded = !string.IsNullOrEmpty(ocrData.ImageFileID);
                }
                else
                    logger.Error($"Can't find {entry.Key} in ocr result list.");
            };
        }

        protected virtual bool UploadDataToWas(JobData jobData)
        {
            string url = EcmProperty.GetServletUrl(
                ServletTypes.InsertCase);

            if (string.IsNullOrEmpty(url))
            {
                logger.Error("Can't find url.");
                return false;
            }

            JsonSerializerOptions serializerOptions = new JsonSerializerOptions()
            {
                WriteIndented = true,
                PropertyNameCaseInsensitive = true
            };
            string jsonData = JsonSerializer.Serialize(jobData.Cases, serializerOptions);
            logger.Debug($"UPLOAD JSON STRING:\n{jsonData}\n");

            if (!Ecm.SendRequestWithJson(url, jsonData, out string rs))
                return false;

            BaseResponse rsp = JsonSerializer
                .Deserialize<BaseResponse>(rs, serializerOptions);
            if (rsp.ResultCode < 0)
            {
                logger.Error("Can't update case data.");
                return false;
            }

            return true;
        }
        #endregion

        #region PRIVATE METHODS
        private FtpClient CreateFtpClient()
        {
            if (ftpProperty != null)
            {
                return new FtpClient(ftpProperty.Ip, 
                    ftpProperty.Port, ftpProperty.UserName, 
                    ftpProperty.Password);
            }

            logger.Error("Can't create ftp client.");
            return null;
        }

        private string GetFtpDir()
        {
            StringBuilder dir = new StringBuilder(
                ftpProperty.GetRemoteDir(JobType));

            switch (JobType)
            {
                case JobTypes.KBank:
                case JobTypes.HNBank:
                case JobTypes.NHBank:
                case JobTypes.WRBank:
                    dir.AppendFormat("/{0}",
                        DateTime.Now.AddDays(-1).ToString("yyyyMMdd", 
                            CultureInfo.GetCultureInfo("ko-KR")));
                    break;
                case JobTypes.CHBank:
                case JobTypes.SCBank:
                    dir.AppendFormat("/{0}",
                        DateTime.Now.ToString("yyyyMMdd",
                            CultureInfo.GetCultureInfo("ko-KR")));
                    break;
                default: break;
            }
            return dir.ToString();
        }

        /// <summary>
        /// Generate file name by date and time.
        /// https://forums.asp.net/t/1170804.aspx?Generate+unique+number+in+C+
        /// </summary>
        /// <returns></returns>
        private string GenerateFileName(int seq, string fileExtension)
        {
            return string.Format(StringTemplate.DateTimeFileName,
                DateTime.Now.ToString("yyyyMMddHHmmss"),
                seq, fileExtension);
        }

        private string[] ParsingBarcode(string rawData)
        {
            if (string.IsNullOrEmpty(rawData)) return null;

            string[] barcode = rawData.Split('_');
            if (JobType == JobTypes.Application && barcode.Length == 7
                || JobType == JobTypes.Agreement && barcode.Length == 9)
                return barcode;
            else
                logger.Error("The length of the splitted barcode is not 7 or 9.");

            return null;
        }

        private bool GetValidBarcode(Barcode barcode)
        {
            if (barcode == null)
            {
                logger.Error("barcode is nll.");
                return false;
            }
            if (string.IsNullOrEmpty(barcode.Code))
            {
                logger.Error("Barcode is empty or null.");
                return false;
            }

            switch (JobType)
            {
                case JobTypes.Agreement:
                    if (barcode.Code.Split('_').Length != 9)
                    {
                        logger.Error(string.Format(
                            "{0} splitted array length is not 9.",
                            EnumHelper.GetDescription(JobType)));
                        return false;
                    }
                    break;
                case JobTypes.Application:
                    if (barcode.Code.Split('_').Length != 7)
                    {
                        logger.Error(string.Format(
                            "{0} splitted array length is not 7.",
                            EnumHelper.GetDescription(JobType)));
                        return false;
                    }
                    break;
                default:
                    logger.Error(string.Format("{0} is invalid jobtype.",
                        EnumHelper.GetDescription(JobType)));
                    return false;
            }
            return true;
        }

        private bool IsEnableSsl()
        {
            switch (JobType)
            {
                case JobTypes.Agreement:
                case JobTypes.Application:
                case JobTypes.Payment:
                case JobTypes.Benefit:
                case JobTypes.Digital:
                    return true;
                default: return false;
            }
        }

        private Barcode ReadBarcode(string filename, bool removeNoise = false)
        {
            List<Barcode> barcodes = BarcodeReader.Read(filename, removeNoise);
            if (barcodes != null)
            {
                foreach (Barcode code in barcodes)
                {
                    if (GetValidBarcode(code))
                        return code;
                }
            }
            return null;
        }

        private UploadResponse UploadFilesToECM(string[] files)
        {
            string url = EcmProperty.GetServletUrl(ServletTypes.FileUpload);
            if (!Ecm.UploadRequest(url, string.Empty, files, out string rs))
            {
                logger.Error("Image file upload failed.");
                return null;
            }

            JsonSerializerOptions serializerOptions = new JsonSerializerOptions()
            {
                WriteIndented = true,
                PropertyNameCaseInsensitive = true
            };

            // parsing upload response (json format).
            UploadResponse rsp = JsonSerializer.Deserialize<UploadResponse>(
                rs, serializerOptions);
            if (rsp.ResultCode < 0)
            {
                logger.Error($"Image file upload failed. {rsp.ResultCode}");
                return null;
            }

            return rsp;
        }
        #endregion
    }
}
