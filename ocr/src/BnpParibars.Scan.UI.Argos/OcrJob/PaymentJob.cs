﻿//
// BnpParibars.Scan.UI.Argos.OcrJob.PaymentJob
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using log4net;
using BnpParibars.Scan.IO;
using BnpParibars.Scan.Data;

namespace BnpParibars.Scan.UI.Argos.OcrJob
{
    /// <summary>
    /// 유입경로: WEBFAX
    /// 문서유형: 제지급
    /// 업무유형: POS
    /// 처리방법: 
    ///     특정번호(8899)에 해당하는 FTP 원격폴더(899)에 들어오는 문서는 제지급으로
    ///     판단하며, QR 코드가 존재하지 않기 때문에 문서의 분리 및 변환 후 인식
    ///     과정을 거치치 않고 대분류는 제지급서류(P0), 중분류는 제지급문서(0060)으로
    ///     분류하고 'P'로 시작하는 10자리 가증번을 생성하여 ECM 및 DB에 등록.
    /// </summary>
    public class PaymentJob : Job
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("PaymentJob");
        private string businessNumber = string.Empty;
        #endregion

        #region CONSTRUCTORS
        public PaymentJob() : base(JobTypes.Payment) { }
        #endregion

        #region OVERRIDE METHODS
        protected override bool Import()
        {
            return Download();
        }

        protected override bool Preprocessing(DownloadFile downloadFile)
        {
            logger.Debug(string.Format("{0} image preprocessing started.",
                Util.EnumHelper.GetDescription(JobType)));

            if (!ConvertTiff(downloadFile)) return false;
            ResizeImage();

            logger.Debug(string.Format("{0} image preprocessing done.",
                Util.EnumHelper.GetDescription(JobType)));

            return true;
        }

        protected override void Recognize(JobData jobData)
        {
            logger.Debug(string.Format("{0} image recognition is started.",
                Util.EnumHelper.GetDescription(JobType)));

            businessNumber = GenerateBusinessNumber();
            foreach (ConvertFile f in ConvertFiles)
            {
                jobData.OcrDatas.Add(new OcrData()
                {
                    BusinessTypeCode = BusinessTypes.GetType(JobType),
                    ProductCategoryCode = ProductCategories.Unknown,
                    ContractorTypeCode = ContractorTypes.Unknown,
                    BusinessNumber = businessNumber,
                    DocumentCategoryCode = "P0",
                    DocumentSubcategoryCode = "0060",
                    ImageFileName = f.FullName
                });
            }

            logger.Debug(string.Format("{0} image recognition is done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void Classfication(JobData jobData)
        {
            logger.Debug(string.Format("{0} classfication started.",
                Util.EnumHelper.GetDescription(JobType)));

            CreateCase(BusinessTypes.GetType(JobType), 
                ProductCategories.Unknown, 
                ContractorTypes.Unknown, 
                businessNumber, jobData);

            logger.Debug(string.Format("{0} classfication done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void UploadFiles(JobData jobData)
        {
            logger.Debug(string.Format("{0} Upload started.",
                Util.EnumHelper.GetDescription(JobType)));

            UploadOcrFiles(jobData);

            logger.Debug(string.Format("{0} Upload done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void GenerateData(JobData jobData)
        {
            if (jobData.Cases[0] == null)
            {
                logger.Error("Can't find any case.");
                return;
            }

            DocumentCategory category = jobData.DocCategories.Find("P0");
            DocumentSubcategory subcategory = jobData.DocSubcategories.Find("0060");
            CaseDocument newDoc = new CaseDocument()
            {
                documentCategoryId = category.documentCategoryId,
                documentCategoryCode = category.code,
                documentCategoryName = category.name,
                documentCategorySortOrder = category.sortOrder,
                documentSubcategoryId = subcategory.documentSubcategoryId,
                documentSubcategoryCode = subcategory.code,
                documentSubcategoryName = subcategory.title,
                documentSubcategorySortOrder = subcategory.sortOrder
            };

            int pageNo = 1;
            foreach (OcrData r in jobData.OcrDatas)
            {
                newDoc.caseDocumentPageList.Add(new CaseDocumentPage()
                {
                    pageNumber = pageNo++,
                    isEncrypted = 1,
                    imageFileId = r.ImageFileID,
                    annotationFileId = string.Empty
                });
            }
            jobData.Cases[0].caseDocumentList.Add(newDoc);
        }

        protected override void Verify(JobData jobData)
        {
            logger.Debug(string.Format("{0} is not required verification.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void Export(JobData jobData)
        {
            logger.Debug(string.Format("{0} export started.",
                Util.EnumHelper.GetDescription(JobType)));

            UploadDataToWas(jobData);

            logger.Debug(string.Format("{0} export done.",
                Util.EnumHelper.GetDescription(JobType)));
        }
        #endregion
    }
}
