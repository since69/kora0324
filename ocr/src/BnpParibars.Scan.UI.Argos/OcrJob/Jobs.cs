﻿//
// BnpParibars.Scan.UI.Argos.Job.OcrJob
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System.Collections;
using System.Collections.Generic;

namespace BnpParibars.Scan.UI.Argos.OcrJob
{
    public class Jobs : IList<Job>
    {
        #region VARIABLES
        private readonly IList<Job> _list = new List<Job>();
        private int currentJobIndex = 0;
        #endregion

        #region PROPERTIES
        public int Count { get { return _list.Count; } }

        public bool IsReadOnly { get { return _list.IsReadOnly; } }

        public Job this[int index]
        {
            get { return _list[index]; }
            set { _list[index] = value; }
        }
        #endregion

        #region CONSTRUCTORS
        public Jobs() { }
        #endregion

        #region VIRTUAL METHODS
        public virtual void Add(Job item)
        {
            _list.Add(item);
        }

        public virtual void Clear()
        {
            _list.Clear();
        }

        public virtual bool Contains(Job item)
        {
            return _list.Contains(item);
        }

        public virtual void CopyTo(Job[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public virtual int IndexOf(Job item)
        {
            return _list.IndexOf(item);
        }

        public virtual void Insert(int index, Job item)
        {
            _list.Insert(index, item);
        }

        public virtual bool Remove(Job item)
        {
            return _list.Remove(item);
        }

        public virtual void RemoveAt(int index) { _list.RemoveAt(index); }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public virtual IEnumerator<Job> GetEnumerator()
        {
            return _list.GetEnumerator();
        }
        #endregion

        #region PUBLIC METHODS
        public void ChangeCurrentJob()
        {
            currentJobIndex = currentJobIndex < Count - 1
                ? currentJobIndex + 1 
                : 0;
        }

        public Job GetCurrentJob()
        {
            return this[currentJobIndex];
        }
        #endregion
    }
}
