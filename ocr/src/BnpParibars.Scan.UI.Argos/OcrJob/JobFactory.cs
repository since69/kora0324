﻿//
// BnpParibars.Scan.UI.Argos.OcrJob.JobFactory
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using log4net;

namespace BnpParibars.Scan.UI.Argos.OcrJob
{
    public class JobFactory
    {
        private static readonly ILog logger = LogManager.GetLogger("JobFactory");

        public Job CreateJob(JobTypes jobType)
        {
            switch (jobType)
            {
                case JobTypes.Application:
                    return new ApplicationJob();
                case JobTypes.Agreement:
                    return new AgreementJob();
                case JobTypes.Benefit:
                    return new BenefitJob();
                case JobTypes.Payment:
                    return new PaymentJob();
                case JobTypes.Digital:
                    return new DigitalJob();
                case JobTypes.KBank:
                    return new KBankJob();
                case JobTypes.HNBank:
                    return new HNBankJob();
                case JobTypes.NHBank:
                    return new NHBankJob();
                case JobTypes.SCBank:
                    return new SCBankJob();
                case JobTypes.WRBank:
                    return new WRBankJob();
                case JobTypes.CHBank:
                    return new CHBankJob();
                default:
                    logger.Error($"{jobType} is not valid job.");
                    return null;
            }
        }
    }
}
