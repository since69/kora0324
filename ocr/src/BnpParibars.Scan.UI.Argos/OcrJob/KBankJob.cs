﻿//
// BnpParibars.Scan.UI.Argos.OcrJob.KBankJob
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System.Text;
using log4net;
using BnpParibars.Scan.IO;

namespace BnpParibars.Scan.UI.Argos.OcrJob
{
    /// <summary>
    /// 유입경로: BSL Server.
    /// 문서유형: 청약서(20/NB), 상품설명서(40/NB), 청약철회신청서(51/POS), 고객정보변경신청서(53/POS)
    /// 업무유형: 신계약(New Business) 및 POS
    /// 처리방법:
    ///     FTP 원격폴더에서 내려받은 파일의 파일 명을 '_'를 분리자로 분해한 후
    ///     인식을 거치지 않고 케이스와 부가 정보(문서/페이지)를 생성하여 전송.
    ///     파일명은 아래와 같은 형식을 가짐.
    ///     
    ///     문서유형(2)_증권번호(10)(청약일(8))_key.pdf
    /// </summary>
    public class KBankJob : Job
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("KBankJob");
        private string documentCategoryCode = string.Empty;
        private string documentSubcategoryCode = string.Empty;
        private string businessNumber = string.Empty;
        #endregion

        #region CONSTRUCTORS
        public KBankJob() : base(JobTypes.KBank) { }
        #endregion

        #region OVERRIDE METHODS
        protected override bool Import()
        {
            return Download();
        }

        protected override bool Preprocessing(DownloadFile downloadFile)
        {
            logger.Debug(string.Format("{0} image preprocessing started.",
                Util.EnumHelper.GetDescription(JobType)));

            string[] parts = downloadFile.NameWithoutExtension.Split('_');
            if (!VerifyFileName(parts))
                return false;

            switch (parts[0].ToUpper())
            {
                case "20":
                    documentCategoryCode = "S1";
                    documentSubcategoryCode = "0001";
                    break;
                case "40":
                    documentCategoryCode = "D1";
                    documentSubcategoryCode = "0032";
                    break;
                case "51":
                    documentCategoryCode = "P0";
                    documentSubcategoryCode = "0060";
                    break;
                case "53":
                    documentCategoryCode = "P0";
                    documentSubcategoryCode = "0060";
                    break;
                default:
                    documentCategoryCode = string.Empty;
                    documentSubcategoryCode = string.Empty;
                    logger.Error($"Unknown document code. {parts[0]}");
                    return false;
            }
            businessNumber = parts[1].Substring(0, 10);

            if (!ConvertPdf(downloadFile)) return false;
            ResizeImage();

            logger.Debug(string.Format("{0} image preprocessing done.",
                Util.EnumHelper.GetDescription(JobType)));

            return true;
        }

        protected override void Recognize(JobData jobData)
        {
            logger.Debug(string.Format("{0} image recognition started.",
                Util.EnumHelper.GetDescription(JobType)));

            foreach (ConvertFile f in ConvertFiles)
            {
                jobData.OcrDatas.Add(new OcrData()
                {
                    BusinessTypeCode = BusinessTypes.GetType(JobType),
                    ProductCategoryCode = ProductCategories.Unknown,
                    ContractorTypeCode = ContractorTypes.Unknown,
                    BusinessNumber = businessNumber,
                    DocumentCategoryCode = documentCategoryCode,
                    DocumentSubcategoryCode = documentSubcategoryCode,
                    ImageFileName = f.FullName
                });
            }

            logger.Debug(string.Format("{0} image recognition is done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void Classfication(JobData jobData)
        {
            logger.Debug(string.Format("{0} classfication started.",
                Util.EnumHelper.GetDescription(JobType)));

            CreateCase(BusinessTypes.GetType(JobType), 
                ProductCategories.Unknown, ContractorTypes.Unknown, 
                businessNumber, jobData);

            logger.Debug(string.Format("{0} classfication done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void UploadFiles(JobData jobData)
        {
            logger.Debug(string.Format("{0} Upload started.",
                Util.EnumHelper.GetDescription(JobType)));

            UploadOcrFiles(jobData);

            logger.Debug(string.Format("{0} Upload done.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void GenerateData(JobData jobData)
        {
            GenerateDataForPDF(jobData,
                documentCategoryCode,
                documentSubcategoryCode);
        }

        protected override void Verify(JobData jobData)
        {
            logger.Debug(string.Format("{0} is not required verification.",
                Util.EnumHelper.GetDescription(JobType)));
        }

        protected override void Export(JobData jobData)
        {
            logger.Debug(string.Format("{0} export started.",
                Util.EnumHelper.GetDescription(JobType)));

            UploadDataToWas(jobData);

            logger.Debug(string.Format("{0} export done.",
                Util.EnumHelper.GetDescription(JobType)));
        }
        #endregion

        #region PRIVATE METHODS
        private bool VerifyFileName(string[] parts)
        {
            StringBuilder sb = new StringBuilder();

            if (parts.Length != 2)
            {
                sb.Append("Invalid file name.");
                sb.AppendFormat("splited file name length is {0}", parts.Length);
                sb.Append("length must be 2");
                logger.Error(sb.ToString());
                return false;
            }

            if (parts[0].Length != 2)
            {
                logger.Error($"Invalid document category. {parts[0]}");
                return false;
            }

            if (parts[1].Length != 20)
            {
                logger.Error($"Invalid insurance policy number & date. {parts[1]}");
                return false;
            }
            return true;
        }
        #endregion
    }
}
