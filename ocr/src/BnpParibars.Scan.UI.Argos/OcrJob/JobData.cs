﻿//
// BnpParibars.Scan.UI.Argos.OcrJob.JobData
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System.Text;
using System.IO;
using System.Collections.Generic;
using log4net;
using BnpParibars.Scan.Data;

namespace BnpParibars.Scan.UI.Argos.OcrJob
{
    /// <summary>
    /// Type of insurance business.
    /// See BusinessType table schema.
    /// </summary>
    public static class BusinessTypes
    {
        public const string Unknown = "";
        public const string Bancassurance = "B";
        public const string Claim = "C";
        public const string GeneralAgency = "G";
        public const string Market = "M";
        public const string Pos = "P";

        public static string GetType(JobTypes jobType)
        {
            switch (jobType)
            {
                case JobTypes.Agreement:
                    return Market;
                case JobTypes.Application:
                    return GeneralAgency;
                case JobTypes.Benefit:
                    return Claim;
                case JobTypes.Payment:
                    return Pos;
                case JobTypes.Digital:
                    return GeneralAgency;
                case JobTypes.KBank:
                case JobTypes.HNBank:
                case JobTypes.NHBank:
                case JobTypes.SCBank:
                case JobTypes.WRBank:
                case JobTypes.CHBank:
                    return Bancassurance;
                default:
                    return Unknown;
            }
        }
    }

    public static class BusinessNumberType
    {
        public const int Unknown = 0;
        /// <summary>
        /// 증권번호 (POS/GA/BA)
        /// </summary>
        public const int PolicyNumber = 1;
        /// <summary>
        /// 접수번호 (Claim)
        /// </summary>
        public const int RegistrationNumber = 2;
        /// <summary>
        /// 동의번호(가입설계동의서)
        /// </summary>
        public const int AgreementNumber = 3;

        public static int GetType(JobTypes jobType)
        {
            switch (jobType)
            {
                case JobTypes.Agreement:
                    return AgreementNumber;
                case JobTypes.Benefit:
                    return RegistrationNumber;
                default:
                    //case JobTypes.Application:
                    //case JobTypes.Payment:
                    return PolicyNumber;
            }
        }
    }

    /// <summary>
    /// Category of insurance products.
    /// See ProductCategory table schema.
    /// </summary>
    public static class ProductCategories
    {
        /// <summary>
        /// 보장성
        /// </summary>
        public const string Coverage = "1";
        /// <summary>
        /// 변액저축
        /// </summary>
        public const string VariableDeposit = "2";
        /// <summary>
        /// 변액 연금
        /// </summary>
        public const string VariableAnnuity = "3";
        /// <summary>
        /// 저축
        /// </summary>
        public const string Deposit = "4";
        /// <summary>
        /// 연금
        /// </summary>
        public const string Annuity = "5";
        /// <summary>
        /// 간편고지
        /// </summary>
        public const string Notice = "6";
        public const string Unknown = "X";
    }

    public static class ContractorTypes
    {
        /// <summary>
        /// 개인
        /// </summary>
        public const string Individual = "1";
        /// <summary>
        /// 법인
        /// </summary>
        public const string Corporation = "2";
        /// <summary>
        /// 미성년자
        /// </summary>
        public const string UnderAge = "3";
        public const string Unknown = "X";
    }

    public static class Funnels
    {
        public const string Fax = "FAX";
        public const string Pdf = "PDF";
        public const string Scan = "SCAN";

        public static string GetFunnel(JobTypes jobType)
        {
            switch (jobType)
            {
                case JobTypes.Agreement:
                case JobTypes.Application:
                case JobTypes.Benefit:
                case JobTypes.Payment:
                    return Fax;
                case JobTypes.Digital:
                case JobTypes.KBank:
                case JobTypes.HNBank:
                case JobTypes.SCBank:
                case JobTypes.NHBank:
                case JobTypes.WRBank:
                case JobTypes.CHBank:
                    return Pdf;
                default:
                    return string.Empty;
            }
        }
    }

    public static class CaseStatus
    {
        // Other then NewBusiness 
        public const int Unknown = 0;
        public const int NotArrived = 1;
        public const int Complement = 2;
        public const int Accepted = 3;
        // New Business & WebFax
        public const int FaxReceipt= 4;
        // New Business & other then WebFax
        public const int Mailbag = 5;
        public const int KYC = 6;
        public const int Arrived = 7;

        public static int GetStatus(JobTypes jobType)
        {
            switch (jobType)
            {
                case JobTypes.Application:
                case JobTypes.Agreement:
                case JobTypes.Payment:
                case JobTypes.Benefit:
                    return FaxReceipt;
                default:
                    return Mailbag;
            }
        }
    }

    public static class ArchiveStatus
    {
        public const int NotArchived = 0;
        public const int Archived = 1;
    }

    public class JobData
    {
        #region PROPERTIES
        public DocCategories DocCategories { get; }
        public DocSubcategories DocSubcategories { get; }
        public MandatoryDocs MandatoryDocs { get; }
        public RecogTemplates RecogTemplates { get; }
        public Supplements Supplements { get; }

        public List<Case> Cases { get; set; }
        public OcrDatas OcrDatas { get; set; }
        public OcrDatas SignatureDatas { get; set; }
        #endregion

        #region CONSTRUCTORS
        public JobData(DocCategories docCategories,
            DocSubcategories docSubcategories,
            MandatoryDocs mandatoryDocs,
            RecogTemplates recogTemplates,
            Supplements supplements) 
        {
            DocCategories = docCategories;
            DocSubcategories = docSubcategories;
            MandatoryDocs = mandatoryDocs;
            RecogTemplates = recogTemplates;
            Supplements = supplements;

            Cases = new List<Case>();
            OcrDatas = new OcrDatas();
            SignatureDatas = new OcrDatas();
        }
        #endregion

        #region METHODS
        public bool CaseDocumentExists(int categoryId,
            int subcategoryId,
            List<CaseDocument> docs)
        {
            foreach (CaseDocument doc in docs)
            {
                if (doc.documentCategoryId == categoryId
                    && doc.documentSubcategoryId == subcategoryId)
                    return true;
            }
            return false;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("\nRECOGNITION RESULTS: ");
            foreach (OcrData ocrData in OcrDatas)
            {
                sb.AppendFormat("{0} ({1})\n",
                    ocrData.RawData,
                    Path.GetFileNameWithoutExtension(ocrData.ImageFileName));
                foreach (RecognitionData recogData in ocrData.RecognitionDatas)
                    sb.AppendLine(recogData.ToString());
                sb.AppendLine();
            }
            return sb.ToString();
        }
        #endregion
    }
}
