﻿//
// BnpParibars.Scan.UI.Argos.MandatoryDocs
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System.Collections;
using System.Collections.Generic;
using log4net;
using BnpParibars.Scan.Data;

namespace BnpParibars.Scan.UI.Argos
{
    public class MandatoryDocs : IList<MandatoryDocument>
    {
        #region VARIABLES
        private static ILog logger = LogManager.GetLogger("MandatoryDocs");
        private readonly IList<MandatoryDocument> _list = new List<MandatoryDocument>();
        #endregion

        #region PROPERTIES
        public int Count { get { return _list.Count; } }

        public bool IsReadOnly { get { return _list.IsReadOnly; } }

        public MandatoryDocument this[int index]
        {
            get { return _list[index]; }
            set { _list[index] = value; }
        }
        #endregion

        #region CONSTRUCTORS
        public MandatoryDocs() { }
        #endregion

        #region VIRTUAL METHODS
        public virtual void Add(MandatoryDocument item)
        {
            _list.Add(item);
        }

        public virtual void Clear()
        {
            _list.Clear();
        }

        public virtual bool Contains(MandatoryDocument item)
        {
            return _list.Contains(item);
        }

        public virtual void CopyTo(MandatoryDocument[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public virtual int IndexOf(MandatoryDocument item)
        {
            return _list.IndexOf(item);
        }

        public virtual void Insert(int index, MandatoryDocument item)
        {
            _list.Insert(index, item);
        }

        public virtual bool Remove(MandatoryDocument item)
        {
            return _list.Remove(item);
        }

        public virtual void RemoveAt(int index) { _list.RemoveAt(index); }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public virtual IEnumerator<MandatoryDocument> GetEnumerator()
        {
            return _list.GetEnumerator();
        }
        #endregion

        #region PUBLIC METHODS
        public void CopyFrom(List<MandatoryDocument> docs)
        {
            foreach (MandatoryDocument doc in docs)
                _list.Add(doc);
        }

        /// <summary>
        /// Find and return mandatory document by parameters.
        /// </summary>
        public MandatoryDocument Find(
            string contractorTypeCode, 
            string businessTypeCode, 
            string productCategoryCode, 
            int documentCategoryId,
            int documentSubcategoryId)
        {
            foreach (MandatoryDocument doc in _list)
            {
                if (!doc.ContractorTypeCode.Equals(contractorTypeCode))
                    continue;
                if (!doc.BusinessTypeCode.Equals(businessTypeCode))
                    continue;
                if (!doc.ProductCategoryCode.Equals(productCategoryCode))
                    continue;
                if (doc.DocumentCategoryID != documentCategoryId)
                    continue;
                if (doc.DocumentSubcategoryID == documentSubcategoryId)
                    return doc;
            }
            return null;
        }
        #endregion
    }
}
