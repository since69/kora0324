﻿//
// BnpParibars.Scan.UI.Argos.DocSubcategories
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System.Collections;
using System.Collections.Generic;
using BnpParibars.Scan.Data;
using log4net;

namespace BnpParibars.Scan.UI.Argos
{
    public class DocSubcategories : IList<DocumentSubcategory>
    {
        #region VARIABLES
        private static ILog logger = LogManager.GetLogger("DocCategories");
        private readonly IList<DocumentSubcategory> _list = new List<DocumentSubcategory>();
        #endregion

        #region PROPERTIES
        public int Count { get { return _list.Count; } }

        public bool IsReadOnly { get { return _list.IsReadOnly; } }

        public DocumentSubcategory this[int index]
        {
            get { return _list[index]; }
            set { _list[index] = value; }
        }
        #endregion

        #region CONSTRUCTORS
        public DocSubcategories() { }
        #endregion

        #region VIRTUAL METHODS
        public virtual void Add(DocumentSubcategory item)
        {
            _list.Add(item);
        }

        public virtual void Clear()
        {
            _list.Clear();
        }

        public virtual bool Contains(DocumentSubcategory item)
        {
            return _list.Contains(item);
        }

        public virtual void CopyTo(DocumentSubcategory[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public virtual int IndexOf(DocumentSubcategory item)
        {
            return _list.IndexOf(item);
        }

        public virtual void Insert(int index, DocumentSubcategory item)
        {
            _list.Insert(index, item);
        }

        public virtual bool Remove(DocumentSubcategory item)
        {
            return _list.Remove(item);
        }

        public virtual void RemoveAt(int index) { _list.RemoveAt(index); }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public virtual IEnumerator<DocumentSubcategory> GetEnumerator()
        {
            return _list.GetEnumerator();
        }
        #endregion

        #region PUBLIC METHODS
        public void CopyFrom(List<DocumentSubcategory> catgories)
        {
            foreach (DocumentSubcategory cat in catgories)
                _list.Add(cat);
        }

        /// <summary>
        /// Find and return document subcategory id by subcategory code.
        /// </summary>
        /// <param name="code">The code of the document subcategory you want to find.</param>
        /// <returns></returns>
        public DocumentSubcategory Find(string code)
        {
            foreach (DocumentSubcategory cat in _list)
            {
                if (cat.code.Equals(code))
                    return cat;
            }
            return null;
        }

        public DocumentSubcategory Find(int subcategoryID)
        {
            foreach (DocumentSubcategory cat in _list)
            {
                if (cat.documentSubcategoryId == subcategoryID)
                    return cat;
            }
            return null;
        }
        #endregion
    }
}
