﻿namespace BnpParibars.Scan.UI.Argos
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ilMenuIcons = new System.Windows.Forms.ImageList(this.components);
            this.msMainForm = new MaterialSkin.Controls.MaterialContextMenuStrip();
            this.pgJob = new System.Windows.Forms.TabPage();
            this.lblVersion = new MaterialSkin.Controls.MaterialLabel();
            this.panFailed = new MaterialSkin.Controls.MaterialCard();
            this.lblFailed = new MaterialSkin.Controls.MaterialLabel();
            this.lblNumOfFailed = new MaterialSkin.Controls.MaterialLabel();
            this.panProcess = new MaterialSkin.Controls.MaterialCard();
            this.lblProcessed = new MaterialSkin.Controls.MaterialLabel();
            this.lblNumOfProcessed = new MaterialSkin.Controls.MaterialLabel();
            this.panStatus = new MaterialSkin.Controls.MaterialCard();
            this.lblLastActingTime = new MaterialSkin.Controls.MaterialLabel();
            this.pbOperation = new System.Windows.Forms.PictureBox();
            this.lblStatus = new MaterialSkin.Controls.MaterialLabel();
            this.lblOperationStatus = new MaterialSkin.Controls.MaterialLabel();
            this.btnOperation = new MaterialSkin.Controls.MaterialButton();
            this.tabMainForm = new MaterialSkin.Controls.MaterialTabControl();
            this.pgJob.SuspendLayout();
            this.panFailed.SuspendLayout();
            this.panProcess.SuspendLayout();
            this.panStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbOperation)).BeginInit();
            this.tabMainForm.SuspendLayout();
            this.SuspendLayout();
            // 
            // ilMenuIcons
            // 
            this.ilMenuIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilMenuIcons.ImageStream")));
            this.ilMenuIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.ilMenuIcons.Images.SetKeyName(0, "scanner_white_24dp.png");
            this.ilMenuIcons.Images.SetKeyName(1, "settings_white_24dp.png");
            this.ilMenuIcons.Images.SetKeyName(2, "statistics_white_24dp.png");
            // 
            // msMainForm
            // 
            this.msMainForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.msMainForm.Depth = 0;
            this.msMainForm.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.msMainForm.MouseState = MaterialSkin.MouseState.HOVER;
            this.msMainForm.Name = "msMainForm";
            this.msMainForm.Size = new System.Drawing.Size(61, 4);
            // 
            // pgJob
            // 
            this.pgJob.BackColor = System.Drawing.Color.White;
            this.pgJob.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pgJob.Controls.Add(this.lblVersion);
            this.pgJob.Controls.Add(this.panFailed);
            this.pgJob.Controls.Add(this.panProcess);
            this.pgJob.Controls.Add(this.panStatus);
            this.pgJob.Controls.Add(this.btnOperation);
            this.pgJob.ImageKey = "scanner_white_24dp.png";
            this.pgJob.Location = new System.Drawing.Point(4, 31);
            this.pgJob.Name = "pgJob";
            this.pgJob.Padding = new System.Windows.Forms.Padding(25, 20, 25, 20);
            this.pgJob.Size = new System.Drawing.Size(1186, 503);
            this.pgJob.TabIndex = 0;
            this.pgJob.Text = "JOB";
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Depth = 0;
            this.lblVersion.Font = new System.Drawing.Font("Roboto", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblVersion.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle1;
            this.lblVersion.Location = new System.Drawing.Point(20, 20);
            this.lblVersion.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(57, 19);
            this.lblVersion.TabIndex = 12;
            this.lblVersion.Text = "v1.0.0.0";
            // 
            // panFailed
            // 
            this.panFailed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panFailed.Controls.Add(this.lblFailed);
            this.panFailed.Controls.Add(this.lblNumOfFailed);
            this.panFailed.Depth = 0;
            this.panFailed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panFailed.Location = new System.Drawing.Point(719, 80);
            this.panFailed.Margin = new System.Windows.Forms.Padding(14);
            this.panFailed.MouseState = MaterialSkin.MouseState.HOVER;
            this.panFailed.Name = "panFailed";
            this.panFailed.Padding = new System.Windows.Forms.Padding(14);
            this.panFailed.Size = new System.Drawing.Size(320, 280);
            this.panFailed.TabIndex = 11;
            // 
            // lblFailed
            // 
            this.lblFailed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFailed.AutoSize = true;
            this.lblFailed.Depth = 0;
            this.lblFailed.Font = new System.Drawing.Font("Roboto Medium", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblFailed.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            this.lblFailed.Location = new System.Drawing.Point(259, 14);
            this.lblFailed.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblFailed.Name = "lblFailed";
            this.lblFailed.Size = new System.Drawing.Size(41, 17);
            this.lblFailed.TabIndex = 14;
            this.lblFailed.Text = "Failed";
            this.lblFailed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNumOfFailed
            // 
            this.lblNumOfFailed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNumOfFailed.Depth = 0;
            this.lblNumOfFailed.Font = new System.Drawing.Font("Roboto Light", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblNumOfFailed.FontType = MaterialSkin.MaterialSkinManager.fontType.H2;
            this.lblNumOfFailed.Location = new System.Drawing.Point(17, 100);
            this.lblNumOfFailed.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblNumOfFailed.Name = "lblNumOfFailed";
            this.lblNumOfFailed.Size = new System.Drawing.Size(286, 72);
            this.lblNumOfFailed.TabIndex = 3;
            this.lblNumOfFailed.Text = "0";
            this.lblNumOfFailed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panProcess
            // 
            this.panProcess.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panProcess.Controls.Add(this.lblProcessed);
            this.panProcess.Controls.Add(this.lblNumOfProcessed);
            this.panProcess.Depth = 0;
            this.panProcess.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panProcess.Location = new System.Drawing.Point(382, 80);
            this.panProcess.Margin = new System.Windows.Forms.Padding(14);
            this.panProcess.MouseState = MaterialSkin.MouseState.HOVER;
            this.panProcess.Name = "panProcess";
            this.panProcess.Padding = new System.Windows.Forms.Padding(14);
            this.panProcess.Size = new System.Drawing.Size(320, 280);
            this.panProcess.TabIndex = 10;
            // 
            // lblProcessed
            // 
            this.lblProcessed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProcessed.AutoSize = true;
            this.lblProcessed.Depth = 0;
            this.lblProcessed.Font = new System.Drawing.Font("Roboto Medium", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblProcessed.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            this.lblProcessed.Location = new System.Drawing.Point(230, 14);
            this.lblProcessed.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblProcessed.Name = "lblProcessed";
            this.lblProcessed.Size = new System.Drawing.Size(67, 17);
            this.lblProcessed.TabIndex = 13;
            this.lblProcessed.Text = "processed";
            this.lblProcessed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNumOfProcessed
            // 
            this.lblNumOfProcessed.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNumOfProcessed.Depth = 0;
            this.lblNumOfProcessed.Font = new System.Drawing.Font("Roboto Light", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblNumOfProcessed.FontType = MaterialSkin.MaterialSkinManager.fontType.H2;
            this.lblNumOfProcessed.Location = new System.Drawing.Point(17, 100);
            this.lblNumOfProcessed.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblNumOfProcessed.Name = "lblNumOfProcessed";
            this.lblNumOfProcessed.Size = new System.Drawing.Size(286, 72);
            this.lblNumOfProcessed.TabIndex = 2;
            this.lblNumOfProcessed.Text = "0";
            this.lblNumOfProcessed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panStatus
            // 
            this.panStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panStatus.Controls.Add(this.lblLastActingTime);
            this.panStatus.Controls.Add(this.pbOperation);
            this.panStatus.Controls.Add(this.lblStatus);
            this.panStatus.Controls.Add(this.lblOperationStatus);
            this.panStatus.Depth = 0;
            this.panStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panStatus.Location = new System.Drawing.Point(45, 80);
            this.panStatus.Margin = new System.Windows.Forms.Padding(14);
            this.panStatus.MouseState = MaterialSkin.MouseState.HOVER;
            this.panStatus.Name = "panStatus";
            this.panStatus.Padding = new System.Windows.Forms.Padding(14);
            this.panStatus.Size = new System.Drawing.Size(320, 280);
            this.panStatus.TabIndex = 9;
            // 
            // lblLastActingTime
            // 
            this.lblLastActingTime.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblLastActingTime.Depth = 0;
            this.lblLastActingTime.Font = new System.Drawing.Font("Roboto Medium", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblLastActingTime.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            this.lblLastActingTime.Location = new System.Drawing.Point(14, 229);
            this.lblLastActingTime.Margin = new System.Windows.Forms.Padding(0);
            this.lblLastActingTime.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblLastActingTime.Name = "lblLastActingTime";
            this.lblLastActingTime.Size = new System.Drawing.Size(292, 23);
            this.lblLastActingTime.TabIndex = 12;
            this.lblLastActingTime.Text = "2020-12-25 16:30:30";
            this.lblLastActingTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pbOperation
            // 
            this.pbOperation.Image = ((System.Drawing.Image)(resources.GetObject("pbOperation.Image")));
            this.pbOperation.Location = new System.Drawing.Point(57, 42);
            this.pbOperation.Name = "pbOperation";
            this.pbOperation.Size = new System.Drawing.Size(188, 114);
            this.pbOperation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbOperation.TabIndex = 12;
            this.pbOperation.TabStop = false;
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStatus.AutoSize = true;
            this.lblStatus.Depth = 0;
            this.lblStatus.Font = new System.Drawing.Font("Roboto Medium", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblStatus.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            this.lblStatus.Location = new System.Drawing.Point(258, 14);
            this.lblStatus.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(41, 17);
            this.lblStatus.TabIndex = 12;
            this.lblStatus.Text = "status";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblOperationStatus
            // 
            this.lblOperationStatus.AutoSize = true;
            this.lblOperationStatus.Depth = 0;
            this.lblOperationStatus.Font = new System.Drawing.Font("Roboto Medium", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblOperationStatus.FontType = MaterialSkin.MaterialSkinManager.fontType.H6;
            this.lblOperationStatus.Location = new System.Drawing.Point(112, 179);
            this.lblOperationStatus.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblOperationStatus.Name = "lblOperationStatus";
            this.lblOperationStatus.Size = new System.Drawing.Size(89, 24);
            this.lblOperationStatus.TabIndex = 6;
            this.lblOperationStatus.Text = "STOPPED";
            // 
            // btnOperation
            // 
            this.btnOperation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOperation.AutoSize = false;
            this.btnOperation.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnOperation.Depth = 0;
            this.btnOperation.DrawShadows = true;
            this.btnOperation.HighEmphasis = true;
            this.btnOperation.Icon = null;
            this.btnOperation.Location = new System.Drawing.Point(1031, 20);
            this.btnOperation.Margin = new System.Windows.Forms.Padding(0);
            this.btnOperation.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnOperation.Name = "btnOperation";
            this.btnOperation.Size = new System.Drawing.Size(130, 36);
            this.btnOperation.TabIndex = 1;
            this.btnOperation.Text = "Start";
            this.btnOperation.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.btnOperation.UseAccentColor = false;
            this.btnOperation.UseVisualStyleBackColor = true;
            this.btnOperation.Click += new System.EventHandler(this.btnOperation_Click);
            // 
            // tabMainForm
            // 
            this.tabMainForm.Controls.Add(this.pgJob);
            this.tabMainForm.Depth = 0;
            this.tabMainForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMainForm.ImageList = this.ilMenuIcons;
            this.tabMainForm.Location = new System.Drawing.Point(3, 59);
            this.tabMainForm.MouseState = MaterialSkin.MouseState.HOVER;
            this.tabMainForm.Name = "tabMainForm";
            this.tabMainForm.SelectedIndex = 0;
            this.tabMainForm.Size = new System.Drawing.Size(1194, 538);
            this.tabMainForm.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1200, 600);
            this.ContextMenuStrip = this.msMainForm;
            this.Controls.Add(this.tabMainForm);
            this.DrawerShowIconsWhenHidden = true;
            this.DrawerTabControl = this.tabMainForm;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1200, 600);
            this.Name = "MainForm";
            this.Padding = new System.Windows.Forms.Padding(3, 59, 3, 3);
            this.Text = "MainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.pgJob.ResumeLayout(false);
            this.pgJob.PerformLayout();
            this.panFailed.ResumeLayout(false);
            this.panFailed.PerformLayout();
            this.panProcess.ResumeLayout(false);
            this.panProcess.PerformLayout();
            this.panStatus.ResumeLayout(false);
            this.panStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbOperation)).EndInit();
            this.tabMainForm.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ilMenuIcons;
        private MaterialSkin.Controls.MaterialContextMenuStrip msMainForm;
        private System.Windows.Forms.TabPage pgJob;
        private MaterialSkin.Controls.MaterialCard panFailed;
        private MaterialSkin.Controls.MaterialLabel lblFailed;
        private MaterialSkin.Controls.MaterialLabel lblNumOfFailed;
        private MaterialSkin.Controls.MaterialCard panProcess;
        private MaterialSkin.Controls.MaterialLabel lblProcessed;
        private MaterialSkin.Controls.MaterialLabel lblNumOfProcessed;
        private MaterialSkin.Controls.MaterialCard panStatus;
        private MaterialSkin.Controls.MaterialLabel lblLastActingTime;
        private System.Windows.Forms.PictureBox pbOperation;
        private MaterialSkin.Controls.MaterialLabel lblStatus;
        private MaterialSkin.Controls.MaterialLabel lblOperationStatus;
        private MaterialSkin.Controls.MaterialButton btnOperation;
        private MaterialSkin.Controls.MaterialTabControl tabMainForm;
        private MaterialSkin.Controls.MaterialLabel lblVersion;
    }
}