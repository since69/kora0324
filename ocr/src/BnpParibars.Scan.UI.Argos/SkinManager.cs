﻿//
// BnpParibars.Scan.UI.Argos.SkinManager
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibars.Scan.UI.Argos
{
    using MaterialSkin;
    using MaterialSkin.Controls;
    using System.Windows.Forms;

    public class SkinManager
    {
        #region VARIABLES
        private readonly MaterialSkinManager skinManager = MaterialSkinManager.Instance;
        private int colorSchemaIndex = 0;
        #endregion

        #region CONSTRUCTORS
        public SkinManager()
        {
        }
        #endregion

        #region METHODS
        public void InitializeSkin(MaterialForm form)
        {
            // Set this to false to disable backcolor enforcing on non-materialSkin components
            // This HAS to be set before the AddFormToManage()
            skinManager.EnforceBackcolorOnAllComponents = true;

            // MaterialSkinManager properties
            skinManager.AddFormToManage(form);
            //skinManager.Theme = MaterialSkinManager.Themes.DARK;
            skinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            skinManager.ColorScheme = CreateColorSchema();

            // Set drawer properties
            form.DrawerShowIconsWhenHidden = true;
            form.DrawerUseColors = false;
            form.DrawerHighlightWithAccent = true;
            form.DrawerBackgroundWithAccent = false;
        }

        public void UpdateTheme(MaterialSkinManager.Themes theme, MaterialForm form)
        {
            skinManager.Theme = theme;
            UpdateSkin(form);
        }

        public void UpdateSkin(MaterialForm form)
        {
            colorSchemaIndex = colorSchemaIndex < 2 ? ++colorSchemaIndex : 0;
            skinManager.ColorScheme = CreateColorSchema(colorSchemaIndex);
            form.Invalidate();
        }

        /// <summary>
        /// Create color schema of material skin.
        /// </summary>
        /// <param name="index">color schema index</param>
        /// <returns>Color schema</returns>
        private MaterialSkin.ColorScheme CreateColorSchema(int index = 0)
        {
            switch (index)
            {
                case 1:
                    return new ColorScheme(Primary.Green600, Primary.Green700,
                        Primary.Green200, Accent.Red100, TextShade.WHITE);
                case 2:
                    return new ColorScheme(Primary.BlueGrey800,
                        Primary.BlueGrey900, Primary.BlueGrey500,
                        Accent.LightBlue200, TextShade.WHITE);
                default:
                    return new ColorScheme(
                        skinManager.Theme == MaterialSkinManager.Themes.DARK ? Primary.Teal500 : Primary.Indigo500,
                        skinManager.Theme == MaterialSkinManager.Themes.DARK ? Primary.Teal700 : Primary.Indigo700,
                        skinManager.Theme == MaterialSkinManager.Themes.DARK ? Primary.Teal200 : Primary.Indigo200,
                        Accent.Pink200, TextShade.WHITE);
            }
        }
        #endregion
    }
}
