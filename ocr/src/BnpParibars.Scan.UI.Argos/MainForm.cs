﻿//
// BnpParibars.Scan.UI.Argos.MainForm
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System;
using System.Globalization;
using System.Windows.Forms;
using log4net;
using MaterialSkin.Controls;
using BnpParibars.Scan.IO;
using BnpParibars.Scan.Configuration;
using BnpParibars.Scan.UI.Argos.OcrJob;

namespace BnpParibars.Scan.UI.Argos
{
    public partial class MainForm : MaterialForm
    {
        #region VARIABLES
        private static AppContext ctx = new AppContext();
        private static ILog logger = LogManager.GetLogger("MainForm");
        private const int timerInterval = 1000;
        private Timer timer = new Timer();
        private SkinManager skinManager = new SkinManager();
        private JobManager jobManager;
        #endregion

        #region CONSTRUCTORS
        public MainForm()
        {
            InitializeComponent();
            if (!Initialize())
            {
                MessageBox.Show("Application initialize failed.",
                    "Fatal Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                btnOperation.Enabled = false;
            }
        }
        #endregion

        #region PUBLIC METHODS
        #endregion

        #region PRIVATE METHODS
        private bool Initialize()
        {
            logger.Info("OCR Started.");

            LoadConfiguration();

            // Initialize environment settings.
            jobManager = new JobManager(ctx);
            if (!jobManager.Initialize())
                return false;

            // Initialize timer
            timer.Interval = timerInterval;
            timer.Tick += new EventHandler(Timer_Tick);

            // Initialize ui
            skinManager.InitializeSkin(this);
            this.Text = Properties.Resources.AppName;
            lblLastActingTime.Text = string.Empty;

            lblVersion.Text = ctx.App.Version;

            return true;
        }

        private void LoadConfiguration()
        {
            var ocr = Configurator.OcrSettings;

            // Load working directory
            if (!System.IO.Directory.Exists(ocr.AppDir.BaseDir))
                System.IO.Directory.CreateDirectory(ocr.AppDir.BaseDir);

            ctx.ConvertDir = new ConvertDir(ocr.AppDir.BaseDir);
            ctx.ConvertDir.Combine(ocr.AppDir.ConvertDir);
            if (!ctx.ConvertDir.Exists()) ctx.ConvertDir.Create();

            ctx.DownloadDir = new DownloadDir(ocr.AppDir.BaseDir);
            ctx.DownloadDir.Combine(ocr.AppDir.DownloadDir);
            if (!ctx.DownloadDir.Exists()) ctx.DownloadDir.Create();

            ctx.FailureDir = new FailureDir(ocr.AppDir.BaseDir);
            ctx.FailureDir.Combine(ocr.AppDir.FailureDir);
            if (!ctx.FailureDir.Exists()) ctx.FailureDir.Create();

            ctx.TemplateDir = new TemplateDir(ocr.AppDir.BaseDir);
            ctx.TemplateDir.Combine(ocr.AppDir.TemplateDir);
            if (!ctx.TemplateDir.Exists()) ctx.TemplateDir.Create();

            // Load ecm configuration
            ctx.EcmProperty.IP = ocr.Ecm.Ip;
            ctx.EcmProperty.Port = ocr.Ecm.Port;
            ctx.EcmProperty.BaseDir = ocr.Ecm.BaseDir;
            foreach (ServletElement servlet in ocr.Ecm.Servlets)
            {
                ctx.EcmProperty.Servlets.Add(new ServletProperties()
                {
                    Name = servlet.Name,
                    Dir = servlet.Dir,
                    Type = servlet.Type
                });
            }

            // Load ftp configuration
            foreach (FtpElement ftp in ocr.Ftps)
            {
                FtpProperty prop = new FtpProperty()
                {
                    Name = ftp.Name,
                    Ip = ftp.Ip,
                    Port = ftp.Port,
                    BaseDir = ftp.BaseDir,
                    NumOfDownloads = ftp.NumOfDownloads,
                    UserName = ftp.UserName,
                    Password = ftp.Password,
                };

                foreach (FtpDirElement dir in ftp.FtpDirs)
                {
                    prop.Dirs.Add(new FtpDir()
                    {
                        Dir = dir.Dir,
                        JobType = dir.JobType
                    });
                }

                ctx.FtpProperties.Add(prop);
            }
        }
        #endregion

        #region TIMER EVENTS
        private void Timer_Tick(object Sender, EventArgs e)
        {
            timer.Stop();
            jobManager.DoRun();
            lblNumOfProcessed.Text = ctx.NumOfProcessed.ToString();
            lblNumOfFailed.Text = ctx.NumOfFailed.ToString();
            timer.Start();
        }
        #endregion

        #region CONTROL EVENTS
        private void btnOperation_Click(object sender, EventArgs e)
        {
            lblLastActingTime.Text = DateTime.UtcNow.ToLocalTime()
                .ToString("u", DateTimeFormatInfo.InvariantInfo);

            if (!jobManager.IsWorking)
            {
                jobManager.IsWorking = true;
                lblOperationStatus.Text = "Started";
                btnOperation.Text = "Stop";
                btnOperation.UseAccentColor = true;
                timer.Enabled = true;
            }
            else
            {
                jobManager.IsWorking = false;
                lblOperationStatus.Text = "Stopped";
                btnOperation.Text = "Start";
                btnOperation.UseAccentColor = false;
                timer.Enabled = false;
            }
        }
        #endregion

        #region FORM EVENTS
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
        }
        #endregion
    }
}
