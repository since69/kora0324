﻿//
// BnpParibars.Scan.UI.Argos.DocCategories
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System.Collections;
using System.Collections.Generic;
using BnpParibars.Scan.Data;
using log4net;

namespace BnpParibars.Scan.UI.Argos
{
    public class DocCategories : IList<DocumentCategory>
    {
        #region VARIABLES
        private static ILog logger = LogManager.GetLogger("DocCategories");
        private readonly IList<DocumentCategory> _list = new List<DocumentCategory>();
        #endregion

        #region PROPERTIES
        public int Count { get { return _list.Count; } }

        public bool IsReadOnly { get { return _list.IsReadOnly; } }

        public DocumentCategory this[int index]
        {
            get { return _list[index]; }
            set { _list[index] = value; }
        }
        #endregion

        #region CONSTRUCTORS
        public DocCategories() { }
        #endregion

        #region VIRTUAL METHODS
        public virtual void Add(DocumentCategory item)
        {
            _list.Add(item);
        }

        public virtual void Clear()
        {
            _list.Clear();
        }

        public virtual bool Contains(DocumentCategory item)
        {
            return _list.Contains(item);
        }

        public virtual void CopyTo(DocumentCategory[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public virtual int IndexOf(DocumentCategory item)
        {
            return _list.IndexOf(item);
        }

        public virtual void Insert(int index, DocumentCategory item)
        {
            _list.Insert(index, item);
        }

        public virtual bool Remove(DocumentCategory item)
        {
            return _list.Remove(item);
        }

        public virtual void RemoveAt(int index) { _list.RemoveAt(index); }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public virtual IEnumerator<DocumentCategory> GetEnumerator()
        {
            return _list.GetEnumerator();
        }
        #endregion

        #region PUBLIC METHODS
        public void CopyFrom(List<DocumentCategory> catgories)
        {
            foreach (DocumentCategory cat in catgories)
                _list.Add(cat);
        }

        public DocumentCategory Find(string code)
        {
            foreach (DocumentCategory cat in _list)
            {
                if (cat.code.Equals(code))
                    return cat;
            }
            return null;
        }

        public DocumentCategory Find(int categoryID)
        {
            foreach (DocumentCategory cat in _list)
            {
                if (cat.documentCategoryId == categoryID)
                    return cat;
            }
            return null;
        }
        #endregion
    }
}
