﻿//
// BnpParibars.Scan.UI.Argos.FtpsProperties
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//



using System.Collections;
using System.Collections.Generic;
using log4net;
using BnpParibars.Scan.Util;

namespace BnpParibars.Scan.UI.Argos
{
    public class FtpProperty
    {
        #region PROPERTIES
        public string Name { get; set; } = string.Empty;
        public string Ip { get; set; } = string.Empty;
        public int Port { get; set; } = 0;
        public string BaseDir { get; set; } = string.Empty;
        public int NumOfDownloads { get; set; } = 5;
        public string UserName { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public List<FtpDir> Dirs = new List<FtpDir>();
        #endregion

        #region CONSTRUCTORS
        public FtpProperty() {}
        #endregion

        #region PUBLIC MTHODS
        public string GetFtpUrl(JobTypes jobType)
        {
            System.Text.StringBuilder url = new System.Text.StringBuilder();
            url.AppendFormat(StringTemplate.FtpUri, Ip, Port, BaseDir);
            foreach (FtpDir dir in Dirs)
            {
                if (dir.JobType == (int)jobType)
                    url.Append(dir.Dir);
            }
            return url.ToString();
        }

        public string GetRemoteDir(JobTypes jobType)
        {
            System.Text.StringBuilder remoteDir = 
                new System.Text.StringBuilder(BaseDir);
            foreach (FtpDir dir in Dirs)
            {
                if (dir.JobType == (int)jobType)
                    remoteDir.Append($"/{dir.Dir}");
            }
            return remoteDir.ToString();
        }
        #endregion
    }

    /// <summary>
    /// FTP remote dir definition.
    /// </summary>
    public class FtpDir
    {
        /// <summary>
        /// Remote directory.
        /// </summary>
        public string Dir { get; set; } = string.Empty;
        /// <summary>
        /// Associated job type.
        /// </summary>
        public int JobType { get; set; } = 0;
    }


    public class FtpProperties : IList<FtpProperty>
    {
        #region VARIABLES
        private static ILog logger = LogManager.GetLogger("FtpProperties");
        private readonly IList<FtpProperty> _list = new List<FtpProperty>();
        #endregion

        #region PROPERTIES
        public int Count { get { return _list.Count; } }

        public bool IsReadOnly { get { return _list.IsReadOnly; } }

        public FtpProperty this[int index]
        {
            get { return _list[index]; }
            set { _list[index] = value; }
        }
        #endregion

        #region CONSTRUCTORS
        public FtpProperties() { }
        #endregion

        #region VIRTUAL METHODS
        public virtual void Add(FtpProperty item)
        {
            _list.Add(item);
        }

        public virtual void Clear()
        {
            _list.Clear();
        }

        public virtual bool Contains(FtpProperty item)
        {
            return _list.Contains(item);
        }

        public virtual void CopyTo(FtpProperty[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public virtual int IndexOf(FtpProperty item)
        {
            return _list.IndexOf(item);
        }

        public virtual void Insert(int index, FtpProperty item)
        {
            _list.Insert(index, item);
        }

        public virtual bool Remove(FtpProperty item)
        {
            return _list.Remove(item);
        }

        public virtual void RemoveAt(int index) { _list.RemoveAt(index); }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public virtual IEnumerator<FtpProperty> GetEnumerator()
        {
            return _list.GetEnumerator();
        }
        #endregion

        #region PUBLIC METHODS
        public FtpProperty Find(JobTypes jobType)
        {
            foreach (FtpProperty prop in _list)
            {
                foreach (FtpDir dir in prop.Dirs)
                {
                    if (dir.JobType == (int)jobType)
                        return prop;
                }
            }

            logger.Error(string.Format(
                "Can't find ftp properties for {0}.",
                EnumHelper.GetDescription(jobType)));
            return null;
        }
        #endregion
    }
}
