﻿//
// BnpParibars.Scan.AppContext
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using BnpParibars.Scan.IO;
using BnpParibars.Scan.UI.Argos.OcrJob;
using System.Drawing.Imaging;
using System.Configuration;

namespace BnpParibars.Scan.UI.Argos
{
    public class ServletProperties
    {
        public string Name { get; set; } = string.Empty;
        public string Dir { get; set; } = string.Empty;
        public int Type { get; set; } = 0;
    }

    public class EcmProperty
    {
        #region PROPERTIES
        public string IP { get; set; }
        public int Port { get; set; }
        public string BaseDir { get; set; }
        public List<ServletProperties> Servlets;
        #endregion

        #region CONSTRUCTORS
        public EcmProperty()
        {
            Servlets = new List<ServletProperties>();
        }
        #endregion

        #region METHODS
        public string GetServletUrl(ServletTypes type)
        {
            foreach (ServletProperties prop in Servlets)
            {
                if (prop.Type == (int)type)
                {
                    //return string.Format("http://{0}:{1}/{2}/{3}/{4}",
                    //    IP, Port, BaseDir, prop.Dir, prop.Name);

                    // pc(developement machine) only
                    return string.Format("{0}:{1}/{2}/{3}/{4}",
                        IP, Port, BaseDir, prop.Dir, prop.Name);
                }
            }
            return string.Empty;
        }
        #endregion
    }

    public class AppContext
    {
        #region VARIABLES
        public ApplicationProperties App = new ApplicationProperties();
        #endregion

        #region PROPERTIES
        public ConvertDir ConvertDir { get; set; }
        public DownloadDir DownloadDir { get; set; }
        public FailureDir FailureDir { get; set; }
        public TemplateDir TemplateDir { get; set; }
        public EcmProperty EcmProperty { get; set; }
        public FtpProperties FtpProperties { get; set; }

        /// <summary>
        /// Number of total ocr processed.
        /// </summary>
        public int NumOfProcessed { get; set; } = 0;
        /// <summary>
        /// Number of failed ocr processing.
        /// </summary>
        public int NumOfFailed { get; set; } = 0;
        #endregion

        #region CONSTRUCTORS
        public AppContext()
        {
            Process process = Process.GetCurrentProcess();
            App.ProcessId = process.Id;
            App.Name = process.ProcessName;
            App.Version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            App.MachineName = Environment.MachineName;
            App.StartTime = DateTime.UtcNow;

            EcmProperty = new EcmProperty();
            FtpProperties = new FtpProperties();
        }
        #endregion

        #region METHODS
        #endregion

        #region INTERNAL CLASSES
        public class ApplicationProperties
        {
            public int ProcessId { get; set; }
            public string Name { get; set; }
            public string Version { get; set; }
            public string MachineName { get; set; }

            public DateTime StartTime { get; set; } 
        }
        #endregion
    }
}
