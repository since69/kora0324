﻿//
// BnpParibars.Scan.UI.Argos.RecogTemplates
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System.Collections;
using System.Collections.Generic;
using log4net;
using BnpParibars.Scan.Data;

namespace BnpParibars.Scan.UI.Argos
{
    public class RecogTemplates : IList<RecognitionTemplate>
    {
        #region VARIABLES
        private static ILog logger = LogManager.GetLogger("RecogTemplates");
        private readonly IList<RecognitionTemplate> _list = new List<RecognitionTemplate>();
        #endregion

        #region PROPERTIES
        public int Count { get { return _list.Count; } }

        public bool IsReadOnly { get { return _list.IsReadOnly; } }

        public RecognitionTemplate this[int index]
        {
            get { return _list[index]; }
            set { _list[index] = value; }
        }
        #endregion

        #region CONSTRUCTORS
        public RecogTemplates() { }
        #endregion

        #region VIRTUAL METHODS
        public virtual void Add(RecognitionTemplate item)
        {
            _list.Add(item);
        }

        public virtual void Clear()
        {
            _list.Clear();
        }

        public virtual bool Contains(RecognitionTemplate item)
        {
            return _list.Contains(item);
        }

        public virtual void CopyTo(RecognitionTemplate[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public virtual int IndexOf(RecognitionTemplate item)
        {
            return _list.IndexOf(item);
        }

        public virtual void Insert(int index, RecognitionTemplate item)
        {
            _list.Insert(index, item);
        }

        public virtual bool Remove(RecognitionTemplate item)
        {
            return _list.Remove(item);
        }

        public virtual void RemoveAt(int index) { _list.RemoveAt(index); }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public virtual IEnumerator<RecognitionTemplate> GetEnumerator()
        {
            return _list.GetEnumerator();
        }
        #endregion

        #region PUBLIC METHODS
        public void CopyFrom(List<RecognitionTemplate> templates)
        {
            foreach (RecognitionTemplate template in templates)
                _list.Add(template);
        }

        /// <summary>
        /// Find and return mandatory document by parameters.
        /// </summary>
        public RecognitionTemplate Find(
            string contractorTypeCode, 
            string businessTypeCode, 
            string productCategoryCode, 
            int documentSubcategoryId,
            string templateVersion)
        {
            foreach (RecognitionTemplate template in _list)
            {
                if (!template.ContractorTypeCode.Equals(contractorTypeCode))
                    continue;
                if (!template.BusinessTypeCode.Equals(businessTypeCode))
                    continue;
                if (!template.ProductCategoryCode.Equals(productCategoryCode))
                    continue;
                if (template.DocumentSubcategoryID != documentSubcategoryId)
                    continue;
                if (template.TemplateVersion.Equals(templateVersion))
                    return template;
            }
            return null;
        }
        #endregion
    }
}
