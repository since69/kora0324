﻿//
// BnpParibars.Scan.UI.Argos.RecognitionVerifier
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020 Thinkbox Co. Ltd
//

using System.Collections.Generic;
using System.Text.RegularExpressions;
using log4net;
using BnpParibars.Scan.UI.Argos.OcrJob;
using BnpParibars.Scan.Data;

namespace BnpParibars.Scan.UI.Argos
{
    public static class VerificationRule
    {
        /// <summary>
        /// 1이 한개 이상여야 함.
        /// </summary>
        public const string More = "MIS";
        /// <summary>
        /// 1이 한개 이하여야 함.
        /// </summary>
        public const string Less = "DUP";
        /// <summary>
        /// 1이 한개만 있어야 함.
        /// </summary>
        public const string One = "DOU";
        /// <summary>
        /// 모두 1이이어야 함.
        /// </summary>
        public const string All = "ALL";
        /// <summary>
        /// 별도의 값과 비교 필요.
        /// </summary>
        public const string Misc = "CHKCORR";
    }

    public static class RecognitionVerifier
    {
        #region VARIABLES
        //private static readonly ILog logger = LogManager.GetLogger("RecognitionVerifier");
        private static readonly System.StringSplitOptions splitOption = 
            System.StringSplitOptions.RemoveEmptyEntries;
        private static readonly Dictionary<string, string> imrCheckList = 
            new Dictionary<string, string>()
        {
            { "P_1", "GUA" },       // 상품분류 - 보장성
            { "P_2", "VARDEP" },    // 상품분류 - 변액저축
            { "P_3", "VARANN" },    // 상품분류 - 변액연금
            { "P_4", "DEP" },       // 상품분류 - 저축
            { "P_5", "ANN" },       // 상품분류 - 연금
            { "P_6", "SIM" },       // 상품분류 - 간편고지
            { "C_1", "PER" },       // 계약자분류 - 개인
            { "C_2", "CORP" },      // 계약자분류 - 법인
            { "C_3", "MINOR" }      // 계약자분류 - 미성년자
        };
        #endregion

        #region PRIVATE METHODS
        private static string VerifySpecialResult(string recogResult,
            string suplementCode)
        {
            string retVal = string.Empty;
            switch (suplementCode)
            {
                case "S10002005":
                    if (!recogResult.Equals("111000")
                        && !recogResult.Equals("000111")
                        && recogResult.Equals("111111"))
                        retVal = VerificationRule.More;
                    break;
                case "S10002007":
                    if (!recogResult.Equals("10111")
                        && !recogResult.Equals("01000"))
                        retVal = VerificationRule.More;
                    break;
                case "SI0019005":
                    if (!recogResult.Equals("111000")
                        && !recogResult.Equals("000111")
                        && recogResult.Equals("111111"))
                        retVal = VerificationRule.More;
                    break;
                case "SI0019007":
                    if (!recogResult.Equals("10111")
                        && !recogResult.Equals("01000"))
                        retVal = VerificationRule.More;
                    break;
                case "S20023001":
                    if (!recogResult.Equals("100")
                        && !recogResult.Equals("011"))
                        retVal = VerificationRule.More;
                    break;
                case "S20024005":
                    if (!recogResult.Equals("1100000")
                        && !recogResult.Equals("0011100")
                        && !recogResult.Equals("0000011")
                        && !recogResult.Equals("1111111"))
                        retVal = VerificationRule.More;
                    break;
                case "S20026004":
                    if (!recogResult.Equals("1110000000000")
                        && !recogResult.Equals("0001111000000")
                        && !recogResult.Equals("0000000111000")
                        && !recogResult.Equals("0000000000111")
                        && !recogResult.Equals("1111111111111"))
                        retVal = VerificationRule.More;
                    break;
                default: break;
            }
            return retVal;
        }

        private static string VerifyResult(string part, string id, 
            string result, string docCategoryCode, string docSubcategoryCode)
        {
            MatchCollection matches;
            switch (part.ToUpper())
            {
                case VerificationRule.More:
                    matches = Regex.Matches(result, "0");
                    if (matches.Count == result.Length)
                        return VerificationRule.More;
                    break;

                case VerificationRule.Less:
                    matches = Regex.Matches(result, "1");
                    if (matches.Count > 1)
                        return VerificationRule.Less;
                    break;

                case VerificationRule.One:
                    bool isMissiongOrDuplicate = false;
                    matches = Regex.Matches(result, "0");
                    if (matches.Count == result.Length)
                        isMissiongOrDuplicate = true;
                    matches = Regex.Matches(result, "1");
                    if (matches.Count > 1) isMissiongOrDuplicate = true;
                    if (isMissiongOrDuplicate) return VerificationRule.One;
                    break;

                case VerificationRule.All:
                    matches = Regex.Matches(result, "1");
                    if (matches.Count != result.Length)
                        return VerificationRule.All;
                    break;

                case VerificationRule.Misc:
                    string supplementCode = string.Format("{0}{1}{2}",
                        docCategoryCode, docSubcategoryCode, id);
                    return VerifySpecialResult(result, supplementCode);

                default: break;

            }
            return string.Empty;
        }

        private static void VerifyOcrDatas(JobData jobData)
        {
            foreach (OcrData ocrData in jobData.OcrDatas)
            {
                if (ocrData.Barcode == null) continue;
                if (ocrData.RecognitionDatas == null) continue;
                if (ocrData.DocumentCategoryCode.Equals("OT")
                    || ocrData.DocumentSubcategoryCode.Equals("0043"))
                    continue;

                foreach (RecognitionData data in ocrData.RecognitionDatas)
                {
                    string title = data.Name.Split('_')[0];
                    if (title.Equals("MASK")) continue;

                    imrCheckList.TryGetValue("P_" + ocrData.ProductCategoryCode,
                        out string needProduct);
                    imrCheckList.TryGetValue("C_" + ocrData.ContractorTypeCode,
                        out string needContractor);

                    bool ignoreVerification = true;
                    string[] parts = title.Split(new char[] { '^' }, splitOption);

                    if (parts.Length == 1)
                        ignoreVerification = false;

                    if (parts.Length > 1)
                    {
                        foreach (string part in parts)
                        {
                            if (part.Equals(needProduct) || part.Equals(needContractor))
                            {
                                ignoreVerification = !ignoreVerification 
                                    && ignoreVerification;
                            }
                        }
                    }

                    if (!ignoreVerification)
                    {
                        string result = VerifyResult(parts[0],
                        data.ID.Substring(2, 3),
                        data.Result,
                        ocrData.DocumentCategoryCode,
                        ocrData.DocumentSubcategoryCode);
                        if (!string.IsNullOrEmpty(result))
                            data.IsNeedSupplement = true;
                    }
                }
            }
        }

        private static void AddCaseSupplements(JobData jobData)
        {
            foreach (Case cs in jobData.Cases)
            {
                string bsn = cs.caseBusinessNumberList[0].businessNumber;
                foreach (OcrData ocrData in jobData.OcrDatas)
                {
                    if (bsn.Equals(ocrData.BusinessNumber) 
                        && ocrData.RecognitionDatas != null)
                    {
                        DocumentCategory cat = jobData.DocCategories.Find(
                            ocrData.DocumentCategoryCode);
                        DocumentSubcategory sub = jobData.DocSubcategories.Find(
                            ocrData.DocumentSubcategoryCode);
                        foreach (RecognitionData dat in ocrData.RecognitionDatas)
                        {
                            if (dat.IsNeedSupplement)
                            {
                                SupplementData sup = jobData.Supplements.Find(
                                    cat.documentCategoryId,
                                    sub.documentSubcategoryId,
                                    dat.SupplementCode);
                                cs.caseSupplementList.Add(new CaseSupplement()
                                {
                                    supplementId = sup.SupplementID,
                                    code = sup.Code,
                                    supplement = sup.Supplement,
                                    documentCategoryId = cat.documentCategoryId,
                                    documentCategoryCode = cat.code,
                                    documentCategoryName = cat.name,
                                    documentSubcategoryId = sub.documentSubcategoryId,
                                    documentSubcategoryCode = sub.code,
                                    documentSubcategoryName = sub.title
                                });
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region PUBLIC METHODS
        public static void Verify(JobData jobData)
        {
            VerifyOcrDatas(jobData);
            AddCaseSupplements(jobData);
        }

        public static void CheckMissingRequiredFiles(JobData jobData)
        {
            List<MandatoryDocument> mandatoryDocs = new List<MandatoryDocument>();

            foreach (Case cs in jobData.Cases)
            {
                mandatoryDocs.Clear();

                // Find case mandatory docs
                foreach (MandatoryDocument doc in jobData.MandatoryDocs)
                {
                    if (cs.businessTypeCode.Equals(doc.BusinessTypeCode)
                        && cs.productCategoryCode.Equals(doc.ProductCategoryCode)
                        && cs.contractorTypeCode.Equals(doc.ContractorTypeCode)
                        && doc.IsMandatory == 1)
                    {
                        mandatoryDocs.Add(doc);
                    }
                }

                // Add complement for missing document
                foreach (MandatoryDocument manDoc in mandatoryDocs)
                {
                    bool isExist = false;
                    foreach (CaseDocument csDoc in cs.caseDocumentList)
                    {
                        if (manDoc.DocumentCategoryID == csDoc.documentCategoryId
                            && manDoc.DocumentSubcategoryID == csDoc.documentSubcategoryId)
                        {
                            isExist = true;
                            break;
                        }
                    }

                    if (isExist) continue;

                    List<SupplementData> supplements = jobData.Supplements.Find(
                        manDoc.DocumentCategoryID, manDoc.DocumentSubcategoryID);
                    if (supplements == null || supplements.Count == 0) continue;

                    foreach (SupplementData s in supplements)
                    {
                        DocumentCategory category = jobData.DocCategories.Find(s.DocumentCategoryID);
                        DocumentSubcategory subcategory = jobData.DocSubcategories.Find(s.DocumentSubcategoryID);
                        cs.caseSupplementList.Add(new CaseSupplement()
                        {
                            documentCategoryId = category.documentCategoryId,
                            documentCategoryCode = category.code,
                            documentCategoryName = category.name,
                            documentSubcategoryId = subcategory.documentSubcategoryId,
                            documentSubcategoryCode = subcategory.code,
                            documentSubcategoryName = subcategory.title,
                            supplementId = s.SupplementID,
                            code = s.Code,
                            supplement = s.Supplement
                        });
                    }
                }
            }
        }
        #endregion
    }
}
