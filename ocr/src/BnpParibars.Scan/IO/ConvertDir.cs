﻿//
// BnpParibars.Scan.IO.ConvertDir
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.IO;

namespace BnpParibars.Scan.IO
{
    public class ConvertDir : BaseDir
    {
        #region CONSTRUCTORS
        /// <summary>
        /// converted dir copy here.
        /// </summary>
        /// <param name="name">The name of directory to created.</param>
        public ConvertDir(string name) : base(DirectoryTypes.Convert)
        {
            Name = name;
        }
        #endregion
    }
}
