﻿//
// BnpParibars.Scan.IO.DownloadFile
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibars.Scan.IO
{
    public class DownloadFile : BaseFile
    {
        #region CONSTRUCTORS
        public DownloadFile(DownloadDir dir, string name) : base(name)
        {
            Dir = dir;
        }
        #endregion
    }
}
