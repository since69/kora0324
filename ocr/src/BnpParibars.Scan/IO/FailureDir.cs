﻿//
// BnpParibars.Scan.IO.FailureDir
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.IO;

namespace BnpParibars.Scan.IO
{
    public class FailureDir : BaseDir
    {
        #region CONSTRUCTORS
        /// <summary>
        /// Recoginition failed image copy here.
        /// </summary>
        /// <param name="name">The name of directory to created.</param>
        public FailureDir(string name) : base(DirectoryTypes.Failure)
        {
            Name = name;
        }
        #endregion
    }
}
