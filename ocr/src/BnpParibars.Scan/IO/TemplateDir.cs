﻿//
// BnpParibars.Scan.IO.TemplateDir
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.IO;

namespace BnpParibars.Scan.IO
{
    public class TemplateDir : BaseDir
    {
        #region CONSTRUCTORS
        /// <summary>
        /// Recoginition failed image copy here.
        /// </summary>
        /// <param name="name">The name of directory to created.</param>
        public TemplateDir(string name) : base(DirectoryTypes.Template)
        {
            Name = name;
        }
        #endregion
    }
}
