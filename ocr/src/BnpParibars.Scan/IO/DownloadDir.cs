﻿//
// BnpParibars.Scan.IO.DownloadDir
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.IO;

namespace BnpParibars.Scan.IO
{
    public class DownloadDir : BaseDir
    {
        #region CONSTRUCTORS
        /// <summary>
        /// Recognition destination file download here.
        /// </summary>
        /// <param name="name">The name of directory to created.</param>
        public DownloadDir(string name) : base(DirectoryTypes.Download)
        {
            Name = name;
        }
        #endregion
    }
}
