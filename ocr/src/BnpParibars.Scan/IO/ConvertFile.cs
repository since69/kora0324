﻿//
// BnpParibars.Scan.IO.ConvertFile
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibars.Scan.IO
{
    public class ConvertFile : BaseFile
    {
        #region CONSTRUCTORS
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dir">Converted file(s) location</param>
        /// <param name="name">Converted file name</param>
        public ConvertFile(ConvertDir dir, string name) : base(name)
        {
            Dir = dir;
        }
        #endregion
    }
}
