﻿//
// BnpParibars.Scan.StringTemplate
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//


namespace BnpParibars.Scan
{
    public static class StringTemplate
    {
        #region PROPERTIES
        public static string DateTimeFileName { get; private set; } = "{0}_{1:D3}.{2}";
        /// <summary>
        /// URI template for FtpWebRequest.
        /// ftp://{ip}:{port}/{filename}
        /// </summary>
        public static string FtpUri { get; private set; } = "ftp://{0}:{1}/{2}/";

        public static string Report { get; private set; } = "| {0,-25} | {1,-12} | {2,-12} | {3,-12} |\n";
        #endregion
    }
}
