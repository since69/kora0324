﻿//
// BnpParibars.Scan.RecognitionData
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//


namespace BnpParibars.Scan
{
    /// <summary>
    /// Hold image recognition result data.
    /// </summary>
    public class RecognitionData
    {
        #region PROPERTIES
        /// <summary>
        /// 인식에 사용된 템플릿의 필드 아이디
        /// ex) 0100300
        /// </summary>
        public string ID { get; set; } = string.Empty;
        /// <summary>
        /// 인식에 사용된 템플릿의 필드 명
        /// ex) ALL^MINOR_미성년자 기/서명 및 연락처
        /// </summary>
        public string Name { get; set; } = string.Empty;
        /// <summary>
        /// 인식한 결과
        /// ex) 1111
        /// </summary>
        public string Result { get; set; } = string.Empty;
        /// <summary>
        /// The top coordination of the image identification area.
        /// </summary>
        public int Top { get; set; } = 0;
        /// <summary>
        /// The right coordination of the image identification area.
        /// </summary>
        public int Right { get; set; } = 0;
        /// <summary>
        /// The bottom coordination of the image identification area.
        /// </summary>
        public int Bottom { get; set; } = 0;
        /// <summary>
        /// The left coordination of the image identification area.
        /// </summary>
        public int Left { get; set; } = 0;
        /// <summary>
        /// 인식 결과를 검증한 보완코드 필요여부
        /// </summary>
        private bool isNeedSupplement = false;
        public bool IsNeedSupplement
        {
            get { return isNeedSupplement; }
            set
            {
                isNeedSupplement = value;
                SupplementCode = ID.Substring(2, 3);
            }
        }
        public string SupplementCode { get; private set; } = string.Empty;
        #endregion

        #region CONSTRUCTORS
        public RecognitionData() { }
        #endregion

        #region METHODS
        public override string ToString()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendFormat("{0,-5} | {1,-30} | {2,-3} | ",
                ID, Result, SupplementCode);
            sb.Append(Name);
            return sb.ToString();
        }
        #endregion
    }
}
