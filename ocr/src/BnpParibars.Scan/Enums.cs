﻿//
// BnpParibars.Scan.Enums
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System.ComponentModel;

namespace BnpParibars.Scan
{
    public enum CompressionTypes
    {
        UNCOMPRESSED = 1,
        RLE = 2,
        G3 = 3,
        G4 = 4,
        LZW = 5,
        DEFLATE = 6,
        JPEG = 7,
        JBIG = 34664,
        BPEG2000 = 34712,
        // JBIG2
    }

    public enum DirectoryTypes
    {
        Convert = 1,
        Download,
        Failure,
        Template
    }

    public enum ImageFormats
    {
        Unknown = 0,
        Bmp = 1,
        Jpeg = 2,
        Jpeg2000 = 3,
        Tiff = 4,
        Jbig = 5,
        Izt = 6,
        Gif = 7,
        Png = 8,
        Pdf = 11,
        Tig = 99,
        J2k = 100
    }

    public enum JobTypes
    {
        [Description("Unknown")]
        Unknown = 0,
        /// <summary>
        /// 웹팩스 청약서, Insurance Application.
        /// New Business. QR & Policy number start with 'G'.
        /// </summary>
        [Description("청약서")]
        Application = 1,
        /// <summary>
        /// 웹팩스 가입설계동의서, Plan Agreement.
        /// New Business. QR & Agreement number start with 'M'.
        /// </summary>
        [Description("가입설계동의서")]
        Agreement = 2,
        /// <summary>
        /// 웹팩스 제지급, Insurance Payment.
        /// Pos. Need to generate fake policy number start with 'P'.
        /// </summary>
        [Description("제지급")]
        Payment = 3,
        /// <summary>
        /// 웹팩스 보헙금, Insurance Benefit.
        /// </summary>
        [Description("보험금")]
        Benefit = 4,
        /// <summary>
        /// Digital home server. New business
        /// </summary>
        [Description("디지털홈")]
        Digital = 5,
        /// <summary>
        /// 국민은행, BSL
        /// </summary>
        [Description("국민은행")]
        KBank = 6,
        /// <summary>
        /// 하나은행, BSL
        /// </summary>
        [Description("하나은행")]
        HNBank = 7,
        /// <summary>
        /// 농협, BSL
        /// </summary>
        [Description("농협")]
        NHBank = 8,
        /// <summary>
        /// 제일은행, BSL
        /// </summary>
        [Description("제일은행")]
        SCBank = 9,
        /// <summary>
        /// 우리은행, BSL
        /// </summary>
        [Description("우리은행")]
        WRBank = 10,
        /// <summary>
        /// 신한은행, BSL
        /// </summary>
        [Description("신한은행")]
        CHBank = 11
    }

    public enum ServletTypes
    {
        Unknown = 0,
        /// <summary>
        /// Upload image file to ecm
        /// </summary>
        FileUpload = 1,
        /// <summary>
        /// Download image file from ecm
        /// </summary>
        FileDownload = 2,
        /// <summary>
        /// Delete image file from ecm
        /// </summary>
        FileDelete = 3,
        /// <summary>
        /// Inquery business type from db
        /// </summary>
        InquireBsnType = 4,
        /// <summary>
        /// Inquery product category form db
        /// </summary>
        InquirePrdCategory = 5,
        /// <summary>
        /// Inquery contractory type from db
        /// </summary>
        InquireCtrType = 6,
        /// <summary>
        /// Inquery document category type form db
        /// </summary>
        InquireDocCategory = 7,
        /// <summary>
        /// Inquery document subcategory type from db
        /// </summary>
        InquireDscCategory = 8,
        /// <summary>
        /// Inquery mandatory document from db
        /// </summary>
        InquireMdtDocument = 9,
        /// <summary>
        /// Inquery recognition template from db
        /// </summary>
        InqueryRcgTemplate = 10,
        /// <summary>
        /// Inquery supplement code list from db
        /// </summary>
        InquerySupplements = 11,
        /// <summary>
        /// Inquery contract information
        /// </summary>
        InqueryCotInformation = 12,
        /// <summary>
        /// Regist document status.
        /// </summary>
        RegistCotStatus = 13,
        /// <summary>
        /// Insert optical caracter recognition result case to db.
        /// </summary>
        InsertCase = 14
    }

    public enum BsnVerifyResults
    {
        /// <summary>
        /// Invliad insurance police number.
        /// </summary>
        Invalid = 1,
        /// <summary>
        /// Valid insurance police number.
        /// </summary>
        Valid = 2,
        /// <summary>
        /// Underwriting started
        /// </summary>
        AlreadyRegistered = 3,
    }
}
