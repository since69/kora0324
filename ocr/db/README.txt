
README

All schema related files are stored here.
Do not let database administrators change schemas directly 
in the database management system.
In addition to schema update, data addition is the same, and whenever a schema 
update or data addition is performed, the database administrator should write an 
SQL script for it, and put the scripts in this folder for version control.
This will allow you to port one version of the database to another.


[ Usage ]
- connect to oracle using sqlplus
sqlplus username/password@tns_alias
sqlplus username/password@ip:port/sid

- execute the script using sqlplus
sqlplus id/pw@ip:port/sid @script


[ Environment ]
- connection server
 ip: 10.10.77.52
 id: svckrcdfsvocr
 pw: Rlawngud@20

- db server
 ip: 10.10.74.46:1525
 id: scan
 pw: scan
sid: imgscanu


[ Create db using sqlplus ]

- operation
sqlplus SVCKRCDFSRVIMGSCAN/Zkelvmrhdrhddlf1@10.10.74.46:1525/imgscanu @instimgscandb

- local (notebook)
sqlplus scan/scan@localhost:1521/imgscanu @instimgscandb


[ Script ]

- image scan db install scripts
.\schema\instlegacyscandb.sql - remove and create legacy schema to image scan db.
.\schema\instimgscandb.sql - create table, constraints, etc to image scan db.
.\schema\removeimgscandb.sql - remove table, constraints(pk,fk,index), etc. call by instimgscandb.sql.
.\schema\ActivityType.ctl - user activity type record file for sql loader. call by instimgscandb.sql.
.\schema\Department.ctl - department record file for sql loader. call by instimgscandb.sql.
.\schema\Usr.ctl - user record file for sql loader. call by instimgscandb.sql.

