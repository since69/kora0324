/* -----------------------------------------------------------------------------
	File: instmigdb.sql
	
	Summary: Create the image scan migration OLTP database.
	
	Created at: 2020-09-10
	Created by: Joohyoung Kim

	Updated at: 2020-09-10
	Updated by: Joohyoung Kim
	
	Oracle version: 19C
--------------------------------------------------------------------------------
	Copyright (c) Thinkbox Corperation. All rights reserved.	
----------------------------------------------------------------------------- */

/*
 * HOW TO RUN THIS SCRIPT:
 *
 * 1. Login with Oracle account
 *
 * 2. Open terminal
 *
 * 3. Copy this script and the install like this.
 *
 * 		$ sqlplus 'SVCKRCDFSRVIMGSCAN/Zkelvmrhdrhddlf1!@10.10.74.46:1525/imgscanu' @instmigdb
 * 		$ sqlplus scan/scan@localhost:1521/imgscanu @instmigdb
 */

SET ECHO OFF
--  USE LINUX ONLY
SET SQLBLANKLINES ON


/*
 * Cleanup section
 */
PROMPT
PROMPT *** Cleanup Section
PROMPT

/*
 * Drop foreign keys.
 */

PROMPT
PROMPT *** Dropping Foreign keys
PROMPT

ALTER TABLE MG_CaseBusinessNumber DROP CONSTRAINT FK_MG_CaseBusinessNumber_Case_CaseID;
ALTER TABLE MG_CaseComment DROP CONSTRAINT FK_MG_CaseComment_Case_CaseID;
ALTER TABLE MG_CaseComment DROP CONSTRAINT FK_MG_CaseComment_Usr_CreateUserID;
ALTER TABLE MG_CaseDocument DROP CONSTRAINT FK_MG_CaseDocument_Case_CaseID;
ALTER TABLE MG_CaseDocument DROP CONSTRAINT FK_MG_CaseDocument_DocumentCategory_DocumentCategoryID;
ALTER TABLE MG_CaseDocument DROP CONSTRAINT FK_MG_CaseDocument_DocumentSubcategory_DocumentSubcategoryID;
ALTER TABLE MG_CaseDocumentPage DROP CONSTRAINT FK_MG_CaseDocumentPage_CaseDocument_CaseDocumentID;


/*
 * Drop indexes.
 */

PROMPT
PROMPT *** Dropping Indexes
PROMPT 

-- DROP INDEX IX_NAME;


/*
 * Drop primary keys.
 */

PROMPT
PROMPT *** Dropping Primary Keys
PROMPT

ALTER TABLE MG_Case DROP CONSTRAINT PK_MG_Case_CaseID;
ALTER TABLE MG_CaseBusinessNumber DROP CONSTRAINT PK_MG_CaseBusinessNumber_CI_BNT_BN;
ALTER TABLE MG_CaseComment DROP CONSTRAINT PK_MG_CaseComment_CaseID_CommentID;
ALTER TABLE MG_CaseDocument DROP CONSTRAINT PK_MG_CaseDocument_CaseDocumentID;
ALTER TABLE MG_CaseDocumentPage DROP CONSTRAINT PK_MG_CaseDocumentPage_CaseDocumentPageID;


/*
 * Drop sequence.
 */

PROMPT
PROMPT *** Dropping Sequence
PROMPT

DROP SEQUENCE SQ_MG_CaseEntityID;
DROP SEQUENCE SQ_MG_CommonEntityID;


/*
 * Drop tables.
 */

PROMPT
PROMPT *** Dropping Tables
PROMPT

DROP TABLE MG_Case;
DROP TABLE MG_CaseBusinessNumber;
DROP TABLE MG_CaseComment;
DROP TABLE MG_CaseDocument;
DROP TABLE MG_CaseDocumentPage;


/*
 * Create tables
 */

PROMPT
PROMPT *** Create Tables
PROMPT

CREATE TABLE MG_Case
(
	CaseID					NUMBER(38) NOT NULL
	, BusinessTypeCode		VARCHAR2(15) NOT NULL
	, ProductCategoryCode	VARCHAR2(15) NOT NULL
	, ContractorTypeCode	VARCHAR2(15) NOT NULL
	, Funnel				VARCHAR2(15) NOT NULL
	, Status				NUMBER(3) NOT NULL
	, CreatedDate			DATE DEFAULT SYSDATE NOT NULL
	, ModifiedDate			DATE DEFAULT SYSDATE NOT NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE MG_CaseBusinessNumber
(
	CaseID					NUMBER(38) NOT NULL
	, BusinessNumberType	NUMBER(3) NOT NULL
	, BusinessNumber 		VARCHAR2(20) NOT NULL
	, ArchiveStatus			NUMBER(3) DEFAULT 1 NOT NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE MG_CaseDocument
(
	CaseDocumentID 			NUMBER(38) NOT NULL
	, CaseID				NUMBER(38) NOT NULL
	, DocumentCategoryID 	NUMBER(38) NOT NULL
	, DocumentSubcategoryID	NUMBER(38) NOT NULL
	, TransferDocType 		NUMBER(3) NULL
	, TransferType 			NUMBER(3) NULL
	, TranferRequestedDate 	DATE NULL	 
	, DocumentChangeNumber 	NUMBER(3) NOT NULL
	, ModifiedDate			DATE DEFAULT SYSDATE NOT NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE MG_CaseDocumentPage
(
	CaseDocumentPageID 		NUMBER(38) NOT NULL
	, CaseDocumentID 		NUMBER(38) NOT NULL
	, PageNumber 			NUMBER(3) NOT NULL
	, IsEncrypted			NUMBER(3) DEFAULT 0 NOT NULL
	, ImageFileID 			VARCHAR2(20) NOT NULL
	, AnnotationFileID 		VARCHAR2(20) NULL
)
TABLESPACE TS_ECM_DT;



/*
 * Add Check Constraint
 */

PROMPT
PROMPT *** Adding Check Constraint
PROMPT 

ALTER TABLE MG_CaseBusinessNumber
	ADD CONSTRAINT CK_MG_CaseBusinessNumber_ArchiveStatus CHECK (ArchiveStatus IN (1,2,3));
ALTER TABLE MG_CaseBusinessNumber
	ADD CONSTRAINT CK_MG_CaseBusinessNumber_BusinessNumberType CHECK (BusinessNumberType IN (1,2,3));

ALTER TABLE MG_CaseDocumentPage
	ADD CONSTRAINT CK_MG_CaseDocumentPage_IsEncrypted CHECK (IsEncrypted IN (0,1));


/*
 * Add database sequence
 */

PROMPT
PROMPT *** Creating Database Sequence
PROMPT 

CREATE SEQUENCE SQ_MG_CaseEntityID
	START WITH 	 1
	INCREMENT BY 1
	NOCACHE	NOCYCLE;
CREATE SEQUENCE SQ_MG_CommonEntityID
	START WITH 	 1
	INCREMENT BY 1
	NOCACHE	NOCYCLE;


/*
 * Add Primary Key
 */

PROMPT
PROMPT *** Adding Primary Keys
PROMPT

ALTER TABLE MG_Case
	ADD CONSTRAINT PK_MG_Case_CaseID
	PRIMARY KEY (CaseID)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE MG_CaseBusinessNumber
	ADD CONSTRAINT PK_MG_CaseBusinessNumber_CI_BNT_BN
	PRIMARY KEY (CaseID, BusinessNumberType, BusinessNumber)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE MG_CaseComment
	ADD CONSTRAINT PK_MG_CaseComment_CaseID_CommentID
	PRIMARY KEY (CaseID, CommentID)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE MG_CaseDocument
	ADD CONSTRAINT PK_MG_CaseDocument_CaseDocumentID
	PRIMARY KEY (CaseDocumentID)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE MG_CaseDocumentPage
	ADD CONSTRAINT PK_MG_CaseDocumentPage_CaseDocumentPageID
	PRIMARY KEY (CaseDocumentPageID)
	USING INDEX TABLESPACE TS_ECM_DT;



/*
 * Add indexes
 */

PROMPT
PROMPT *** Adding Indexes
PROMPT 


/*
 * Create Foreign key constraints
 */

PROMPT
PROMPT *** Create Foreign key constraints
PROMPT

-- OCR Subject Area

ALTER TABLE MG_CaseBusinessNumber
	ADD CONSTRAINT FK_MG_CaseBusinessNumber_Case_CaseID
	FOREIGN KEY (CaseID) REFERENCES MG_Case (CaseID);	

ALTER TABLE MG_CaseComment
	ADD CONSTRAINT FK_MG_CaseComment_Case_CaseID
	FOREIGN KEY (CaseID) REFERENCES MG_Case (CaseID);	

ALTER TABLE MG_CaseDocument
	ADD CONSTRAINT FK_MG_CaseDocument_Case_CaseID
	FOREIGN KEY (CaseID) REFERENCES MG_Case (CaseID);	
ALTER TABLE MG_CaseDocument
	ADD CONSTRAINT FK_MG_CaseDocument_DocumentCategory_DocumentCategoryID
	FOREIGN KEY (DocumentCategoryID) REFERENCES DocumentCategory (DocumentCategoryID);	
ALTER TABLE MG_CaseDocument
	ADD CONSTRAINT FK_MG_CaseDocument_DocumentSubcategory_DocumentSubcategoryID
	FOREIGN KEY (DocumentSubcategoryID) REFERENCES DocumentSubcategory (DocumentSubcategoryID);	

ALTER TABLE MG_CaseDocumentPage
	ADD CONSTRAINT FK_MG_CaseDocumentPage_CaseDocument_CaseDocumentID
	FOREIGN KEY (CaseDocumentID) REFERENCES MG_CaseDocument (CaseDocumentID);	


/*
 * Add database triggers
 */

PROMPT
PROMPT *** Creating Table Triggers
PROMPT


/*
 * Add database views
 */

PROMPT
PROMPT *** Creating Database Views
PROMPT


/*
 * Add database functions
 */

PROMPT
PROMPT *** Creating Database Functions
PROMPT


/*
 * Add Extended Properties
 */

PROMPT
PROMPT *** Creating Extended Properties
PROMPT

COMMENT ON TABLE MG_Case									IS '';	
COMMENT ON COLUMN MG_Case.CaseID							IS 'Primary key for Case records.';
COMMENT ON COLUMN MG_Case.BusinessTypeCode					IS 'Business type code. Foreign key to BusinessType.BusinessTypeCode';
COMMENT ON COLUMN MG_Case.ProductCategoryCode				IS 'Product category code. Foreign key to ProductCategory.ProductCategoryCode';
COMMENT ON COLUMN MG_Case.ContractorTypeCode				IS 'Contractor type code. Foreign key to Contractor.ContractorTypeCode';
COMMENT ON COLUMN MG_Case.Funnel							IS 'Funnels of case. 1 - WebFax, 2 - PDF, 3 - Misc';
COMMENT ON COLUMN MG_Case.Status							IS 'Current status of case. 1 - Fax Reception, 2 - Complement, 3 - Arrived.';
COMMENT ON COLUMN MG_Case.CreatedDate						IS 'Date and time the record was created.';
COMMENT ON COLUMN MG_Case.ModifiedDate						IS 'Date and time the record was last updated.';

COMMENT ON TABLE MG_CaseBusinessNumber						IS 'Archiving status of document';
COMMENT ON COLUMN MG_CaseBusinessNumber.CaseID				IS 'Primary key for CaseBusinessNumber records. Foreign key to Case.CaseID.';
COMMENT ON COLUMN MG_CaseBusinessNumber.BusinessNumberType	IS 'Type of business number. 1 - Registration number, 2 - Insurance policy number';
COMMENT ON COLUMN MG_CaseBusinessNumber.BusinessNumber 		IS 'Registration number or Insurance policy number. depend on BusinessnumberType column.';
COMMENT ON COLUMN MG_CaseBusinessNumber.ArchiveStatus		IS 'Archiving status of document. 1 - Not archived, 2 - Archived, 3 - Discontinued';

COMMENT ON TABLE MG_CaseDocument							IS 'Cross-reference table mapping case to document in the DocumentCategory and DocumentSubcategory table.';
COMMENT ON COLUMN MG_CaseDocument.CaseID					IS 'Case identification number. Foreign key to Case.CaseID.';
COMMENT ON COLUMN MG_CaseDocument.DocumentCategoryID		IS 'Document category identification number. Foreign key to DocumentCategory.DocumentCategoryID.';
COMMENT ON COLUMN MG_CaseDocument.DocumentSubcategoryID		IS 'Document subcategory identification number. Foreign key to DocumentSubcategory.DocumentSubcategoryID.';
COMMENT ON COLUMN MG_CaseDocument.DocumentChangeNumber		IS '';
COMMENT ON COLUMN MG_CaseDocument.ModifiedDate				IS 'Date and time the record was last updated.';

COMMENT ON TABLE MG_CaseDocumentPage						IS '';
COMMENT ON COLUMN MG_CaseDocumentPage.CaseDocumentPageID	IS '';
COMMENT ON COLUMN MG_CaseDocumentPage.CaseDocumentID  		IS '';
COMMENT ON COLUMN MG_CaseDocumentPage.PageNumber 		 	IS '';
COMMENT ON COLUMN MG_CaseDocumentPage.IsEncrypted		 	IS '';
COMMENT ON COLUMN MG_CaseDocumentPage.ImageFileID 		 	IS '';
COMMENT ON COLUMN MG_CaseDocumentPage.AnnotationFileID 		IS '';
