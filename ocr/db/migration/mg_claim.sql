DECLARE
	cCommitPlateau CONSTANT PLS_INTEGER := 10000;
	vStartMsg NVARCHAR2(60) := 'Claim data migration started.';
	vFinishMsg NVARCHAR2(60) := 'Claim data migration finished.';
	vNumOfRecords NUMBER := 0;
	vNumOfProcessed NUMBER := 0;
	vWorkerID NVARCHAR2(50);
	vCaseID NUMBER := 0;
	vCaseDocumentID NUMBER := 0;
	vDocCategoryID NUMBER;
	vDocCategoryCode VARCHAR2(15) := 'C0';
	vDocSubcategoryID NUMBER;
	vDocSubcategoryCode VARCHAR2(15) := '0061';
	vBusinessTypeCode VARCHAR2(15) := 'C';
	vDocChangeNumber NUMBER;
	CURSOR csrClaim
	IS
	-- WITH Base AS
	-- (
		SELECT SSF_CLAIM_NO, SSF_CLAIM_NO_IDX, DEPT_CD, SSF_REG_DATE, REG_USER_ID
		FROM TBL_CONT_DATA_CLAIM
		ORDER BY SSF_REG_DATE;
	-- )
	-- SELECT * FROM Base WHERE ROWNUM < 5;
BEGIN
	DBMS_OUTPUT.PUT_LINE(vStartMsg);

	SELECT COUNT(*) INTO vNumOfRecords FROM Claims;

	SELECT DocumentCategoryID INTO vDocCategoryID
	FROM DocumentCategory
	WHERE Code = vDocCategoryCode;

	SELECT DocumentSubcategoryID INTO vDocSubcategoryID
	FROM DocumentSubcategory
	WHERE Code = vDocSubcategoryID;

	FOR vClaim IN csrClaim LOOP
		SELECT NVL((
			SELECT MAX(UserID)
			FROM UserID
			WHERE Usr.Name = vClaim.REG_USER_ID), 'scan01')
		INTO vWorkerID
		FROM DUAL;

		SELECT NVL(MAX(CS.CaseID), 0) INTO vCaseID
		FROM MG_Case CS	INNER JOIN MG_CaseBusinessNumber BN
				ON CS.CaseID = BN.CaseID
				AND CS.BusinessTypeCode = vBusinessTypeCode
		WHERE BN.BusinessNumber = vClaim.SSF_CLAIM_NO;

		IF (vCaseID = 0)
		THEN
			vCaseID := SQ_MG_CaseEntityID.NEXTVAL;
			INSERT INTO MG_Case
			(
				CaseID
				, BusinessTypeCode
				, ProductCategoryCode
				, ContractorTypeCode
				, Funnel
				, Status
				, CreatedDate
				, ModifiedDate
			)
			VALUES
			(
				vCaseID
				, 'C'
				, 'X'
				, 'X'
				, 'SCAN'
				, 7
				, TO_DATE(vClaim.SSF_REG_DATE, 'YYYYMMDD')
				, TO_DATE(vClaim.SSF_REG_DATE, 'YYYYMMDD')
			);

			INSERT INTO MG_CaseBusinessNumber
			(
				CaseID
				, BusinessNumberType
				, BusinessNumber
				, ArchiveStatus
			)
			VALUES
			(
				vCaseID
				, 2				-- Registration number
				, vClaim.SSF_CLAIM_NO
				, 1				-- Not archived
			);

			INSERT INTO MG_CaseBusinessNumber
			SELECT
				vCaseID
				, 1 			-- Business number type
				, A.SSF_ATST_NO	-- Policy number
				, 1
			FROM 
			(
				SELECT SSF_ATST_NO 
				FROM TBL_CLAIM_MAP 
				WHERE SSF_CLAIM_NO = vClaim.SSF_CLAIM_NO
			) A;

			vDocChangeNumber := 0;
		ELSE
			SELECT 
				NVL(MAX(DC.DocumentChangeNumber), 0) + 1 INTO vDocChangeNumber
			FROM 
				MG_Case CS
				INNER JOIN MG_CaseBusinessNumber BN
					ON CS.CaseID = BN.CaseID
					AND CS.CaseID = vCaseID
				INNER JOIN MG_CaseDocument DC
					ON CS.CaseID = DC.CaseID
					AND DC.DocumentCategoryID = vDocCategoryID
					AND DC.DocumentSubcategoryID = vDocSubcategoryID;
		END IF;

		vCaseDocumentID := SQ_MG_CaseEntityID.NEXTVAL;
		INSERT INTO MG_CaseDocument
		(
			CaseDocumentID
			, CaseID
			, DocumentCategoryID
			, DocumentSubcategoryID
			, DocumentChangeNumber
			, ModifiedDate
		)
		VALUES
		(
			vCaseDocumentID
			, vCaseID
			, vDocCategoryID
			, vDocSubcategoryID
			, 1
			, TO_DATE(vClaim.SSF_REG_DATE, 'YYYYMMDD')
		);

		INSERT INTO MG_CseDocumentPage
		SELECT
			SQ_MG_CaseEntityID.NEXTVAL
			, vCaseDocumentID
			, ROWNUM
			, 0					-- Not encrypted
			, A.ELEMENT_ID
			, NULL				-- Annotation is not exists
		FROM
		(
			SELECT TRIM(ELEMENT_ID)
			FROM TBL_EDMS_MAP
			WHERE SSF_ATST_NO = vClaim.SSF_CLAIM_NO
				AND SSF_ATST_NO_IDX = vClaim.SSF_CLAIM_NO_IDX
				AND DEPT_CD = vClaim.DEPT_CD
			ORDER BY ELEMENT_ID
		);

		vNumOfProcessed := vNumOfProcessed + 1;
		IF (MOD(vNumOfProcessed, cCommitPlateau) = 0)
		THEN
			DBMS_OUTPUT.PUT_LINE('Reached the commit point. '
				|| TO_CHAR(vNumOfProcessed));
			COMMIT;
		END IF;

		-- DBMS_OUTPUT.PUT_LINE('No: ' || vNumOfProcessed || ', '
		-- 	'Case id: ' || vCaseID || ', ' 
		-- 	'Claim no: ' || vClaim.SSF_CLAIM_NO || ', ' 
		-- 	'Reg date: ' || vClaim.SSF_REG_DATE);
	END LOOP;
	COMMIT;

	DBMS_OUTPUT.PUT_LINE(vFinishMsg);
	DBMS_OUTPUT.PUT_LINE(TO_CHAR(vNumOfProcessed) 
		|| ' OF '
		|| TO_CHAR(vNumOfRecords)
		|| ' Records have been processed.');
END;
