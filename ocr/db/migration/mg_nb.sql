DECLARE
	cCommitPlateau CONSTANT PLS_INTEGER := 10000;
	vStartMsg NVARCHAR2(60) := 'New business data migration started.';
	vFinishMsg NVARCHAR2(60) := 'New business data migration finished.';
	vNumOfRecords NUMBER := 0;
	vNumOfProcessed NUMBER := 0;
	vDeptCD VARCHAR2(9);
	vDeptNM VARCHAR2(400);
	vWorkerID NVARCHAR2(50);
	vCaseID NUMBER := 0;
	vCaseDocumentID NUMBER := 0;
	vDocCategoryID NUMBER;
	vDocSubcategoryID NUMBER;
	vBusinessTypeCode VARCHAR2(15);
	vDocCategoryCode VARCHAR2(15);
	vDocSubcategoryCode VARCHAR2(15);
	vDocChangeNumber NUMBER;
	CURSOR csrNewBusiness
	IS
	-- WITH Base AS
	-- (
		SELECT SSF_ATST_NO, SSF_ATST_NO_IDX, DEPT03_CD, DEPT_CD, SSF_REG_DATE, RG_USER_ID
		FROM TBL_CONT_DATA
		ORDER BY SSF_REG_DATE;
	-- )
	-- SELECT * FROM Base WHERE ROWNUM < 5;
BEGIN
	DBMS_OUTPUT.PUT_LINE(vStartMsg);
	SELECT COUNT(*) INTO vNumOfRecords FROM TBL_CONT_DATA;

	FOR vNewBusiness IN csrNewBusiness LOOP

		WITH Base AS
		(
			SELECT A.DEPT_CD, A.DEPT_NAME
			FROM
			(
				SELECT DEPT_CD, DEPT_NAME
				FROM TBL_POS_DEPT
				WHERE DEPT_LVL = 3
				GROUP BY DEPT_CD, DEPT_NAME
			) A
		)
		SELECT
			Base.DEPT_CD
			, Base.DEPT_NAME
			, CASE 
				WHEN Base.DEPT_CD IN ('024', '025', '026', '028', '029', '030', '031', '033'
					, '034', '035', '036', '038', '039', '040', '041', '043', '044', '045'
					, '046', '053', '054', '056', '106', '132', '133', '134', '139', '140'
					, '141', '142', '143', '144', '249', '250', '251', '252', '253', '254'
					, '255', '262', '263', '264', '265', '267', '310', '311', '312', '313'
					, '315', '358', '359', '360', '361', '363', '382', '383', '384', '385'
					, '387', '406', '407', '411', '430', '431', '432', '433', '434', '435'
					, '550', '551', '552', '553', '555', '574', '575', '576', '577', '578'
					, '579', '598', '599', '600', '601', '603', '627', '628', '629', '630'
					, '632', '724', '725', '726', '729', '772', '773', '774', '775', '777'
					, '796', '797', '798', '799', '800', '801', '820', '821', '824', '825'
					, '836', '838', '840', '881', '927', '938', '947', '956', '1171', '1181'
					, '1194', '1198', '1243', '1244', '1246', '1247')
				THEN 'B'
				WHEN Base.DEPT_CD IN ('502', '503', '504', '505', '506', '507', '818', '819'
					, '822', '823', '1151', '1153', '1091', '1093', '1095', '1099', '1122'
					, '1155', '1156', '1157', '1123', '1111', '1125', '1115', '1126', '1164'
					, '1216', '1264', '1288', '1310', '1314', '1331', '1335', '1352', '1415')
				THEN 'G'
				WHEN Base.DEPT_CD IN ('027', '032', '037', '042', '047', '083', '094', '110'
					, '111', '112', '114', '115', '119', '120', '121', '122', '123', '127'
					, '146', '147', '149', '150', '178', '179', '184', '185', '186', '190'
					, '191', '202', '203', '213', '214', '215', '216', '217', '218', '268'
					, '269', '277', '279', '325', '327', '373', '375', '397', '399', '445'
					, '447', '561', '562', '565', '567', '580', '581', '588', '589', '590'
					, '591', '604', '605', '739', '741', '787', '789', '802', '803', '811'
					, '813', '839', '841', '868', '869', '870', '871', '872', '873', '875'
					, '877', '965', '966', '1127', '1129', '1133', '1135', '1139', '1141'
					, '1144', '1146', '1147', '1150', '1182', '1206', '1208', '1231', '1235'
					, '1253', '1254', '1256', '1280', '1302')
				THEN 'P'
				ELSE 'T'
			END AS BSN_TYPE
			, CASE 
				WHEN Base.DEPT_CD IN ('024', '029', '034', '039', '044', '054', '143', '263'
					, '311', '359', '383', '407', '431', '551', '503', '575', '599', '628'
					, '725', '773', '797', '821', '819', '840', '1156', '1244')
				THEN 'D1'	
				WHEN Base.DEPT_CD IN ('505', '823', '836') THEN 'OT'
				WHEN Base.DEPT_CD IN ('027', '032', '037', '042', '047', '083', '094', '110'
					, '111', '112', '114', '115', '119', '120', '121', '122', '123', '127'
					, '146', '147', '149', '150', '178', '179', '184', '185', '186', '190'
					, '191', '202', '203', '213', '214', '215', '216', '217', '218', '268'
					, '269', '277', '279', '325', '327', '373', '375', '397', '399', '445'
					, '447', '561', '562', '565', '567', '580', '581', '588', '589', '590'
					, '591', '604', '605', '739', '741', '787', '789', '802', '803', '811'
					, '813', '839', '841', '868', '869', '870', '871', '872', '873', '875'
					, '877', '965', '966', '1127', '1129', '1133', '1135', '1139', '1141'
					, '1144', '1146', '1147', '1150', '1182', '1206', '1208', '1231', '1235'
					, '1253', '1254', '1256', '1280', '1302') 
				THEN 'P0'
				WHEN Base.DEPT_CD IN ('026', '028', '031', '033', '036', '038', '041', '043'
					, '046', '053', '056', '106', '132', '133', '134', '139', '140', '131'
					, '142', '144', '249', '250', '251', '252', '253', '254', '255', '262'
					, '264', '265', '267', '310', '312', '313', '315', '358', '360', '361'
					, '353', '382', '384', '385', '387', '406', '411', '430', '432', '433'
					, '435', '550', '502', '507', '552', '553', '555', '574', '576', '577'
					, '579', '598', '600', '601', '603', '672', '629', '630', '632', '724'
					, '726', '729', '772', '774', '775', '777', '796', '798', '799', '801'
					, '820', '825', '818', '838', '881', '927', '938', '947', '956', '1151'
					, '1091', '1095', '1099', '1122', '1155', '1123', '1111', '1125', '1115'
					, '1126', '1164', '1177', '1181', '1194', '1198', '1216', '1243', '1246'
					, '1247', '1264', '1288', '1310', '1314', '1331', '1335', '1352', '1415') 
				THEN 'S1'
				WHEN Base.DEPT_CD IN ('504') THEN 'S2'
				WHEN Base.DEPT_CD IN ('058', '059', '060', '062', '077', '092', '116', '117'
					, '166', '167', '168', '211', '247', '248', '286', '287', '288', '289'
					, '290', '292', '293', '526', '527', '529', '530', '532', '533', '616'
					, '618', '619', '620', '652', '653', '655', '656', '657', '658', '659'
					, '667', '669', '676', '677', '679', '680', '682', '683', '691', '693'
					, '700', '701', '703', '704', '705', '715', '717', '748', '749', '751'
					, '752', '753', '754', '755', '763', '765') 
				THEN 'T0'
				WHEN Base.DEPT_CD IN ('025', '030', '035', '040', '045', '434', '506'
					, '578', '800', '824', '822', '1153', '1093', '1157') 
				THEN 'UW'
				ELSE 'OT'
			END AS DOC_CATEGORY
			, CASE 
				WHEN Base.DEPT_CD IN ('502', '818', '1151', '1091', '1095', '1099', '1122'
					, '1155', '1123', '1111', '1125', '1115', '1126', '1164', '1216'
					, '1264', '1288', '1310', '1314', '1331', '1335', '1352', '1415') 
				THEN '0001'
				WHEN Base.DEPT_CD IN ('507') THEN '0016'
				WHEN Base.DEPT_CD IN ('504') THEN '0023'
				WHEN Base.DEPT_CD IN ('503', '819', '1156') THEN '0032'
				WHEN Base.DEPT_CD IN ('025', '030', '035', '040', '045', '434', '506'
					, '578', '800', '824', '822', '1153', '1093', '1157') 
				THEN '0040'
				WHEN Base.DEPT_CD IN ('505', '823', '836') THEN '0043'
				WHEN Base.DEPT_CD IN ('026', '028', '031', '033', '036', '038', '041'
					, '043', '046', '053', '056', '106', '132', '133', '134', '139', '140'
					, '141', '142', '144', '249', '250', '251', '252', '253', '254', '255'
					, '262', '264', '265', '267', '310', '312', '313', '315', '358', '360'
					, '361', '363', '382', '384', '385', '387', '406', '411', '430', '432'
					, '433', '435', '550', '552', '553', '555', '574', '576', '577', '579'
					, '598', '600', '601', '603', '627', '629', '630', '632', '724', '726'
					, '729', '772', '774', '775', '777', '796', '798', '799', '801', '820'
					, '825', '838', '881', '927', '938', '947', '956', '1157', '1181', '1194'
					, '1198', '1243', '1246', '1247')
				THEN '0044'
				WHEN Base.DEPT_CD IN ('024', '029', '034', '039', '044', '054', '143'
					, '263', '311', '359', '383', '407', '431', '551', '575', '599', '628'
					, '725', '773', '797', '821', '840', '1244')
				THEN '0045'
				WHEN Base.DEPT_CD IN ('027', '032', '037', '042', '047', '110', '111'
					, '112', '114', '115', '146', '147', '268', '269', '580', '581', '604'
					, '605', '802', '803', '839', '965', '966', '1127', '1129', '1133'
					, '1135', '1139', '1141', '1182', '1231')
				THEN '0059'
				WHEN Base.DEPT_CD IN ('083', '094', '119', '120', '121', '122', '123'
					, '127', '149', '150', '178', '179', '184', '185', '186', '190', '191'
					, '202', '203', '213', '214', '215', '216', '217', '218', '277', '279'
					, '325', '327', '373', '375', '397', '399', '445', '447', '561', '562'
					, '565', '567', '588', '589', '590', '591', '739', '741', '787', '789'
					, '811', '813', '841', '868', '869', '870', '871', '872', '873', '875'
					, '877', '1144', '1146', '1147', '1150', '1206', '1208', '1235', '1253'
					, '1254', '1256', '1280', '1302')
				THEN '0060'
				WHEN Base.DEPT_CD IN ('058', '059', '062', '092', '286', '287', '288'
					, '289', '290', '526', '527', '529', '530', '616', '618', '619', '620'
					, '652', '653', '655', '656', '657', '676', '677', '679', '680', '700'
					, '701', '703', '704', '705', '748', '749', '751', '752', '753')
				THEN '0064'
				WHEN Base.DEPT_CD IN ('060', '116', '247', '248', '292', '293', '532'
					, '533', '658', '659', '682', '683', '754', '755')
				THEN '0065'
				WHEN Base.DEPT_CD IN ('077', '117', '166', '167', '168', '211', '667'
					, '669', '691', '693', '715', '717', '763', '765')
				THEN '0066'
				ELSE '0043'
			END AS DOC_SUBCATEGORY
		INTO vDeptCD, vDeptNM, vBusinessTypeCode, vDocCategoryCode, vDocSubcategoryCode
		FROM Base
		WHERE Base.DEPT_CD = vNewBusiness.DEPT_CD;

		SELECT DocumentCategoryID INTO vDocCategoryID
		FROM DocumentCategory
		WHERE Code = vDocCategoryCode;

		SELECT DocumentSubcategoryID INTO vDocSubcategoryID
		FROM DocumentSubcategoryID
		WHERE Code = vDocSubcategoryCode;

		-- SELECT NVL(UserID, 'scan01') INTO vWorkerID
		-- FROM Usr
		-- WHERE Usr.Name = vNewBusiness.REG_USER_ID;

		SELECT NVL((
			SELECT UserID 
			FROM Usr 
			WHERE Usr.Name = vNewBusiness.REG_USER_ID), 'scan01') 
		INTO vWrokerID
		FROM DUAL;

		-- DBMS_OUTPUT.PUT_LINE(vDeptCD || ', ' || vDeptNM || ', '
		-- 	|| vBusinessTypeCode || ', '
		-- 	|| vDocCategoryCode || ', ' || vDocCategoryID || ', '
		-- 	|| vDocSubcategoryCode || ', ' || vDocSubcategoryID);

		SELECT NVL(MAX(CS.CaseID), 0) INTO vCaseID
		FROM MG_Case CS	INNER JOIN MG_CaseBusinessNumber BN
			ON CS.CaseID = BN.CaseID
			AND CS.BusinessTypeCode = vBusinessTypeCode
		WHERE BN.BusinessNumber = vNewBusiness.SSF_ATST_NO;

		IF (vCaseID = 0) 
		THEN
			vCaseID := SQ_MG_CaseEntityID.NEXTVAL;
			INSERT INTO MG_Case
			(
				CaseID
				, BusinessTypeCode
				, ProductCategoryCode
				, ContractorTypeCode
				, Funnel
				, Status
				, WorkerID
				, CreatedDate
				, ModifiedDate
			)
			VALUES
			(
				vCaseID
				, vBusinessTypeCode
				, 'X'
				, 'X'
				, 'SCAN'
				, 7
				, vWorkerID
				, TO_DATE(vNewBusiness.SSF_REG_DATE, 'YYYYMMDD')
				, TO_DATE(vNewBusiness.SSF_REG_DATE, 'YYYYMMDD')
			);

			INSERT INTO MG_CaseBusinessNumber
			(
				CaseID
				, BusinessNumberType
				, BusinessNumber
				, ArchiveStatus
			)
			VALUES
			(
				vCaseID
				, 1 	-- Policy number
				, vNewBusiness.SSF_ATST_NO
				, 1 	-- Not archived
			);
			
			vDocChangeNumber := 0;
		ELSE
			SELECT NVL(MAX(DC.DocumentChangeNumber), 0) + 1 INTO vDocChangeNumber
			FROM
				MG_Case CS
				INNER JOIN MG_CaseBusinessNumber BN
					ON CS.CaseID = BN.CaseID
					AND CS.CaseID = vCaseID
				INNER JOIN MG_CaseDocument DC
					ON CS.CaseID = DC.CaseID
					AND DC.DocumentCategoryID = vDocCategoryID
					AND DOC.DocumentSubcategoryID = vDocSubcategoryID;
		END IF;

		vCaseDocumentID := SQ_MG_CaseEntityID.NEXTVAL;
		INSERT INTO MG_CaseDocument
		(
			CaseDocumentID
			, CaseID
			, DocumentCategoryID
			, DocumentSubcategoryID
			, DocumentChangeNumber
			, ModifiedDate
		)
		VALUES
		(
			vCaseDocumentID
			, vCaseID
			, vDocCategoryID
			, vDocSubcategoryID
			, vDocChangeNumber
			, TO_DATE(vNewBusiness.SSF_REG_DATE, 'YYYYMMDD')
		);

		INSERT INTO MG_CaseDocumentPage
		(
			CaseDocumentPageID
			, CaseDocumentID
			, PageNumber
			, IsEncrypted
			, ImageFileID
			-- , AnnotationFileID
		)
		SELECT
			SQ_MG_CaseEntityID.NEXTVAL
			, vCaseDocumentID
			, 1
			, 0
			, NVL(A.ELEMENT_ID, '0')
		FROM
		(
			SELECT ELEMENT_ID
			FROM TBL_EDMS_MAP
			WHERE SSF_ATST_NO = vNewBusiness.SSF_ATST_NO
				AND SSF_ATST_NO_IDX = vNewBusiness.SSF_ATST_NO_IDX
				AND SSF_DEPT_CD = vNewBusiness.DEPT_CD
		) A;

		vNumOfProcessed := vNumOfProcessed + 1;
		IF (MOD(vNumOfProcessed, cCommitPlateau) = 0)
		THEN
			DBMS_OUTPUT.PUT_LINE('Reached the commit point. '
				|| TO_CHAR(vNumOfProcessed));
			COMMIT;
		END IF;
	END LOOP;
	COMMIT;

	DBMS_OUTPUT.PUT_LINE(vFinishMsg);
	DBMS_OUTPUT.PUT_LINE(TO_CHAR(vNumOfProcessed) 
		|| ' Of '
		|| TO_CHAR(vNumOfRecords)
		|| ' Records have been processed.');
END;