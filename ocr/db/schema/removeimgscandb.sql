/* -----------------------------------------------------------------------------
	File: removeimgscandb.sql
	
	Summary: Remove image scan db objects.
	
	Created at: 2020-07-16
	Created by: Joohyoung Kim

	Updated at: 2020-07-23
	Updated by: Joohyoung Kim
	
	Oracle version: 11GR2
	Script version: 1.9
--------------------------------------------------------------------------------
	Copyright (c) Thinkbox Corperation. All rights reserved.	
----------------------------------------------------------------------------- */

SET ECHO OFF

/*
 * Drop foreign keys.
 */

PROMPT
PROMPT *** Dropping Foreign keys
PROMPT

-- ALTER TABLE TBL_NAME DROP CONSTRAINT FK_NAME; 

-- Security Subject Area

ALTER TABLE UserActivity DROP CONSTRAINT FK_UserActivity_Usr_UserID;
ALTER TABLE UserActivity DROP CONSTRAINT FK_UserActivity_ActivityType_ActivityTypeID;

-- OCR Subject Area

ALTER TABLE Case DROP CONSTRAINT FK_Case_Usr_WorkerID;
ALTER TABLE CaseBusinessNumber DROP CONSTRAINT FK_CaseBusinessNumber_Case_CaseID;
ALTER TABLE CaseComment DROP CONSTRAINT FK_CaseComment_Case_CaseID;
ALTER TABLE CaseComment DROP CONSTRAINT FK_CaseComment_Usr_CreateUserID;
ALTER TABLE CaseDocument DROP CONSTRAINT FK_CaseDocument_Case_CaseID;
ALTER TABLE CaseDocument DROP CONSTRAINT FK_CaseDocument_DocumentCategory_DocumentCategoryID;
ALTER TABLE CaseDocument DROP CONSTRAINT FK_CaseDocument_DocumentSubcategory_DocumentSubcategoryID;
ALTER TABLE CaseDocumentPage DROP CONSTRAINT FK_CaseDocumentPage_CaseDocument_CaseDocumentID;
ALTER TABLE CaseSupplement DROP CONSTRAINT FK_CaseSupplement_Case_CaseID;
ALTER TABLE CaseSupplement DROP CONSTRAINT FK_CaseSupplement_Supplement_SupplementID;
ALTER TABLE MandatoryDocument DROP CONSTRAINT FK_MandatoryDocument_ContractorType_ContractorTypeCode;
ALTER TABLE MandatoryDocument DROP CONSTRAINT FK_MandatoryDocument_BusinessType_BusinessTypeCode;
ALTER TABLE MandatoryDocument DROP CONSTRAINT FK_MandatoryDocument_ProductCategory_ProductCategoryCode;
ALTER TABLE MandatoryDocument DROP CONSTRAINT FK_MandatoryDocument_DocumentCategory_DocumentCategoryID;
ALTER TABLE MandatoryDocument DROP CONSTRAINT FK_MandatoryDocument_DocumentSubcategory_DocumentSubcategoryID;
ALTER TABLE RecognitionTemplate DROP CONSTRAINT FK_RecognitionTemplate_ContractorType_ContractorTypeCode;
ALTER TABLE RecognitionTemplate DROP CONSTRAINT FK_RecognitionTemplate_BusinessType_BusinessTypeCode;
ALTER TABLE RecognitionTemplate DROP CONSTRAINT FK_RecognitionTemplate_ProductCategory_ProductCategoryCode;
ALTER TABLE RecognitionTemplate DROP CONSTRAINT FK_RecognitionTemplate_DocumentCategory_DocumentCategoryID;
ALTER TABLE RecognitionTemplate DROP CONSTRAINT FK_RecognitionTemplate_DocumentSubcategory_DocumentSubcategoryID;
ALTER TABLE Supplement DROP CONSTRAINT FK_Supplement_DocumentCategory_DocumentCategoryID;
ALTER TABLE Supplement DROP CONSTRAINT FK_Supplement_DocumentSubcategory_DocumentSubcategoryID;
ALTER TABLE SeperateArchiveHistory DROP CONSTRAINT FK_SeperateArchiveHistory_Case_CaseID;


/*
 * Drop indexes.
 */

PROMPT
PROMPT *** Dropping Indexes
PROMPT 

-- DROP INDEX IX_NAME;


/*
 * Drop primary keys.
 */

PROMPT
PROMPT *** Dropping Primary Keys
PROMPT

-- ALTER TABLE TBL_NAME DROP CONSTRAINT PK_NAME;

-- Security Subject Area

ALTER TABLE ActivityType DROP CONSTRAINT PK_ActivityType_ActivityTypeID;
ALTER TABLE Department DROP CONSTRAINT PK_Department_DepartmentID;
ALTER TABLE Usr DROP CONSTRAINT PK_Usr_UserID;
ALTER TABLE UserActivity DROP CONSTRAINT PK_UserActivity_UserID_ActivityTypeID_ActivityDate;

-- OCR Subject Area

ALTER TABLE BusinessType DROP CONSTRAINT PK_BusinessType_BusinessTypeCode;
ALTER TABLE Case DROP CONSTRAINT PK_Case_CaseID;
ALTER TABLE CaseBusinessNumber DROP CONSTRAINT PK_CaseBusinessNumber_CI_BNT_BN;
ALTER TABLE CaseComment DROP CONSTRAINT PK_CaseComment_CaseID_CommentID;
ALTER TABLE CaseDocument DROP CONSTRAINT PK_CaseDocument_CaseDocumentID;
ALTER TABLE CaseDocumentPage DROP CONSTRAINT PK_CaseDocumentPage_CaseDocumentPageID;
ALTER TABLE CaseSupplement DROP CONSTRAINT PK_CaseSupplement_CaseID_SupplementID;
ALTER TABLE ContractorType DROP CONSTRAINT PK_ContractorType_ContractorTypeCode;
ALTER TABLE DocumentCategory DROP CONSTRAINT PK_DocumentCategory_DocumentCategoryID;
ALTER TABLE DocumentSubcategory DROP CONSTRAINT PK_DocumentSubcategory_DocumentSubcategoryID;
ALTER TABLE MandatoryDocument DROP CONSTRAINT PK_MandatoryDocument_CTC_BTC_PCC_DCI_DSI;
ALTER TABLE ProductCategory DROP CONSTRAINT PK_ProductCategory_ProductCategoryCode;
ALTER TABLE RecognitionTemplate DROP CONSTRAINT PK_RecognitionTemplate_CTC_BTC_PCC_DCI_DSI_TV;
ALTER TABLE Supplement DROP CONSTRAINT PK_Supplement_SupplementID;
ALTER TABLE SeperateArchiveHistory DROP CONSTRAINT PK_SeperateArchiveHistory_CaseID;


/*
 * Drop sequence.
 */

PROMPT
PROMPT *** Dropping Sequence
PROMPT

DROP SEQUENCE SQ_CaseEntityID;
DROP SEQUENCE SQ_CommonEntityID;


 /*
 * Drop views.
 */

PROMPT
PROMPT *** Dropping Views
PROMPT

-- DROP MATERIALIZED VIEW MVW_NAME;
-- DROP VIEW VW_NAME;


/*
 * Drop functions.
 */

PROMPT
PROMPT *** Dropping Functions
PROMPT


/*
 * Drop tables.
 */

PROMPT
PROMPT *** Dropping Tables
PROMPT

-- Security Subject Area

DROP TABLE ActivityType;
DROP TABLE Department;
DROP TABLE Usr;
DROP TABLE UserActivity;

-- OCR Subject Area

DROP TABLE BusinessType;
DROP TABLE ContractorType;
DROP TABLE Case;
DROP TABLE CaseBusinessNumber;
DROP TABLE CaseComment;
DROP TABLE CaseDocument;
DROP TABLE CaseDocumentPage;
DROP TABLE CaseSupplement;
DROP TABLE DocumentCategory;
DROP TABLE DocumentSubcategory;
DROP TABLE MandatoryDocument;
DROP TABLE ProductCategory;
DROP TABLE RecognitionTemplate;
DROP TABLE Supplement;
DROP TABLE SeperateArchiveHistory;
DROP TABLE TB_SEP_CONT_LISTS;