LOAD DATA
INFILE *
REPLACE TRUNCATE INTO TABLE BusinessType
FIELDS TERMINATED BY ','
TRAILING NULLCOLS
(
	BusinessTypeCode
	, Name
)
BEGINDATA
B,Bancassurance
C,Claim
G,General Agency
M,Market
P,Pos
X,Unknown