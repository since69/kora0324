LOAD DATA
INFILE *
REPLACE TRUNCATE INTO TABLE Supplement
FIELDS TERMINATED BY ','
TRAILING NULLCOLS
(
	DocumentCategoryID
	, DocumentSubcategoryID
	, Code
	, Supplement
	, SupplementID "SQ_CommonEntityID.NEXTVAL"
)
BEGINDATA
11,22,001,청약서 인장상이 - 계약자
11,22,002,청약서 인장상이 - 피보험자
11,22,003,청약자 근무지 확인
11,22,004,청약일자
11,22,005,"계약자서명, 주피 서명"
11,22,006,청약서 법정대리인(친권자)확인 서명
11,23,001,계약자 휴대폰 수신여부 동의여부
11,23,002,계약자 이메일 수신여부 동의여부
11,23,003,피보험자 휴대폰 수신여부 동의여부
11,23,004,피보험자 이메일 수신여부 동의여부
11,23,005,계약자의 실명확인
11,23,006,보험료 실소유자 여부 확인
11,23,007,보험금 지정대리청구인 제도
11,23,008,보험수익자 지정/변경권 행사 관련 추가 약정
11,23,009,수익자지정동의서 (법정상속인 아닌경우) 작성
11,24,001,고지 체크 1번 의료행위 사실
11,24,002,고지 체크 2번 약물 상시복용
11,24,003,고지 체크 3번 추가검사 이력
11,24,004,"고지 체크 4번 입원,수술 이력"
11,24,005,고지 체크 5번 10대 질병 이력
11,24,006,고지 체크 6번 임신여부
11,24,007,고지 체크 9번 체격고지
11,24,008,고지 체크 10번 체격고지
11,24,009,고지 체크 11번 체격고지
11,24,010,계약전 알릴사항 상시 기재사항
11,25,001,고지 체크 1번 해외출국예정
11,25,002,"고지 체크 2번 부업,겸업여부"
11,25,003,고지 체크 3번 타보험사 가입여부
11,25,004,계약자 월 소득
11,25,005,계약전 알릴의무 따라쓰기
11,25,006,EDD이행을 위한 거래관련 기본정보 체크 목적
11,25,007,EDD이행을 위한 거래관련 기본정보 체크 자금원천
11,25,008,EDD이행을 위한 거래관련 기본정보 체크 속독원천
11,25,009,EDD이행을 위한 거래관련 기본정보 체크 추정자산
11,25,010,고객면담보고서 (모집인확인사항)
11,26,001,보험료 출금계좌정보 입력
11,26,002,자동이체일자
11,26,003,출금계좌 신청 체크
11,26,004,자동 송금신청 체크
11,26,005,출금이체 정보제공 동의
11,26,006,청약일자
11,26,007,"계약자서명, 주피 서명"
11,26,008,법정대리인(친권인) 서명
11,27,001,동의 체크
11,28,001,동의체크
11,28,002,청약일자
11,28,003,기본서명(계약자)
11,28,004,수익자 서명 (법정상속인 아닌경우)
11,28,005,법정 대리인 기/서명
11,29,001,해외납세 의무자 확인
11,29,002,계약자 기/서명
11,29,003,해외납세의무 여부확인서 - 법정대리인(친권자) 서명
11,29,004,본인확인서 - 고객인적사항
11,29,005,본인확인서 - 해외거주자 확인(가)
11,29,006,본인확인서 - 해외거주자 확인(나)
11,29,007,본인확인서 - 청약일자
11,29,008,본인확인 - 본인 서명
11,29,009,본인확인 - 법정대리인(친권자) 서명
11,29,010,판매인 서명 확인
11,29,011,판매인 관리자 서명 확인
11,30,001,가. 고객정보 - 전화번호,
11,30,002,조세목적상 거주지 - 납세자 번호 미제출 사유
11,30,003,나. 확인사항 1번 check
11,30,004,나. 확인사항 2번 check
11,30,005,나. 확인사항 3번 check
11,30,006,나. 확인사항 4번 check
11,30,007,나. 확인사항 5번 check
11,30,008,다. 본인확인 청약일자
11,30,009,다. 법인명 날인
11,30,010,다. 판매인 서명
11,31,001,추가확인 사항 - A FATCA 보고
11,31,002,추가확인 사항 - B 금융회사
11,31,003,추가확인 사항 - C 실질적 지배자
11,32,001,청약일자
11,32,002,계약자 서명
11,32,003,판매인 서명
12,27,001,동의 체크
12,28,001,동의체크
12,28,002,청약일자
12,28,003,기본서명(계약자)
12,28,004,수익자 서명 (법정상속인 아닌경우)
12,28,005,법정 대리인 기/서명
12,29,001,해외납세 의무자 확인
12,29,002,계약자 기/서명
12,29,003,해외납세의무 여부확인서 - 법정대리인(친권자) 서명
12,29,004,본인확인서 - 고객인적사항
12,29,005,본인확인서 - 해외거주자 확인(가)
12,29,006,본인확인서 - 해외거주자 확인(나)
12,29,007,본인확인서 - 청약일자
12,29,008,본인확인 - 본인 서명
12,29,009,본인확인 - 법정대리인(친권자) 서명
12,29,010,판매인 서명 확인
12,29,011,판매인 관리자 서명 확인
12,30,001,가. 고객정보 - 전화번호
12,30,002,조세목적상 거주지 - 납세자 번호 미제출 사유
12,30,003,나. 확인사항 1번 check
12,30,004,나. 확인사항 2번 check
12,30,005,나. 확인사항 3번 check
12,30,006,나. 확인사항 4번 check
12,30,007,나. 확인사항 5번 check
12,30,008,다. 본인확인 청약일자
12,30,009,다. 법인명 날인
12,30,010,다. 판매인 서명
12,31,001,추가확인 사항 - A FATCA 보고
12,31,002,추가확인 사항 - B 금융회사
12,31,003,추가확인 사항 - C 실질적 지배자
12,32,001,청약일자
12,32,002,계약자 서명
12,32,003,판매인 서명
11,35,001,청약일자
11,35,002,서명
11,36,001,청약일자
11,36,002,계약자 서명
11,36,003,친권자 기명 + 서명
11,37,001,청약일자
11,37,002,계약자 서명
11,37,003,친권자 기명 + 서명
12,39,001,청약일자
12,39,002,"계약자서명, 주피 서명"
12,39,003,청약서 법정대리인(친권자)확인 서명
12,39,004,고객면담보고서 (모집인확인사항)
12,40,001,계약자 휴대폰 수신여부 동의여부
12,40,002,계약자 이메일 수신여부 동의여부
12,40,003,피보험자 휴대폰 수신여부 동의여부
12,40,004,피보험자 이메일 수신여부 동의여부
12,40,005,계약자의 실명확인
12,40,006,보험료 실소유자 여부 확인
12,40,007,보험금 지정대리청구인 제도
12,40,008,보험수익자 지정/변경권 행사 관련 추가 약정
12,40,009,수익자지정동의서 (법정상속인 아닌경우) 작성
12,41,001,고지 체크 1번 의료행위 사실
12,41,002,고지 체크 2번 입원수술 이력
12,41,003,고지 체크 3번 암진단 이력
12,41,004,청약자 직업 / 근무지 확인
12,41,005,계약자 월 소득 & 피보험자 월소득
12,41,006,계약전 알릴의무 따라쓰기
12,42,001,보험료 출금계좌정보 입력
12,42,002,자동이체일자
12,42,003,송금계좌 신청 체크
12,42,004,자동 송금신청 체크
12,42,005,출금이체 정보제공 동의
12,42,006,청약일자
12,42,007,계약자서명
12,42,008,법정대리인(친권인) 서명
12,42,009,EDD이행을 위한 거래관련 기본정보 체크 목적
12,42,010,EDD이행을 위한 거래관련 기본정보 체크 자금원천
12,42,011,EDD이행을 위한 거래관련 기본정보 체크 속독원천
12,42,012,EDD이행을 위한 거래관련 기본정보 체크 추정자산
12,43,001,계약사항-계약일자
12,43,002,청약목적
12,43,003,청약일자
12,43,004,계약자 서명
12,43,005,친권자 기/서명 및 관계
12,43,006,모집인 서명
12,43,007,관리자 기/서명
11,43,001,계약사항-계약일자
11,43,002,청약목적
11,43,003,청약일자
11,43,004,계약자 서명
11,43,005,친권자 기/서명 및 관계
11,43,006,모집인 서명
11,43,007,관리자 기/서명
13,44,001,고객체크사항 1번 예 or 아니오 check
13,44,002,고객체크사항 2번 예 or 아니오 check
13,44,003,청약일자
13,44,004,계약자서명
13,44,005,비교안내 설명사항 체크
13,45,001,"성명, 실명번호"
13,45,002,계약자와의 관계
13,45,003,"직업, 직장명, 주소, 연락처"
13,45,004,실명확인증표 - 종류
13,45,005,실명확인증표 - 방법
13,45,006,EDD이행을 위한 거래관련 기본정보 체크 목적
13,45,007,EDD이행을 위한 거래관련 기본정보 체크 자금관련
13,45,008,EDD이행을 위한 거래관련 기본정보 체크 추정자산
13,45,009,작성일
13,45,010,작성자 서명
13,46,001,기본정보 - 법인구분
13,46,002,기본정보 - 법인단체명(국문)
13,46,003,기본정보 - 사업자등록번호
13,46,004,기본정보 - 법인등록번호
13,46,005,기본정보 - 설립국가
13,46,006,기본정보 - 전화번호
13,46,007,기본정보 - 주소(소재지)
13,46,008,기본정보 - 업종
13,46,009,대표자정보 - 성명 + 국적 + 생년월일 + 연락처 + 주소
13,46,010,추가정보 - 법인정보
13,46,011,추가정보 - 상장여부
13,46,012,추가정보 - 사업체 설립일
13,46,013,추가정보 - 거래목적
13,46,014,추가정보 - 거래자금원천
13,47,001,실제소유자확인
13,47,002,실제소유자 - 성명 + 생년월일 + 국적
13,47,003,대리인정보 - 성명 + 직위 + 실명번호
13,47,004,대리인정보 - 실명확인방법
13,47,005,작성일자
13,47,006,법인단체 인감 날인
13,48,001,청약일자
13,48,002,계약자 서명
13,48,003,친권자 기명 + 서명
13,49,001,보험계약자의 선택 따라쓰기
13,49,002,청약일자
13,49,003,계약자 서명
13,49,004,친권자 서명
13,50,001,소비자 유형확인 체크
13,50,002,청약일자
13,50,003,계약자 서명
13,50,004,친권자 기명 + 서명
13,51,001,주요제재국가 질문 항목 1
13,51,002,주요제재국가 질문 항목 2
13,51,003,주요제재국가 질문 항목 3
13,51,004,주요제재국가 질문 항목 4
13,51,005,"회사명, 날짜, 회사주소, 작성자, 서명"
13,52,001,청약변경사항 안내 고객확인 및 동의 6개 항목 check
13,52,002,변경신청일자
13,52,003,계약자 서명
13,52,004,지점 판매인 서명
13,52,005,지점 관리자 서명
14,54,001,계약일자
14,54,002,점검항목  체크
14,54,003,청약일자
14,54,004,모집인 기/서명
14,55,001,주요설명 내용 따라쓰기 4개 항목
14,55,002,통신수단에 의한 계약해지 신청 동의(법인고객 제외) 체크
14,84,001,주요설명 내용 따라쓰기 4개 항목
14,56,001,금융소비자 우선 실명확인 체크
14,56,002,통신수단에 의한 계약해지 신청 동의(법인고객 제외) 체크
14,57,001,보험판매인 따라쓰기
14,57,002,보험판매인 확인일자
14,57,003,보험판매인 서명
14,57,004,보험계약자 따라쓰기
14,57,005,보험계약자 확인일자
14,57,006,보험계약자 서명
14,57,007,통신수단에 의한 계약해지 신청 동의(법인고객 제외) 체크
14,58,001,보험판매인 따라쓰기
14,58,002,보험판매인 확인일자
14,58,003,보험판매인 서명
14,58,004,보험계약자 따라쓰기
14,58,005,보험계약자 확인일자
14,58,006,보험계약자 서명
15,59,001,펀드 자동변경 동의 체크
15,59,002,청약일자
15,59,003,계약자 서명
15,60,001,알림서비스 신청여부
15,60,002,신청일자
15,60,003,계약자 서명
21,83,001,동의 체크
21,83,002,동의자 서명
21,83,003,미성년자 기/서명 및 연락처
21,83,004,법정대리인 기명/관계/연락처/서명
21,83,005,동의일자
11,65,001,청약서 인장상이 - 계약자
11,65,002,청약서 인장상이 - 피보험자
11,65,003,청약서 직업 / 근무처 확인
11,65,004,청약서 p.1 계약전 알릴의무 확인
11,65,005,청약서 서명 확인
11,65,006,청약서 친권자 확인
11,65,007,청약서 p.2 계약자 주소 확인
11,65,008,청약서 p.2 피보험자 주소 확인
11,65,009,청약서 p.2 증권수령지 확인
11,65,010,청약서 p.2 우편물 수령지 확인
11,65,011,청약서 p.2 고객정보 확인
11,65,012,지정대리청구인 확인
11,65,013,수익자지정 추가약정 확인
11,65,014,고객확인정보 확인
11,65,015,EDD 기본정보 확인
11,65,016,실소유자 확인
11,65,017,청약서 p.2 계약전 알릴의무 기타사항 확인
11,65,018,면담보고서 확인
11,65,019,초회보험료 계좌정보 확인
11,65,020,자동이체계좌정보 확인
11,65,021,송금계좌정보 확인
11,65,022,출금이체 동의서 체크 확인
11,65,023,출금이체 동의서 계약자 확인
11,65,024,계약자와 예금주 상이건 확인
11,65,025,계약체결이행동의서 체크확인
11,65,026,계약체결이행동의서 계약관계자 확인
11,65,027,해외납세의무여부확인서 체크확인
11,65,028,해외납세의무여부확인서 계약자 확인
11,65,029,해외납세의무자 정보확인
11,65,030,해외납세의무여부확인서 판매인 확인
11,65,031,해외납세의무여부확인서 체크확인
11,65,032,해외납세의무여부확인서 계약자 확인
11,65,35,해외납세의무자 정보확인
11,65,36,해외납세의무여부확인서 판매인 확인
11,65,37,적합성진단표 정보확인서 계약자 확인
11,65,38,적합성진단표 정보확인서 친권자 확인
11,65,39,적합성진단표 결과확인 계약자 확인
11,65,40,적합성진단표 결과확인 친권자 확인
11,65,41,적합성진단표 부적합확인서 계약자 확인
11,65,42,적합성진단표 부적합확인서 친권자 확인
14,67,001,보험계약완전판매 체크리스트
14,67,002,모집인 기/서명
14,68,001,청약서 별지 서명 누락
14,69,001,상품설명서 주요설명내용 자필 확인
14,69,002,통신수단에 의한 계약해지 신청 동의(법인고객 제외) 체크
14,70,001,판매인서명
14,70,002,보험계약자 확인 - 따라쓰기
14,70,003,보험계약자 서명
14,70,004,친권자 기/서명
14,71,001,금융소비자 설명확인서 동의 여부
14,71,002,우선 설명 확인 - 따라쓰기
14,71,003,일반적 판매에 따라 설명 - 따라쓰기
14,71,004,보험계약자 서명
14,72,001,보험계약자 서명
14,72,002,친권자 기/서명
14,72,003,판매인 서명
15,74,001,동의여부
15,74,002,보험계약자 서명
15,75,001,동의여부
15,75,002,보험계약자 서명
15,76,001,고객체크사항 1번 예 or 아니오 check
15,76,002,고개체크사항 2번 예 or 아니오 check
15,76,003,보험계약자 서명
15,76,004,친권자 기/서명
15,77,001,주요제재국가 질문 항목 1
15,77,002,주요제재국가 질문 항목 2
15,77,003,주요제재국가 질문 항목 3
15,77,004,주요제재국가 질문 항목 4
15,77,005,"회사명, 날짜, 회사주소, 작성자, 서명"
15,78,001,계약일자
15,78,002,보험종류
15,78,003,증권번호
15,78,004,피보험자
15,78,005,사망시 수익자
15,78,006,청약일자
15,78,007,피보험자 기/서명
18,64,001,위임장 확인
18,64,002,법인서류 확인(담당자 확인)
18,64,003,외국인 실명증표 확인
18,64,004,친권자 서류 확인
18,64,005,위임자 신분증 확인
