LOAD DATA
INFILE *
REPLACE TRUNCATE INTO TABLE Department
FIELDS TERMINATED BY ','
TRAILING NULLCOLS
(
	Name
	, GroupName
	, DepartmentID "SQ_CommonEntityID.NEXTVAL"
)
BEGINDATA
BABS-KYC RM,BA Business Support
BABS-MCO,BA Business Support
Compilance,Compilance
CSC,Customer Consultation Center
GAB-GA Helpdesk,GA Planning & Support
OPS-Claim,OPS
OPS-CS,OPS
OPS-NB,OPS
HR&GA-GA,HR&GA-GA
