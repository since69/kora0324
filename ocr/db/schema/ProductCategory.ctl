LOAD DATA
INFILE *
REPLACE TRUNCATE INTO TABLE ProductCategory
FIELDS TERMINATED BY ','
TRAILING NULLCOLS
(
	ProductCategoryCode
	, Name
)
BEGINDATA
1,보장성
2,변액저축
3,변액연금
4,저축
5,연금
6,간편고지
X,알수없음
