LOAD DATA
INFILE *
REPLACE TRUNCATE INTO TABLE ActivityType
FIELDS TERMINATED BY ','
TRAILING NULLCOLS
(
	Name 
	, ActivityTypeID "SQ_CommonEntityID.NEXTVAL"
)
BEGINDATA
Login
Logout