LOAD DATA
INFILE *
REPLACE TRUNCATE INTO TABLE DocumentCategory
FIELDS TERMINATED BY ','
TRAILING NULLCOLS
(
	Code
	, Name
	, SortOrder
	, DocumentCategoryID "SQ_CommonEntityID.NEXTVAL"
)
BEGINDATA
S1,일반 청약서,1,
SI,SI 청약서,1,
S2,기타 청약서,2,
D1,상품설명서(1),3,
D2,상품설명서(2),4,
UW,심사(UW)보완서류,5,
PT,부분스캔,6,
OT,미분류(기타),7,
P0,제지급 서류,8,
C0,Claim 서류,9,
M0,가입설계동의서,10,
