/* -----------------------------------------------------------------------------
	File: instseparchivedb.sql
	
	Summary: Create the image scan seperate archive OLTP database.
	
	Created at: 2020-10-08
	Created by: Joohyoung Kim

	Updated at: 2020-11-23
	Updated by: Joohyoung Kim
	
	Oracle version: 19C
	Script version: 1.0
--------------------------------------------------------------------------------
	Copyright (c) Thinkbox Corperation. All rights reserved.	
----------------------------------------------------------------------------- */

/*
 * HOW TO RUN THIS SCRIPT:
 *
 * 1. Login with Oracle account
 *
 * 2. Open terminal
 *
 * 3. Copy this script and the install like this.
 *
 * 		$ sqlplus 'SVCKRCDFSRVIMGSCAN/Zkelvmrhdrhddlf1!@10.10.74.46:1525/imgscanu' @instimgscandb
 * 		$ sqlplus scan/scan@localhost:1521/imgscanu @instimgscandb
 */

SET ECHO OFF
--  USE LINUX ONLY
SET SQLBLANKLINES ON


/*
 * Create tables
 */

PROMPT
PROMPT *** Create Tables
PROMPT

CREATE TABLE CaseArchiveBusinessNumber
(
	CaseID					NUMBER(38) NOT NULL
	, BusinessNumberType	NUMBER(3) NOT NULL
	, BusinessNumber 		VARCHAR2(20) NOT NULL
	, ArchiveStatus			NUMBER(3) DEFAULT 1 NOT NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE CaseArchiveImage
(
	CaseID					NUMBER(38) NOT NULL
	, OriginalLocation		NVARCHAR2(256) NOT NULL
	, ArchivedLocation 		NVARCHAR2(256) NOT NULL
)
TABLESPACE TS_ECM_DT;


/*
 * Add Primary Key
 */

PROMPT
PROMPT *** Adding Primary Keys
PROMPT

ALTER TABLE CaseBusinessNumber
	ADD CONSTRAINT PK_CaseArchiveBusinessNumber_CaseID_BusinessNumber
	PRIMARY KEY (CaseID, BusinessNumber)
	USING INDEX TABLESPACE TS_XXX_DT;

ALTER TABLE CaseArchiveImage
	ADD CONSTRAINT PK_CaseArchiveImage_CaseID
	PRIMARY KEY (CaseID)
	USING INDEX TABLESPACE TS_XXX_DT;