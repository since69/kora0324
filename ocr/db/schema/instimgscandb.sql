/* -----------------------------------------------------------------------------
	File: instimgscandb.sql
	
	Summary: Create the image scan OLTP database.
	
	Created at: 2020-07-16
	Created by: Joohyoung Kim

	Updated at: 2020-07-23
	Updated by: Joohyoung Kim
	
	Oracle version: 19C
	Script version: 1.9
--------------------------------------------------------------------------------
	Copyright (c) Thinkbox Corperation. All rights reserved.	
----------------------------------------------------------------------------- */

/*
 * HOW TO RUN THIS SCRIPT:
 *
 * 1. Login with Oracle account
 *
 * 2. Open terminal
 *
 * 3. Copy this script and the install like this.
 *
 * 		$ sqlplus 'SVCKRCDFSRVIMGSCAN/Zkelvmrhdrhddlf1!@10.10.74.46:1525/imgscanu' @instimgscandb
 * 		$ sqlplus scan/scan@localhost:1521/imgscanu @instimgscandb
 */

SET ECHO OFF
--  USE LINUX ONLY
SET SQLBLANKLINES ON


/*
 * Cleanup section
 */
PROMPT
PROMPT *** Cleanup Section
PROMPT

@removeimgscandb


/*
 * Create tables
 */

PROMPT
PROMPT *** Create Tables
PROMPT

-- Security Subject Area

CREATE TABLE ActivityType
(
	ActivityTypeID			NUMBER(38) NOT NULL
	, Name 					NVARCHAR2(50) NOT NULL
	, ModifiedDate 			DATE DEFAULT SYSDATE NOT NULL	
)
TABLESPACE TS_ECM_DT;

CREATE TABLE Department
(
	DepartmentID			NUMBER(38) NOT NULL
	, Name					NVARCHAR2(50) NOT NULL
	, GroupName 			NVARCHAR2(50) NOT NULL
	, ModifiedDate			DATE DEFAULT SYSDATE NOT NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE Usr
(
	UserID					NVARCHAR2(50) NOT NULL
	, DepartmentID			NUMBER(38) NOT NULL
	, Name 					NVARCHAR2(50) NOT NULL
	, Password				VARCHAR2(50) NOT NULL
	, IPAddress				VARCHAR2(15) NOT NULL
	, IsAdministrable 		NUMBER(3) DEFAULT 0 NOT NULL 
	, IsScanable 			NUMBER(3) DEFAULT 0 NOT NULL 
	, IsInquiryable			NUMBER(3) DEFAULT 0 NOT NULL 
	, IsDeleteable 			NUMBER(3) DEFAULT 0 NOT NULL 
	, IsChangeable			NUMBER(3) DEFAULT 0 NOT NULL 
	, IsMaskable			NUMBER(3) DEFAULT 0 NOT NULL 
	, IsLocked				NUMBER(3) DEFAULT 0 NOT NULL 
	, ModifiedDate			DATE DEFAULT SYSDATE NOT NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE UserActivity
(
	UserID					NVARCHAR2(50) NOT NULL
	, ActivityTypeID		NUMBER(38) NOT NULL
	, ActivityDate 			DATE DEFAULT SYSDATE NOT NULL
)
TABLESPACE TS_ECM_DT;

-- OCR Subject Area

CREATE TABLE BusinessType
(
	BusinessTypeCode		VARCHAR2(15) NOT NULL
	, Name 					NVARCHAR2(50) NOT NULL
	, ModifiedDate			DATE DEFAULT SYSDATE NOT NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE ContractorType
(
	ContractorTypeCode		VARCHAR2(15) NOT NULL
	, Name 					NVARCHAR2(50) NOT NULL
	, ModifiedDate			DATE DEFAULT SYSDATE NOT NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE Case
(
	CaseID					NUMBER(38) NOT NULL
	, BusinessTypeCode		VARCHAR2(15) NOT NULL
	, ProductCategoryCode	VARCHAR2(15) NOT NULL
	, ContractorTypeCode	VARCHAR2(15) NOT NULL
	, Funnel				VARCHAR2(15) NOT NULL
	, Status				NUMBER(3) NOT NULL
	, WorkerID				NVARCHAR2(50) NOT NULL
	, CreatedDate			DATE DEFAULT SYSDATE NOT NULL
	, ModifiedDate			DATE DEFAULT SYSDATE NOT NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE CaseBusinessNumber
(
	CaseID					NUMBER(38) NOT NULL
	, BusinessNumberType	NUMBER(3) NOT NULL
	, BusinessNumber 		VARCHAR2(20) NOT NULL
	, ArchiveStatus			NUMBER(3) DEFAULT 1 NOT NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE CaseComment
(
	CaseID					NUMBER(38) NOT NULL
	, CommentID 			NUMBER(38) NOT NULL
	, CreateUserID 			NVARCHAR2(50) NOT NULL
	, CaseComment 			NVARCHAR2(600) NOT NULL
	, CreatedDate			DATE DEFAULT SYSDATE NOT NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE CaseDocument
(
	CaseDocumentID 			NUMBER(38) NOT NULL
	, CaseID				NUMBER(38) NOT NULL
	, DocumentCategoryID 	NUMBER(38) NOT NULL
	, DocumentSubcategoryID	NUMBER(38) NOT NULL
	, TransferDocType 		NUMBER(3) NULL
	, TransferType 			NUMBER(3) NULL
	, TransferRequestedDate DATE NULL	 
	, DocumentChangeNumber 	NUMBER(3) NOT NULL
	, ModifiedDate			DATE DEFAULT SYSDATE NOT NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE CaseDocumentPage
(
	CaseDocumentPageID 		NUMBER(38) NOT NULL
	, CaseDocumentID 		NUMBER(38) NOT NULL
	, PageNumber 			NUMBER(3) NOT NULL
	, IsEncrypted			NUMBER(3) DEFAULT 0 NOT NULL
	, ImageFileID 			VARCHAR2(20) NOT NULL
	, AnnotationFileID 		VARCHAR2(20) NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE CaseSupplement
(
	CaseID					NUMBER(38) NOT NULL
	, SupplementID 			NUMBER(38) NOT NULL
	, ModifiedDate			DATE DEFAULT SYSDATE NOT NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE DocumentCategory
(
	DocumentCategoryID 		NUMBER(38) NOT NULL
	, Code					VARCHAR2(15) NOT NULL
	, Name 					NVARCHAR2(50) NOT NULL
	, SortOrder 			NUMBER(3) NOT NULL
	, ModifiedDate			DATE DEFAULT SYSDATE NOT NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE DocumentSubcategory
(
	DocumentSubcategoryID	NUMBER(38) NOT NULL	 
	, Code 					VARCHAR2(15) NOT NULL
	, Title					NVARCHAR2(50) NOT NULL
	, SortOrder 			NUMBER(3) NOT NULL
	, ModifiedDate			DATE DEFAULT SYSDATE NOT NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE MandatoryDocument
(
	ContractorTypeCode		VARCHAR2(15) NOT NULL
	, BusinessTypeCode		VARCHAR2(15) NOT NULL
	, ProductCategoryCode	VARCHAR2(15) NOT NULL
	, DocumentCategoryID 	NUMBER(38) NOT NULL
	, DocumentSubcategoryID	NUMBER(38) NOT NULL	 
	, IsMandatory			NUMBER(3) DEFAULT 0 NOT NULL
	, ModifiedDate			DATE DEFAULT SYSDATE NOT NULL	
)
TABLESPACE TS_ECM_DT;

CREATE TABLE ProductCategory
(
	ProductCategoryCode		VARCHAR2(15) NOT NULL
	, Name 					NVARCHAR2(50) NOT NULL
	, ModifiedDate			DATE DEFAULT SYSDATE NOT NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE RecognitionTemplate
(
	ContractorTypeCode		VARCHAR2(15) NOT NULL
	, BusinessTypeCode		VARCHAR2(15) NOT NULL
	, ProductCategoryCode	VARCHAR2(15) NOT NULL
	, DocumentCategoryID 	NUMBER(38) NOT NULL
	, DocumentSubcategoryID	NUMBER(38) NOT NULL	 
	, TemplateVersion		VARCHAR2(15) NOT NULL
	, TemplateFileID		VARCHAR2(20) NOT NULL
	, ZoneDefinitionFileID	VARCHAR2(20) NOT NULL
	, ModifiedDate			DATE DEFAULT SYSDATE NOT NULL	
)
TABLESPACE TS_ECM_DT;

CREATE TABLE Supplement
(
	SupplementID 			NUMBER(38) NOT NULL	
	, DocumentCategoryID 	NUMBER(38) NOT NULL
	, DocumentSubcategoryID	NUMBER(38) NOT NULL	 
	, Code 					VARCHAR2(15) NOT NULL
	, Supplement			NVARCHAR2(200) NOT NULL
	, ModifiedDate			DATE DEFAULT SYSDATE NOT NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE SeperateArchiveHistory
(
	CaseID 					NUMBER(38) NOT NULL
	, BusinessNumber 		VARCHAR2(20) NOT NULL
	, ArchivedDate			DATE NOT NULL
	, DestructedDate		DATE NULL
)
TABLESPACE TS_ECM_DT;

CREATE TABLE TB_SEP_CONT_LISTS
(
	OPERATION_MONTH			VARCHAR2(6) NULL
	, CONTRACT_NO 			VARCHAR2(20) NULL
	, IS_EXECUTED 			NUMBER(3) DEFAULT 0 NOT NULL
)
TABLESPACE TS_ECM_DT;

/*
 * Add Check Constraint
 */

PROMPT
PROMPT *** Adding Check Constraint
PROMPT 

-- Security Subject Area

ALTER TABLE Usr
	ADD CONSTRAINT CK_User_IsScanable CHECK (IsScanable IN (0,1))
	ADD CONSTRAINT CK_User_IsInquiryable CHECK (IsInquiryable IN (0,1))
	ADD CONSTRAINT CK_User_IsDeleteable CHECK (IsDeleteable IN (0,1))
	ADD CONSTRAINT CK_User_IsChangeable CHECK (IsChangeable IN (0,1))
	ADD CONSTRAINT CK_User_IsMaskable CHECK (IsMaskable IN (0,1))
	ADD CONSTRAINT CK_User_IsLocked CHECK (IsLocked IN (0,1));

-- OCR Subject Area

ALTER TABLE CaseBusinessNumber
	ADD CONSTRAINT CK_CaseBusinessNumber_ArchiveStatus CHECK (ArchiveStatus IN (1,2,3));
ALTER TABLE CaseBusinessNumber
	ADD CONSTRAINT CK_CaseBusinessNumber_BusinessNumberType CHECK (BusinessNumberType IN (1,2,3));

ALTER TABLE CaseDocumentPage
	ADD CONSTRAINT CK_CaseDocumentPage_IsEncrypted CHECK (IsEncrypted IN (0,1));


/*
 * Add database sequence
 */

PROMPT
PROMPT *** Creating Database Sequence
PROMPT 

CREATE SEQUENCE SQ_CaseEntityID
	START WITH 	 1
	INCREMENT BY 1
	NOCACHE	NOCYCLE;
CREATE SEQUENCE SQ_CommonEntityID
	START WITH 	 1
	INCREMENT BY 1
	NOCACHE	NOCYCLE;


/*
* Load data
*/

PROMPT
PROMPT *** Loading Data
PROMPT

-- Linux
!sqlldr SVCKRCDFSRVIMGSCAN/Zkelvmrhdrhddlf1! control="Usr.ctl"
!sqlldr SVCKRCDFSRVIMGSCAN/Zkelvmrhdrhddlf1! control="ActivityType.ctl"
!sqlldr SVCKRCDFSRVIMGSCAN/Zkelvmrhdrhddlf1! control="BusinessType.ctl"
!sqlldr SVCKRCDFSRVIMGSCAN/Zkelvmrhdrhddlf1! control="ContractorType.ctl"
!sqlldr SVCKRCDFSRVIMGSCAN/Zkelvmrhdrhddlf1! control="Department.ctl"
!sqlldr SVCKRCDFSRVIMGSCAN/Zkelvmrhdrhddlf1! control="DocumentCategory.ctl"
!sqlldr SVCKRCDFSRVIMGSCAN/Zkelvmrhdrhddlf1! control="DocumentSubcategory.ctl"
!sqlldr SVCKRCDFSRVIMGSCAN/Zkelvmrhdrhddlf1! control="MandatoryDocument.ctl"
!sqlldr SVCKRCDFSRVIMGSCAN/Zkelvmrhdrhddlf1! control="ProductCategory.ctl"
!sqlldr SVCKRCDFSRVIMGSCAN/Zkelvmrhdrhddlf1! control="Supplement.ctl"


/*
 * Add Primary Key
 */

PROMPT
PROMPT *** Adding Primary Keys
PROMPT

-- Security Subject Area

ALTER TABLE ActivityType
	ADD CONSTRAINT PK_ActivityType_ActivityTypeID
	PRIMARY KEY (ActivityTypeID)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE Department
	ADD CONSTRAINT PK_Department_DepartmentID
	PRIMARY KEY (DepartmentID)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE Usr
	ADD CONSTRAINT PK_Usr_UserID
	PRIMARY KEY (UserID)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE UserActivity
	ADD CONSTRAINT PK_UserActivity_UserID_ActivityTypeID_ActivityDate
	PRIMARY KEY (UserID, ActivityTypeID, ActivityDate)
	USING INDEX TABLESPACE TS_ECM_DT;

-- OCR Subject Area

ALTER TABLE BusinessType
	ADD CONSTRAINT PK_BusinessType_BusinessTypeCode
	PRIMARY KEY (BusinessTypeCode)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE Case
	ADD CONSTRAINT PK_Case_CaseID
	PRIMARY KEY (CaseID)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE CaseBusinessNumber
	ADD CONSTRAINT PK_CaseBusinessNumber_CI_BNT_BN
	PRIMARY KEY (CaseID, BusinessNumberType, BusinessNumber)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE CaseComment
	ADD CONSTRAINT PK_CaseComment_CaseID_CommentID
	PRIMARY KEY (CaseID, CommentID)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE CaseDocument
	ADD CONSTRAINT PK_CaseDocument_CaseDocumentID
	PRIMARY KEY (CaseDocumentID)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE CaseDocumentPage
	ADD CONSTRAINT PK_CaseDocumentPage_CaseDocumentPageID
	PRIMARY KEY (CaseDocumentPageID)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE CaseSupplement
	ADD CONSTRAINT PK_CaseSupplement_CaseID_SupplementID
	PRIMARY KEY (CaseID, SupplementID)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE ContractorType
	ADD CONSTRAINT PK_ContractorType_ContractorTypeCode
	PRIMARY KEY (ContractorTypeCode)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE DocumentCategory
	ADD CONSTRAINT PK_DocumentCategory_DocumentCategoryID
	PRIMARY KEY (DocumentCategoryID)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE DocumentSubcategory
	ADD CONSTRAINT PK_DocumentSubcategory_DocumentSubcategoryID
	PRIMARY KEY (DocumentSubcategoryID)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE MandatoryDocument
	ADD CONSTRAINT PK_MandatoryDocument_CTC_BTC_PCC_DCI_DSI
	PRIMARY KEY (ContractorTypeCode, BusinessTypeCode, ProductCategoryCode, DocumentCategoryID, DocumentSubcategoryID)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE ProductCategory
	ADD CONSTRAINT PK_ProductCategory_ProductCategoryCode
	PRIMARY KEY (ProductCategoryCode)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE RecognitionTemplate
	ADD CONSTRAINT PK_RecognitionTemplate_CTC_BTC_PCC_DCI_DSI_TV
	PRIMARY KEY (ContractorTypeCode, BusinessTypeCode, ProductCategoryCode, DocumentCategoryID, DocumentSubcategoryID, TemplateVersion)	
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE Supplement
	ADD CONSTRAINT PK_Supplement_SupplementID
	PRIMARY KEY (SupplementID)
	USING INDEX TABLESPACE TS_ECM_DT;

ALTER TABLE SeperateArchiveHistory
	ADD CONSTRAINT PK_SeperateArchiveHistory_CaseID
	PRIMARY KEY (CaseID)
	USING INDEX TABLESPACE TS_ECM_DT;


/*
 * Add indexes
 */

PROMPT
PROMPT *** Adding Indexes
PROMPT 


/*
 * Create Foreign key constraints
 */

PROMPT
PROMPT *** Create Foreign key constraints
PROMPT

-- Security Subject Area

ALTER TABLE UserActivity
	ADD CONSTRAINT FK_UserActivity_Usr_UserID
	FOREIGN KEY (UserID) REFERENCES Usr (UserID);
ALTER TABLE UserActivity
	ADD CONSTRAINT FK_UserActivity_ActivityType_ActivityTypeID
	FOREIGN KEY (ActivityTypeID) REFERENCES ActivityType (ActivityTypeID);	

-- OCR Subject Area

ALTER TABLE Case
	ADD CONSTRAINT FK_Case_Usr_WorkerID
	FOREIGN_KEY (WorkerID) REFERENCES Usr (UserID);

ALTER TABLE CaseBusinessNumber
	ADD CONSTRAINT FK_CaseBusinessNumber_Case_CaseID
	FOREIGN KEY (CaseID) REFERENCES Case (CaseID);	

ALTER TABLE CaseComment
	ADD CONSTRAINT FK_CaseComment_Case_CaseID
	FOREIGN KEY (CaseID) REFERENCES Case (CaseID);	
ALTER TABLE CaseComment
	ADD CONSTRAINT FK_CaseComment_Usr_CreateUserID
	FOREIGN KEY (CreateUserID) REFERENCES Usr (UserID);	

ALTER TABLE CaseDocument
	ADD CONSTRAINT FK_CaseDocument_Case_CaseID
	FOREIGN KEY (CaseID) REFERENCES Case (CaseID);	
ALTER TABLE CaseDocument
	ADD CONSTRAINT FK_CaseDocument_DocumentCategory_DocumentCategoryID
	FOREIGN KEY (DocumentCategoryID) REFERENCES DocumentCategory (DocumentCategoryID);	
ALTER TABLE CaseDocument
	ADD CONSTRAINT FK_CaseDocument_DocumentSubcategory_DocumentSubcategoryID
	FOREIGN KEY (DocumentSubcategoryID) REFERENCES DocumentSubcategory (DocumentSubcategoryID);	

ALTER TABLE CaseDocumentPage
	ADD CONSTRAINT FK_CaseDocumentPage_CaseDocument_CaseDocumentID
	FOREIGN KEY (CaseDocumentID) REFERENCES CaseDocument (CaseDocumentID);	

ALTER TABLE CaseSupplement
	ADD CONSTRAINT FK_CaseSupplement_Case_CaseID
	FOREIGN KEY (CaseID) REFERENCES Case (CaseID);
ALTER TABLE CaseSupplement
	ADD CONSTRAINT FK_CaseSupplement_Supplement_SupplementID
	FOREIGN KEY (SupplementID) REFERENCES Supplement (SupplementID);	

ALTER TABLE MandatoryDocument
	ADD CONSTRAINT FK_MandatoryDocument_ContractorType_ContractorTypeCode
	FOREIGN KEY (ContractorTypeCode) REFERENCES ContractorType (ContractorTypeCode);			
ALTER TABLE MandatoryDocument
	ADD CONSTRAINT FK_MandatoryDocument_BusinessType_BusinessTypeCode
	FOREIGN KEY (BusinessTypeCode) REFERENCES BusinessType (BusinessTypeCode);
ALTER TABLE MandatoryDocument
	ADD CONSTRAINT FK_MandatoryDocument_ProductCategory_ProductCategoryCode
	FOREIGN KEY (ProductCategoryCode) REFERENCES ProductCategory (ProductCategoryCode);
ALTER TABLE MandatoryDocument
	ADD CONSTRAINT FK_MandatoryDocument_DocumentCategory_DocumentCategoryID
	FOREIGN KEY (DocumentCategoryID) REFERENCES DocumentCategory (DocumentCategoryID);
ALTER TABLE MandatoryDocument
	ADD CONSTRAINT FK_MandatoryDocument_DocumentSubcategory_DocumentSubcategoryID
	FOREIGN KEY (DocumentSubcategoryID) REFERENCES DocumentSubcategory (DocumentSubcategoryID);

ALTER TABLE RecognitionTemplate
	ADD CONSTRAINT FK_RecognitionTemplate_ContractorType_ContractorTypeCode
	FOREIGN KEY (ContractorTypeCode) REFERENCES ContractorType (ContractorTypeCode);			
ALTER TABLE RecognitionTemplate
	ADD CONSTRAINT FK_RecognitionTemplate_BusinessType_BusinessTypeCode
	FOREIGN KEY (BusinessTypeCode) REFERENCES BusinessType (BusinessTypeCode);
ALTER TABLE RecognitionTemplate
	ADD CONSTRAINT FK_RecognitionTemplate_ProductCategory_ProductCategoryCode
	FOREIGN KEY (ProductCategoryCode) REFERENCES ProductCategory (ProductCategoryCode);
ALTER TABLE RecognitionTemplate
	ADD CONSTRAINT FK_RecognitionTemplate_DocumentCategory_DocumentCategoryID
	FOREIGN KEY (DocumentCategoryID) REFERENCES DocumentCategory (DocumentCategoryID);	
ALTER TABLE RecognitionTemplate
	ADD CONSTRAINT FK_RecognitionTemplate_DocumentSubcategory_DocumentSubcategoryID
	FOREIGN KEY (DocumentSubcategoryID) REFERENCES DocumentSubcategory (DocumentSubcategoryID);	

ALTER TABLE Supplement
	ADD CONSTRAINT FK_Supplement_DocumentCategory_DocumentCategoryID
	FOREIGN KEY (DocumentCategoryID) REFERENCES DocumentCategory (DocumentCategoryID);
ALTER TABLE Supplement
	ADD CONSTRAINT FK_Supplement_DocumentSubcategory_DocumentSubcategoryID
	FOREIGN KEY (DocumentSubcategoryID) REFERENCES DocumentSubcategory (DocumentSubcategoryID);

ALTER TABLE SeperateArchiveHistory
	ADD CONSTRAINT FK_SeperateArchiveHistory_Case_CaseID
	FOREIGN KEY (CaseID) REFERENCES Case (CaseID);


/*
 * Add database triggers
 */

PROMPT
PROMPT *** Creating Table Triggers
PROMPT


/*
 * Add database views
 */

PROMPT
PROMPT *** Creating Database Views
PROMPT


/*
 * Add database functions
 */

PROMPT
PROMPT *** Creating Database Functions
PROMPT


/*
 * Add Extended Properties
 */

PROMPT
PROMPT *** Creating Extended Properties
PROMPT

COMMENT ON TABLE ActivityType								IS 'Types of activity stored in the UserActivity table.';
COMMENT ON COLUMN ActivityType.ActivityTypeID				IS 'Primary key for ActivityType records.';
COMMENT ON COLUMN ActivityType.Name							IS 'Activity type description. For example, Login, Logout, or ....';
COMMENT ON COLUMN ActivityType.ModifiedDate					IS 'Date and time the record was last updated.';

COMMENT ON TABLE Department									IS 'Lookup table containing the departments within the BNP Paribars cardif insurance company.';
COMMENT ON COLUMN Department.DepartmentID					IS 'Primary key for Department records';
COMMENT ON COLUMN Department.Name							IS 'Name of the department.';
COMMENT ON COLUMN Department.GroupName						IS 'Name of the group to which the department belongs.';
COMMENT ON COLUMN Department.ModifiedDate					IS 'Date and time the record was last updated.';

COMMENT ON TABLE Usr 										IS 'Contains user account information';
COMMENT ON COLUMN Usr.UserID								IS 'Primary key for Usr records.';
COMMENT ON COLUMN Usr.DepartmentID							IS 'Department identification number. Foreign key to Department.DepartmentID.';	
COMMENT ON COLUMN Usr.Name 									IS 'Name of user.';
COMMENT ON COLUMN Usr.Password								IS 'Password of user.';
COMMENT ON COLUMN Usr.IPAddress								IS 'User computer ip address';
COMMENT ON COLUMN Usr.IsAdministrable 						IS 'User have administrator permission. 0 - have not. 1 - have';
COMMENT ON COLUMN Usr.IsScanable 							IS 'User scan permission. 0 - have not. 1 - have';
COMMENT ON COLUMN Usr.IsInquiryable							IS 'User inquiry permission. 0 - have not. 1 - have';
COMMENT ON COLUMN Usr.IsDeleteable 							IS 'User delete permission. 0 - have not. 1 - have';
COMMENT ON COLUMN Usr.IsChangeable							IS 'User change permission. 0 - have not. 1 - have';
COMMENT ON COLUMN Usr.IsMaskable							IS 'User masking permission. 0 - have not. 1 - have';
COMMENT ON COLUMN Usr.IsLocked								IS 'User account locking status. 0 - Account is not locked, 1 - Account is locked';	 
COMMENT ON COLUMN Usr.ModifiedDate							IS 'Date and time the record was last updated.';

COMMENT ON TABLE UserActivity 								IS 'Cross-reference table mapping user to activity in the ActivityType table.';
COMMENT ON COLUMN UserActivity.UserID 						IS 'User identification name. Foreign key to Usr.UserID.';
COMMENT ON COLUMN UserActivity.ActivityTypeID 				IS 'Activity type identification number. Foreign key to ActivityType.ActivityTypeID';
COMMENT ON COLUMN UserActivity.ActivityDate 				IS 'Date and time the action occurred.';

COMMENT ON TABLE BusinessType 								IS 'Includes insurance job type information.';
COMMENT ON COLUMN BusinessType.BusinessTypeCode 			IS 'Code of business type.';
COMMENT ON COLUMN BusinessType.Name 						IS 'Name of business type.';
COMMENT ON COLUMN BusinessType.ModifiedDate 				IS 'Date and time the record was last updated.';

COMMENT ON TABLE ContractorType 							IS 'Includes contractor type information.';
COMMENT ON COLUMN ContractorType.ContractorTypeCode			IS 'Code of contractor type.';
COMMENT ON COLUMN ContractorType.Name 						IS 'Name of contractor type.';
COMMENT ON COLUMN ContractorType.ModifiedDate				IS 'Date and time the record was last updated.';

COMMENT ON TABLE Case 										IS '';	
COMMENT ON COLUMN Case.CaseID								IS 'Primary key for Case records.';
COMMENT ON COLUMN Case.BusinessTypeCode						IS 'Business type code. Foreign key to BusinessType.BusinessTypeCode';
COMMENT ON COLUMN Case.ProductCategoryCode					IS 'Product category code. Foreign key to ProductCategory.ProductCategoryCode';
COMMENT ON COLUMN Case.ContractorTypeCode					IS 'Contractor type code. Foreign key to Contractor.ContractorTypeCode';
COMMENT ON COLUMN Case.Funnel								IS 'Funnels of case. SCAN/FAX/PDF';
COMMENT ON COLUMN Case.Status								IS 'Current status of case. 1 - Fax Reception, 2 - Complement, 3 - Arrived.';
COMMENT ON COLUMN Case.CreatedDate							IS 'Date and time the record was created.';
COMMENT ON COLUMN Case.ModifiedDate							IS 'Date and time the record was last updated.';

COMMENT ON TABLE CaseBusinessNumber							IS 'Archiving status of document';
COMMENT ON COLUMN CaseBusinessNumber.CaseID					IS 'Primary key for CaseBusinessNumber records. Foreign key to Case.CaseID.';
COMMENT ON COLUMN CaseBusinessNumber.BusinessNumberType		IS 'Type of business number. 1 - Registration number, 2 - Insurance policy number';
COMMENT ON COLUMN CaseBusinessNumber.BusinessNumber 		IS 'Registration number or Insurance policy number. depend on BusinessnumberType column.';
COMMENT ON COLUMN CaseBusinessNumber.ArchiveStatus			IS 'Archiving status of document. 1 - Not archived, 2 - Archived, 3 - Discontinued';

COMMENT ON TABLE CaseComment								IS '';
COMMENT ON COLUMN CaseComment.CaseID						IS '';
COMMENT ON COLUMN CaseComment.CommentID 					IS '';
COMMENT ON COLUMN CaseComment.CreateUserID					IS '';
COMMENT ON COLUMN CaseComment.CaseComment 					IS '';
COMMENT ON COLUMN CaseComment.CreatedDate					IS 'Date and time the record was created.';

COMMENT ON TABLE CaseDocument 								IS 'Cross-reference table mapping case to document in the DocumentCategory and DocumentSubcategory table.';
COMMENT ON COLUMN CaseDocument.CaseID						IS 'Case identification number. Foreign key to Case.CaseID.';
COMMENT ON COLUMN CaseDocument.DocumentCategoryID			IS 'Document category identification number. Foreign key to DocumentCategory.DocumentCategoryID.';
COMMENT ON COLUMN CaseDocument.DocumentSubcategoryID		IS 'Document subcategory identification number. Foreign key to DocumentSubcategory.DocumentSubcategoryID.';
COMMENT ON COLUMN CaseDocument.DocumentChangeNumber			IS '';
COMMENT ON COLUMN CaseDocument.ModifiedDate					IS 'Date and time the record was last updated.';

COMMENT ON TABLE CaseDocumentPage 							IS '';
COMMENT ON COLUMN CaseDocumentPage.CaseDocumentPageID 		IS '';
COMMENT ON COLUMN CaseDocumentPage.CaseDocumentID  			IS '';
COMMENT ON COLUMN CaseDocumentPage.PageNumber 		 		IS '';
COMMENT ON COLUMN CaseDocumentPage.IsEncrypted		 		IS '';
COMMENT ON COLUMN CaseDocumentPage.ImageFileID 		 		IS '';
COMMENT ON COLUMN CaseDocumentPage.AnnotationFileID 		IS '';

COMMENT ON TABLE CaseSupplement 							IS 'Cross-reference table mapping supplement to case in the Case table.';
COMMENT ON COLUMN CaseSupplement.CaseID			 			IS 'Case identification number. Foreign key to Case.CaseID';
COMMENT ON COLUMN CaseSupplement.SupplementID  				IS 'Supplement identification number. Foreign key to Supplement.SupplementID';
COMMENT ON COLUMN CaseSupplement.ModifiedDate 				IS 'Date and time the record was last updated.';

COMMENT ON TABLE DocumentCategory 							IS '';
COMMENT ON COLUMN DocumentCategory.DocumentCategoryID 	 	IS 'Primary key for DocumentCategory records.';
COMMENT ON COLUMN DocumentCategory.Code 					IS '';
COMMENT ON COLUMN DocumentCategory.Name 				 	IS '';
COMMENT ON COLUMN DocumentCategory.SortOrder			 	IS '';
COMMENT ON COLUMN DocumentCategory.ModifiedDate		 		IS 'Date and time the record was last updated.';

COMMENT ON TABLE DocumentSubcategory 						IS '';
COMMENT ON COLUMN DocumentSubcategory.DocumentSubcategoryID IS 'Primary key for DocumentSubcategory records.';
COMMENT ON COLUMN DocumentSubcategory.Code 				 	IS '';
COMMENT ON COLUMN DocumentSubcategory.Title				 	IS '';
COMMENT ON COLUMN DocumentSubcategory.SortOrder			 	IS '';
COMMENT ON COLUMN DocumentSubcategory.ModifiedDate		 	IS 'Date and time the record was last updated.';

COMMENT ON TABLE MandatoryDocument 							IS '';
COMMENT ON COLUMN MandatoryDocument.ContractorTypeCode		IS '';
COMMENT ON COLUMN MandatoryDocument.BusinessTypeCode		IS '';
COMMENT ON COLUMN MandatoryDocument.ProductCategoryCode		IS '';
COMMENT ON COLUMN MandatoryDocument.DocumentCategoryID 		IS '';
COMMENT ON COLUMN MandatoryDocument.DocumentSubcategoryID	IS '';
COMMENT ON COLUMN MandatoryDocument.IsMandatory				IS '';
COMMENT ON COLUMN MandatoryDocument.ModifiedDate			IS 'Date and time the record was last updated.';

COMMENT ON TABLE ProductCategory 							IS 'Contains information to categorize insurance products';
COMMENT ON COLUMN ProductCategory.ProductCategoryCode 		IS 'Product category code.';
COMMENT ON COLUMN ProductCategory.Name 				 		IS 'The name of product category.';
COMMENT ON COLUMN ProductCategory.ModifiedDate		 		IS 'Date and time the record was last updated.';

COMMENT ON TABLE RecognitionTemplate 						IS 'Contains information such as specific areas for recognition by document type.';
COMMENT ON COLUMN RecognitionTemplate.ContractorTypeCode	IS 'Contractor type code. Foreign key to ContractorType.ContractorTypeCode.';
COMMENT ON COLUMN RecognitionTemplate.BusinessTypeCode		IS 'Business type code. Foreign key to BusinessType.BusinessTypeCode.';
COMMENT ON COLUMN RecognitionTemplate.ProductCategoryCode	IS 'Product category code. Foreign key to ProductCategory.Code';
COMMENT ON COLUMN RecognitionTemplate.DocumentCategoryID	IS 'Document category identification number. Foreign key to DocumentCategory.DocumentCategoryID';
COMMENT ON COLUMN RecognitionTemplate.DocumentSubcategoryID	IS 'Document subcategory identification number. Foreign key to DocumentSubcategory.DocumentSubcategoryID';
COMMENT ON COLUMN RecognitionTemplate.TemplateVersion		IS '';
COMMENT ON COLUMN RecognitionTemplate.TemplateFileID		IS '';
COMMENT ON COLUMN RecognitionTemplate.ZoneDefinitionFileID	IS '';
COMMENT ON COLUMN RecognitionTemplate.ModifiedDate			IS 'Date and time the record was last updated.';

COMMENT ON TABLE Supplement 								IS '';
COMMENT ON COLUMN Supplement.SupplementID 			 		IS 'Primary key for Supplement records.';
COMMENT ON COLUMN Supplement.DocumentCategoryID 	 		IS 'Document category identification number. Foreign key to DocumentCategory.DocumentCategoryID';
COMMENT ON COLUMN Supplement.DocumentSubcategoryID 			IS 'Document subcategory identification number. Foreign key to DocumentSubcategory.DocumentSubcategoryID';
COMMENT ON COLUMN Supplement.Code 					 		IS '';
COMMENT ON COLUMN Supplement.Supplement			 			IS '';
COMMENT ON COLUMN Supplement.ModifiedDate			 		IS 'Date and time the record was last updated.';