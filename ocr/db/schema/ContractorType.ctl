LOAD DATA
INFILE *
REPLACE TRUNCATE INTO TABLE ContractorType
FIELDS TERMINATED BY ','
TRAILING NULLCOLS
(
	ContractorTypeCode
	, Name
)
BEGINDATA
1,개인
2,법인
3,미성년자
X,알수없음
