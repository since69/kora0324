//
// https://www.bardev.com/2013/11/17/kickstart-c-custom-configuration/
//

using System;
using System.Collection.Generic;

namespace SchoolBlog
{
	public class SchoolConfig : ConfigurationSection
	{
		private static SchoolConfig _schoolConfig = 
			(SchoolConfig)ConfigurationManager.GetSection("school");
		public static SchoolConfig Settings { get { return _schoolConfig; } }

		[ConfigurationProperty("name")]
		public string Name { get { return (string)base["name"]; }}

		[ConfigurationProperty("address")]
		public AddressElement Address { get { return (AddressElement)base["address"]; }}
	}

	public class AddressElement : ConfigurationElement
	{
		[ConfigurationProperty("street", IsRequired = true)]
		public string Street { get { return (string)base["street"]; }}

		[ConfigurationProperty("city", IsRequired = true)]
		public string City { get { return (string)base["city"]; }}	

		[ConfigurationProperty("state", IsRequired = true)]
		public string State { get { return (string)base["state"]; }}	
	}

	public class CourseElement : ConfigurationElement
	{
		[ConfigurationProperty("title", IsRequired = true)]
		public string Title { get { return (string)base["title"]; }}

		[ConfigurationProperty("instructor", IsRequired = true)]
		public string Instructor { get { return (string)base["instructor"]; }}	

		[ConfigurationProperty("courses")]
		public CourseElementCollection Courses 
		{ 
			get { return (CourseElementCollection)base["course"]; }
		}
	}

	public class StudentElement : ConfigurationElement
	{
		[ConfigurationProperty("studentId", IsRequired = true)]
		public string StudentId { get { return (string)base["studentId"]; }}		
	}

	[ConfigurationCollection(typeof(CourseElement), AddItemName="course", 
		CollectionType="ConfigurationElementCollectionType.BasicMap")]
	public class CourseElementCollection : ConfigurationElementCollection
	{
		public ConfigurationElementCollectionType CollectionType 
		{
			get { return ConfigurationElementCollectionType.BasicMap; }
		}
		protected override string ElementName 
		{	
			get { return "course"; }
		}
		protected override ConfigurationElement CreateNewElement() 
		{ 
			return new CourseElement(); 
		}
		protected override GetElementKey(ConfigurationElement element)
		{
			return (element as CourseElement).Title;
		}
		public CourseElement this[int index]
		{
			get { return (CourseElement)base.BaseGet(index); }
			set 
			{
				if (base.BaseGet(index) != null)
					base.BaseRemoveAt(index);
				base.BaseAdd(index, value);
			}
		}
		public CourseElement this[string title]
		{
			get { return (CourseElement)base.BaseGet(title); }
		}
	}

	public class StudentElementCollection : ConfigurationElementCollection
	{
		protected override ConfigurationElement CreateNewElement()
		{
			return new StudentElement();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return (element as StudentElement).StudentId;
		}

		public StudentElement this[int index]
		{
			get { return (StudentElement)base.BaseGet(index); }
			set
			{
				if (base.BaseGet(index) != null)
				{
					base.BaseRemoveAt(index);
				}
				base.BaseAdd(index, value);
			}
		}

		public StudentElement this[stirng id]
		{
			get { return (StudentElement)base.BasetGet(id); }
		}
	}

	class Program
	{
		static void Main(string[] arg)
		{
			string schoolName = SchoolConfig.Settings.Name;
			string schoolStreet = SchoolConfig.Settings.Address.Street;
			string schoolCity = SchoolConfig.Settings.Address.City;
			string schoolState = SchoolConfig.Settings.Address.State;
			Console.WriteLine("School Name: {0}", schoolName);
			Console.WriteLine("School Address: {0} {1} {2}", schoolStreet, schoolCity, schoolState);

			int courseCount = SchoolConfig.Settings.Courses.Count;
			string firstCourseTitle = SchoolConfig.Settings.Courses[0].Title;
			string secondCourseInstructor = SchoolConfig.Settings.Courses[1].Instructor;
			Console.WriteLine("Course Count: {0}", coursesCount);
			Console.WriteLine("First Course Title: {0}", firstCourseTitle);			
			Console.WriteLine("Second Course Instructor: {0}", SecondCourseInstructor);
		}
	}
}