//=======================================================
/* Copyright (c) 1999-2016 Mobileleader Co., Ltd. All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited 
 * All information contained herein is, and remains the property of Mobileleader Co., Ltd.
 * The intellectual and technical concepts contained herein are proprietary to Mobileleader  Co., Ltd.
 * Dissemination of this information or reproduction of this material is 
 * strictly forbidden unless prior written permission is obtained from Mobileleader Co., Ltd.
 * Proprietary and Confidential.
 */
//=======================================================

// ECM_Imaging_Client_CLR_API.h

#pragma once

#include <stdexcept>
#include <Windows.h>

#include "ECM_Imaging_Client_API.h"

using namespace System;
using namespace System::Runtime::InteropServices;

#pragma managed

namespace ECM_Imaging_Client_CLR_API
{
	/**
	 * 상수 정의
	 */
	public ref class ECMIC_Constants
	{
	public:
		literal Int32 INVALID_SESSION_ID            = INVALID_SESSION;
		literal Int32 INVALID_TRANSACTION_ID        = INVALID_TRANSACTION;
		literal Int32 RET_OTHER_TRANSACTION_RUNNING = OTHER_TRANSACTION_RUNNING;
		literal Int32 ECMIC_EVENT_WINDOW_MESSAGE    = WM_ECMIC_EVENT;
	};

	/**
	 * Session ID Type
	 */
	public value struct ECMIC_SessionID
	{
	public:
		Int32 _value;
	public:
		ECMIC_SessionID(Int32 value) { _value = value; }
		bool Equals(Int32 value1) { return _value == value1; }
		bool Equals(ECMIC_SessionID value1) { return _value != value1._value; }
		String^ StrVal() { return _value.ToString(); }
	};

	/**
	 * Transaction ID Type
	 */
	public value struct ECMIC_TransactionID
	{
	public:
		Int32 _value;
	public:
		ECMIC_TransactionID(Int32 value) { _value = value; }
		bool Equals(Int32 value1) { return _value == value1; }
		bool Equals(ECMIC_SessionID value1) { return _value != value1._value; }
		String^ StrVal() { return _value.ToString(); }
	};

	/**
	 * Transaction 유형
	 */
	public enum class ECMIC_EN_TransactionType
	{
		TT_INITIAL                   = EN_TransactionType::TT_INITIAL,
		TT_UPLOAD                    = EN_TransactionType::TT_UPLOAD,
		TT_DOWNLOAD                  = EN_TransactionType::TT_DOWNLOAD,
		TT_UPDATE                    = EN_TransactionType::TT_UPDATE,
		TT_SOFT_DELETE               = EN_TransactionType::TT_SOFT_DELETE,
		TT_HARD_DELETE               = EN_TransactionType::TT_HARD_DELETE,
		TT_HISTORY_GET_LIST_SIZE     = EN_TransactionType::TT_HISTORY_GET_LIST_SIZE,
		TT_HISTORY_GET_LIST          = EN_TransactionType::TT_HISTORY_GET_LIST,
		TT_HISTORY_GET_BY_VERSION    = EN_TransactionType::TT_HISTORY_GET_BY_VERSION,
		TT_HISTORY_GET_BY_HISTORY_ID = EN_TransactionType::TT_HISTORY_GET_BY_HISTORY_ID,
		TT_REGISTER                  = EN_TransactionType::TT_REGISTER,
		TT_UPDATE2                   = EN_TransactionType::TT_UPDATE2,
		TT_MASS_REGISTER             = EN_TransactionType::TT_MASS_REGISTER,
		TT_SEARCH                    = EN_TransactionType::TT_SEARCH,
		TT_PING                      = EN_TransactionType::TT_PING
	};

	/**
	 * Transaction 상태
	 */
	public enum class ECMIC_EN_TransactionState
	{
		TS_INITIAL            = EN_TransactionState::TS_INITIAL,
		TS_INPROGRESS_SEND    = EN_TransactionState::TS_INPROGRESS_SEND,
		TS_INPROGRESS_RECEIVE = EN_TransactionState::TS_INPROGRESS_RECEIVE,
		TS_ENDED              = EN_TransactionState::TS_ENDED,
		TS_SUSPENDED          = EN_TransactionState::TS_SUSPENDED,
		TS_ABORTED            = EN_TransactionState::TS_ABORTED
	};

	/**
	 * Transaction 결과
	 */
	public enum class ECMIC_EN_TransactionResult
	{
		TR_UNKNOWN = EN_TransactionResult::TR_UNKNOWN,
		TR_SUCCESS = EN_TransactionResult::TR_SUCCESS,
		TR_FAILURE = EN_TransactionResult::TR_FAILURE
	};

	/**
	* Callback 함수에게 이벤트를 제공함에 있어 이벤트 간격의 유형 : 시간 간격, 진행율
	*/
	public enum class ECMIC_EN_EventIntervalType
	{
		IT_UNKNOWN        = EN_EventIntervalType::IT_UNKNOWN,
		IT_TIME_INTERVAL  = EN_EventIntervalType::IT_TIME_INTERVAL,
		IT_PROGRESS_RATIO = EN_EventIntervalType::IT_PROGRESS_RATIO
	};

	// cholh20150702 : FR_PATH_CREATE_FAIL 에러 추가 & 각 에러 코드 값 정의
	/**
	 * 트랜잭션 실패 이유
	 */
	public enum class ECMIC_EN_TransactionFailReason
	{
		FR_SUCCESS                   = EN_TransactionFailReason::FR_SUCCESS,

		// 개발 관련 에러
		FR_NOT_IMPLEMENTED           = EN_TransactionFailReason::FR_NOT_IMPLEMENTED,

		// 프로그램 논리에 의한 에러
		FR_OTHER_TRANSACTION_RUNNING = EN_TransactionFailReason::FR_OTHER_TRANSACTION_RUNNING,
		FR_ABORTED                   = EN_TransactionFailReason::FR_ABORTED,

		// 메모리 관련 에러
		FR_MEMORY_ALLOCATION_FAIL    = EN_TransactionFailReason::FR_MEMORY_ALLOCATION_FAIL,

		// 파일 I/O 관련 에러
		FR_PATH_CREATE_FAIL          = EN_TransactionFailReason::FR_PATH_CREATE_FAIL,
		FR_FILE_OPEN_FAIL            = EN_TransactionFailReason::FR_FILE_OPEN_FAIL,
		FR_FILE_READ_FAIL            = EN_TransactionFailReason::FR_FILE_READ_FAIL,

		// 소켓 관련 에러
		FR_SOCKET_SEND_FAIL = EN_TransactionFailReason::FR_SOCKET_SEND_FAIL,
		FR_SOCKET_RECV_FAIL          = EN_TransactionFailReason::FR_SOCKET_RECV_FAIL,

		// 서버 에러 코드 기본값
		FR_ERROR_RESPONSE            = EN_TransactionFailReason::FR_ERROR_RESPONSE,

		FR_UNKNOWN                   = EN_TransactionFailReason::FR_UNKNOWN
	};

	/**
	 * ECMIC_EVENT의 유형 정의
	 */
	public enum class ECMIC_EN_EventType
	{
		ET_TRANSACTION_STATE    = EN_ECMICEventType::ET_TRANSACTION_STATE,
		ET_TRANSACTION_PROGRESS = EN_ECMICEventType::ET_TRANSACTION_PROGRESS,
		ET_TRANSACTION_RESULT   = EN_ECMICEventType::ET_TRANSACTION_RESULT
	};

	/**
	 * Transaction 진행 상태
	 */
	public value struct ECMIC_ST_TransactionProgress
	{
	public:
		ECMIC_EN_TransactionState enState;
		LONG64                    lTotalSendBytes;
		LONG64                    lSendBytes;
		LONG64                    lTotalRecvBytes;
		LONG64                    lRecvBytes;
	};

	// cholh20150605 : RequestFileHistoryGetList 추가
	/**
	* 파일 수정 히스토리 레코드
	*/
	public value struct ECMIC_ST_FileHistoryInfo
	{
	public:
		// History ID
		String^ strFileHistoryID;

		// File ID
		String^ strFileID;

		// History ID에 해당하는 파일의 생성 시각
		String^ strCreateTime;

		// History ID에 해당하는 파일의 수정 시각
		String^ strModifyTime;

		// 삭제 여부 플래그
		bool    bDeleteFlag;

		// 파일 크기(바이트 크기)
		LONG64  lFileSize;

		// 파일의 버전 값
		LONG64  lFileVersion;
	};

	// cholh20150605 : RequestFileHistoryGetList 추가
	/**
	* 파일 수정 히스토리 목록
	*/
	public value struct ECMIC_ST_FileHistoryList
	{
	public:
		// File ID
		String^                           strFileID;

		// 파일 수정 히스토리 목록의 전체 크기
		int                               iHistoryListSize;

		// 다음 페이지에 포함된 파일 수정 히스토리 목록의 크기(파일 수정 히스토리 레코드의 개수)
		int                               iNextPageCountInPage;

		// 다음 페이지에 포함된 첫번째 레코드의 전체 파일 수정 히스토리 목록에서의 인덱스 값
		int                               iNextPageStartIndex;

		// 현재 페이지에 포함된 파일 수정 히스토리 목록의 크기(파일 수정 히스토리 레코드의 개수)
		int                               iCountInPage;

		// 현재 페이지에 포함된 파일 수정 히스토리 레코드들의 배열
		array<ECMIC_ST_FileHistoryInfo^>^ arHistoryInfo;
	};

	/**
	 * Transaction 수행 결과
	 */
	public value struct ECMIC_ST_TransactionResult
	{
	public:
		// 트랜잭션 수행 결과 코드 : ECMIC_EN_TransactionResult 참조
		ECMIC_EN_TransactionResult enResult;

		// 트랜잭션 수행 실패 시 - 실패 이유 문자열
		// 트랜잭션 수행 성공 시 - 각 함수 별 내용
		//   RequestUpload                    : FileID 문자열 : "FileID=<파일 ID>"
		//   RequestDownload                  : FileID 문자열 : "FileID=<파일 ID>"
		//   RequestUpdate                    : FileID 문자열 : "FileID=<파일 ID>"
		//   RequestSoftDelete                : FileID 문자열 : "FileID=<파일 ID>"
		//   RequestHardDelete                : FileID 문자열 : "FileID=<파일 ID>"
		//   RequestFileHistoryGetListSize    : 파일 수정 히스토리 목록 크기 문자열 : "HistoryListSize=<파일 수정 히스토리 목록 크기>"
		//   RequestFileHistoryGetList        : NULL
		//   RequestFileHistoryGetByVersion   : 파일 수정 정보 : "FileHistoryID=<파일 수정 히스토리 ID>,FileID=<파일 ID>,FileName=<파일 이름>,FileVersion=<파일 버전>,FileSize=<파일 크기>"
		//   RequestFileHistoryGetByHistoryID : 파일 수정 정보 : "FileHistoryID=<파일 수정 히스토리 ID>,FileID=<파일 ID>,FileName=<파일 이름>,FileVersion=<파일 버전>,FileSize=<파일 크기>"
		//   RequestPing                      : "Pong" 문자열 : "Message=Pong"
		String^                    strResultOrFailReason;

		// 트랜잭션 수행 실패 이유 코드 : ECMIC_EN_TransactionFailReason 참조
		int                        iErrorCode;

		// cholh20150605 : RequestFileHistoryGetList 추가
		// RequestFileHistoryGetList 수행 성공 시 파일 수정 히스토리 목록인 ECMIC_ST_FileHistoryList 구조체 인스턴스
		ECMIC_ST_FileHistoryList^  pFileHistoryList;
	};

	/**
	 * Callback 함수에 전달되는 이벤트에 들어 있는 transaction 진행 상태를 나타내는 정보
	 */
	public value struct ECMIC_ST_Event
	{
	public:
		ECMIC_EN_EventType            enEventType;
		SessionID                     iSessionID;
		TransactionID                 iTransactionID;
		ECMIC_EN_TransactionType      enTransactionType;
		ECMIC_EN_TransactionState     enTransactionState;
		ECMIC_ST_TransactionProgress^ pTransactionProgress;
		ECMIC_ST_TransactionResult^   pTransactionResult;
	};

	/**
	 * 이벤트를 수령하기 위한 callback 함수 타입 선언
	 */
	public delegate void ECMIC_CALLBACK_FUNCTION_T(ECMIC_ST_Event^ pEvent);

	public ref class ECMIC_API
	{
	private:
		static ECMIC_CALLBACK_FUNCTION_T^ m_pDelegateCallbackFunction;
	public:
		static void CallDelegateFunction(ECMIC_ST_Event^ pEvent);

	public:
		// Constructor
		ECMIC_API();


		// Wrapper Methods

		////////////////////////////////////////////////////////////////////////////////
		// Session 관련 함수들
		////////////////////////////////////////////////////////////////////////////////

		/**
		 * 지정된 서버의 IP, Port에 소켓 접속하여 성공하면 새로 session을 생성하고 그 ID를 리턴한다.
		 * 소켓 접속에 실패한 경우 INVALID_SESSION(-1)을 리턴한다.
		 *
		 * 주의 : 설정된 세션의 최대 개수를 넘는 경우 예고 없이 가장 먼저 생성된 세션이 없어지게 되므로 주의하여야 한다.
		 *       strUserPW의 문자열은 암호화되지 않은 문자열이다.
		 */
		ECMIC_SessionID Connect(String^ strServerIP, int iServerPort, String^ strUserID, String^ strUserPW);

		// cholh20150619
		/**
		* 지정된 서버의 IP, Port에 소켓 접속하여 성공하면 새로 session을 생성하고 그 ID를 리턴한다.
		* 소켓 접속에 실패한 경우 INVALID_SESSION(-1)을 리턴한다.
		*
		* 주의 : 설정된 세션의 최대 개수를 넘는 경우 예고 없이 가장 먼저 생성된 세션이 없어지게 되므로 주의하여야 한다.
		*       bPasswordCrypted 파라미터의 값이 false인 경우 strUserPW 문자열은 암호화되어 있지 않아야 한다.
		*       bPasswordCrypted 파라미터의 값이 true인 경우 strUserPW 문자열은 SHA-256, URL safe Base64 순서로 암호화되어 있어야 한다.
		*/
		ECMIC_SessionID ConnectEx(String^ strServerIP, int iServerPort, String^ strUserID, String^ strUserPW, bool bPasswordCrypted);

		/**
		 * 지정된 Session의 Socket Connection을 닫을 수 있는지 질문한다. 즉, 진행중인 transaction이 있는지 물어본다.
		 * 닫을 수 있으면 true를, 닫을 수 없으면 false를 리턴한다.
		 */
		bool QueryClose(ECMIC_SessionID iSessionID);

		/**
		 * 지정된 Session의 Socket Connection을 닫고 그 결과를 리턴한다. 성공한 경우 해당 session을 제거한다. 성공한 경우 true를, 실패한 경우 false를 리턴한다.
		 */
		bool Close(ECMIC_SessionID iSessionID);

		// cholh20150514
		/**
		 * 모든 세션을 종료하고, 시스템 자원을 반납한다. 성공한 경우 true를, 실패한 경우 false를 리턴한다.
		 */
		bool CloseAll();


		////////////////////////////////////////////////////////////////////////////////
		// Transaction 진행 상황 이벤트 관련 함수들
		////////////////////////////////////////////////////////////////////////////////

		/**
		 * 지정된 session에서 진행되는 transaction의 진행 상황 이벤트를 받게 될 callback 함수를 등록한다.
		 */
		bool RegisterCallbackFunction(ECMIC_SessionID iSessionID, ECMIC_CALLBACK_FUNCTION_T^ pCallbackFunction);

		/**
		 * 지정된 session에서 진행되는 transaction의 진행 상황 이벤트를 받게 될 CWnd*를 등록한다.
		 */
		bool RegisterCallbackWindow(ECMIC_SessionID iSessionID, IntPtr hWnd);

		///**
		// * Callback 함수가 transaction의 진행 상황 이벤트를 받는 간격의 유형(시간 간격 또는 진행율 간격)과 간격을 설정한다.
		// * IntervalType : 일정 시간 간격(ms) or 진행율 간격(%)
		// */
		//extern bool WINAPI ECMIC_SetProgressEventInterval(SessionID iSessionID, EN_EventIntervalType enEventIntervalType, int iIntervalValue);

		///**
		// * 진행 상황 이벤트 유형(시간 간격 또는 진행율 간격)
		// */
		//extern EN_EventIntervalType WINAPI ECMIC_GetProgressEventIntervalType(SessionID iSessionID);

		///**
		// * 진행 상황 이벤트 간격값(ms 또는 %)
		// */
		//extern int WINAPI ECMIC_GetProgressEventIntervalValue(SessionID iSessionID);


		////////////////////////////////////////////////////////////////////////////////
		// Transaction 요청 함수들
		////////////////////////////////////////////////////////////////////////////////

		/**
		 * szArchiveID로 지정된 아카이브에 szLocaFilePath로 지정되는 로컬 파일을 업로드 및 등록하도록 요청한다.
		 * iSessionID로 지정된 세션이 없거나 기타 이유로 요청을 하지 못하는 상황에서는 INVALID_SESSION(-1)을 리턴한다.
		 *
		 * 리턴값으로 트랜잭션을 ID를 받으면, 이를 이용하여 GetTransactionResult 함수를 호출하여 함수 수행 결과를 얻을 수 있다.
		 * 함수 수행 결과는 ECMIC_ST_TransactionResult 구조체의 인스턴스이다.
		 * 함수 수행 성공 여부는 ECMIC_ST_TransactionResult.enResult 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 코드는 ECMIC_ST_TransactionResult.iErrorCode 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 문자열은 ECMIC_ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		 * 함수 수행 성공 시 획득하기를 원하는 정보는 ECMIC_ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		 * 함수 수행 성공 시 ECMIC_ST_TransactionResult.strResultOrFailReason에 들어 있는 정보는 Field=Value,Field=Value,... 형태를 취하고 있다.
		 *
		 * 새로운 transaction을 시작하고, 그 ID를 리턴한다. 개별 transaction은 담당 thread가 존재하게 된다.
		 * 본 함수는 transaction 담당 thread를 생성하여 실행시키고 서버와 통신하여 결과가 결정되면 리턴한다.
		 * Transaction의 결과는 별도의 함수(ECMIC_GetTransactionResult)를 통하거나,
		 * RegisterCallbackFunction 함수를 통하여 등록한 callback 함수를 통하여 전달되는 이벤트를 통하여 알 수 있다. 또는
		 * RegisterCallbackWindow 함수를 통하여 등록한 윈도우에 대해
		 * WM_ECMIC_EVENT 윈도우메시지를 통하여 전달되는 ST_TransactionResult 구조체를 통해 알 수 있다.
		 */
		ECMIC_TransactionID RequestUpload(
			ECMIC_SessionID iSessionID, String^ strArchiveID, String^ strContentClass,
			String^ strPrefixPath, String^ strLocalFilePath);

		/**
		 * szFileID로 지정된 FileID에 해당하는 파일을 다운로드하도록 요청한다.
		 * iSessionID로 지정된 세션이 없거나 기타 이유로 요청을 하지 못하는 상황에서는 INVALID_SESSION(-1)을 리턴한다.
		 *
		 * 리턴값으로 트랜잭션을 ID를 받으면, 이를 이용하여 GetTransactionResult 함수를 호출하여 함수 수행 결과를 얻을 수 있다.
		 * 함수 수행 결과는 ECMIC_ST_TransactionResult 구조체의 인스턴스이다.
		 * 함수 수행 성공 여부는 ECMIC_ST_TransactionResult.enResult 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 코드는 ECMIC_ST_TransactionResult.iErrorCode 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 문자열은 ECMIC_ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		 * 함수 수행 성공 시 획득하기를 원하는 정보는 ECMIC_ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		 * 함수 수행 성공 시 ECMIC_ST_TransactionResult.strResultOrFailReason에 들어 있는 정보는 Field=Value,Field=Value,... 형태를 취하고 있다.
		 *
		 * 새로운 transaction을 시작하고, 그 ID를 리턴한다. 개별 transaction은 담당 thread가 존재하게 된다.
		 * 본 함수는 transaction 담당 thread를 생성하여 실행시키고 서버와 통신하여 결과가 결정되면 리턴한다.
		 * Transaction의 결과는 별도의 함수(ECMIC_GetTransactionResult)를 통하거나,
		 * ECMIC_RegisterCallbackFunction 함수를 통하여 등록한 callback 함수를 통하여 전달되는 이벤트를 통하여 알 수 있다. 또는
		 * ECMIC_RegisterCallbackWindow 함수를 통하여 등록한 윈도우에 대해
		 * WM_ECMIC_EVENT 윈도우메시지를 통하여 전달되는 ST_TransactionResult 구조체를 통해 알 수 있다.
		 */
		ECMIC_TransactionID RequestDownload(
			ECMIC_SessionID iSessionID, String^ strFileID, String^ strLocalFilePath);

		// cholh20150605 : 기능 추가
		/**
		 * strArchiveID로 지정된 아카이브에 strFileID로 지정된 FileID에 해당하는 파일을 strLocalFilePath로 지정되는 파일로 업데이트한다.
		 * iSessionID로 지정된 세션이 없거나 기타 이유로 요청을 하지 못하는 상황에서는 INVALID_SESSION(-1)을 리턴한다.
		 *
		 * 리턴값으로 트랜잭션을 ID를 받으면, 이를 이용하여 GetTransactionResult 함수를 호출하여 함수 수행 결과를 얻을 수 있다.
		 * 함수 수행 결과는 ECMIC_ST_TransactionResult 구조체의 인스턴스이다.
		 * 함수 수행 성공 여부는 ECMIC_ST_TransactionResult.enResult 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 코드는 ECMIC_ST_TransactionResult.iErrorCode 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 문자열은 ECMIC_ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		 * 함수 수행 성공 시 획득하기를 원하는 정보는 ECMIC_ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		 * 함수 수행 성공 시 ECMIC_ST_TransactionResult.strResultOrFailReason에 들어 있는 정보는 Field=Value,Field=Value,... 형태를 취하고 있다.
		 *
		 * 새로운 transaction을 시작하고, 그 ID를 리턴한다. 개별 transaction은 담당 thread가 존재하게 된다.
		 * 본 함수는 transaction 담당 thread를 생성하여 실행시키고 서버와 통신하여 결과가 결정되면 리턴한다.
		 * Transaction의 결과는 별도의 함수(ECMIC_GetTransactionResult)를 통하거나,
		 * RegisterCallbackFunction 함수를 통하여 등록한 callback 함수를 통하여 전달되는 이벤트를 통하여 알 수 있다. 또는
		 * RegisterCallbackWindow 함수를 통하여 등록한 윈도우에 대해
		 * WM_ECMIC_EVENT 윈도우메시지를 통하여 전달되는 ST_TransactionResult 구조체를 통해 알 수 있다.
		 */
		ECMIC_TransactionID RequestUpdate(
			ECMIC_SessionID iSessionID, String^ strArchiveID, String^ strFileID,
			String^ strContentClass, String^ strPrefixPath, String^ strLocalFilePath);

		// cholh20150605 : 기능 추가
		/**
		 * 파일 임시 삭제
		 * strFileID로 지정된 File ID에 해당하는 파일을 삭제하도록(DB 데이터에 플래그 변경) 요청한다.
		 * ECM이 탑재된 서버장비에서 접근 가능한 파일 시스템에 존재하는 파일을 삭제하는 것이 아니라, DB의 데이터에 파일이 삭제된 것으로 처리한다.
		 * iSessionID로 지정된 세션이 없거나 기타 이유로 요청을 하지 못하는 상황에서는 INVALID_SESSION(-1)을 리턴한다.
		 *
		 * 리턴값으로 트랜잭션을 ID를 받으면, 이를 이용하여 GetTransactionResult 함수를 호출하여 함수 수행 결과를 얻을 수 있다.
		 * 함수 수행 결과는 ECMIC_ST_TransactionResult 구조체의 인스턴스이다.
		 * 함수 수행 성공 여부는 ECMIC_ST_TransactionResult.enResult 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 코드는 ECMIC_ST_TransactionResult.iErrorCode 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 문자열은 ECMIC_ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		 * 함수 수행 성공 시 획득하기를 원하는 정보는 ECMIC_ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		 * 함수 수행 성공 시 ECMIC_ST_TransactionResult.strResultOrFailReason에 들어 있는 정보는 Field=Value,Field=Value,... 형태를 취하고 있다.
		 *
		 * 새로운 transaction을 시작하고, 그 ID를 리턴한다. 개별 transaction은 담당 thread가 존재하게 된다.
		 * 본 함수는 transaction 담당 thread를 생성하여 실행시키고 서버와 통신하여 결과가 결정되면 리턴한다.
		 * Transaction의 결과는 별도의 함수(ECMIC_GetTransactionResult)를 통하거나,
		 * RegisterCallbackFunction 함수를 통하여 등록한 callback 함수를 통하여 전달되는 이벤트를 통하여 알 수 있다. 또는
		 * RegisterCallbackWindow 함수를 통하여 등록한 윈도우에 대해
		 * WM_ECMIC_EVENT 윈도우메시지를 통하여 전달되는 ST_TransactionResult 구조체를 통해 알 수 있다.
		 */
		ECMIC_TransactionID RequestSoftDelete(ECMIC_SessionID iSessionID, String^ strFileID);

		// cholh20150605 : 기능 추가
		/**
		 * 파일 완전 삭제
		 * strFileID로 지정된 File ID에 해당하는 파일을 삭제하도록(DB레코드 삭제, 파일 삭제) 요청한다.
		 * iSessionID로 지정된 세션이 없거나 기타 이유로 요청을 하지 못하는 상황에서는 INVALID_SESSION(-1)을 리턴한다.
		 *
		 * 리턴값으로 트랜잭션을 ID를 받으면, 이를 이용하여 GetTransactionResult 함수를 호출하여 함수 수행 결과를 얻을 수 있다.
		 * 함수 수행 결과는 ECMIC_ST_TransactionResult 구조체의 인스턴스이다.
		 * 함수 수행 성공 여부는 ECMIC_ST_TransactionResult.enResult 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 코드는 ECMIC_ST_TransactionResult.iErrorCode 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 문자열은 ECMIC_ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		 * 함수 수행 성공 시 획득하기를 원하는 정보는 ECMIC_ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		 * 함수 수행 성공 시 ECMIC_ST_TransactionResult.strResultOrFailReason에 들어 있는 정보는 Field=Value,Field=Value,... 형태를 취하고 있다.
		 *
		 * 새로운 transaction을 시작하고, 그 ID를 리턴한다. 개별 transaction은 담당 thread가 존재하게 된다.
		 * 본 함수는 transaction 담당 thread를 생성하여 실행시키고 서버와 통신하여 결과가 결정되면 리턴한다.
		 * Transaction의 결과는 별도의 함수(ECMIC_GetTransactionResult)를 통하거나,
		 * RegisterCallbackFunction 함수를 통하여 등록한 callback 함수를 통하여 전달되는 이벤트를 통하여 알 수 있다. 또는
		 * RegisterCallbackWindow 함수를 통하여 등록한 윈도우에 대해
		 * WM_ECMIC_EVENT 윈도우메시지를 통하여 전달되는 ST_TransactionResult 구조체를 통해 알 수 있다.
		 */
		ECMIC_TransactionID RequestHardDelete(ECMIC_SessionID iSessionID, String^ strFileID);

		// cholh20150605 : 기능 추가
		/**
		 * 파일의 수정 이력의 모든 레코드들의 개수를 획득한다.
		 * iSessionID로 지정된 세션이 없거나 기타 이유로 요청을 하지 못하는 상황에서는 INVALID_SESSION(-1)을 리턴한다.
		 *
		 * 리턴값으로 트랜잭션을 ID를 받으면, 이를 이용하여 GetTransactionResult 함수를 호출하여 함수 수행 결과를 얻을 수 있다.
		 * 함수 수행 결과는 ECMIC_ST_TransactionResult 구조체의 인스턴스이다.
		 * 함수 수행 성공 여부는 ECMIC_ST_TransactionResult.enResult 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 코드는 ECMIC_ST_TransactionResult.iErrorCode 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 문자열은 ECMIC_ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		 * 함수 수행 성공 시 획득하기를 원하는 정보는 ECMIC_ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		 * 함수 수행 성공 시 ECMIC_ST_TransactionResult.strResultOrFailReason에 들어 있는 정보는 Field=Value,Field=Value,... 형태를 취하고 있다.
		 *
		 * 새로운 transaction을 시작하고, 그 ID를 리턴한다. 개별 transaction은 담당 thread가 존재하게 된다.
		 * 본 함수는 transaction 담당 thread를 생성하여 실행시키고 서버와 통신하여 결과가 결정되면 리턴한다.
		 * Transaction의 결과는 별도의 함수(ECMIC_GetTransactionResult)를 통하거나,
		 * ECMIC_RegisterCallbackFunction 함수를 통하여 등록한 callback 함수를 통하여 전달되는 이벤트를 통하여 알 수 있다. 또는
		 * ECMIC_RegisterCallbackWindow 함수를 통하여 등록한 윈도우에 대해
		 * WM_ECMIC_EVENT 윈도우메시지를 통하여 전달되는 ST_TransactionResult 구조체를 통해 알 수 있다.
		 */
		ECMIC_TransactionID RequestFileHistoryGetListSize(ECMIC_SessionID iSessionID, String^ strFileID);

		// cholh20150605 : 기능 추가
		/**
		 * 파일의 수정 이력 획득
		 * strFileID로 지정된 File ID에 대한 수정 이력 리스트를 요청한다.
		 * 수정 이력 리스트(FileHistoryList)의 모든 레코드들을 가져오는 것이 아니라 iStartIndex번 째부터 iCountInPage 개의 레코드들의 리스트를 획득한다.
		 * iSessionID로 지정된 세션이 없거나 기타 이유로 요청을 하지 못하는 상황에서는 INVALID_SESSION(-1)을 리턴한다.
		 *
		 * 리턴값으로 트랜잭션을 ID를 받으면, 이를 이용하여 GetTransactionResult 함수를 호출하여 함수 수행 결과를 얻을 수 있다.
		 * 함수 수행 결과는 ECMIC_ST_TransactionResult 구조체의 인스턴스이다.
		 * 함수 수행 성공 여부는 ECMIC_ST_TransactionResult.enResult 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 코드는 ECMIC_ST_TransactionResult.iErrorCode 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 문자열은 ECMIC_ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		 * 함수 수행 성공 시 획득하기를 원하는 파일 수정 히스토리 목록은 ECMIC_ST_TransactionResult.pFileHistoryList 변수를 확인한다.
		 *
		 * 새로운 transaction을 시작하고, 그 ID를 리턴한다. 개별 transaction은 담당 thread가 존재하게 된다.
		 * 본 함수는 transaction 담당 thread를 생성하여 실행시키고 서버와 통신하여 결과가 결정되면 리턴한다.
		 * Transaction의 결과는 별도의 함수(ECMIC_GetTransactionResult)를 통하거나,
		 * ECMIC_RegisterCallbackFunction 함수를 통하여 등록한 callback 함수를 통하여 전달되는 이벤트를 통하여 알 수 있다. 또는
		 * ECMIC_RegisterCallbackWindow 함수를 통하여 등록한 윈도우에 대해
		 * WM_ECMIC_EVENT 윈도우메시지를 통하여 전달되는 ST_TransactionResult 구조체를 통해 알 수 있다.
		 */
		ECMIC_TransactionID RequestFileHistoryGetList(
			ECMIC_SessionID iSessionID, String^ strFileID, int iStartIndex, int iCountInPage);

		// cholh20150605 : 기능 추가
		/**
		 * strFileID 파일의 iFileVersion에 해당하는 수정된 버전 다운로드
		 * iSessionID로 지정된 세션이 없거나 기타 이유로 요청을 하지 못하는 상황에서는 INVALID_SESSION(-1)을 리턴한다.
		 *
		 * 리턴값으로 트랜잭션을 ID를 받으면, 이를 이용하여 GetTransactionResult 함수를 호출하여 함수 수행 결과를 얻을 수 있다.
		 * 함수 수행 결과는 ECMIC_ST_TransactionResult 구조체의 인스턴스이다.
		 * 함수 수행 성공 여부는 ECMIC_ST_TransactionResult.enResult 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 코드는 ECMIC_ST_TransactionResult.iErrorCode 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 문자열은 ECMIC_ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		 * 함수 수행 성공 시 획득하기를 원하는 정보는 ECMIC_ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		 * 함수 수행 성공 시 ECMIC_ST_TransactionResult.strResultOrFailReason에 들어 있는 정보는 Field=Value,Field=Value,... 형태를 취하고 있다.
		 *
		 * 새로운 transaction을 시작하고, 그 ID를 리턴한다. 개별 transaction은 담당 thread가 존재하게 된다.
		 * 본 함수는 transaction 담당 thread를 생성하여 실행시키고 서버와 통신하여 결과가 결정되면 리턴한다.
		 * Transaction의 결과는 별도의 함수(ECMIC_GetTransactionResult)를 통하거나,
		 * ECMIC_RegisterCallbackFunction 함수를 통하여 등록한 callback 함수를 통하여 전달되는 이벤트를 통하여 알 수 있다. 또는
		 * ECMIC_RegisterCallbackWindow 함수를 통하여 등록한 윈도우에 대해
		 * WM_ECMIC_EVENT 윈도우메시지를 통하여 전달되는 ST_TransactionResult 구조체를 통해 알 수 있다.
		 */
		ECMIC_TransactionID RequestFileHistoryGetByVersion(
			ECMIC_SessionID iSessionID, String^ strFileID, int iFileVersion, String^ strLocalFileSavePath);

		// cholh20150605 : 기능 추가
		/**
		 * 파일의 szHistoryID에 해당하는 수정된 버전 다운로드
		 * iSessionID로 지정된 세션이 없거나 기타 이유로 요청을 하지 못하는 상황에서는 INVALID_SESSION(-1)을 리턴한다.
		 *
		 * 리턴값으로 트랜잭션을 ID를 받으면, 이를 이용하여 GetTransactionResult 함수를 호출하여 함수 수행 결과를 얻을 수 있다.
		 * 함수 수행 결과는 ECMIC_ST_TransactionResult 구조체의 인스턴스이다.
		 * 함수 수행 성공 여부는 ECMIC_ST_TransactionResult.enResult 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 코드는 ECMIC_ST_TransactionResult.iErrorCode 변수를 확인한다.
		 * 함수 수행 실패 시 실패 이유 문자열은 ECMIC_ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		 * 함수 수행 성공 시 획득하기를 원하는 정보는 ECMIC_ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		 * 함수 수행 성공 시 ECMIC_ST_TransactionResult.strResultOrFailReason에 들어 있는 정보는 Field=Value,Field=Value,... 형태를 취하고 있다.
		 *
		 * 새로운 transaction을 시작하고, 그 ID를 리턴한다. 개별 transaction은 담당 thread가 존재하게 된다.
		 * 본 함수는 transaction 담당 thread를 생성하여 실행시키고 서버와 통신하여 결과가 결정되면 리턴한다.
		 * Transaction의 결과는 별도의 함수(ECMIC_GetTransactionResult)를 통하거나,
		 * ECMIC_RegisterCallbackFunction 함수를 통하여 등록한 callback 함수를 통하여 전달되는 이벤트를 통하여 알 수 있다. 또는
		 * ECMIC_RegisterCallbackWindow 함수를 통하여 등록한 윈도우에 대해
		 * WM_ECMIC_EVENT 윈도우메시지를 통하여 전달되는 ST_TransactionResult 구조체를 통해 알 수 있다.
		 */
		ECMIC_TransactionID RequestFileHistoryGetByHistoryID(
			ECMIC_SessionID iSessionID, String^ strHistoryID, String^ strLocalFileSavePath);

		///**
		// * szArchiveID로 지정된 아카이브에 szLocaFilePath로 지정되는 로컬 파일을 업로드 및 등록하도록 요청한다. szMetaData는 NULL이거나 빈 문자열일 수 있다.
		// *
		// * 새로운 transaction을 시작하고, 그 ID를 리턴한다. 개별 transaction은 담당 thread가 존재하게 된다.
		// * 본 함수는 transaction 담당 thread를 생성하여 실행시키고 서버와 통신하여 결과가 결정되면 리턴한다.
		// * Transaction의 결과는 별도의 함수(ECMIC_GetTransactionResult)를 통하거나,
		// * RegisterCallbackFunction 함수를 통하여 등록한 callback 함수를 통하여 전달되는 이벤트를 통하여 알 수 있다. 또는
		// * RegisterCallbackWindow 함수를 통하여 등록한 윈도우에 대해
		// * WM_ECMIC_EVENT 윈도우메시지를 통하여 전달되는 ST_TransactionResult 구조체를 통해 알 수 있다.
		// */
		//extern TransactionID WINAPI ECMIC_RequestRegister(SessionID iSessionID, const char* szArchiveID, const char* szContentClass, const char* szLocalFilePath, const char* szMetaData);

		///**
		// * !!! 미구현 !!!
		// * 파일 수정 & 메타 정보 등록
		// *
		// * 리턴값으로 트랜잭션 ID를 받으면, 이를 이용하여 ECMIC_GetTransactionResult 함수를 호출하여 함수 수행 결과를 얻을 수 있다.
		// * 함수 수행 결과는 ST_TransactionResult 구조체의 인스턴스이다.
		// * 함수 수행 성공 여부는 ST_TransactionResult.enResult 변수를 확인한다.
		// * 함수 수행 실패 시 실패 이유 코드는 ST_TransactionResult.iErrorCode 변수를 확인한다.
		// * 함수 수행 실패 시 실패 이유 문자열은 ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		// * 함수 수행 성공 시 획득하기를 원하는 정보는 ST_TransactionResult.strResultOrFailReason 변수를 확인한다.
		// * 함수 수행 성공 시 ST_TransactionResult.strResultOrFailReason에 들어 있는 정보는 Field=Value,Field=Value,... 형태를 취하고 있다.
		// *
		// * 새로운 transaction을 시작하고, 그 ID를 리턴한다. 개별 transaction은 담당 thread가 존재하게 된다.
		// * 본 함수는 transaction 담당 thread를 생성하여 실행시키고 서버와 통신하여 결과가 결정되면 리턴한다.
		// * Transaction의 결과는 별도의 함수(ECMIC_GetTransactionResult)를 통하거나,
		// * ECMIC_RegisterCallbackFunction 함수를 통하여 등록한 callback 함수를 통하여 전달되는 이벤트를 통하여 알 수 있다. 또는
		// * ECMIC_RegisterCallbackWindow 함수를 통하여 등록한 윈도우에 대해
		// * WM_ECMIC_EVENT 윈도우메시지를 통하여 전달되는 ST_TransactionResult 구조체를 통해 알 수 있다.
		// */
		//extern TransactionID WINAPI ECMIC_RequestUpdate2(
		//	SessionID iSessionID, const char* szArchiveID, const char* szFileID, const char* szContentClass,
		//	const char* szPrefixPath, const char* szLocalFilePath, const char* szMetaData);

		///**
		// * szArchiveID로 지정된 아카이브에 szLocalFilePaths로 지정되는 파일들을 대량 등록한다. 각 파일들과 관련된 메타데이터는 szInfFilePath로 지정되는 .inf 파일 안에 들어 있다.
		// * szLocalFilePaths 파라미터의 사용 예 : "/path1/file1,/path2/file2"
		// * 아직 구상중이지만 대량등록(mass register)을 구현하기 위해 서버-클라이언트 사이에 여러개의 socket connection을 체결하여 사용하는 방안을 고려중이다.
		// *
		// * 새로운 transaction을 시작하고, 그 ID를 리턴한다. 개별 transaction은 담당 thread가 존재하게 된다.
		// * 본 함수는 transaction 담당 thread를 생성하여 실행시키고 서버와 통신하여 결과가 결정되면 리턴한다.
		// * Transaction의 결과는 별도의 함수(ECMIC_GetTransactionResult)를 통하거나,
		// * RegisterCallbackFunction 함수를 통하여 등록한 callback 함수를 통하여 전달되는 이벤트를 통하여 알 수 있다. 또는
		// * RegisterCallbackWindow 함수를 통하여 등록한 윈도우에 대해
		// * WM_ECMIC_EVENT 윈도우메시지를 통하여 전달되는 ST_TransactionResult 구조체를 통해 알 수 있다.
		// */
		//extern TransactionID WINAPI ECMIC_RequestMassRegister(
		//	SessionID iSessionID, const char* szArchiveID, const char* szContentClass,
		//	const char* szPrefixPath, const char* szLocalFilePaths, const char* szInfFilePath);

		///**
		// * 임의의 DB에 대한 select query를 수행하여 그 결과를 돌려줄 것을 요청한다.
		// * 원래의 의도는 다양한 조건으로 ECM 서버가 사용하는 DB에서 file들을 검색하기 위한 것이다.
		// * 임의의 select query를 수행하는 것에 대해서는 보다 많은 검토가 필요하다.
		// * 일단은 구현하지 말고 자리만 만들어 두도록 하자.
		// *
		// * 새로운 transaction을 시작하고, 그 ID를 리턴한다. 개별 transaction은 담당 thread가 존재하게 된다.
		// * 본 함수는 transaction 담당 thread를 생성하여 실행시키고 서버와 통신하여 결과가 결정되면 리턴한다.
		// * Transaction의 결과는 별도의 함수(ECMIC_GetTransactionResult)를 통하거나,
		// * RegisterCallbackFunction 함수를 통하여 등록한 callback 함수를 통하여 전달되는 이벤트를 통하여 알 수 있다. 또는
		// * RegisterCallbackWindow 함수를 통하여 등록한 윈도우에 대해
		// * WM_ECMIC_EVENT 윈도우메시지를 통하여 전달되는 ST_TransactionResult 구조체를 통해 알 수 있다.
		// */
		//extern TransactionID WINAPI ECMIC_RequestSearch(
		//	SessionID iSessionID, const char* szArchiveID, const char* szSearchConditions);

		// cholh20150615 : 기능 추가 : Authentication
		/**
		 * 서버와의 소켓 통신을 담당하는 세션이 정상적으로 연결되어 있는지 서버로 Ping 메시지를 보내고 Pong 응답을 받는다.
		 * 서버와의 통신 연결이 정상적인지 확인하는 용도 이외에도, 서버와의 통신 연결이 끊어지지 않게 하려는 용도도 있다.
		 * 통상적으로 클라이언트가 서버로 5분 이상 아무런 데이터도 전송하지 않으면 서버에 의해 소켓 통신 연결이 끊어진다.
		 *
		 * 새로운 transaction을 시작하고, 그 ID를 리턴한다. 개별 transaction은 담당 thread가 존재하게 된다.
		 * 본 함수는 transaction 담당 thread를 생성하여 실행시키고 서버와 통신하여 결과가 결정되면 리턴한다.
		 * Transaction의 결과는 별도의 함수(ECMIC_GetTransactionResult)를 통하거나,
		 * ECMIC_RegisterCallbackFunction 함수를 통하여 등록한 callback 함수를 통하여 전달되는 이벤트를 통하여 알 수 있다. 또는
		 * ECMIC_RegisterCallbackWindow 함수를 통하여 등록한 윈도우에 대해
		 * WM_ECMIC_EVENT 윈도우메시지를 통하여 전달되는 ST_TransactionResult 구조체를 통해 알 수 있다.
		 */
		ECMIC_TransactionID RequestAuthentication(ECMIC_SessionID iSessionID, String^ strUserID, String^ strUserPW);

		// cholh20150526 : 서버와의 통신 연결 확인
		/**
		 * 서버와의 소켓 통신 연결이 체결되어 세션이 형성된 상태에서, 사용자 ID와 패스워드를 이용하여 인증을 받는다.
		 *
		 * 새로운 transaction을 시작하고, 그 ID를 리턴한다. 개별 transaction은 담당 thread가 존재하게 된다.
		 * 본 함수는 transaction 담당 thread를 생성하여 실행시키고 서버와 통신하여 결과가 결정되면 리턴한다.
		 * Transaction의 결과는 별도의 함수(ECMIC_GetTransactionResult)를 통하거나,
		 * ECMIC_RegisterCallbackFunction 함수를 통하여 등록한 callback 함수를 통하여 전달되는 이벤트를 통하여 알 수 있다. 또는
		 * ECMIC_RegisterCallbackWindow 함수를 통하여 등록한 윈도우에 대해
		 * WM_ECMIC_EVENT 윈도우메시지를 통하여 전달되는 ST_TransactionResult 구조체를 통해 알 수 있다.
		 */
		ECMIC_TransactionID RequestPing(ECMIC_SessionID iSessionID);


		////////////////////////////////////////////////////////////////////////////////
		// Transaction Suspend/Resume/Abort
		////////////////////////////////////////////////////////////////////////////////

		/**
		 * 현재 진행중인 transaction을 임시 중단한다. 임시 중단에 성공하면 true를, 실패하면 false를 리턴한다.
		 */
		bool Suspend(ECMIC_SessionID iSessionID, ECMIC_TransactionID iTransactionID);

		/**
		 * 임시 중단된 transaction을 다시 진행시킨다. 재진행에 성공하면 true를, 실패하면 false를 리턴한다.
		 */
		bool Resume(ECMIC_SessionID iSessionID, ECMIC_TransactionID iTransactionID);

		/**
		 * 임시 중단된 transaction을 포기하여 종료시킨다. 포기 종료에 성공하면 true를, 실패하면 false를 리턴한다.
		 */
		bool Abort(ECMIC_SessionID iSessionID, ECMIC_TransactionID iTransactionID);


		////////////////////////////////////////////////////////////////////////////////
		// Transaction 상태/결과 관련 함수들
		////////////////////////////////////////////////////////////////////////////////

		/**
		 * Transaction의 상태값을 리턴한다.
		 *
		 * Transaction의 상태값
		 * 	TS_INPROGRESS : 진행중
		 *	TS_ENDED      : 종료
		 *	TS_SUSPENDED  : 임시 중단
		 *	TS_ABORTED    : 포기 종료
		 */
		ECMIC_EN_TransactionState GetTransactionState(ECMIC_SessionID iSessionID, ECMIC_TransactionID iTransactionID);

		/**
		 * Transaction의 진행 상태 정보를 획득한다. 중요 진행 상태 정보에는 진행(송신 또는 수신)된 데이터 양, 전체 데이터 양, 진행률이 있다.
		 */
		ECMIC_ST_TransactionProgress^ GetTransactionProgress(ECMIC_SessionID iSessionID, ECMIC_TransactionID iTransactionID);

		/**
		 * Transaction 결과 정보를 획득한다. 중요 결과 정보에는 성공/실패, 실패 이유, 전체 데이터 양, 진행(송신 또는 수신)된 데이터 양이 있다.
		 */
		ECMIC_ST_TransactionResult^ GetTransactionResult(ECMIC_SessionID iSessionID, ECMIC_TransactionID iTransactionID);

		/**
		 * Session에서 transaction에 대한 정보를 제거하고, transaction에 할당된 시스템 자원을 반납한다.
		 */
		bool RemoveTransaction(ECMIC_SessionID iSessionID, ECMIC_TransactionID iTransactionID);


		////////////////////////////////////////////////////////////////////////////////
		// Session 개수와 개별 ID/Transaction 개수와 개별 ID 관련 함수들
		////////////////////////////////////////////////////////////////////////////////

		/**
		 * 현재 생성되어 있는 session들의 개수를 리턴한다.
		 */
		int GetSessionCount();

		/**
		 * iIndex 값으로 지정되는 순서에 해당하는 session의 ID를 리턴한다.
		 */
		ECMIC_SessionID GetSessionID(int iIndex);

		/**
		 * iSessionID로 지정된 session에 존재하는 transaction의 개수를 리턴한다.
		 */
		int GetTransactionCount(ECMIC_SessionID iSessionID);

		/**
		 * iIndex 값으로 지정되는 순서에 해당하는 transaction의 ID를 리턴한다.
		 */
		ECMIC_TransactionID GetTransactionID(ECMIC_SessionID iSessionID, int iIndex);

		/**
		 * 설정되어 있는 세션의 최대 개수를 리턴한다. 별도로 설정하지 않은 경우 기본값은 5이다.
		 */
		int GetMaxSessionCount();

		/**
		 * 세션의 최대 개수를 설정하고 기존에 설정되어 있던 세션의 최대 개수를 리턴한다. 별도로 설정하지 않은 경우 기본값은 5이다.
		 */
		int SetMaxSessionCount(int iNewValue);

		/**
		 * 세션별로 설정되어 있는 트랜잭션의 최대 개수를 리턴한다. 별도로 설정하지 않은 경우 기본값은 10이다. 해당 세션이 없는 경우 -1을 리턴한다.
		 */
		int GetMaxTransactionCount(ECMIC_SessionID iSessionID);

		/**
		 * 세션별로 트랜잭션의 최대 개수를 설정하고 기존에 설정되어 있던 트랜잭션의 최대 개수를 리턴한다. 별도로 설정하지 않은 경우 기본값은 10이다. 해당 세션이 없는 경우 -1을 리턴한다.
		 */
		int SetMaxTransactionCount(ECMIC_SessionID iSessionID, int iNewValue);


		////////////////////////////////////////////////////////////////////////////////
		// PingPongThread 관련 함수
		////////////////////////////////////////////////////////////////////////////////

		// cholh20150526 : Ping Pong Thread 관련 함수
		/**
		 * 세션별로 내부에는 서버와 소켓 연결을 유지하기 위한 PingPongThread가 존재한다.
		 * 서버와 소켓 연결을 유지하기 위해서는 일정 시간 안에 통신이 이루어져야 한다.
		 * 5분 이상 서버와 통신이 일어나지 않으면 소켓 연결이 끊이진다.
		 * PingPongThread는 별도의 통신이 일어나지 않으면 4분 간격으로 서버에 Ping 메시지를 전송하고 Pong 메시지를 수신한다.
		 * 기본적으로 PingPongThread는 존재하지만 PingPong을 실행하지 않는 상태로 생성된다.
		 * 응용프로그램에서 세션이 서버와 소켓 연결을 유지하기를 바라는 경우 본 함수를 파라미터 bEnable의 값을 true로 설정하여 호출하여야 한다.
		 *
		 * 리턴값 : 0 - 성공, 기타 - 에러코드
		 *
		 * 에러코드
		 *  -1 : iSessionID에 해당하는 세션을 찾을 수 없음
		 */
		int EnablePingPongThread(ECMIC_SessionID iSessionID, bool bEnable);

		// cholh20150526 : Ping Pong Thread 관련 함수
		/**
		 * PingPongThread에서 서버로 Ping을 보내는 시간 간격을 획득한다.
		 * 리턴값 : 0 이상 - Ping을 보내는 시간 간격(ms 단위), 음수 - 실패 에러 코드
		 * 에러 코드 : -1 - 유효하지 않은 세션 ID
		 * 시간 간격 범위 : 10초(10000ms) ~ 4분(240000ms)
		 */
		int GetPingInterval(ECMIC_SessionID iSessionID);

		// cholh20150526 : Ping Pong Thread 관련 함수
		/**
		 * PingPongThread에서 서버로 Ping을 보내는 시간 간격을 설정한다.
		 * 본 함수를 호출하지 않은 경우 기본으로 4분(=240초=240000ms) 간격으로 Ping을 보낸다.
		 * 리턴값 : 0 - 성공, 음수 - 실패 에러 코드
		 * 에러 코드 : -1 - 유효하지 않은 세션 ID, -2 - 인터벌 값이 범위를 벗어남
		 * 시간 간격 범위 : 10초(10000ms) ~ 4분(240000ms)
		 */
		int SetPingInterval(ECMIC_SessionID iSessionID, int iPingIntervalMilliseconds);


		////////////////////////////////////////////////////////////////////////////////
		// 대기 시간 설정 관련 함수
		////////////////////////////////////////////////////////////////////////////////

		// cholh20151007 : API 추가 : 네트워크 소켓 연결 대기 시간 획득
		/**
		 * ECM Imaging 서버와 소켓 통신을 위한 connect 시도 시 대기 시간(초)을 획득한다.
		 * 기본 대기 시간은 10초이다. 대기 시간은 최소 1초이며 최대 600초이다.
		 *
		 * 리턴값 : 네트워크 소켓 연결 대기 시간
		 * 기본 대기 시간 : 10초, 최소 대기 시간 : 1초, 최대 대기 시간 : 600초
		 */
		int GetServerConnectWaitSeconds();

		// cholh20151007 : API 추가 : 네트워크 소켓 연결 대기 시간 설정
		/**
		 * ECM Imaging 서버와 소켓 통신을 위한 connect 시도 시 대기 시간(초)을 설정한다.
		 * 기본 대기 시간은 10초이다. 대기 시간은 최소 1초이며 최대 600초이다.
		 * 본 함수를 호출하는 경우 존재하는 모든 세션과 향후 새로 생성되는 세션에 변경된 네트워크 소켓 연결 대기 시간이 반영된다.
		 *
		 * 리턴값 : > 0 - 이전 네트워크 소켓 연결 대기 시간, 음수 - 에러 코드
		 * 에러 코드 : -1 - 유효하지 않은 대기 시간
		 * 기본 대기 시간 : 10초, 최소 대기 시간 : 1초, 최대 대기 시간 : 600초
		 */
		int SetServerConnectWaitSeconds(int iServerConnectWaitSeconds);

		// cholh20151007 : API 추가 : 서버 응답 대기 시간 획득
		/**
		 * ECM Imaging 서버로부터의 응답 대기 시간(초)을 획득한다.
		 * 기본 대기 시간은 30초이다. 대기 시간은 최소 1초이며 최대 600초이다.
		 *
		 * 리턴값 : 서버 응답 대기 시간
		 * 기본 대기 시간 : 30초, 최소 대기 시간 : 1초, 최대 대기 시간 : 600초
		 */
		int GetServerResponseWaitSeconds();

		// cholh20151007 : API 추가 : 서버 응답 대기 시간 설정
		/**
		 * ECM Imaging 서버로부터의 응답 대기 시간(초)을 설정한다.
		 * 기본 대기 시간은 30초이다. 대기 시간은 최소 1초이며 최대 600초이다.
		 * 본 함수를 호출하는 경우 존재하는 모든 세션과 향후 새로 생성되는 세션에 변경된 서버 응답 대기 시간이 반영된다.
		 *
		 * 리턴값 : > 0 - 이전 서버 응답 대기 시간, 음수 - 에러 코드
		 * 에러 코드 : -1 - 유효하지 않은 대기 시간
		 * 기본 대기 시간 : 30초, 최소 대기 시간 : 1초, 최대 대기 시간 : 600초
		 */
		int SetServerResponseWaitSeconds(int iServerResponseWaitSeconds);
	};
}
