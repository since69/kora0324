﻿//
// BnpParibas.Service.ImageService.SeparateArchiving
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

using System.Diagnostics;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Timers;

namespace BnpParibas.Service.ImageService
{
    public partial class SeparateArchiving : ServiceBase
    {
        #region VARIABLES
        private const string EventLogSourceName = "SeparateArchivingSource";
        private const string EventLogName = "SeperateArchivingLog";
        private readonly EventLog eventLog = new EventLog();
        private readonly Timer timer = new Timer();
        private int eventId = 1;
        #endregion

        #region EXTERNAL LIBRARY METHODS
        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(
            System.IntPtr handle, 
            ref ServiceStatus serviceStatus);
        #endregion

        #region CONSTRUCTORS
        public SeparateArchiving()
        {
            InitializeComponent();
            InitializeService();
        }
        #endregion

        #region PRIVATE METHODS
        private void InitializeService()
        {
            InitalizeEventLog();
            InitializeTimer();
        }

        private void InitalizeEventLog()
        {
            if (!EventLog.SourceExists(EventLogSourceName))
            {
                EventLog.CreateEventSource(EventLogSourceName, EventLogName);
            }
            eventLog.Source = EventLogSourceName;
            eventLog.Log = EventLogName;
        }

        private void InitializeTimer()
        {
            // 60 seconds
            timer.Interval = 6000;
            timer.Elapsed += new ElapsedEventHandler(OnTimer);
            timer.Start();
        }
        #endregion

        #region SERVICE EVENTS
        protected override void OnStart(string[] args)
        {
            eventLog.WriteEntry("In OnStart");

            // Update the service state to Start Pending.
            //ServiceStatus serviceStatus = new ServiceStatus();
            //serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
            //serviceStatus.dwWaitHint = 100000;
            //SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            // Update the service state to Running.
            //ServiceStatus serviceStatus = new ServiceStatus();
            //serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            //SetServiceStatus(ServiceHandle, ref serviceStatus);
        }

        protected override void OnStop()
        {
            eventLog.WriteEntry("In OnStop");

            // Update the service state to Stop Pending.
            //ServiceStatus serviceStatus = new ServiceStatus();
            //serviceStatus.dwCurrentState = ServiceState.SERVICE_STOP_PENDING;
            //serviceStatus.dwWaitHint = 100000;
            //SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            // Update the service state to Stopped.
            //ServiceStatus serviceStatus = new ServiceStatus();
            //serviceStatus.dwCurrentState = ServiceState.SERVICE_STOPPED;
            //SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        protected override void OnContinue()
        {
            eventLog.WriteEntry("In OnContinue");
        }
        #endregion

        #region TIMER EVENTS
        private void OnTimer(object sender, ElapsedEventArgs args)
        {
            eventLog.WriteEntry("Monitoring the system", EventLogEntryType.Information, eventId++);
        }
        #endregion

    }
}
