﻿//
// BnpParibas.Service.Configuration.AppElement
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.Configuration;

namespace BnpParibas.Service.Configuration
{
    public class AppElement : ConfigurationElement
    {
        #region PROPERTIES
        [ConfigurationProperty("baseDir", DefaultValue = @".\")]
        public string BaseDir => (string)base["baseDir"];

        [ConfigurationProperty("downloadDir", DefaultValue = @"download\")]
        public string DownloadDir => (string)base["downloadDir"];

        [ConfigurationProperty("uploadDir", DefaultValue = @"upload\")]
        public string UploadDir => (string)base["uploadDir"];
        #endregion
    }
}
