﻿//
// BnpParibas.Service.Configuration.CaseElement
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.Configuration;

namespace BnpParibas.Service.Configuration
{
    public class CaseElement : ConfigurationElement
    {
        #region PROPERTIES
        /// <summary>
        /// Database connection string
        /// </summary
        public string ConnectionString
        {
            get
            {
                string connStringName;

                if (string.IsNullOrEmpty(this.ConnectionStringName))
                {
                    connStringName = Configurator.ImageServiceSettings
                        .DefaultConnectionStringName;
                }
                else
                    connStringName = this.ConnectionStringName;

                return ConfigurationManager.ConnectionStrings[connStringName]
                    .ConnectionString;
            }
        }

        /// <summary>
        /// Database connection string name
        /// </summary
        [ConfigurationProperty("connectionStringName")]
        public string ConnectionStringName
        {
            get { return (string)base["connectionStringName"]; }
            set { base["connectionStringName"] = value; }
        }

        [ConfigurationProperty("providerType",
            DefaultValue = "BnpParibas.Service.Data.DAL.OracleClient.OracleCaseProvider")]
        public string ProviderType
        {
            get { return (string)base["providerType"]; }
        }
        #endregion
    }
}
