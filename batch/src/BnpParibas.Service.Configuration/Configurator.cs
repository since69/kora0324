﻿//
// BnpParibas.Service.Configuration.Configurator
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibas.Service.Configuration
{
    using System.Configuration;
    using log4net;

    public static class Configurator
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("Configurator");
        #endregion

        #region PROPERTIES
        public static ImageServiceSection ImageServiceSettings { get; private set; } = null;
        #endregion

        #region CONSTRUCTORS
        static Configurator()
        {
            string sectionName = string.Empty;

            try
            {
                sectionName = "imageService";
                ImageServiceSettings = (ImageServiceSection)ConfigurationManager
                    .GetSection(sectionName);
            }
            catch (ConfigurationException e)
            {
                logger.Fatal($"Can't load {sectionName} section. " + e.Message);
            }
        }
        #endregion
    }
}
