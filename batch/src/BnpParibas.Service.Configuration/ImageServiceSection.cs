﻿//
// BnpParibas.Service.Configuration.ImageServiceSection
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.Configuration;
using log4net;

namespace BnpParibas.Service.Configuration
{
    public class ImageServiceSection : ConfigurationSection
    {
        #region PROPERTIES
        [ConfigurationProperty("defaultConnectionStringName", IsRequired = true,
            DefaultValue = "GmsUatServer")]
        public string DefaultConnectionStringName
        {
            get { return (string)base["defaultConnectionStringName"]; }
            set { base["defaultConnectionStringName"] = value; }
        }

        [ConfigurationProperty("app", IsRequired = true)]
        public AppElement App => (AppElement)base["app"];

        [ConfigurationProperty("case", IsRequired = true)]
        public CaseElement Case => (CaseElement)base["case"];

        [ConfigurationProperty("ecm", IsRequired = true)]
        public EcmElement Ecm => (EcmElement)base["ecm"];
        #endregion
    }
}
