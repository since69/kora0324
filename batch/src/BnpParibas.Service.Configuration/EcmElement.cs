﻿//
// BnpParibas.Service.Configuration.EcmElement
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.Configuration;

namespace BnpParibas.Service.Configuration
{
    public class EcmElement : ConfigurationElement
    {
        #region PROPERTIES
        [ConfigurationProperty("ip", DefaultValue = "http://10.10.77.53")]
        public string Ip
        {
            get { return (string)base["ip"]; }
        }

        [ConfigurationProperty("port", DefaultValue = "9080")]
        public int Port
        {
            get { return (int)base["port"]; }
        }

        [ConfigurationProperty("baseDir", DefaultValue = "/")]
        public string BaseDir
        {
            get { return (string)base["baseDir"]; }
        }

        [ConfigurationProperty("servlets", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(ServletElementCollection),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]
        public ServletElementCollection Servlets
        {
            get { return (ServletElementCollection)base["servlets"]; }
        }
        #endregion
    }
}
