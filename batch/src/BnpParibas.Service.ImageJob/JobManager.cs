﻿//
// BnpParibas.Service.ImageJob.JobManager
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.Collections.Generic;
using BnpParibas.Service.Data.BLL;
using BnpParibas.Service.Net.Ecm;

namespace BnpParibas.Service.ImageJob
{
    public class JobManager
    {
        #region PROPERTIES
        public JobData JobData { get; set; } = null;
        #endregion

        #region CONSTRUCTORS
        public JobManager()
        {
        }
        #endregion

        #region PUBLIC METHODS
        public int AddJobRecord()
        {
            foreach (ArchiveTarget target in JobData.ArchiveTargets)
            {
                Case cs = GetArchiveTargetCase(target.ContractNo);
                if (cs == null) continue;

                JobData.JobRecords.Add(new JobRecord()
                {
                    TargetPolicyNo = target.ContractNo,
                    Case = cs
                });
            }

            return JobData.JobRecords.Count;
        }

        public void CreateJobFiles()
        {
            JobData.CreateJobFiles();
        }

        public void DownloadImageFiles()
        {

        }

        public int GetArchiveTargetCount()
        {
            JobData.NumOfTargets = ArchiveTarget.GetArciveTargetCount();
            return JobData.NumOfTargets;
        }

        public int GetArchiveTargets()
        {
            JobData.ArchiveTargets = ArchiveTarget.GetArchiveTargets();
            if (JobData.ArchiveTargets == null) return 0;
            return JobData.ArchiveTargets.Count;
        }
        #endregion

        #region PRIVATE METHODS
        private Case GetArchiveTargetCase(string bsn)
        {
            var cbn = CaseBusinessNumber.GetCaseBusinessNumber(
                BusinessNumberType.PolicyNumber, bsn);
            if (cbn != null) return Case.GetCase(cbn.CaseId);

            return null;
        }
        #endregion
    }
}
