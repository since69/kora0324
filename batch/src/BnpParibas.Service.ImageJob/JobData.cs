﻿//
// BnpParibas.Service.ImageJob.JobDatas
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.Collections.Generic;
using BnpParibas.Service.IO;
using BnpParibas.Service.Data.BLL;

namespace BnpParibas.Service.ImageJob
{
    public class JobFile
    {
        public string ElementId { get; set; } = string.Empty;
        public DownloadFile DownloadFile { get; set; }
        public UploadFile UploadFile { get; set; }
    }

    public class JobRecord
    {
        #region PROPERTIES
        public string TargetPolicyNo { get; set; } = string.Empty;
        public Case Case { get; set; } = null;
        public List<JobFile> JobFiles { get; set; } = new List<JobFile>();
        #endregion

        #region CONSTRUCTORS
        public JobRecord() {}
        #endregion

        #region PUBLIC METHODS
        public void CreateJobFile()
        {
            foreach (CaseDocument doc in Case.CaseDocuments)
            {
                foreach (CaseDocumentPage page in doc.CaseDocumentPages)
                {
                    if (string.IsNullOrEmpty(page.ImageFileId)) continue;

                    JobFiles.Add(new JobFile()    
                    {
                        ElementId = page.ImageFileId,
                    });
                }
            }
        }
        #endregion
    }

    public class JobData
    {
        #region PROPERTIES
        public int NumOfTargets { get; set; } = 0;
        public List<ArchiveTarget> ArchiveTargets { get; set; } = null;
        public List<JobRecord> JobRecords { get; set; }
        #endregion

        #region CONSTRUCTORS
        public JobData()
        {
            JobRecords = new List<JobRecord>();
        }
        #endregion

        #region PUBLIC METHODS
        public void CreateJobFiles()
        {
            foreach (JobRecord r in JobRecords)
                r.CreateJobFile();
        }
        #endregion
    }
}
