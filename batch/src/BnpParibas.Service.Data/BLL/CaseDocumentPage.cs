﻿//
// BnpParibas.Service.Data.BLL.CaseDocumentPage
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.Collections.Generic;
using BnpParibas.Service.Data.DAL;

namespace BnpParibas.Service.Data.BLL
{
    public class CaseDocumentPage : BaseCase
    {
        #region PROPERTIES
        public int CaseDocumentPageId { get; set; } = int.MinValue;
        public int CaseDocumentId { get; set; } = int.MinValue;
        public string ImageFileId { get; set; } = string.Empty;
        #endregion

        #region CONSTRUCTORS
        public CaseDocumentPage() { }

        public CaseDocumentPage(int caseDocumentPageId, 
            int caseDocumentId, string imageFileId)
        {
            CaseDocumentPageId = caseDocumentPageId;
            CaseDocumentId = caseDocumentId;
            ImageFileId = imageFileId;
        }
        #endregion

        #region PUBLIC METHODS
        public static List<CaseDocumentPage> GetCaseDocumentPages(
            int caseDocumentId)
        {
            return GetCaseDocumentPagesFromModel(
                SiteProvider.Case.GetCaseDocumentPages(caseDocumentId));
        }
        #endregion

        #region PRIVATE METHODS
        private static CaseDocumentPage GetCaseDocumentPageFromModel(
            DAL.Model.CaseDocumentPage r)
        {
            if (r == null) return null;

            return new CaseDocumentPage(r.CaseDocumentPageId, 
                r.CaseDocumentId, 
                r.ImageFileId);
        }

        private static List<CaseDocumentPage> GetCaseDocumentPagesFromModel(
            List<DAL.Model.CaseDocumentPage> rs)
        {
            List<CaseDocumentPage> pages = new List<CaseDocumentPage>();
            foreach (DAL.Model.CaseDocumentPage r in rs)
                pages.Add(GetCaseDocumentPageFromModel(r));
            return pages;
        }
        #endregion
    }
}
