﻿//
// BnpParibas.Service.Data.BLL.CaseBusinessNumber
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.Collections.Generic;
using BnpParibas.Service.Data.DAL;

namespace BnpParibas.Service.Data.BLL
{
    public class CaseBusinessNumber : BaseCase
    {
        #region PROPERTIES
        public int CaseId { get; set; } = int.MinValue;
        /// <summary>
        /// Business number types.
        /// 1 - 증번, 2 - 접수번호, 3 - 가입설계동의번호
        /// </summary>
        public int NumberType { get; set; } = int.MinValue;
        public string BusinessNumber { get; set; } = string.Empty;
        #endregion

        #region CONSTRUCTORS
        public CaseBusinessNumber() { }

        public CaseBusinessNumber(int caseId, int numberType, 
            string businessNumber)
        {
            CaseId = caseId;
            NumberType = numberType;
            BusinessNumber = businessNumber;
        }
        #endregion

        #region PUBLIC METHODS
        public static CaseBusinessNumber GetCaseBusinessNumber(int caseId)
        {
            return GetCaseBusinessNumberFromModel(
                SiteProvider.Case.GetCaseBusinessNumber(caseId));
        }

        public static CaseBusinessNumber GetCaseBusinessNumber(int bsnType, string bsn)
        {
            return GetCaseBusinessNumberFromModel(
                SiteProvider.Case.GetCaseBusinessNumber(bsnType, bsn));
        }

        public static List<CaseBusinessNumber> GetCaseBusinessNumbers(string bsn)
        {
            return GetCaseBusinessNumbersFromModel(
                    SiteProvider.Case.GetCaseBusinessNumbers(bsn));
        }

        public static bool DeleteCaseBusinessNumber(int caseId)
        {
            return SiteProvider.Case.DeleteCaseBusinessNumber(caseId);
        }
        #endregion

        #region PRIVATE METHODS
        private static CaseBusinessNumber GetCaseBusinessNumberFromModel(
            DAL.Model.CaseBusinessNumber r)
        {
            if (r == null) return null;

            return new CaseBusinessNumber(r.CaseId, 
                r.NumberType, r.BusinessNumber);
        }

        private static List<CaseBusinessNumber> 
            GetCaseBusinessNumbersFromModel(List<DAL.Model.CaseBusinessNumber> rs)
        {
            List<CaseBusinessNumber> numbers = new List<CaseBusinessNumber>();
            foreach (DAL.Model.CaseBusinessNumber r in rs)
                numbers.Add(GetCaseBusinessNumberFromModel(r));
            return numbers;
        }
        #endregion
    }
}
