﻿//
// BnpParibas.Service.Data.BLL.Case
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.Collections.Generic;
using BnpParibas.Service.Data.DAL;

namespace BnpParibas.Service.Data.BLL
{
    public class Case : BaseCase
    {
        #region PROPERTIES
        public int CaseId { get; set; } = int.MinValue;
        public string BusinessTypeCode { get; set; } = string.Empty;
        public string ProductCategoryCode { get; set; } = string.Empty;
        public string ContractorTypeCode { get; set; } = string.Empty;
        public CaseBusinessNumber CaseBusinessNumber { get; set; } = null;
        public List<CaseDocument> CaseDocuments { get; set; }
            = new List<CaseDocument>();
        #endregion

        #region CONSTRUCTORS
        public Case() { }

        public Case(int caseId, string businessTypeCode, 
            string productCategoryCode, string contractorTypeCode)
        {
            CaseId = caseId;
            BusinessTypeCode = businessTypeCode;
            ProductCategoryCode = productCategoryCode;
            ContractorTypeCode = contractorTypeCode;
            CaseBusinessNumber = CaseBusinessNumber.GetCaseBusinessNumber(caseId);
            CaseDocuments = CaseDocument.GetCaseDocuments(caseId);
        }
        #endregion

        #region PUBLIC METHODS
        public static Case GetCase(int id)
        {
            return GetCaseFromModel(SiteProvider.Case.GetCase(id));
        }
        #endregion

        #region PRIVATE METHODS
        private static Case GetCaseFromModel(DAL.Model.Case r)
        {
            if (r == null) return null;

            return new Case(r.CaseId, 
                r.BusinessTypeCode, 
                r.ProductCategoryCode, 
                r.ContractorTypeCode);
        }

        private static List<Case> GetCasesFromModel(List<DAL.Model.Case> rs)
        {
            List<Case> cases = new List<Case>();
            foreach (DAL.Model.Case r in rs)
                cases.Add(GetCaseFromModel(r));
            return cases;
        }
        #endregion
    }
}
