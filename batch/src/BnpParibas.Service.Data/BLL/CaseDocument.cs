﻿//
// BnpParibas.Service.Data.BLL.CaseDocument
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.Collections.Generic;
using BnpParibas.Service.Data.DAL;

namespace BnpParibas.Service.Data.BLL
{
    public class CaseDocument : BaseCase
    {
        #region PROPERTIES
        public int CaseDocumentId { get; set; } = int.MinValue;
        public int CaseId { get; set; } = int.MinValue;
        public int DocumentCategoryId { get; set; } = int.MinValue;
        public int DocumentSubcategoryId { get; set; } = int.MinValue;
        public List<CaseDocumentPage> CaseDocumentPages { get; set; } 
            = new List<CaseDocumentPage>();
        #endregion

        #region CONSTRUCTORS
        public CaseDocument() { }

        public CaseDocument(int caseDocId, int caseId, int docCategoryId, 
            int docSubcategoryId)
        {
            CaseDocumentId = caseDocId;
            CaseId = caseId;
            DocumentCategoryId = docCategoryId;
            DocumentSubcategoryId = docSubcategoryId;
            CaseDocumentPages = CaseDocumentPage.GetCaseDocumentPages(caseDocId);
        }
        #endregion

        #region PUBLIC METHODS
        public static List<CaseDocument> GetCaseDocuments(int caseId)
        {
            return GetCaseDocumentsFromModel(
                SiteProvider.Case.GetCaseDocuments(caseId));
        }
        #endregion

        #region PRIVATE METHODS
        private static CaseDocument GetCaseDocumentFromModel(
            DAL.Model.CaseDocument r)
        {
            if (r == null) return null;

            return new CaseDocument(r.CaseDocumentId, 
                r.CaseId,
                r.DocumentCategoryId, 
                r.DocumentSubcategoryId);
        }

        private static List<CaseDocument> GetCaseDocumentsFromModel(
            List<DAL.Model.CaseDocument> rs)
        {
            List<CaseDocument> docs = new List<CaseDocument>();
            foreach (DAL.Model.CaseDocument r in rs)
                docs.Add(GetCaseDocumentFromModel(r));
            return docs;
        }
        #endregion
    }
}
