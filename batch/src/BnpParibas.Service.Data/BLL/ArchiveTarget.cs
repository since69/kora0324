﻿//
// BnpParibas.Service.Data.BLL.ArchiveTarget
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.Collections.Generic;
using BnpParibas.Service.Data.DAL;

namespace BnpParibas.Service.Data.BLL
{
    public class ArchiveTarget : BaseCase
    {
        #region PROPERTIES
        /// <summary>
        /// 분리보관이 실행된 날자와 시간.
        /// </summary>
        public string OperationMonth { get; set; } = string.Empty;
        /// <summary>
        /// 증권번호.
        /// </summary>
        public string ContractNo { get; set; } = string.Empty;
        /// <summary>
        /// 분리보관 실행 여부.
        /// 0 - 미실행, 1 - 실행
        /// </summary>
        public int IsExecuted { get; set; } = 0;
        #endregion

        #region CONSTRUCTORS
        public ArchiveTarget() { }

        public ArchiveTarget(string operationMonth,
            string contractNo,
            int isExcuted)
        {
            OperationMonth = operationMonth;
            ContractNo = contractNo;
            IsExecuted = isExcuted;
        }
        #endregion

        #region PUBLIC METHODS
        public static int GetArciveTargetCount()
        {
            return SiteProvider.Case.GetArchiveTargetCount();
        }

        public static List<ArchiveTarget> GetArchiveTargets()
        {
            List<ArchiveTarget> targets = GetArchiveTargetListFromModels(
                SiteProvider.Case.GetArchiveTargets());
            return targets;
        }
        #endregion

        #region PRIVATE METHODS
        private static ArchiveTarget GetArchiveTargetFromModel(
            DAL.Model.ArchiveTarget r)
        {
            if (r == null) return null;

            return new ArchiveTarget(r.OperationMonth, 
                r.ContractNo, r.IsExecuted);
        }

        private static List<ArchiveTarget> GetArchiveTargetListFromModels(
            List<DAL.Model.ArchiveTarget> rs)
        {
            List<ArchiveTarget> targets = new List<ArchiveTarget>();
            foreach (DAL.Model.ArchiveTarget r in rs)
                targets.Add(GetArchiveTargetFromModel(r));
            return targets;
        }
        #endregion
    }
}
