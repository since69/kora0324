﻿//
// BnpParibas.Service.Data.DAL.DataAccess
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace BnpParibas.Service.Data.DAL
{
    /// <summary>
    /// All database abstract providers must inherit.
    /// This class has some properties that wrap the value from configuration 
    /// file (app.config or web.config) and wrapping methods for DbCommand
    /// object's(SqlCommand, OleDbCommand, OracleCommand) default 
    /// methods(ExecuteNonQuery, ExecuteReader, ExecuteScalar).
    /// </summary>
    public abstract class DataAccess
    {
        #region PROPERTIES
        public string ConnectionString { get; set; } = string.Empty;
        #endregion

        #region METHODS
        protected DbParameter AddParameter(DbCommand command,
            string parameterName,
            object parameterValue)
        {
            return AddParameter(command, parameterName, parameterValue,
                ParameterDirection.Input);
        }

        protected DbParameter AddParameter(DbCommand command,
            string parameterName,
            object parameterValue,
            ParameterDirection direction)
        {
            DbParameter dbp = command.CreateParameter();
            dbp.ParameterName = parameterName;
            dbp.Value = parameterValue;
            dbp.Direction = direction;
            command.Parameters.Add(dbp);
            return dbp;
        }

        protected DbParameter AddParameter(DbCommand command,
            string parameterName,
            object parameterValue,
            DbType dbType,
            ParameterDirection direction)
        {
            DbParameter dbp = command.CreateParameter();
            dbp.ParameterName = parameterName;
            dbp.Value = parameterValue;
            dbp.DbType = dbType;
            dbp.Direction = direction;
            command.Parameters.Add(dbp);
            return dbp;
        }

        protected void AddParameters(DbCommand command, 
            List<DataParameter> parameters)
        {
            foreach (DataParameter param in parameters)
            {
                if (param.Value == null && param.IsOptional)
                    continue;
                AddParameter(command, param.Name, param.Value, param.DbType,
                    param.Direction);
            }
        }

        protected int ExecuteNonQuery(DbCommand cmd)
        {
            return cmd.ExecuteNonQuery();
        }

        protected IDataReader ExecuteReader(DbCommand cmd)
        {
            return ExecuteReader(cmd, CommandBehavior.Default);
        }

        protected IDataReader ExecuteReader(DbCommand cmd,
            CommandBehavior beheavior)
        {
            return cmd.ExecuteReader(beheavior);
        }

        protected object ExecuteScalar(DbCommand cmd)
        {
            return cmd.ExecuteScalar();
        }
        #endregion
    }
}
