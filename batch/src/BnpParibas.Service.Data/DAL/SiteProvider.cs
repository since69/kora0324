﻿//
// BnpParibas.Service.Configuration.Configurator
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibas.Service.Data.DAL
{
    public static class SiteProvider
    {
        public static CaseProvider Case
        {
            get { return CaseProvider.Instance; }
        }
    }
}
