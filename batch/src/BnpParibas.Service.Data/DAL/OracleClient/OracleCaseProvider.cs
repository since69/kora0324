﻿//
// BnpParibas.Service.Data.DAL.OracleClient.OracleCaseProvider
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Oracle.ManagedDataAccess.Client;
using log4net;
using BnpParibas.Service.Data.DAL.Model;

namespace BnpParibas.Service.Data.DAL.OracleClient
{
    public class OracleCaseProvider : CaseProvider
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("OracleCaseProvider");
        private readonly StringBuilder sql = new StringBuilder();
        #endregion

        #region PUBLIC METHODS
        public override int GetArchiveTargetCount()
        {
            sql.Clear();
            sql.Append("SELECT\n");
            sql.Append("\tCOUNT(CONTRACT_NO)\n");
            sql.Append("FROM\n");
            sql.Append("\tTB_SEP_CONT_LISTS\n");
            sql.Append("WHERE\n");
            sql.Append("\tIS_EXECUTED = 0\n");
            logger.Debug(sql.ToString());

            using (OracleConnection cn = new OracleConnection(ConnectionString))
            {
                OracleCommand cmd = new OracleCommand(sql.ToString(), cn);
                cn.Open();
                return Convert.ToInt32(ExecuteScalar(cmd));
            }
        }
        public override List<ArchiveTarget> GetArchiveTargets()
        {
            sql.Clear();
            sql.Append("SELECT\n");
            sql.Append("\tOPERATION_MONTH\n");
            sql.Append("\tCONTRACT_NO\n");
            sql.Append("\tIS_EXECUTED\n");
            sql.Append("FROM\n");
            sql.Append("\tTB_SEP_CONT_LISTS\n");
            sql.Append("WHERE\n");
            sql.Append("\tIS_EXECUTED = 0\n");
            logger.Debug(sql.ToString());

            using (OracleConnection cn = new OracleConnection(ConnectionString))
            {
                OracleCommand cmd = new OracleCommand(sql.ToString(), cn);
                cn.Open();
                return GetArchiveTargetsFromReader(ExecuteReader(cmd));
            }
        }

        public override Case GetCase(int id)
        {
            sql.Append("SELECT\n");
            sql.Append("\tCaseID,\n");
            sql.Append("\tBusinessTypeCode,\n");
            sql.Append("\tProductCategoryCode,\n");
            sql.Append("\tContractorTypeCode,\n");
            sql.Append("FROM\n");
            sql.Append("\tCase\n");
            sql.Append("WHERE\n");
            sql.Append($"\tCaseID = {id}\n");
            logger.Debug(sql.ToString());

            using (OracleConnection cn = new OracleConnection(ConnectionString))
            {
                OracleCommand cmd = new OracleCommand(sql.ToString(), cn);
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetCaseFromReader(reader);
                return null;
            }
        }

        public override CaseBusinessNumber GetCaseBusinessNumber(int caseId)
        {
            sql.Clear();
            sql.Append("SELECT\n");
            sql.Append("\tCaseID\n");
            sql.Append("\t, BusinessNumberType\n");
            sql.Append("\t, BusinessNumber\n");
            sql.Append("FROM\n");
            sql.Append("\tCaseBusinessNumber\n");
            sql.Append("WHERE\n");
            sql.Append($"\tCaseID = {caseId}\n");
            logger.Debug(sql.ToString());

            using (OracleConnection cn = new OracleConnection(ConnectionString))
            {
                OracleCommand cmd = new OracleCommand(sql.ToString(), cn);
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetCaseBusinessNumberFromReader(reader);
                return null;
            }
        }

        public override CaseBusinessNumber GetCaseBusinessNumber(int bsnType, string bsn)
        {
            sql.Clear();
            sql.Append("SELECT\n");
            sql.Append("\tCaseID\n");
            sql.Append("\t, BusinessNumberType\n");
            sql.Append("\t, BusinessNumber\n");
            sql.Append("FROM\n");
            sql.Append("\tCaseBusinessNumber\n");
            sql.Append("WHERE\n");
            sql.Append($"\tBusinessNumberType = {bsnType}\n");
            sql.Append($"\tAND BusinessNumber = {bsn}\n");
            logger.Debug(sql.ToString());

            using (OracleConnection cn = new OracleConnection(ConnectionString))
            {
                OracleCommand cmd = new OracleCommand(sql.ToString(), cn);
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read()) return GetCaseBusinessNumberFromReader(reader);
                return null;
            }
        }

        public override List<CaseBusinessNumber> 
            GetCaseBusinessNumbers(string bsn)
        {
            sql.Clear();
            sql.Append("SELECT\n");
            sql.Append("\tCaseID\n");
            sql.Append("\t, BusinessNumberType\n");
            sql.Append("\t, BusinessNumber\n");
            sql.Append("FROM\n");
            sql.Append("\tCaseBusinessNumber\n");
            sql.Append("WHERE\n");
            sql.Append($"\tBusinessNumber = '{bsn}'\n");
            logger.Debug(sql.ToString());

            using (OracleConnection cn = new OracleConnection(ConnectionString))
            {
                OracleCommand cmd = new OracleCommand(sql.ToString(), cn);
                cn.Open();
                return GetCaseBusinessNumbersFromReader(ExecuteReader(cmd));
            }
        }

        public override bool DeleteCaseBusinessNumber(int caseId)
        {
            sql.Clear();
            sql.Append("DELETE FROM CaseBusinessNumber\n");
            sql.Append($"WHERE CaseID = {caseId}");
            logger.Debug(sql.ToString());

            using (OracleConnection cn = new OracleConnection(ConnectionString))
            {
                OracleCommand cmd = new OracleCommand(sql.ToString(), cn);
                cn.Open();
                int retVal = ExecuteNonQuery(cmd);
                return (retVal == 1);
            }
        }

        public override List<CaseDocument> GetCaseDocuments(int caseId)
        {
            sql.Clear();
            sql.Append("SELECT\n");
            sql.Append("\tCaseDocumentID\n");
            sql.Append("\tCaseID\n");
            sql.Append("\tDocumentCategoryID\n");
            sql.Append("\tDocumentSubcategoryID\n");
            sql.Append("FROM\n");
            sql.Append("\tCaseDocument\n");
            sql.Append("WHERE\n");
            sql.Append($"\tCaseId = {caseId}\n");
            logger.Debug(sql.ToString());

            using (OracleConnection cn = new OracleConnection(ConnectionString))
            {
                OracleCommand cmd = new OracleCommand(sql.ToString(), cn);
                cn.Open();
                return GetCaseDocumentsFromReader(ExecuteReader(cmd));
            }
        }

        public override List<CaseDocumentPage>
            GetCaseDocumentPages(int caseDocumentId)
        {
            sql.Clear();
            sql.Append("SELECT\n");
            sql.Append("\tCaseDocumentPageID\n");
            sql.Append("\tCaseDocumentID\n");
            sql.Append("\tImageFileID\n");
            sql.Append("FROM\n");
            sql.Append("\tCaseDocumentPage\n");
            sql.Append("WHERE\n");
            sql.Append($"\tCaseDocumentID = {caseDocumentId}\n");
            logger.Debug(sql.ToString());

            using (OracleConnection cn = new OracleConnection(ConnectionString))
            {
                OracleCommand cmd = new OracleCommand(sql.ToString(), cn);
                cn.Open();
                return GetCaseDocumentPagesFromReader(ExecuteReader(cmd));
            }
        }
        #endregion
    }
}
