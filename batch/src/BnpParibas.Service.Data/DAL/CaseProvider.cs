﻿//
// BnpParibas.Service.Data.DAL.ImageScanProvider
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System;
using System.Collections.Generic;
using System.Data;
using BnpParibas.Service.Configuration;
using BnpParibas.Service.Data.DAL.Model;

namespace BnpParibas.Service.Data.DAL
{
    public abstract class CaseProvider : DataAccess
    {
        #region PROPERTIES
        private static CaseProvider instance = null;
        /// <summary>
        /// Returns an instance of the provider type specified in the config file.
        /// </summary>
        public static CaseProvider Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (CaseProvider)Activator.CreateInstance(
                        Type.GetType(Configurator.ImageServiceSettings
                            .Case.ProviderType));
                }
                return instance;
            }
        }
        #endregion

        #region CONSTRUCTORS
        public CaseProvider()
        {
            ConnectionString = Configurator.ImageServiceSettings
                .Case.ConnectionString;
        }
        #endregion

        #region ABSTRACT MTHODS
        // methods that work with SepContLists.
        public abstract int GetArchiveTargetCount();
        public abstract List<ArchiveTarget> GetArchiveTargets();


        // methods that work with Case.
        public abstract Case GetCase(int id);


        // methods that work with CaseBusinessNumber.
        public abstract CaseBusinessNumber GetCaseBusinessNumber(int caseId);
        public abstract CaseBusinessNumber GetCaseBusinessNumber(int bsnType, string bsn);
        public abstract List<CaseBusinessNumber> 
            GetCaseBusinessNumbers(string bsn);
        public abstract bool DeleteCaseBusinessNumber(int caseId);


        // methods htat work with CaseDocument.
        public abstract List<CaseDocument> GetCaseDocuments(int caseId);


        // methods htat work with CaseDocumentPage.
        public abstract List<CaseDocumentPage> 
            GetCaseDocumentPages(int caseDocumentId);
        #endregion

        #region VIRTUAL METHODS
        protected virtual ArchiveTarget
            GetArchiveTargetFromReader(IDataReader reader)
        {
            return new ArchiveTarget()
            {
                OperationMonth = reader["OPERATION_MONTH"].ToString(),
                ContractNo = reader["CONTRACT_NO"].ToString(),
                IsExecuted = Convert.ToInt32(reader["IS_EXECUTED"]),
            };
        }

        protected virtual List<ArchiveTarget>
            GetArchiveTargetsFromReader(IDataReader reader)
        {
            List<ArchiveTarget> targets = new List<ArchiveTarget>();
            while (reader.Read())
                targets.Add(GetArchiveTargetFromReader(reader));
            return targets;
        }


        protected virtual Case GetCaseFromReader(IDataReader reader)
        {
            return new Case()
            {
                CaseId = Convert.ToInt32(reader["CaseID"]),
                BusinessTypeCode = reader["BusinessTypeCode"].ToString(),
                ProductCategoryCode = reader["ProductCategoryCode"].ToString(),
                ContractorTypeCode = reader["ContractorTypeCode"].ToString()
            };
        }

        protected virtual List<Case> GetCasesFromReader(IDataReader reader)
        {
            List<Case> cases = new List<Case>();
            while (reader.Read())
                cases.Add(GetCaseFromReader(reader));
            return cases;
        }


        protected virtual CaseBusinessNumber 
            GetCaseBusinessNumberFromReader(IDataReader reader)
        {
            return new CaseBusinessNumber()
            {
                CaseId = Convert.ToInt32(reader["CaseID"]),
                NumberType = Convert.ToInt32(reader["NumberType"]),
                BusinessNumber = reader["BusinessNumber"].ToString(),
            };
        }

        protected virtual List<CaseBusinessNumber> 
            GetCaseBusinessNumbersFromReader(IDataReader reader)
        {
            List<CaseBusinessNumber> numbers = new List<CaseBusinessNumber>();
            while (reader.Read())
                numbers.Add(GetCaseBusinessNumberFromReader(reader));
            return numbers;
        }


        protected virtual CaseDocument
            GetCaseDocumentFromReader(IDataReader reader)
        {
            return new CaseDocument()
            {
                CaseDocumentId = Convert.ToInt32(reader["CaseDocumentID"]),
                CaseId = Convert.ToInt32(reader["CaseID"]),
                DocumentCategoryId = Convert.ToInt32(reader["DocumentCategoryID"]),
                DocumentSubcategoryId = Convert.ToInt32(reader["DocumentSubcategoryID"])
            };
        }

        protected virtual List<CaseDocument>
            GetCaseDocumentsFromReader(IDataReader reader)
        {
            List<CaseDocument> docs = new List<CaseDocument>();
            while (reader.Read())
                docs.Add(GetCaseDocumentFromReader(reader));
            return docs;
        }


        protected virtual CaseDocumentPage
            GetCaseDocumentPageFromReader(IDataReader reader)
        {
            return new CaseDocumentPage()
            {
                CaseDocumentPageId = Convert.ToInt32(reader["CaseDocumentPageID"]),
                CaseDocumentId = Convert.ToInt32(reader["CaseDocumentId"]),
                ImageFileId = reader["ImageFileId"].ToString()
            };
        }

        protected virtual List<CaseDocumentPage>
            GetCaseDocumentPagesFromReader(IDataReader reader)
        {
            List<CaseDocumentPage> pages = new List<CaseDocumentPage>();
            while (reader.Read())
                pages.Add(GetCaseDocumentPageFromReader(reader));
            return pages;
        }
        #endregion
    }
}
