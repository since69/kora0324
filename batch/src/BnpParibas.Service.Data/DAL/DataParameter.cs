﻿//
// BnpParibars.Service.Data.DAL.ParameterWrapper
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020 Thinkbox Co. Ltd
//

using System.Data;

namespace BnpParibas.Service.Data.DAL
{
    /// <summary>
    /// Database paremeter wrapping class.
    /// </summary>
    public class DataParameter
    {
        #region PROPERTIES
        public string Name { get; set; }
        public DbType DbType { get; set; }
        public object Value { get; set; }
        public ParameterDirection Direction { get; set; }
        public bool IsOptional { get; set; }
        #endregion

        #region CONSTRUCTORS
        public DataParameter(string name, object value, bool isOptional = false)
        {
            Name = name;
            Value = value;
            Direction = ParameterDirection.Input;
            IsOptional = isOptional;
        }

        public DataParameter(string name, object value, DbType dbType,
            bool isOptional = false)
        {
            Name = name;
            Value = value;
            DbType = dbType;
            Direction = ParameterDirection.Input;
            IsOptional = isOptional;
        }

        public DataParameter(string name, object value, DbType dbType,
            ParameterDirection direction)
        {
            Name = name;
            Value = value;
            DbType = dbType;
            Direction = direction;
        }
        #endregion
    }
}
