﻿//
// BnpParibas.Service.Data.DAL.Model.CaseDocument
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibas.Service.Data.DAL.Model
{
    /// <summary>
    /// IMGSCANP.CaseDocument wrapping class.
    /// </summary>
    public class CaseDocument
    {
        public int CaseDocumentId { get; set; } = int.MinValue;
        public int CaseId { get; set; } = int.MinValue;
        public int DocumentCategoryId { get; set; } = int.MinValue;
        public int DocumentSubcategoryId { get; set; } = int.MinValue;
    }
}
