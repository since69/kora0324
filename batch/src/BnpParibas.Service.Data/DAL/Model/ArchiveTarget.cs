﻿//
// BnpParibas.Service.Data.DAL.Model.ArchiveTarget
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibas.Service.Data.DAL.Model
{
    /// <summary>
    /// IMGSCANP.TB_SEP_CONT_LISTS wrapping class.
    /// </summary>
    public class ArchiveTarget
    {
        /// <summary>
        /// 분리보관이 실행된 날자와 시간.
        /// </summary>
        public string OperationMonth { get; set; } = string.Empty;
        /// <summary>
        /// 증권번호.
        /// </summary>
        public string ContractNo { get; set; } = string.Empty;
        /// <summary>
        /// 분리보관 실행 여부.
        /// 0 - 미실행, 1 - 실행
        /// </summary>
        public int IsExecuted { get; set; } = 0;
    }
}
