﻿//
// BnpParibas.Service.Data.DAL.Model.CaseDocumentPage
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibas.Service.Data.DAL.Model
{
    /// <summary>
    /// IMGSCANP.CaseDocumentPage wrapping class.
    /// </summary>
    public class CaseDocumentPage
    {
        public int CaseDocumentPageId { get; set; } = int.MinValue;
        public int CaseDocumentId { get; set; } = int.MinValue;
        public string ImageFileId { get; set; } = string.Empty;
    }
}
