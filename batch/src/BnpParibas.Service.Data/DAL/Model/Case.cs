﻿//
// BnpParibas.Service.Data.DAL.Model.Case
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibas.Service.Data.DAL.Model
{
    /// <summary>
    /// IMGSCANP.CaseBusinessNumber wrapping class.
    /// </summary>
    public class Case
    {
        public int CaseId { get; set; } = int.MinValue;
        public string BusinessTypeCode { get; set; } = string.Empty;
        public string ProductCategoryCode { get; set; } = string.Empty;
        public string ContractorTypeCode { get; set; } = string.Empty;
    }
}
