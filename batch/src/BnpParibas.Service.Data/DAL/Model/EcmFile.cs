﻿//
// BnpParibas.Service.Data.DAL.Model.EcmFile
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibas.Service.Data.DAL.Model
{
    /// <summary>
    /// IMGSCANP.ECM_FILE wrapping class.
    /// </summary>
    public class EcmFile
    {
        public string FileId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileExt { get; set; }
        public int FileSize { get; set; }
        public int FileVersion { get; set; }
        public string ArchiveId { get; set; }
    }
}
