﻿//
// BnpParibas.Service.Data.DAL.Model.CaseBusinessNumber
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibas.Service.Data.DAL.Model
{
    /// <summary>
    /// IMGSCANP.CaseBusinessNumber wrapping class.
    /// </summary>
    public class CaseBusinessNumber
    {
        public int CaseId { get; set; } = int.MinValue;
        /// <summary>
        /// Business number types.
        /// 1 - 증번, 2 - 접수번호, 3 - 가입설계동의번호
        /// </summary>
        public int NumberType { get; set; } = int.MinValue;
        public string BusinessNumber { get; set; } = string.Empty;
    }
}
