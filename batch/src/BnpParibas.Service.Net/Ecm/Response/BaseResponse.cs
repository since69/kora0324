﻿//
// BnpParibas.Service.Net.Ecm.Response.BaseResponse
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibas.Service.Net.Ecm.Response
{
    public class BaseResponse
    {
        public int ErrCode { get; set; }
        public string ErrName { get; set; }
        public string ErrDesc { get; set; }
        public int ResultCode { get; set; }
        public string ResultDesc { get; set; }
    }
}
