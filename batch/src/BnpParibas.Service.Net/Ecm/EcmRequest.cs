﻿//
// BnpParibas.Service.Net.EcmRequest
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System.Net;
using log4net;
using BnpParibas.Service.Configuration;

namespace BnpParibas.Service.Net.Ecm
{
    public class EcmRequest
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("EcmRequest");
        #endregion

        #region PUBLIC METHODS
        public static bool Download(string elementId)
        {
            string url = GetServletUrl(EcmServletTypes.DownloadFile);
            if (string.IsNullOrEmpty(url)) return false;



            return true;
        }

        /// <summary>
        /// Delete image file from ECM.
        /// This method use only separate archiving job.
        /// </summary>
        /// <param name="elementId"></param>
        /// <returns></returns>
        public static bool Delete(string elementId)
        {
            return true;
        }

        /// <summary>
        /// Delete image file and file information from ECM.
        /// This method use only destroy document job.
        /// </summary>
        /// <param name="elementId"></param>
        /// <returns></returns>
        public static bool DeleteFileAndData(string elementId)
        {
            return true;
        }
        #endregion

        #region PRIVATE METHODS
        private static string GetServletUrl(EcmServletTypes type)
        {
            var ecm = Configurator.ImageServiceSettings.Ecm;
            foreach (ServletElement servlet in ecm.Servlets)
            {
                if (servlet.Type.Equals((int)type))
                {
                    return string.Format("{0}:{1}/{2}/{3}/{4}", 
                        ecm.Ip, ecm.Port, ecm.BaseDir, servlet.Dir, 
                        servlet.Name);
                }
            }
            logger.Error("Can't find ECM servlet url.");
            return string.Empty;
        }
        #endregion
    }
}
