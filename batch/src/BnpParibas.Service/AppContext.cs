﻿//
// BnpParibars.Service.AppContext
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using BnpParibas.Service.IO;

namespace BnpParibas.Service
{
    /// <summary>
    /// 실행중인 응용 프로그램의 정보를 담고 있습니다. 이 클래스는 상속될 수 없습니다.
    /// </summary>
    public sealed class AppContext
    {
        #region PROPERTIES
        public string AppName { get; set; } = "";
        public System.Version AppVersion { get; set; }
        public string AppDescription { get; set; } = "";
        public string Copyright { get; set; } = "";
        public DownloadDir DownloadDir { get; set; }
        public UploadDir UploadDir { get; set; }
        #endregion

        #region CONSTRUCTORS
        public AppContext()
        {
        }
        #endregion
    }
}
