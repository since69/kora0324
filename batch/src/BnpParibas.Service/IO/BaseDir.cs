﻿//
// BnpParibas.Service.IO.BaseDir
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibas.Service.IO
{
    using System;
    using System.IO;
    using log4net;

    public abstract class BaseDir
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("BaseDir");
        #endregion

        #region PROPERTIES
        public string Name { get; set; }
        /// <summary>
        /// Type of directory.
        /// See DirectoryTypes enum.
        /// </summary>
        public DirectoryTypes Type { get; protected set; }
        #endregion

        #region CONSTRUCTORS
        public BaseDir(DirectoryTypes type)
        {
            Type = type;
        }

        public BaseDir() { }
        #endregion

        #region METHODS
        public virtual void Create()
        {
            try
            {
                if (!Exists())
                    Directory.CreateDirectory(Name);
            }
            catch (Exception e)
            {
                logger.Error($"Path '{Name}' creation failed! {e.Message}");
            }
        }

        public virtual void Combine(string path)
        {
            try
            {
                Name = Path.Combine(Name, path);
            }
            catch (Exception e)
            {
                logger.Error($"Combine {Name} to {path} failed! {e.Message}");
            }
        }

        public virtual void DeleteAll()
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(Name);
                foreach (FileInfo f in di.GetFiles())
                    if (f.Exists) f.Delete();
            }
            catch (Exception e)
            {
                logger.Error($"DeleteAll failed! {e.Message}");
            }
        }

        public virtual string[] GetFiles()
        {
            return Directory.GetFiles(Name);
        }

        public virtual bool Exists()
        {
            return Directory.Exists(Name);
        }
        #endregion

    }
}
