﻿//
// BnpParibas.Service.IO.UploadDir
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibas.Service.IO
{
    public class UploadDir : BaseDir
    {
        #region CONSTRUCTORS
        /// <summary>
        /// Recognition destination file upload here.
        /// </summary>
        /// <param name="name">The name of directory to created.</param>
        public UploadDir(string name) : base(DirectoryTypes.Upload)
        {
            Name = name;
        }
        #endregion
    }
}
