﻿//
// BnpParibas.Service.IO.UploadFile
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibas.Service.IO
{
    public class UploadFile : BaseFile
    {
        #region CONSTRUCTORS
        public UploadFile(UploadDir dir, string name) : base(name)
        {
            Dir = dir;
        }
        #endregion
    }
}
