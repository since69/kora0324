﻿//
// BnpParibas.Service.IO.BaseFile
//
// Authors:
//  Joohyoung Kim
//
//  (C) 2020
//

namespace BnpParibas.Service.IO
{
    using System.IO;
    using log4net;

    public abstract class BaseFile
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("BaseFile");
        #endregion

        #region PROPERTIES
        public BaseDir Dir { get; protected set; }

        public string Name { get; set; }

        public string FullName
        {
            get { return Path.Combine(Dir.Name, Name); }
        }

        public string NameWithoutExtension
        {
            get { return Path.GetFileNameWithoutExtension(Name); }
        }

        public string Extension
        {
            get { return Path.GetExtension(Name); }
        }

        public long Length
        {
            get
            {
                if (Exists())
                    return new FileInfo(FullName).Length;

                logger.Error($"Can't get size. {FullName} is not exists.");
                return 0;
            }
        }

        public bool Status { get; set; } = false;
        #endregion

        #region CONSTRUCTORS
        public BaseFile(string name)
        {
            Name = name;
        }
        #endregion

        #region VIRTUAL METHODS
        public void Copy(string destFileName)
        {
            try
            {
                File.Copy(FullName, destFileName);
            }
            catch (System.Exception ex)
            {
                logger.Error($"Can't copy {FullName} to {destFileName}. {ex}");
            }
        }

        public bool Exists()
        {
            return File.Exists(FullName);
        }

        public void Delete()
        {
            try
            {
                if (Exists())
                    File.Delete(FullName);
            }
            catch (System.Exception ex)
            {
                logger.Error($"Can't delete {FullName}. {ex}");
            }
        }

        public void Move(string destFileName)
        {
            try
            {
                File.Move(FullName, destFileName);
            }
            catch (System.Exception ex)
            {
                logger.Error($"Can't move {FullName} to {destFileName}. {ex}");
            }
        }
        #endregion
    }
}
