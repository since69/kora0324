﻿//
// BnpParibars.Service.BusinessNumberType
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibas.Service
{
    public static class BusinessNumberType
    {
        public const int Unknown = 0;
        /// <summary>
        /// 증권번호 (POS/GA/BA)
        /// </summary>
        public const int PolicyNumber = 1;
        /// <summary>
        /// 접수번호 (Claim)
        /// </summary>
        public const int RegistrationNumber = 2;
        /// <summary>
        /// 동의번호(가입설계동의서)
        /// </summary>
        public const int AgreementNumber = 3;
    }
}
