﻿//
// BnpParibars.Util.VersionHelper
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibas.Util
{
    using System;

    public sealed class VersionHelper
    {
        #region PUBLIC METHODS
        public static Version GetAssemblyVersion(Type type)
        {
            return type.Assembly.GetName().Version;
        }
        #endregion
    }
}
