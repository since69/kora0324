﻿//
// BnpParibars.Util.ConsoleHelper
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

namespace BnpParibas.Util
{
    using System;
    using System.Threading;

    public sealed class ConsoleHelper
    {
        // usage: An error occurrend preparing the update.
        private const string ErrorBriefTemplate = "\tAn error occurred {0}";
        // usage: Failed to personalize the software update. Please try again.
        private const string ErrorDescriptionTemplate = "\tFailed to {0}";

        public static void WriteError(string brief, string description = "")
        {
            WriteBrief(string.Format(ErrorBriefTemplate, brief));
            if (string.IsNullOrEmpty(description))
                WriteDescription(description);
        }

        public static void WriteBrief(string brief, int sleepTime = 1000)
        {
            WriteBrief(brief, Console.CursorTop, Console.CursorLeft, sleepTime);
        }

        public static void WriteBrief(string brief, int topPos, int leftPos, 
            int sleepTime = 1000)
        {
            Overwrite(brief, topPos, leftPos, ConsoleColor.White);
            if (sleepTime > 0) Thread.Sleep(sleepTime);
        }

        public static void WriteDescription(string description)
        {
            WriteDescription(description, Console.CursorTop, Console.CursorLeft);
        }

        public static void WriteDescription(string description, 
            int topPos, 
            int leftPos,
            bool isSequentialWrite = false,
            int sleepTime = 1000)
        {
            if (!isSequentialWrite)
                Overwrite(description, topPos, leftPos);
            else
                SequentialWrite(description, topPos, leftPos);

            if (sleepTime > 0)  Thread.Sleep(sleepTime);
        }

        public static void SequentialWrite(string str)
        {
            SequentialWrite(str, Console.CursorTop, Console.CursorLeft);
        }

        public static void SequentialWrite(string str, int topPos, int leftPos)
        {
            string[] words = str.Split('|');

            for (int i = 0; i < words.Length; i++)
            {
                string s = string.Empty;
                for (int j = 0; j <= i; j++)
                    s += words[j];
                Overwrite(s, topPos, leftPos);
                Thread.Sleep(800);
            }
        }

        public static void Overwrite(string str)
        {
            Overwrite(str, Console.CursorTop, Console.CursorLeft);
        }

        public static void Overwrite(string str,
            int topPosition,
            int leftPosition,
            ConsoleColor foregroundColor = ConsoleColor.Gray)
        {
            ConsoleColor prevColor = Console.ForegroundColor;
            Console.ForegroundColor = foregroundColor;
            Console.SetCursorPosition(leftPosition, topPosition);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(leftPosition, topPosition);
            Console.Write(str);
            Console.ForegroundColor = prevColor;
        }

        public static void Write(string str, 
            ConsoleColor foregroundColor = ConsoleColor.Gray)
        {
            ConsoleColor prevColor = Console.ForegroundColor;
            Console.ForegroundColor = foregroundColor;
            Console.Write(str);
            Console.ForegroundColor = prevColor;
        }

        /// <summary>
        /// 선택된 색으로 콘솔에 출력합니다.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="foregroundColor"></param>
        public static void WriteLine(string str, 
            ConsoleColor foregroundColor = ConsoleColor.Gray)
        {
            ConsoleColor prevColor = Console.ForegroundColor;
            Console.ForegroundColor = foregroundColor;
            Console.WriteLine(str);
            Console.ForegroundColor = prevColor;
        }
    }
}
