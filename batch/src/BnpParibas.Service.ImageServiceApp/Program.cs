﻿//
// BnpParibas.Service.ImageServiceApp
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2020
//

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Timers;
using System.IO;
using log4net;
using BnpParibas.Service.Configuration;
using BnpParibas.Service.Data.BLL;
using BnpParibas.Service.ImageJob;
using BnpParibas.Util;

namespace BnpParibas.Service.ImageServiceApp
{
    public class Program
    {
        #region VARIABLES
        private static readonly ILog logger = LogManager.GetLogger("Program");
        private static readonly AppContext appContext = new AppContext();
        private static readonly JobManager jobManager = new JobManager();
        private static bool esc = false;
        private enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }
        private static readonly LinePosition briefLine = new LinePosition()
        {
            TopPos = 15,
            LeftPos = 0
        };
        private static readonly LinePosition dscrLine = new LinePosition()
        {
            TopPos = 16,
            LeftPos = 0
        };
        private static readonly System.Text.StringBuilder msg = 
            new System.Text.StringBuilder();
        #endregion

        #region EXTERNAL LIBRARY METHODS
        // https://msdn.microsoft.com/en-us/library/windows/desktop/ms686016.aspx
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(
            SetConsoleCtrlEventHandler handler, bool add);
        // https://msdn.microsoft.com/en-us/library/windows/desktop/ms683242.aspx
        private delegate bool SetConsoleCtrlEventHandler(CtrlType sig);
        #endregion

        #region CONSTRUCTORS
        static void Main()
        {
            logger.Info("Image Service Started.");
            Console.CursorVisible = false;

            if (!Initialize())
            {
                ShowAppInfo();
                msg.Append("Initialize failed. Application will be terminated.");
                ConsoleHelper.WriteError(msg.ToString());
                return;
            }

            msg.AppendFormat("{0} v{1} IS STARTED.", 
                appContext.AppName, appContext.AppVersion);
            logger.Info(msg.ToString());

            ShowAppInfo();
            while (!esc)
            {
                DoSeparateArchiving();
                System.Threading.Thread.Sleep(5000);
                //DoDestroyImage();
            }

            msg.Clear();
            msg.AppendFormat("\t{0} v{1} IS TERMINATED.",
                appContext.AppName, appContext.AppVersion);
            logger.Info(msg.ToString());

            Console.WriteLine("\n\n\n\n\n" + msg.ToString() + "\n");
            Console.CursorVisible = true;
            Console.WriteLine("\tPress any key to continue.");
            Console.ReadKey();
            Console.Clear();
            logger.Info("Image Service Terminated.");
        }
        #endregion

        #region PRIVATE METHODS
        private static bool Initialize()
        {
            // Register the handler
            SetConsoleCtrlHandler(ConsoleCtrlHandler, true);

            appContext.AppName = "Hecatonchires";
            appContext.AppVersion = System.Reflection.Assembly
                .GetExecutingAssembly().GetName().Version;
            appContext.AppDescription = "Image related batch processing application.";
            appContext.Copyright = "2020 @ Thinkbox Co. Ltd";

            if (!LoadConfiguration()) return false;

            Console.Title = appContext.AppName;
            Console.ResetColor();
            ShowAppInfo();

            WriteBrief("\tINITIALIZING...\n");
            WriteDescription("\tAPPLICATION |INITIALZING. |WAIT |JUST |A MOMENT.",
                true);
            WriteDescription("\tINITIALIZE |FTP CONNECTION.", true);
            WriteDescription("\tFTP CONNECTION IS INITIALIZED.", true);

            return true;
        }
        
        private static bool LoadConfiguration()
        {
            var imgService = Configurator.ImageServiceSettings;

            try
            {
                if (!Directory.Exists(imgService.App.BaseDir))
                    Directory.CreateDirectory(imgService.App.BaseDir);

                appContext.DownloadDir = 
                    new IO.DownloadDir(imgService.App.BaseDir);
                appContext.DownloadDir.Combine(imgService.App.DownloadDir);
                if (!appContext.DownloadDir.Exists()) 
                    appContext.DownloadDir.Create();

                appContext.UploadDir =
                    new IO.UploadDir(imgService.App.BaseDir);
                appContext.UploadDir.Combine(imgService.App.UploadDir);
                if (!appContext.UploadDir.Exists())
                    appContext.UploadDir.Create();

            }
            catch (Exception ex)
            {
                logger.Error($"Error occurred during configuration loading. {ex}");
                return false;
            }

            return true;
        }

        private static void ShowAppInfo()
        {
            Console.Clear();
            Console.WriteLine("\n\n\n\n\n");
            ConsoleHelper.Write($"\t{appContext.AppName.ToUpper()} ", 
                ConsoleColor.Blue);
            ConsoleHelper.Overwrite($"v{appContext.AppVersion}\n\n");
            ConsoleHelper.WriteLine($"\t{appContext.AppDescription}\n");
            ConsoleHelper.WriteLine($"\t{appContext.Copyright}");
            Console.WriteLine("\n\n\n");
        }

        private static void DoSeparateArchiving()
        {
            WriteBrief("\tSEPARATE ARCHIVING...\n");

            jobManager.JobData = new JobData();

            // Get number of separate archiving targets.
            WriteDescription("\tSEARCH THE ITEMS |TO BE ARCHIVED SEPARATELY.", true);
            if (jobManager.GetArchiveTargetCount() == 0)
            {
                WriteDescription("\tSEPARATE ARCHIVING ITEMS |DOES NOT EXISTS.", true);
                return;
            }
            msg.Clear();
            msg.AppendFormat("\tFOUND {0} ITEMS.", 
                jobManager.JobData.NumOfTargets);
            WriteDescription(msg.ToString(), false);

            // Get separate archiving targets.
            WriteDescription("\tGET IMAGE FILE INFORMATION.", false);
            if (jobManager.GetArchiveTargets() == 0)
            {
                msg.Clear();
                msg.Append("\tSEPARATE ARCHIVING ITEM |IS NULL OR EMPTY.");
                WriteDescription(msg.ToString(), true);
                return;
            }

            if (jobManager.AddJobRecord() == 0)
            {
                WriteDescription("\tCAN'T FIND |SUITABLE CASE(S).", true);
                return;
            }
            msg.Clear();
            msg.Append($"\tGOT {jobManager.JobData.JobRecords.Count} INFORMATION.");
            WriteDescription(msg.ToString(), false);

            // Download from ECM 
            WriteDescription("\tDOWNLOAD IMAGE FILE.", false);
            jobManager.CreateJobFiles();
            jobManager.DownloadImageFiles();

        }

        private static void DoDestroyImage()
        {
            WriteBrief("\tDESTROY IMAGES...\n");
            WriteDescription("\tSEARCH THE ITEMS |TO BE DELETED.", true);
            WriteDescription("\tDELETE IMAGE |DOES NOT EXISTS.", true);
        }

        private static void WriteBrief(string brief, int sleepTime = 500)
        {
            ConsoleHelper.WriteBrief(brief, 
                briefLine.TopPos, 
                briefLine.LeftPos, 
                sleepTime);
        }

        private static void WriteDescription(string description, 
            bool isSequentialWrite = false, int sleepTime = 1000)
        {
            ConsoleHelper.WriteDescription(description, dscrLine.TopPos, 
                dscrLine.LeftPos, isSequentialWrite, sleepTime);
        }
        #endregion

        #region EVENTS
        private static bool ConsoleCtrlHandler(CtrlType signal)
        {
            switch (signal)
            {
                case CtrlType.CTRL_BREAK_EVENT:
                case CtrlType.CTRL_C_EVENT:
                case CtrlType.CTRL_LOGOFF_EVENT:
                case CtrlType.CTRL_SHUTDOWN_EVENT:
                case CtrlType.CTRL_CLOSE_EVENT:
                    esc = true;
                    return true;
                default: return false;
            }
        }
        #endregion

        #region INTERNAL CLASS
        private class LinePosition
        {
            public int TopPos { get; set; }
            public int LeftPos { get; set; }

            public LinePosition(int topPos, int leftPos)
            {
                TopPos = topPos;
                LeftPos = leftPos;
            }

            public LinePosition() { }
        }
        #endregion
    }
}
